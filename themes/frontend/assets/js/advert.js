(function($) {

	var methods = {
		init : function(options){
		   
		},

		updateMap: function(lat, lng){
			var latlng = new google.maps.LatLng(lat, lng);
			$(".lngAddress").val(lng);
	      	$(".latAddress").val(lat);
		    var map_update = new google.maps.Map(document.getElementById('map_update'), {
		        center: latlng,
		        zoom: 15,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });
		    // create marker
		    var marker_update = new google.maps.Marker({
		        position: latlng,
		        map: map_update,
		        title: '',
		        draggable: false
		    });
		}

	};

	$.fn.advert = function(method)
	{
		var pausesInfoTimeout;
		var  refreshSetInterval; //переменная для хранения SetInterval ИД
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод ' + method + ' не существует');
			return false;
		}
	};

	//events
	$(function(){
        var lat = ($(".latAddress").val()) ? $(".latAddress").val() : "40.183308";
		var lng = ($(".lngAddress").val()) ? $(".lngAddress").val() : "44.516674";
	   	$.fn.advert("updateMap", lat, lng);
	});

})( jQuery );

