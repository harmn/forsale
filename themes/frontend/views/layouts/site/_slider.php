<div id="sequence">
	<img class="sequence-prev" src="<?=$this->assetsUrl?>/images/slider/bt-prev.png" alt="Previous" />
	<img class="sequence-next" src="<?=$this->assetsUrl?>/images/slider/bt-next.png" alt="Next" />

	<ul class="sequence-canvas">
		<li class="animate-in">
			<div class="info">
				<h2>Built using Sequence.js</h2>
				<p>The Responsive Slider with Advanced CSS3 Transitions</p>
			</div>
			<img class="sky" src="<?=$this->assetsUrl?>/images/slider/bg-clouds.png" alt="Blue Sky" />
			<img class="balloon" src="<?=$this->assetsUrl?>/images/slider/balloon.png" alt="Balloon" />
		</li>
		<li>
			<div class="info">
				<h2>Creative Control</h2>
				<p>Create unique sliders using CSS3 transitions -- no jQuery knowledge required!</p>
			</div>
			<img class="sky" src="<?=$this->assetsUrl?>/images/slider/bg-clouds.png" alt="Blue Sky" />
			<img class="aeroplane" src="<?=$this->assetsUrl?>/images/slider/aeroplane.png" alt="Aeroplane" />
		</li>
		<li>
			<div class="info">
				<h2>Cutting Edge</h2>
				<p>Supports modern browsers, old browsers (IE7+), touch devices and responsive designs</p>
			</div>
			<img class="sky" src="<?=$this->assetsUrl?>/images/slider/bg-clouds.png" alt="Blue Sky" />
			<img class="kite" src="<?=$this->assetsUrl?>/images/slider/kite.png" alt="Kite" />
		</li>
	</ul>
</div>