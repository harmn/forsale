<?php 
	$action = "index";
	if(Yii::app()->controller && Yii::app()->controller->action){
		if(Yii::app()->controller->action->id == "about"){
			$action = "about";
		}else if(Yii::app()->controller->action->id == "contacts"){
			$action = "contacts";
		}
	}
?>
<ul id="menu" class="fl">
	<li class="fl mr20 tcenter <?=($action == "index") ? "active" : "";?>"><a href="<?=$this->createUrl('site/index');?>" class="home c-dark-gray block"><i></i><span class="block mt5"><?=t('front', 'Главная');?></span></a></li>
	<li class="fl mr20 tcenter  <?=($action == "about") ? "active" : "";?>"><a href="<?=$this->createUrl('site/about');?>" class="about c-dark-gray block"><i></i><span class="block mt5"><?=t('front', 'О нас');?></span></a></li>
	<li class="fl tcenter  <?=($action == "contacts") ? "active" : "";?>"><a href="<?=$this->createUrl('site/contacts');?>" class="contacts c-dark-gray block"><i></i><span class="block mt5"><?=t('front', 'Контакты');?></span></a></li>
</ul>
