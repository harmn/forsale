<footer>
	<div class="footer">
		<div class="subscribe">
			<div class="footer-wrap">
				<span class="mr20">
					<span><?=t('front', 'Подписка');?></span> 
					<?=t('front', 'На новости');?>
				</span>
				<div class="input inline">
					<input type="text" placeholder="<?=t('front', 'Введите электронный адрес');?>">
					<div class="send"><input type="submit" value=""></div>
				</div>
			</div>
		</div>
			
		<div class="wrapper row clearfix">
			<div class="col-md-3">
				<a href="#" class="w200 inline mb10"><img src="<?=$this->assetsUrl?>/images/placeholders/logo2.png" alt="" class="w100p"></a>
			</div>

			<div class="col-md-6 row">
				<ul class="col-md-4">
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Дом');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Авто');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Электроника');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Техника');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Мебель');?></a></li>
				</ul>

				<ul class="col-md-4">
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Мода');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Спорт и Игры');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Животное и растения');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Работа');?></a></li>
					<li class="mb15"><a href="#" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Службы');?></a></li>
				</ul>

				<ul class="col-md-4">
					<li class="mb15"><a href="<?=$this->createUrl('site/index');?>" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Главная');?></a></li>
					<li class="mb15"><a href="<?=$this->createUrl('site/about');?>" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'О нас');?></a></li>
					<li class="mb15"><a href="<?=$this->createUrl('site/contacts');?>" class="c-black"><i class="fa fa-chevron-right c-blue mr10"></i><?=t('front', 'Контакты');?></a></li>
				</ul>
			</div>	

			<div class="col-md-3">
				<div class="fr">
					<p class="title mb15"><?=t('front', 'Контакты');?></p>
					<span class="bold block mb10 fsize14"><i class="fa fa-phone fsize18 c-blue mr10"></i>(+374 55) 108 180</span>
					<span class="bold block fsize14"><i class="fa fa-envelope c-blue fsize16 mr10"></i>info@forsale.am</span>

					<ul class="social clearfix mt20">
						<li><a href="https://www.facebook.com/pages/ForSaleam/1601694066728233" target="_blunk" class="fa fa-facebook"></a></li>
						<li><a href="#" class="fa fa-twitter"></a></li>
						<li><a href="#" class="fa fa-vk fsize13"></a></li>
					</ul>	
				</div>
				
			</div>
		</div>
		<p class="copyright">Copyright 2015-2016. All rights reserved.</p>
	</div>
	
</footer>