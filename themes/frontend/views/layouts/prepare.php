<!DOCTYPE html>
<html>
<head>
	<?php echo CHtml::tag('meta', ['charset'=>Yii::app()->charset]); ?>
	<?php echo CHtml::tag('meta', ['description'=> 'Քո անվճար օնլայն բրոկերը: Անվճար հայտարարությունների լավագույն  և հարմարավետ կայքը Հայաստանում: Անշարջ գույք, տրանսպորտ, աշխատանք, ծառայություններ , ապրանքների առք, վաճառք և վարձակալում:']); ?>
	<?php echo CHtml::tag('meta', ['keywords'=> 'Հայտարարություն, Անշարժ գույք, Տուն, Տրանսպորտ, մեքենա, ավտո, Էլեկտրոնիկա, Տեխնիկա, Կահույք, Նորաձևություն, Սպորտ և խաղեր, Կենդանիներ և բույսեր, Աշխատանք, Ծառայություններ, առք , վաճառք, վարձակալում, անվճար, haytararutyun, ansharj guyq, tun, avtyo, meqena, elektronika, texnika, kahuyq, nora4evutyun, sport, xaxer, kendaniner, buyser, ashxatanq, carayutyun, arq, vacharq, vardzakalum, anvchar, Дом, Авто, Электроника,
	Техника, Мебель, Мода, Спорт и Игры, Животное и растения, Работа, Службы, купить, продать, аренда, бесплатно, Realty, Car, Electronics, Technique, Furniture, Fashion, Sport and Games, Animals and plants, Job, Services, sale, for, free, rate']); ?>
	<?php echo CHtml::tag('meta', ['author'=> 'Harutyun Mnatsakanyan']); ?>
	
	<title><?php echo strip_tags($this->pageTitle); ?></title>
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	
</head>
<body>
	<?php echo $content; ?>
</body>	
</html>