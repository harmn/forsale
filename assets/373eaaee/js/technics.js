/**
 * 
 */
(function($) {
 
	var methods = {
		init : function(options){
		   
		},

		/**
		 * 
		 * берем информацию о обявлении
		 */
		getTechnicsInfo: function(element){
			var idTechnics = element.data('id');

			jPost(element.data('url'), {idTechnics: idTechnics}, function(data){
				if(data.success){
					var additional = {
						"target" : element.data('target'),
						"model"  : element.data('model'),
						"title"	 : element.data('title'),
						"action" : element.data('action')
					};
					additional.beforeOpen =	function() {
						if(data.photos)
							$("#dropZone_0 .qq-upload-list").html(data.photos);
						else
							$("#dropZone_0 .qq-upload-list").html('');
						var template = FineUploader_dropZone_0._options.request.endpointTemplate;
			            FineUploader_dropZone_0._options.request.endpoint = template.replace('__id__', element.data('id'));
			            FineUploader_dropZone_0._options.request.endpoint = FineUploader_dropZone_0._options.request.endpoint.replace('__params__', 'parts');
					},
					$.fn.openModal(data.attributes, additional);

					var ltd = (data.attributes.ltd) ? data.attributes.ltd : "40.183308";
					var lng = (data.attributes.lng) ? data.attributes.lng : "44.516674";
				   	$.fn.technics("updateMap", ltd, lng, 2);
				}
			});
		},

		updateMap: function(ltd, lng, mapType){
			var latlng = new google.maps.LatLng(ltd, lng);
			if(mapType == 2){
				$("#Technics_update_lng").val(lng);
		      	$("#Technics_update_lat").val(ltd);
			    var map_update = new google.maps.Map(document.getElementById('map_update'), {
			        center: latlng,
			        zoom: 15,
			        mapTypeId: google.maps.MapTypeId.ROADMAP
			    });
			    // create marker
			    var marker_update = new google.maps.Marker({
			        position: latlng,
			        map: map_update,
			        title: 'technics',
			        draggable: true
			    });
			    // drag marker
			    google.maps.event.addListener(marker_update, 'dragend', function(a) {
			    	map_update.panTo(marker_update.getPosition());
			      	$("#Technics_update_lng").val(marker_update.position.lng());
			      	$("#Technics_update_ltd").val(marker_update.position.lat());
			    });
			}else{
				$("#Technics_registration_lng").val(lng);
		      	$("#Technics_registration_lat").val(ltd);
				var map_registration = new google.maps.Map(document.getElementById('map_registration'), {
			        center: latlng,
			        zoom: 15,
			        mapTypeId: google.maps.MapTypeId.ROADMAP
			    });
			    // create marker
			    var marker_registration = new google.maps.Marker({
			        position: latlng,
			        map: map_registration,
			        title: 'Technics',
			        draggable: true
			    });
			    // drag marker
			    google.maps.event.addListener(marker_registration, 'dragend', function(a) {
			    	map_registration.panTo(marker_registration.getPosition());
			      	$("#Technics_registration_lng").val(marker_registration.position.lng());
			      	$("#Technics_registration_lat").val(marker_registration.position.lat());
			    });
			}
		}
	};

	$.fn.technics = function(method)
	{
		var pausesInfoTimeout;
		var  refreshSetInterval; //переменная для хранения SetInterval ИД
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод ' + method + ' не существует');
			return false;
		}
	};

	//events
	$(function(){
		
		$(document)
		.on('click', '#technics-table .getTechnicsInfo', function(e){
			e.preventDefault();
			$.fn.technics("getTechnicsInfo", $(this));
		})
		.on('change', '#Technics_registration_id_region', function(e){
			e.preventDefault();
			var ltd = $(this).find(":selected").data('ltd');
			var lng = $(this).find(":selected").data('lng');
			$.fn.technics("updateMap", ltd, lng, 1);
		})
		.on('change', '#Technics_registration_id_city', function(e){
			e.preventDefault();
			var ltd = $(this).find(":selected").data('ltd');
			var lng = $(this).find(":selected").data('lng');
			if(!ltd || !lng){
				var stock =  $('#Technics_registration_id_region :selected');
				var ltd = stock.data('ltd');
				var lng = stock.data('lng');
			}
			$.fn.technics("updateMap", ltd, lng, 1);
		})
		.on('change', '#Technics_update_id_region', function(e){
			e.preventDefault();
			var ltd = $(this).find(":selected").data('ltd');
			var lng = $(this).find(":selected").data('lng');
			$.fn.technics("updateMap", ltd, lng, 2);
		})
		.on('change', '#Technics_update_id_city', function(e){
			e.preventDefault();
			var ltd = $(this).find(":selected").data('ltd');
			var lng = $(this).find(":selected").data('lng');
			if(!ltd || !lng){
				var stock =  $('#Technics_update_id_region :selected');
				var ltd = stock.data('ltd');
				var lng = stock.data('lng');
			}
			$.fn.technics("updateMap", ltd, lng, 2);
		})
		.on('click', '#addTechnics', function(e){
			e.preventDefault();
			var latlng = new google.maps.LatLng(40.183308, 44.516674);
		    // create map
		    var map_registration = new google.maps.Map(document.getElementById('map_registration'), {
		        center: latlng,
		        zoom: 15,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });
		    // create marker
		    var marker_registration = new google.maps.Marker({
		        position: latlng,
		        map: map_registration,
		        title: 'Technics',
		        draggable: true
		    });
		    // drag marker
		    google.maps.event.addListener(marker_registration, 'dragend', function(a) {
		    	map_registration.panTo(marker_registration.getPosition());
		      	$("#Technics_registration_lng").val(marker_registration.position.lng());
		      	$("#Technics_registration_lat").val(marker_registration.position.lat());
		    });
		});
	});

	/**
	* зависимость между dropdown job и user
	*/
	$("select[data-depend='job']").on('change', function(event){
	 	var self  = $(this);
	 	var idRegion = self.val();
		var users = self.closest('form').find("select[data-depend='city']");

		if(users.length > 0){
			$(users).val('');
			var options = users.find('option:not([value=""])');
			if(idRegion && idRegion > 0){
				options.addClass('hidden'); //hide all users

				//show only users with selected job
				options.filter('[data-idregion='+idRegion+']').removeClass('hidden');
			}
			else{
				options.removeClass('hidden'); //show all users
			}
			
			users.find("option:not(.hidden):first").prop('selected', true);
			users.selectStyler('refresh'); //для скрытия остальных элементов
		}
		
	});

})( jQuery );
