(function($) {

	var methods = {
		init : function(options){
		   
		},

		updateMap: function(lat, lng){
			var latlng = new google.maps.LatLng(lat, lng);
			$(".lngAddress").val(lng);
	      	$(".latAddress").val(lat);
		    var map_update = new google.maps.Map(document.getElementById('map_update'), {
		        center: latlng,
		        zoom: 15,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });
		    // create marker
		    var marker_update = new google.maps.Marker({
		        position: latlng,
		        map: map_update,
		        title: '',
		        draggable: true
		    });
		    // drag marker
		    google.maps.event.addListener(marker_update, 'dragend', function(a) {
		    	map_update.panTo(marker_update.getPosition());
		      	$(".lngAddress").val(marker_update.position.lng());
		      	$(".latAddress").val(marker_update.position.lat());
		    });
		}

	};

	$.fn.listing = function(method)
	{
		var pausesInfoTimeout;
		var  refreshSetInterval; //переменная для хранения SetInterval ИД
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод ' + method + ' не существует');
			return false;
		}
	};

	//events
	$(function(){
		var idGlobal = $("#idGlobal").val();
		var template = FineUploader_dropZone_0._options.request.endpointTemplate;
        FineUploader_dropZone_0._options.request.endpoint = template.replace('__id__', idGlobal);
        FineUploader_dropZone_0._options.request.endpoint = FineUploader_dropZone_0._options.request.endpoint.replace('__params__', 'parts');

        var lat = ($(".latAddress").val()) ? $(".latAddress").val() : "40.183308";
		var lng = ($(".lngAddress").val()) ? $(".lngAddress").val() : "44.516674";
	   	$.fn.listing("updateMap", lat, lng);

	   	$(document)
	   	.on('change', '.regionAddress', function(e){
			e.preventDefault();
			var ltd = $(this).find(":selected").data('ltd');
			var lng = $(this).find(":selected").data('lng');
			$.fn.listing("updateMap", lat, lng);
		})
		.on('change', '.cityAddress', function(e){
			e.preventDefault();
			var ltd = $(this).find(":selected").data('ltd');
			var lng = $(this).find(":selected").data('lng');
			if(!ltd || !lng){
				var stock =  $('.regionAddress :selected');
				var ltd = stock.data('ltd');
				var lng = stock.data('lng');
			}
			$.fn.listing("updateMap", ltd, lng);
		});
	});

})( jQuery );

