(function($){

	/*----------------------------------------------------
	*	-= ПЛАГИН КАРУСЕЛИ =-
	*	предназначенный для прокрутки списка обьектов
	*	либо с помощью кнопок перехода, либо автоматически.
	*	Причем список может содержать элементы 
	*	разного рода, и может быть как горизонтальным
	*	так и вертикальным
	*	
	*	Автор: A-Z Design Studio - http://a-z.am
	*	Email: alikmanukian@gmail.com
	----------------------------------------------------*/
	
	
	$.fn.jqcarousel1 = function(options) {
		//настройки по умолчанию
		var options = jQuery.extend({
				c_width: 230, 					//ширина элементов
				c_height: 100, 					//высота элементов
				c_margin: 25, 					//расстояние между элементами
				c_autostart: true, 				//автоматическая прокрутка
				c_step: 1, 						//количество элементов меняющихся за один переход
				c_pause: 2000, 					//пауза между переходами
				c_orientation: 'horizontal', 	//горизонтальный список или вертикальный
				c_speed: 1200, 					//скорость перехода 
				c_visible: 4, 					//количество видимых элементов
				c_has_arrows: false,
				c_arrow_prev_class: 'carousel_prev',
				c_arrow_next_class: 'carousel_next'
			}, options);
		
		return this.each(function () {
			var self = $(this).css('position', 'relative');
			var $ul = self.find('ul');
			var $li = $ul.find('li');
			
			self.prev_btn = self.find('a.' + options.c_arrow_prev_class);
			self.next_btn = self.find('a.' + options.c_arrow_next_class);
			
			self.qty = $li.length;
			self.offset = (((options.c_orientation == 'horizontal') ? options.c_width : options.c_height) + 
								options.c_margin) * options.c_step;
				
			self.startPosition = -options.c_visible * 
								(((options.c_orientation == 'horizontal') ? options.c_width : options.c_height) + 
								options.c_margin);
			
			//клонирование
			//первые c_visible элементов клонируются в конец списка
			$clone_objects = $li.slice(0, options.c_visible).clone();
			$ul.append($clone_objects);
			//последние c_visible элементов клонируются в начало списка
			$clone_objects = $li.slice(-options.c_visible).clone();
			$ul.prepend($clone_objects);
			$li = $ul.find('li');
			
			//wrapping ul
			$ul.wrap('<div class="carousel-wrapper" />');
			$wrapper  = self.find('.carousel-wrapper');
			
			if(options.c_orientation == 'horizontal'){
				// придать ширину для ul
				$ul.width($li.length * (options.c_width + options.c_margin));
				
				// придать ширину для wrapper
				$wrapper.width(options.c_visible * options.c_width + 
									(options.c_visible - 1) * options.c_margin);
			}
			else {
				// придать высоту для ul
				$ul.height( $li.length * (options.c_height + options.c_margin));
				
				// придать ширину для ul
				$ul.width(options.c_width);
				// придать ширину для wrapper
				$wrapper.width(options.c_width);
				
				// придать высоту для wrapper
				$wrapper.height(options.c_visible * options.c_height + 
								(options.c_visible - 1) * options.c_margin);
			}
			
			//придать margin для li
			$li.css('margin-' + 
								((options.c_orientation == 'horizontal') ? 'right' : 'bottom'),
								options.c_margin);
								
			//$li.find('img').width(options.c_width).height(options.c_height);		
			

			//события для стрелок
			if(options.c_has_arrows){
				self.next_btn.bind('click', function(e){
					e.preventDefault();
					
					
					if(!$ul.filter(':animated').length){
						clearInterval(self.interval);
						move($ul, 'positive');
						if(options.c_autostart)
							self.interval = setInterval(function(){move($ul, 'positive');}, options.c_pause);
					}
				});
				
				self.prev_btn.bind('click', function(e){
					e.preventDefault();
					
					if(!$ul.filter(':animated').length){
						clearInterval(self.interval);
						move($ul, 'negative');
						if(options.c_autostart)
							self.interval = setInterval(function(){move($ul, 'positive');}, options.c_pause);
					}
				});
			}
			
			//пауза при наведении мыши на один из обьчектов
			if(options.c_pause_on_hover){
				$li.hover(
					//on over
					function(){
						clearInterval(self.interval);
					},
					//on out
					function(){
						if(!$ul.filter(':animated').length){
							self.interval = setInterval(function(){move($ul, 'positive');}, options.c_pause);
						}
					}
				);
			}
			
			//функция перемещения
			function move($obj, direction){
				if(!direction) direction = 'positive';
				
				if(direction == 'positive') sign = '-';
				if(direction == 'negative') sign = '+';
				
				if(options.c_orientation == 'horizontal')
					$obj.animate({ 'margin-left' : sign + '=' + self.offset}, 
								{queue:false, 
								duration: options.c_speed, 
								myeasing: "sineEaseOut",
								complete: resetPosition($obj, direction)});
				else 
					$obj.animate({ 'margin-top' : sign + '=' + self.offset}, 
								{queue:false, 
								duration: options.c_speed, 
								myeasing: "sineEaseOut",
								complete: resetPosition($obj, direction)});
					
			}
			
			function resetPosition($obj, direction){
				//сброс координаты
				margin = parseInt($obj.css('margin-' + 
									((options.c_orientation == 'horizontal') ? 'left' : 'top')));
				
				
				m = ((options.c_orientation == 'horizontal') ? 
						options.c_width : options.c_height) + options.c_margin;
				
				last_visible = Math.abs(margin/m) + options.c_visible;
				first_visible = Math.abs(margin/m) + 1;
				all_count = $obj.find('li').length;
				
				if( direction == 'positive' && (last_visible + options.c_step > all_count) ) {
					//reset position
					margin = -(first_visible - self.qty - 1)*m;
				}
				if(direction == 'negative' && (first_visible - options.c_step < 1) ){
					//reset position
					margin = -(first_visible + self.qty - 1)*m;
				}
				
				$obj.css('margin-' + 
						((options.c_orientation == 'horizontal') ? 'left' : 'top'), margin);
			}
			
			function createArrows(){
				
				$img_prev = $('<img src="' + options.c_arrow_prev + '" />');
				self.prev_btn.wrapInner($img_prev);
				$img_next = $('<img src="' + options.c_arrow_next + '" />');
				self.next_btn.wrapInner($img_next);
				
				self.prev_btn.css({
					position:'absolute',
					'text-decoration':'none',
					display:'none'
				});
				self.next_btn.css({
					position:'absolute',
					'text-decoration':'none',
					display:'none'
				});
				
				/*
				выравнивание стрелок
				----------------------*/
				arrows_offset = 10;
				
				//предыдущая стрелка
				$img_prev.load(function(){
					//выравнивание стрелки по вертикали
					if(self.prev_btn.css('left') == 'auto') self.prev_btn.css('left', arrows_offset);
					self.prev_btn.css({
						top: '50%',
						'margin-top': - self.prev_btn.height()/2,
						display:'block'
					});
				});
				//следующая стрелка
				$img_next.load(function(){
					//выравнивание стрелки по вертикали
					if(self.next_btn.css('right') == 'auto') self.next_btn.css('right', arrows_offset);
					self.next_btn.css({
						top: '50%',
						'margin-top': - self.next_btn.height()/2,
						'margin-top': - self.next_btn.height()/2,
						display:'block'
					});
				});
			}
			
			//перемещение
			//установка стартовой позиции
			$ul.css('margin-' + ((options.c_orientation == 'horizontal') ? 'left' : 'top'), self.startPosition);
			
			if(options.c_autostart) 
				self.interval = setInterval(function(){move($ul, 'positive');}, options.c_pause);
			//if(options.c_has_arrows)
				//createArrows();
		});
	}
})(jQuery);

