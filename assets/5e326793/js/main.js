(function($){

	$(function(){

		$('select:visible').not('.nostyler, .tbselect').selectStyler();

		//При увеличении количества
		$(document).on('click', 'div.quantity a.increase-qty', function(e){
			e.preventDefault();

			var currentQty = parseInt($('div.quantity input').val());

			var newQty = currentQty + 1;

			$('div.quantity input').val(newQty);
		});

		//При уменьшении количества
		$(document).on('click', 'div.quantity a.decrease-qty', function(e){
			e.preventDefault();
			
			var currentQty = parseInt($('div.quantity input').val());

			var newQty = currentQty !== 1 ? currentQty - 1 : 1;

			$('div.quantity input').val(newQty);
		});

		//меняет view с  Thumb на Descr
		$(document).on('click','.page-view a.fa-th-list', function(e){
			e.preventDefault();
			$.cookie('advert-list-view', 'description', {path: '/'});
			
			var idFListView = $(this).closest('.list-view').attr('id');
			$.fn.yiiListView.update(idFListView);
		});

		//меняет view с Descr на Thumb
		$(document).on('click','.page-view a.fa-th-large', function(e){
			e.preventDefault();
			$.cookie('advert-list-view', 'thumbnail', {path: '/'});
			
			var idFListView = $(this).closest('.list-view').attr('id');
			$.fn.yiiListView.update(idFListView);
		});

		//быстрый просмотор
		$(document).on('click', '.quick-view', function(e){
			e.preventDefault();
			var quickViewModal = $('#item-modal');
			var self = $(this);
			var url = self.closest('div.link-to-item').attr('href');
			var id = self.closest('div.link-to-item').data('id');
			var model = self.closest('div.link-to-item').data('model');
			jPost(url, {id: id, model: model},
				function(data){
					if(data.success){
						quickViewModal.find('.item-modal-content').html(data.item);
						quickViewModal.find('select').not('.nostyler, .tbselect').selectStyler();

						// galleria 
						quickViewModal.find('#small-images a.zoom').bind('click',function(e){
							e.preventDefault();
							href=$(this).attr("href");
							quickViewModal.find('#big-image >a img').remove();
							 
							$('<img src="'+href+'">').appendTo(quickViewModal.find('#big-image a'));
							quickViewModal.find('#big-image > a').data('id', $(this).data('id'));

						});

						quickViewModal.find('#big-image a').on('click', function(e){
						 	e.preventDefault();
						 	quickViewModal.find('#pirobox_images a[data-id="' + $(this).data('id') + '"]').trigger('click');
						});

						quickViewModal.find('#small-images .jTscroller').runGalleria(quickViewModal.find('#big-image'));

						quickViewModal.doModal('show');
						
						quickViewModal.find("#small-images").thumbnailScroller({ 
						  	scrollerType: "hoverPrecise", //hoverAccelerate
						  	scrollerOrientation:"horizontal", 
						  	scrollSpeed:2, 
						  	scrollEasing:"easeOutCirc", 
						  	scrollEasingAmount:800, 
						  	acceleration:4, 
						  	scrollSpeed:800, 
						  	noScrollCenterSpace:10, 
						  	autoScrolling:0, 
						  	autoScrollingSpeed:2000, 
						  	autoScrollingEasing:"easeInOutQuad", 
						  	autoScrollingDelay:500 
						});

						// favorites 
						quickViewModal.find(".favorites").on('click', function(e){
							event.preventDefault();
							var jthis = $(this);
							var id_advert  	= jthis.closest("li").data('id');
							var model  		= jthis.closest("li").data('model');
							var type 	   	= 1;
							jPost('/advert/favorites', {model: model, id_advert: id_advert, type: type}, function(data){
								if(data.success){
									if(data.add == '1')
										jthis.addClass("activeFavorit");
									else
										jthis.removeClass("activeFavorit");
								}
							});
						});

						// watch later
						quickViewModal.find(".later").on('click', function(e){
							event.preventDefault();
							var jthis = $(this);
							var id_advert  	= jthis.closest("li").data('id');
							var model  		= jthis.closest("li").data('model');
							var type 	   	= 2;
							jPost('/advert/favorites', {model: model, id_advert: id_advert, type: type}, function(data){
								if(data.success){
									if(data.add == '1')
										jthis.addClass("activeFavorit");
									else
										jthis.removeClass("activeFavorit");
								}
							});
						});

						// // carousel for products images
						// quickViewModal.find('.item-modal-content .carousel').jqcarousel1({
						// 	c_width: 60, 						//ширина элементов
						// 	c_height: 40, 						//высота элементов
						// 	c_margin: 5, 						//расстояние между элементами
						// 	c_autostart: false, 				//автоматическая прокрутка
						// 	c_step: 1, 							//количество элементов меняющихся за один переход
						// 	c_pause: 20000, 					//пауза между переходами
						// 	c_orientation: 'horizontal', 		//горизонтальный список или вертикальный
						// 	c_speed: 400, 						//скорость перехода 
						// 	c_visible: 5,						//количество видимых элементов
						// 	c_has_arrows: true,
						// 	c_arrow_prev_class: 'arrow-left',
						// 	c_arrow_next_class: 'arrow-right',
						// 	c_pause_on_hover: true
						// });

					}
				}
			);

		});

		// galleria 
		$("#advertPageGalleria").find('#small-images a.zoom').bind('click',function(e){
			e.preventDefault();
			href=$(this).attr("href");
			$("#advertPageGalleria").find('#big-image >a img').remove();
			 
			$('<img src="'+href+'">').appendTo($("#advertPageGalleria").find('#big-image a'));
			$("#advertPageGalleria").find('#big-image > a').data('id', $(this).data('id'));

		});

		$("#advertPageGalleria").find('#big-image a').on('click', function(e){
		 	e.preventDefault();
		 	$("#advertPageGalleria").find('#pirobox_images a[data-id="' + $(this).data('id') + '"]').trigger('click');
		});

		$("#advertPageGalleria").find('#small-images .jTscroller').runGalleria($("#advertPageGalleria").find('#big-image'));

		$("#advertPageGalleria").find("#small-images").thumbnailScroller({ 
		  	scrollerType: "hoverPrecise", //hoverAccelerate
		  	scrollerOrientation:"horizontal", 
		  	scrollSpeed:2, 
		  	scrollEasing:"easeOutCirc", 
		  	scrollEasingAmount:800, 
		  	acceleration:4, 
		  	scrollSpeed:800, 
		  	noScrollCenterSpace:10, 
		  	autoScrolling:0, 
		  	autoScrollingSpeed:2000, 
		  	autoScrollingEasing:"easeInOutQuad", 
		  	autoScrollingDelay:500 
		});

	});
	
})(jQuery);
