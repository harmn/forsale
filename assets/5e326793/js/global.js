(function($) {

	var methods = {
		init : function(options){
		   
		},

		updateGredListJs : function(id, data){
		 	$('#global-list .main').hoverfold();
		},

		updateListJs : function(options){
		   	$('#global-list .main').hoverfold();
		},

		search: function(data){
			data.filter = 'filter';
			var target = $('#global-list');

			target.addClass('view-loading');
			jPost('', data, function(result){
				if(result.success){
					$('.advert-list').html(result.list);
					$.fn.global("updateListJs");
					target.removeClass('view-loading');
				}
			});
		},
	};

	$.fn.global = function(method)
	{
		var pausesInfoTimeout;
		var  refreshSetInterval; //переменная для хранения SetInterval ИД
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод ' + method + ' не существует');
			return false;
		}

	};
	
	$.fn.global.updateGredListJs = methods.updateGredListJs;

	//events
	$(function(){

		// favorites 
		$(document).on('click', '.favorites', function(e){
			e.preventDefault();
			if(!$("#idUser").val()){
				window.location.href = "/login";
				return false;
			}
			var jthis = $(this);
			var id_advert  	= jthis.closest("li").data('id');
			var model  		= jthis.closest("li").data('model');
			var type 	   	= 1;
			jPost('/advert/favorites', {model: model, id_advert: id_advert, type: type}, function(data){
				if(data.success){
					if(data.add == '1')
						jthis.addClass("activeFavorit");
					else
						jthis.removeClass("activeFavorit");
				}
			});
		});

		// watch later
		$(document).on('click', '.later', function(e){
			e.preventDefault();
			if(!$("#idUser").val()){
				window.location.href = "/login";
				return false;
			}
			var jthis = $(this);
			var id_advert  	= jthis.closest("li").data('id');
			var model  		= jthis.closest("li").data('model');
			var type 	   	= 2;
			jPost('/advert/favorites', {model: model, id_advert: id_advert, type: type}, function(data){
				if(data.success){
					if(data.add == '1')
						jthis.addClass("activeFavorit");
					else
						jthis.removeClass("activeFavorit");
				}
			});
		});

		/**
		* зависимость между dropdown job и user
		*/
		$("select[data-depend='job']").on('change', function(event){
		 	var self  = $(this);
		 	var idRegion = self.val();
			var users = self.closest('form').find("select[data-depend='city']");

			if(users.length > 0){
				$(users).val('');
				var options = users.find('option:not([value=""])');
				if(idRegion && idRegion > 0){
					options.addClass('hidden'); //hide all users

					//show only users with selected job
					options.filter('[data-idregion='+idRegion+']').removeClass('hidden');
				}
				else{
					options.removeClass('hidden'); //show all users
				}
				
				users.find("option:not(.hidden):first").prop('selected', true);
				users.selectStyler('refresh'); //для скрытия остальных элементов
			}
			
		});

		//открывает модальное окно логина
		$('#top a.login-modal').on('click', function(e){
			e.preventDefault();
			$('#login-modal').doModal('show');
		});

		//	show hide languages select
		$('#language-select').hover(function () {
		    $(this).find('li').not(":active").removeClass('hidden');
		}, function () {
		    $(this).find('li').not(":active").addClass('hidden');
		    $(this).find("li.active").removeClass('hidden');
		});

		/*
		* top-search
		*/   
		
		$('#searchButton').click(function(){
	        $('#searchInputDiv form').animate({width: 'toggle'},500);
	        $("#searchButton a").toggleClass("fa-times", "add");
			$("#searchButton a").toggleClass("fa-search", "remove");
	        $('#top-search').find('input').focus();
	    });
		
		// var index = 0;

		// $(document).on('click', '#fa-search', function(e){
		// 	e.preventDefault();
		// 	index = 1;
		// 	if($(this).closest('form').find('input').val())
		// 		$('#top-search').submit();
		// });

		// $("#top-search").submit(function(){
		// 	if(!index){
		// 		index = 0;
		// 	    if($(this).find('input').val())
		// 			$('#top-search').submit();
		// 		else	
		// 	    	return false;
		// 	}    
		// });

		//для слайдера цены
		$('.slider-element').slider().on('slideStop', function(e){
			// var price = e.value;
			// var model = $(".top-filters:visible").data("model");
			// $('#'+model+'_priceFrom').val(price[0]);
			// $('#'+model+'_priceTo').val(price[1]);
		});

		$.fn.global("updateListJs");

	 	$('#myItemCarousel').carousel({
	      	interval: 6000
	    });

	    /**/
		/* accordion */
		/**/
		$('.infoPart .accordion .active').next().show();
		$('.infoPart .accordion').on('click', 'dt', function()
		{
			$(this).toggleClass('active').siblings('dt').removeClass('active');
			$(this).siblings('dd').slideUp('fast');
			$(this).next().stop().slideDown('fast');
		});	

		var checker = 0;
        if($(window).scrollTop() > $('#page-header-bottom-holder').offset().top + 1){
        	$('#page-header-bottom').addClass('chackerBottom');
        }else{
            $('#page-header-bottom').removeClass('chackerBottom');
        }
		/**/
		/* on scroll */
		/**/
		$(window).scroll(function()
		{

			if( $(this).scrollTop() > 200 )
			{
				$('#scroll-top').fadeIn();
			}
			else
			{
				$('#scroll-top').fadeOut();
			}

            if(checker == 1)
			{
				if( $(this).scrollTop() < $('#page-header-bottom-holder').offset().top + 1 )
				{
					$('#top').show();
                    $('#page-header-bottom').removeClass('chackerBottom');
                    checker = 0;
				}
			}
			else
			{
				if($(this).scrollTop() > $('#page-header-bottom-holder').offset().top - $('#page-header-bottom').outerHeight() )
				{
                    $('#top').hide();
					$('#page-header-bottom').addClass('chackerBottom');
                    checker = 1;
				}
			}
		});

		/**/
		/* scroll top */
		/**/
		$('#scroll-top').click(function()
		{
			$('html, body').animate({scrollTop: 0});
			return false;
		});


		//открывает модальное окно для добавления обявления
		$('a.addListing').on('click', function(e){
			e.preventDefault();
			$('#addListing-modal').doModal('show');
		});

		// set filter on change tab
		$('.left-menu a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  var target = $(e.target).attr("href") // activated tab
		  $(target).find(".btn-submit").trigger('click');
		});

		$(document).on('click', '#global-list .slice', function(e){
			var link = $(this).closest('li.thumbLink').data('href');
			window.location.href = link;
		});

		//
		$(".addListBlock").on('click', function(e){
			var model = $(this).data("model");
			var target = $('#addListing-modal');
			target.addClass('view-loading');
			jPost('/listing/addListing', {model: model}, function(data){
				if(data.success){
					window.location.href = "/listing/edit/"+data.id+"?type="+model;
				}
				target.removeClass('view-loading');
			});
		});

		//
		$(".uploadAvatarImage").on("click", function(){$('#avatar-container .browse').trigger('click')});
	});

})( jQuery );


// Hover function
( function( $ ) {
	
	$.fn.hoverfold = function( args ) {

		this.each( function() {
		
			$( this ).children( '.view' ).each( function() {
				var $item 	= $( this ),
					img		= $item.find( 'img' ).attr( 'src' ),
					struct	= '<div class="slice s1">';
						struct	+='<div class="slice s2">';
							struct	+='<div class="slice s3">';
								struct	+='<div class="slice s4">';
									struct	+='<div class="slice s5">';
									struct	+='</div>';
								struct	+='</div>';
							struct	+='</div>';
						struct	+='</div>';
					struct	+='</div>';
					
				var $struct = $( struct );
				
				$item.find( 'img' ).remove().end().append( $struct ).find( 'div.slice' ).css( 'background-image', 'url(' + img + ')' ).prepend( $( '<span class="overlay" ></span>' ) );
				
			} );
			
		});

	};

} )( jQuery );
