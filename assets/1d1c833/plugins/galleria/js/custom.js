(function($){
	var methods = {
		init : function(){
		},

		
	};

	$.fn.custom = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод ' + method + ' не существует');
			return false;
		}
	};

	$(function(){
		$("#small-images").thumbnailScroller({ 
		  	scrollerType: "hoverPrecise", //hoverAccelerate
		  	scrollerOrientation:"horizontal", 
		  	scrollSpeed:2, 
		  	scrollEasing:"easeOutCirc", 
		  	scrollEasingAmount:800, 
		  	acceleration:4, 
		  	scrollSpeed:800, 
		  	noScrollCenterSpace:10, 
		  	autoScrolling:0, 
		  	autoScrollingSpeed:2000, 
		  	autoScrollingEasing:"easeInOutQuad", 
		  	autoScrollingDelay:500 
		});

		$('#small-images a.zoom').bind('click',function(e){
			e.preventDefault();
			href=$(this).attr("href");
			$('#big-image >a img').remove();
			 
			$('<img src="'+href+'">').appendTo($('#big-image a'));
			$('#big-image > a').data('id', $(this).data('id'));

		});

		$('#big-image a').on('click', function(e){
		 	e.preventDefault();
		 	$('#pirobox_images a[data-id="' + $(this).data('id') + '"]').trigger('click');
		});
	});
})(jQuery);