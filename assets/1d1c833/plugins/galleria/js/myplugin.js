/*

*/

(function($){
	$.fn.runGalleria = function(param){

		return this.each(function(index){

			var source_thumbs = $(this);
			var index = $('.galleria-thumbs').length;
			var id = 'galleria-' + index;

			if(param == undefined)
				$(this).attr('data-gal', index);
			else
				param.attr('data-gal', index);

		// Creating structure =========

			thumbs = $('<div id="'+id+'" class="galleria-thumbs" data-gal="'+ index + '"></div>');
			thumbs.appendTo('body');	

			source_thumbs.find('a.zoom').clone().appendTo(thumbs);
			thumbs.html(thumbs.html());

		// Main ===========
			if(param == undefined){
				source_thumbs.find('a').on('click', function(e){
					e.preventDefault();
					index = $(this).parent().data('gal');
					thumbs = $('#galleria-'+index);

					//find index
					var ind = $(this).parent().find('a').index($(this));

					//run
					Galleria.run(thumbs);

					// //fullscreen
					Galleria.get(0).$('stage').css("left", 0);
					// // show(number of image to show);
					Galleria.get(0).enterFullscreen(function(){
						this.show(ind);
					});
					
				});
			}
			else{
				var object = param;
				object.find('a').on('click', function(e){
					e.preventDefault();

					var data_id = $(this).data('id');
					index = $(this).parent().data('gal');

					thumbs = $('#galleria-'+index);

					var ind = thumbs.find("a[data-id='"+data_id+"']").index();

					Galleria.run(thumbs);
					//fullscreen
					Galleria.get(0).$('stage').css("left",0);
					// // show(number of image to show);	
					Galleria.get(0).enterFullscreen(function(){
						this.show(ind);
					});
				});
			};


		//DESTROY GALLERIA =========

			$(document)
			//case: when clicked button close
			.delegate('#galleria-'+index+' .galleria-close', 'click', function(){
				Galleria.get(0).exitFullscreen().destroy();
			})
			//case: when clicked 'Esc'
			.keyup(function(e) {
				if ((e.keyCode == 27) && $('#galleria-'+index+' .galleria-container').length > 0) {
					Galleria.get(0).exitFullscreen().destroy();
				}   // esc
			});
		});
	};
	
}(jQuery));