<?php /* Smarty version Smarty-3.1.14, created on 2015-09-13 20:46:01
         compiled from "/home/bh57137/public_html/protected/modules/core/themes/webarch/views/layouts/tabs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:143966278455f5a849da55a3-39436717%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29b081794576ab4ea9b39a696aa38b2af918de76' => 
    array (
      0 => '/home/bh57137/public_html/protected/modules/core/themes/webarch/views/layouts/tabs.tpl',
      1 => 1433165787,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '143966278455f5a849da55a3-39436717',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_55f5a849e0edd8_47801700',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f5a849e0edd8_47801700')) {function content_55f5a849e0edd8_47801700($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['this']->value->beginContent('//layouts/prepare');?>

	
	<?php echo $_smarty_tpl->tpl_vars['this']->value->renderPartial('//layouts/parts/_header');?>

	
	<div class="page-content tabs <?php if (UIHelpers::hideSideBar()){?>condensed<?php }?>">

		<div class="page-header clearfix">
			<?php echo $_smarty_tpl->tpl_vars['this']->value->renderPartial('//layouts/parts/_breadcrumbs');?>

			<div class="page-title">
				<h3><?php echo Yii::app()->format->custom('normal<semibold>',$_smarty_tpl->tpl_vars['this']->value->pageTitle);?>
</h3>
				
				<?php if ($_smarty_tpl->tpl_vars['this']->value->pageDesc){?>
				 	<p class="mb20"><?php echo $_smarty_tpl->tpl_vars['this']->value->pageDesc;?>
</p>
				<?php }?>
			</div>

			<?php echo $_smarty_tpl->tpl_vars['this']->value->filters;?>

		</div>

		<div class="content">
		
			<?php echo $_smarty_tpl->tpl_vars['content']->value;?>


		</div>
	</div>

<?php echo $_smarty_tpl->tpl_vars['this']->value->endContent();?>


<a href="#" class="scrollup"></a>
<?php }} ?>