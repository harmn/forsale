<?php /* Smarty version Smarty-3.1.14, created on 2015-06-04 12:14:57
         compiled from "/home/bh57137/public_html/protected/modules/core/modules/user/views/back/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:266461165557009013b5a99-31593247%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dcb4037e56e5d22e2b3e06f69c034ba4231ec125' => 
    array (
      0 => '/home/bh57137/public_html/protected/modules/core/modules/user/views/back/login.tpl',
      1 => 1433165715,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '266461165557009013b5a99-31593247',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'action' => 0,
    'model' => 0,
    'form' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_55700901503157_56064443',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55700901503157_56064443')) {function content_55700901503157_56064443($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/bh57137/public_html/protected/modules/core/vendors/Smarty/plugins/block.form.php';
?><div class="login-box animated clearfix">
    
    <div class="logo clearfix">
        <h1 class="mt0"><?php echo param('settings/backendHeaderSiteName');?>
</h1>
    </div>
    
    <hr>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('name'=>"form",'enableClientValidation'=>false,'enableAjaxValidation'=>true,'clientOptions'=>array('validateOnSubmit'=>true,'beforeValidate'=>'js:Auth.beforeLogin','afterValidate'=>'js:Auth.login'),'action'=>$_smarty_tpl->tpl_vars['action']->value,'htmlOptions'=>array('data-ajax-action'=>url('/core/user/back/ajaxlogin'),'autocomplete'=>'off'))); $_block_repeat=true; echo smarty_block_form(array('name'=>"form",'enableClientValidation'=>false,'enableAjaxValidation'=>true,'clientOptions'=>array('validateOnSubmit'=>true,'beforeValidate'=>'js:Auth.beforeLogin','afterValidate'=>'js:Auth.login'),'action'=>$_smarty_tpl->tpl_vars['action']->value,'htmlOptions'=>array('data-ajax-action'=>url('/core/user/back/ajaxlogin'),'autocomplete'=>'off')), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <div class="control-group">
            <?php echo $_smarty_tpl->tpl_vars['form']->value->label($_smarty_tpl->tpl_vars['model']->value,'username',array('class'=>'control-label'));?>

            <div>
                <?php echo $_smarty_tpl->tpl_vars['form']->value->textField($_smarty_tpl->tpl_vars['model']->value,'username',array('class'=>'w100p'));?>

                <?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'username');?>

            </div>
        </div>
        
        <div class="control-group">
            <?php echo $_smarty_tpl->tpl_vars['form']->value->label($_smarty_tpl->tpl_vars['model']->value,'password',array('class'=>'control-label'));?>

            <div>
                <?php echo $_smarty_tpl->tpl_vars['form']->value->passwordField($_smarty_tpl->tpl_vars['model']->value,'password',array('class'=>'w100p'));?>

                <?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'password');?>

            </div>
        </div>
        
        <input type="submit" class="btn btn-success w100p mt10 mb10" value="ВОЙТИ">
        
        <div class="checkbox check-success mt20">
            <?php echo $_smarty_tpl->tpl_vars['form']->value->checkBox($_smarty_tpl->tpl_vars['model']->value,'rememberMe');?>

            <?php echo $_smarty_tpl->tpl_vars['form']->value->label($_smarty_tpl->tpl_vars['model']->value,t('user','rememberMe'));?>

        </div>
    
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"form",'enableClientValidation'=>false,'enableAjaxValidation'=>true,'clientOptions'=>array('validateOnSubmit'=>true,'beforeValidate'=>'js:Auth.beforeLogin','afterValidate'=>'js:Auth.login'),'action'=>$_smarty_tpl->tpl_vars['action']->value,'htmlOptions'=>array('data-ajax-action'=>url('/core/user/back/ajaxlogin'),'autocomplete'=>'off')), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>  
<?php }} ?>