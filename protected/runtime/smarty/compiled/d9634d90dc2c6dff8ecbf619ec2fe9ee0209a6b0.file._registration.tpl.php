<?php /* Smarty version Smarty-3.1.14, created on 2015-09-13 20:46:01
         compiled from "/home/bh57137/public_html/protected/modules/core/modules/user/views/back/_registration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:142023526255f5a8498d0176-86810012%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9634d90dc2c6dff8ecbf619ec2fe9ee0209a6b0' => 
    array (
      0 => '/home/bh57137/public_html/protected/modules/core/modules/user/views/back/_registration.tpl',
      1 => 1433165716,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '142023526255f5a8498d0176-86810012',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'widget' => 0,
    'modal' => 0,
    'model' => 0,
    'form' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_55f5a8499d71c1_66426627',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f5a8499d71c1_66426627')) {function content_55f5a8499d71c1_66426627($_smarty_tpl) {?><?php if (!is_callable('smarty_block_begin_widget')) include '/home/bh57137/public_html/protected/modules/core/vendors/Smarty/plugins/block.begin_widget.php';
if (!is_callable('smarty_block_form')) include '/home/bh57137/public_html/protected/modules/core/vendors/Smarty/plugins/block.form.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('begin_widget', array('name'=>'UIModal','id'=>'register-modal','width'=>800,'bodyClass'=>'body','title'=>'Регистрация пользователя')); $_block_repeat=true; echo smarty_block_begin_widget(array('name'=>'UIModal','id'=>'register-modal','width'=>800,'bodyClass'=>'body','title'=>'Регистрация пользователя'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	        
<?php $_smarty_tpl->tpl_vars['modal'] = new Smarty_variable($_smarty_tpl->tpl_vars['widget']->value, null, 0);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('name'=>'form','modal'=>true,'action'=>array('/core/user/back/registration'),'enableAjaxValidation'=>true,'afterModalClose'=>'function(form){
			$.fn.yiiGridView.update("users-table"); //update table
		}')); $_block_repeat=true; echo smarty_block_form(array('name'=>'form','modal'=>true,'action'=>array('/core/user/back/registration'),'enableAjaxValidation'=>true,'afterModalClose'=>'function(form){
			$.fn.yiiGridView.update("users-table"); //update table
		}'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


<?php echo $_smarty_tpl->tpl_vars['modal']->value->header();?>


<div class="mt20">
	<div class="grid simple">
		
		<div class="grid-body no-border clearfix">
				
			<p class="mb20"><?php echo t('admin','required_fields');?>
</p>

			<div class="row">
				<div class="col-md-6">

					<div class="control-group">
						<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'email');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->textField($_smarty_tpl->tpl_vars['model']->value,'email');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'email');?>

					</div>

					<div class="control-group">
						<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'username');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->textField($_smarty_tpl->tpl_vars['model']->value,'username');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'username');?>

					</div>

					<div class="row">
						<div class="control-group col-md-6">
							<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'password');?>

							<?php echo $_smarty_tpl->tpl_vars['form']->value->passwordField($_smarty_tpl->tpl_vars['model']->value,'password');?>

							<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'password');?>

						</div>

						<div class="control-group col-md-6">
							<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'password2');?>

							<?php echo $_smarty_tpl->tpl_vars['form']->value->passwordField($_smarty_tpl->tpl_vars['model']->value,'password2');?>

							<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'password2');?>

						</div>
					</div>
					
				</div>
				
				<div class="col-md-6">
					<div class="control-group">
						<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'firstname');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->textField($_smarty_tpl->tpl_vars['model']->value,'firstname');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'firstname');?>

					</div>

					<div class="control-group">
						<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'lastname');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->textField($_smarty_tpl->tpl_vars['model']->value,'lastname');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'lastname');?>

					</div>

					<div class="control-group">
						<?php echo $_smarty_tpl->tpl_vars['form']->value->labelEx($_smarty_tpl->tpl_vars['model']->value,'middlename');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->textField($_smarty_tpl->tpl_vars['model']->value,'middlename');?>

						<?php echo $_smarty_tpl->tpl_vars['form']->value->error($_smarty_tpl->tpl_vars['model']->value,'middlename');?>

					</div>
				</div>

			</div>
				
		</div> <!-- // grid body -->
	</div>
</div>

<?php echo $_smarty_tpl->tpl_vars['modal']->value->footer();?>


<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>'form','modal'=>true,'action'=>array('/core/user/back/registration'),'enableAjaxValidation'=>true,'afterModalClose'=>'function(form){
			$.fn.yiiGridView.update("users-table"); //update table
		}'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_begin_widget(array('name'=>'UIModal','id'=>'register-modal','width'=>800,'bodyClass'=>'body','title'=>'Регистрация пользователя'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>