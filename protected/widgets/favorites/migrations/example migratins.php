<?
/**
 * Favorites table-i hamar migraciayi orinak
 * copy aneluc stugel
 *
*/

public function up(){
	
	$transaction = Yii::app()->db->beginTransaction();
	
	try{
		
		if(Yii::app()->db->getSchema()->getTable("{{favorites}}")){
			$this->dropTable("{{favorites}}");
		}

		$this->createTable("{{favorites}}", array(
			"id" 				  => "int UNSIGNED AUTO_INCREMENT",
			"id_user"			  => "int(10)",
			"id_product" 		  => "int(10)",
			"type" 		  		  => "int(10)",
			"created"		      => "datetime",
			"id_creator"	      => "int(10)",
			"changed"		      => "datetime",
			"id_changer"	      => "int(10)",
			"PRIMARY KEY (id)",
			"KEY id_user     (id_user)",
			"KEY id_product  (id_product)",
			"KEY type  		 (type)",
			"KEY id_creator  (id_creator)",
			"KEY changed     (changed)",
			"KEY id_changer  (id_changer)",
		));

		$transaction->commit();
	}
	catch(Exception $e){
		$transaction->rollback();
	}
}

public function down(){
	if(Yii::app()->db->getSchema()->getTable("{{favorites}}")){
		$this->dropTable("{{favorites}}");
	}
}
?>