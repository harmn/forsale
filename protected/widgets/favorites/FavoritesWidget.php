<?php

/**
* FavoritesWidget
*
* виджет для "Избранные" и "Посмотреть позже",
*
* для использования действий в контроллере надо добавить
* public function behaviors(){
*	return [
*		//действия для favorite, watch later
*		'FavoriteActions'
*	];
* }
*
*/
class FavoritesWidget extends SWidget{
	public $model;
	public $linkTitle	= '';
	public $titleStyle	= false;
	public $topIcon		= false;
	public $href		= false;
	public $linkHtmlOptions = [];
	private $url;

	public function run(){

		// js ev css file-ery kardalu hamar
		cs()->registerPackage('favorites');
		cs()->registerScript('favorites-widget', "$.fn.favorites('init')", CClientScript::POS_READY);

		// url icon
		$this->url = ($this->linkHtmlOptions && isset($this->linkHtmlOptions['url'])) ?
			$this->linkHtmlOptions['url'] : '#';

		//set default icon
		if($this->linkHtmlOptions['data-type']){
			switch ($this->linkHtmlOptions['data-type']) {
				case  Favorites::FAVORITES :
					$classIcon = 'fa fa-heart-o c-gray';
					break;
				case  Favorites::WATCH_LATER :
					$classIcon = 'fa fa-clock-o c-gray';
					break;
			}
		}

		if(!user()->isGuest){// login exac jamanak iconkayi active ev voch aktive cuyc tal@
			if($this->model && $this->model->favorites){
				foreach ($this->model->favorites as $favorite){
					if($favorite->type == $this->linkHtmlOptions['data-type']){
						if($favorite->type == Favorites::FAVORITES)   	$classIcon = "fa fa-heart c-gray";
						if($favorite->type == Favorites::WATCH_LATER)   $classIcon = 'fa fa-clock-o c-black';
					}
				}
			}
		}
		else{// cookie-nerov iconkayi active ev voch aktive cuyc tal@
			if($this->linkHtmlOptions['data-type'] == Favorites::FAVORITES && Cookie::get(Favorites::COOKIE_NAME_FAVORITE) && $this->model){
				if(in_array($this->model->id, CJSON::decode(Cookie::get(Favorites::COOKIE_NAME_FAVORITE))))
					$classIcon = "fa fa-heart c-gray";
			}
			elseif($this->linkHtmlOptions['data-type'] == Favorites::WATCH_LATER && Cookie::get(Favorites::COOKIE_NAME_WATCH_LATER) && $this->model){
				if(in_array($this->model->id, CJSON::decode(Cookie::get(Favorites::COOKIE_NAME_WATCH_LATER))))
					$classIcon = 'fa fa-clock-o c-black';
			}
		}

		// top icon - kaxvac favorite-neri qanakic active cuyc tal@
		if(!user()->isGuest && $this->titleStyle && $this->topIcon && Favorites::CheckFavorite($this->linkHtmlOptions['data-type']) > 0){

			if($this->linkHtmlOptions['data-type'] == Favorites::FAVORITES)
				$classIcon = "fa fa-heart c-gray";

			if($this->linkHtmlOptions['data-type'] == Favorites::WATCH_LATER)
				$classIcon =  "fa fa-clock-o c-black";
		}// vereviiconki active vijak@ kaxvac cookineric
		elseif(user()->isGuest && $this->topIcon && $this->titleStyle){
			// stugum enq cookie ka favorite -i hamar
			if ($this->linkHtmlOptions['data-type'] == Favorites::FAVORITES) {
				if(Cookie::get(Favorites::COOKIE_NAME_FAVORITE) && strlen($_COOKIE[Favorites::COOKIE_NAME_FAVORITE]) != 0)
				$classIcon = "fa fa-heart c-gray";
				else $classIcon = "fa fa-heart-o c-gray";
			}
			// stugum enq cookie ka watch later-i hamar
			if ($this->linkHtmlOptions['data-type'] == Favorites::WATCH_LATER) {
				if(Cookie::get(Favorites::COOKIE_NAME_WATCH_LATER) && strlen($_COOKIE[Favorites::COOKIE_NAME_WATCH_LATER]) != 0)
				$classIcon =  "fa fa-clock-o c-black";
				else $classIcon = "fa fa-clock-o c-gray";
			}
		}

		if($this->titleStyle)
			$this->linkTitle = "<i class='".$classIcon." ".$this->titleStyle."'></i>".$this->linkTitle;

		else $this->linkHtmlOptions['class'] = $classIcon ." ". $this->linkHtmlOptions['class'];

		$this->href = ($this->href) ? $this->href : '#';

		// $this->url,
		echo CHtml::link($this->linkTitle, $this->href, $this->linkHtmlOptions);
	}
}
