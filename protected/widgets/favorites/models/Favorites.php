<?php

/**
 * Favorites
 *
 * @property integer	$id 			- id
 * @property integer	$id_user 		- id юзера
 * @property datetime	$id_product 	- id продукта
 * @property datetime	$type 		 	- тип
 * @property integer	$id_creator		- Кто создал
 * @property integer	$id_changer		- Кто изменил
 * @property datetime	$changed 		- изменено
 * @property datetime	$created 		- создано
 */

class Favorites extends AR {
	const FAVORITES   = 1; // type favorite
	const WATCH_LATER = 2; // type watch later
	const COOKIE_NAME_FAVORITE    = "favorite-products";
	const COOKIE_NAME_WATCH_LATER = "watch-later-products";

	const COOKIE_NAME_RESENTLY_VIEW_ITEMS		 = 'Resently-View-Item';
	const RESENTLY_VIEW_ITEMS_LIMIT				 = 20;
	const RESENTLY_VIEW_ITEMS_LIMIT_SHOW_DISPLAY = 5;

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($classname = __CLASS__){
		return parent::model($classname);
	}

	/**
	 * @return string
	 */
	public function tableName(){
		return "favorites";
	}

	/**
	 * Rules
	 * @return array
	 */
	public function rules(){
		return [
			['id_user, id_product, type', 'numerical', 'integerOnly'=>true],
		];
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations(){
		return [];
	}

	/**
	 * AttributeLabels
	 * @return array
	 */
	public function attributeLabels(){
		return [];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => [
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			],
		));
	}

	public static function CheckFavorite($type){
		$criteria = new CDbCriteria;
		$criteria->compare('t.id_user', user()->id);
		$criteria->compare('t.type', $type);

		return Favorites::model()->count($criteria);
	}
}
