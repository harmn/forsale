(function($){
	var urls = {};
    var header, wrap, topFavoriteIcon, topWatchLaterIcon, listView, itemList, profile;
    var methods = {

		init : function(){
            header 	= $('header');
			wrap    = $('#wrap');
            topFavoriteIcon   = header.find('.favorites').find('i');
			topWatchLaterIcon = header.find('.watch-later').find('i');
			profile  = wrap.find('.profile');
			listView = wrap.find('.list-view');
			itemList = wrap.find('.item-list');
        },

        updateFListView : function(self, typeList){
        	var idFListView = self.closest('.list-view').attr('id');
			var type   = self.data('type');

			var data   = {view : typeList, viewType: type, idFListView: idFListView};
			$.fn.yiiListView.update( idFListView, {data: data});
        },

        // цвет иконки при клике Избранные 
        colorIconFavorite : function(data){
        	// verevi iconkayi active vijak@
			// save favorite
        	if(data.save && topFavoriteIcon.hasClass('fa-heart-o'))
				topFavoriteIcon.removeClass('fa-heart-o').addClass('fa-heart');

			// delete favorite
			if(data.countFavorite == 0)
				topFavoriteIcon.removeClass('fa-heart').addClass('fa-heart-o');

			// cookie, active class icon
			if(data.cookie && $.cookie('favorite-products'))
				topFavoriteIcon.removeClass('fa-heart-o').addClass('fa-heart');
			// cookie remove
			if(data.cookie && !$.cookie('favorite-products'))
				topFavoriteIcon.removeClass('fa-heart').addClass('fa-heart-o');
        },

        // цвет иконки при клике  Посмотреть позже
        colorIconWatchLater : function(data){
			if(data.save && topWatchLaterIcon.hasClass('c-gray'))
				topWatchLaterIcon.removeClass('c-gray').addClass('c-black');

			// delete
			if(data.countFavorite == 0)
				topWatchLaterIcon.removeClass('c-black').addClass('c-gray');

			// cookie set
			if(data.cookie && $.cookie('watch-later-products'))
				topWatchLaterIcon.removeClass('c-gray').addClass('c-black');
			// cookie remove
			if(data.cookie && !$.cookie('watch-later-products'))
				topWatchLaterIcon.removeClass('c-black').addClass('c-gray');
        },
	};

    $.fn.favorites = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод ' + method + ' не существует');
            return false;
        }
    };

	$(function(){
		methods.init();

		// добавление и удаление продукта из Избранные и Посмотреть позже
		$(document).on('click', '.favorite', function(event){
			event.preventDefault();
			var self 	   = $(this);
			var type 	   = self.data('type');
			var idProduct  = self.data('id');
			var favorite   = 1;
			var watchLater = 2;

		jPost(self.data('url'), {idProduct: idProduct, type: type},
			function(data){
				if(data.type == favorite){
					// product-i active icon, 
					var selfFavorite = $('a.favorite[data-id='+idProduct+'][data-type='+favorite+']');
					selfFavorite.each(function(){ // widgeti depqum
						if($(this).find('i').length != 0)
							$(this).find('i').toggleClass('fa-heart fa-heart-o');
						else $(this).toggleClass('fa-heart fa-heart-o');
					});

					// цвет иконки Избранные
					methods.colorIconFavorite(data);
				}

				if(data.type == watchLater){
					// show watch later active icon
					if(self.find('i').length != 0)
						self.find('i').toggleClass('c-gray c-black');
					else self.toggleClass('c-gray c-black');
					// цвет иконки Посмотреть позже
					methods.colorIconWatchLater(data);
				}

				// favorite@ profile-ic heracneluc update list
				idFListView = self.closest('.list-view').attr('id');
				activeMenu  = profile.find('li.active').find('a').attr('href');

				if((data.type == watchLater || data.type == favorite) && (activeMenu == '/later' || activeMenu == '/favorite'))
					methods.updateFListView(self, '_description');
				}
			);
		});
	});
})(jQuery);
