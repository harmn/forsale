<?
/**
*
* widget-@ producti favorite-i ev watch later-i hamar e
*
*/
class FavoriteActions extends CBehavior{

	public function actionFavorites(){

		$id_product = request()->getParam('idProduct');
		$type 	    = request()->getParam('type');
		$id_user	= user()->id;

		if(!user()->isGuest && $type && $id_product){ // user online
			// cookie save db and remove cookie
			// $this->deleteCookieFavoriteProducts([Favorites::COOKIE_NAME_FAVORITE, Favorites::COOKIE_NAME_WATCH_LATER]);

			$criteria = new CDbCriteria;
			$criteria->compare('t.id_user', $id_user);
			$criteria->compare('t.id_product', $id_product);
			$criteria->compare('t.type', $type);
			$model = Favorites::model()->find($criteria);

			if($model){ // remove favorite product
				$model->delete();

				$criteria = new CDbCriteria;
				$criteria->compare('t.id_user', $id_user);
				$criteria->compare('t.type', $type);

				$countFavorite = Favorites::model()->count($criteria);///???? может надо STAT сделать

				Common::jsonSuccess(true, compact('countFavorite', 'type'));
			}
			else{// add favorite product
				$model = new Favorites;
				$model->id_user 	= $id_user;
				$model->id_product 	= $id_product;
				$model->type 		= $type;
				if($model->save()) Common::jsonSuccess(true, ['type'=>$type, 'save'=>'save']);
			}
		}// if user online
		elseif(user()->isGuest){ // else user is guest
			if($type == Favorites::FAVORITES)   $cookieKey = Favorites::COOKIE_NAME_FAVORITE;
			if($type == Favorites::WATCH_LATER) $cookieKey = Favorites::COOKIE_NAME_WATCH_LATER;

			//default data for cookie
			$cookieArray = [$id_product];

			if(Cookie::get($cookieKey)){

				$cookieArray = CJSON::decode(Cookie::get($cookieKey));
				// jnjum enq tvyal id-n array-i mejic
				if(($key = array_search($id_product, $cookieArray)) !== false)
				    unset($cookieArray[$key]);		

				// avelacnum enq cookie-i mej
				else array_unshift($cookieArray, $id_product);
			}

			$value = empty($cookieArray)? "" : CJSON::encode($cookieArray);

			setcookie($cookieKey, $value, time()+60*60*2, '/');

			Common::jsonSuccess(true, ['cookie'=>'cookie','type'=>$type]);
		}// else user is guest
	}

	/**
     * Удаляет из куки данные фавориты и посм.позже и добавляет ш базу
     *
     */
    public function deleteCookieFavoriteProducts(){
        $cookieKeys = [Favorites::COOKIE_NAME_FAVORITE, Favorites::COOKIE_NAME_WATCH_LATER];

        foreach ($cookieKeys as $cookieKey) {
        	$type = ($cookieKey == Favorites::COOKIE_NAME_FAVORITE)? 
        		Favorites::FAVORITES : Favorites::WATCH_LATER;

            if(Cookie::get($cookieKey)){

                $criteria = new CDbCriteria;
                $criteria->compare('t.id_user', user()->id);
                $criteria->compare('t.type', $type);
                $models = Favorites::model()->findAll($criteria);

                $cookies = CJSON::decode(Cookie::get($cookieKey));

                foreach ($cookies as $id_product) {
                    if(!in_array($id_product, $models)){
                        $newModel = new Favorites;
                        $newModel->id_user = user()->id;
                        $newModel->id_product = $id_product;
                        $newModel->type = $type;
                        $newModel->save();
                    }
                }

                // bazayum ave enq anum ev jnjum enq cookie-n
                setcookie($cookieKey, '', 0, '/');
            }
        }
    }
}
