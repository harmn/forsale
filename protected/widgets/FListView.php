<?

/**
* FListView
*
* для использования CListView на фронтенде
*/


class FListView extends SListView{

	public $showPageSize = true;
	public $viewType = 'list'; // or thumbnail

	public function renderSorter()
	{
		if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes))
			return;

		echo CHtml::openTag('div', ['class' => 'fr']);
			echo CHtml::openTag('div',array('class'=>$this->sorterCssClass))."\n";
				echo "<span class='fsize12 c-gray mr5 fl'>". ($this->sorterHeader===null ? Yii::t('zii','Sort by: ') : $this->sorterHeader) . "</span>";
				echo "<ul>\n";
				$sort=$this->dataProvider->getSort();

				foreach($this->sortableAttributes as $name=>$label)
				{
					echo "<li>";
					if(is_integer($name))
						echo $sort->link($label);
					else
						echo $sort->link($name,$label);
					echo "</li>\n";
				}
				echo "</ul>";
				echo $this->sorterFooter;

			echo CHtml::closeTag('div');

			echo CHtml::openTag('div', ['class' => 'page-view']);
				$list = $this->viewType == 'list' ? 'active' : '';
				$thumbnail = $this->viewType == 'thumbnail' ? 'active' : '';
	    		echo "<div class='page-view'>
						<a href='#' class='fa fa-th-list $list' data-type='list'></a>
						<a href='#' class='fa fa-th-large $thumbnail' data-type='thumbnail'></a>
					</div>";
	    	echo CHtml::closeTag('div');

    	echo CHtml::closeTag('div');
	}



}