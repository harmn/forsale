<?php
	return array(
		"Регистрация" 			=> "Գրանցում",
		"Выход" 				=> "Ելք",
		"Вход" 					=> "Մուտք",
		"Дом" 					=> "Անշարժ գույք",
		"Авто" 					=> "Տրանսպորտ",
		"Электроника" 			=> "Էլեկտրոնիկա",
		"Техника" 				=> "Տեխնիկա",
		"Мебель" 				=> "Կահույք",
		"Мода" 					=> "Նորաձևություն",
		"Спорт и Игры" 			=> "Սպորտ և խաղեր",
		"Животное и растения" 	=> "Կենդանիներ և բույսեր",
		"Работа" 				=> "Աշխատանք",
		"Службы" 				=> "Ծառայություններ",
		"Главная" 				=> "Գլխավոր",
		"О нас" 				=> "Մեր մասին",
		"Контакты" 				=> "Կապ",
		"Область"				=> "Մարզ",
		"Город"					=> "Քաղաք",
		"Быстрый просмотр"		=> "Արագ դիտում",
		"Объявления"			=> "Հայտարարություններ",
		"Условия"				=> "Գրանցվելով՝ դուք համաձայնվում եք պայմաններին",
		"Войти"					=> "Մուտք",
		"Добавить"				=> "Ավելացնել",
		"Категория"				=> "Կատեգորիա",
		"Продается"				=> "Վաճառվում է",
		"Обменивается"			=> "Փոխանակվում է",
		"Аренда"				=> "Տրվում է վարձով",
		"Тип"					=> "Տեսակ",
		"Квартира"				=> "Բնակարան",
		"Офис"					=> "Օֆիսային տարածք",
		"Земельный участок"		=> "Հողատարածք",
		"Гараж"					=> "Ավտոտնակ",
		"Валюта"				=> "Փոխարժեք",
		"Регион"				=> "Մարզ",
		"Город/Район"			=> "Քաղաք/Վարչ. շրջան",
		"Цена"					=> "Գինը",
		"Количество комнат"		=> "Սենյակների քանակ",
		"Плошадь"				=> "Մակերես",
		"Этаж"					=> "Հարկ",
		"Количество этажей"		=> "Հարկերի քանակ",
		"Название"				=> "Վերնագիր",
		"Описание"				=> "Բնութագիր",
		"Автомобиль"			=> "Ավտոմեքենա",
		"Автозапчасти"			=> "Ավտոպահեստամասեր",
		"Аксессуары"			=> "Պարագաներ",
		"Уникальная"			=> "Եզակի Իրեր",
		"Марка"					=> "Բրենդ",
		"Состояние"				=> "Վիճակը",
		"Новое"					=> "Նոր",
		"С пробегом"			=> "Կիլոմետրաժով",
		"Битые"					=> "Խփած",
		"Пробег"				=> "Կիլոմետրաժ",
		"Кор/п"					=> "Փոխանցման տուփ",
		"Автомат"				=> "Ավտոմատ",
		"Механический"			=> "Մեխանիկական",
		"Типтроник"				=> "Տիպտրոնիկ",
		"Кол/с"					=> "Նստատեղերի քանակը",
		"Кузов"					=> "Թափք",
		"Руль"					=> "Ղեկ",
		"Левый"					=> "Ձախ",
		"Правый"				=> "Աջ",
		"Топливо"				=> "Վառելիք",
		"Бензин"				=> "Բենզին",
		"Дизель"				=> "Դիզել",
		"Гибрид"				=> "Հիբրիդ",
		"Газ/Бензин"			=> "Գազ/Բենզին",
		"белый"					=> "սպիտակ",
		"желтый"				=> "դեղին",
		"зеленый"				=> "կանաչ",
		"золотой"				=> "ոսկեգույն",
		"коричневый"			=> "շականակագույն",
		"красный"				=> "կարմիր",
		"серебристый"			=> "արծաթագույն",
		"серый"					=> "մոխրագույն",
		"синий"					=> "կապույտ",
		"фиолетовый"			=> "մանուշակագույն",
		"черный"				=> "սև",
		"Год выпуска"			=> "Արտադրության Տարի",
		"Сохранить"				=> "Պահպանել",
		"седан"					=> "սեդան",
		"хэтчбек"				=> "հեչբեկ",
		"универсал"				=> "ունիվերսալ",
		"внедорожник"			=> "ամենագնաց",
		"кроссовер"				=> "քրոսովեր",
		"пикап"					=> "պիկապ",
		"купе"					=> "կուպե",
		"кабриолет"				=> "կաբրիոլետ",
		"минивен"				=> "մինիվեն",
		"фургон"				=> "ֆուրգոն",
		"микроавтобус"			=> "միկրոավտոբուս",
		"Телефоны"					=> "Հեռախոս",
		"Телефонные аксессуары"		=> "Հեռախոսի ակսեսուարներ",
		"SIM карты"					=> "SIM քարտ",
		"Ноутбуки"					=> "Նոուտբուք",
		"Планшеты"					=> "Պլանշետ",
		"Компьютеры"				=> "Համակարգիչ",
		"Компьютерные устройства"	=> "Համակարգչային սարքեր",
		"Компьютерные аксессуары"	=> "Համակարգչային ակսեսուարներ",
		"Телевизоры"				=> "Հեռուստացույց",
		"Другое"					=> "Այլ",
		"Использованные"			=> "Օգտագործված",
		"Холодильники"				=> "Սառնարաններ",
		"Газовые плиты"				=> "Սալօջախներ",
		"Стиральные машины"			=> "Լվացքի մեքենաներ",
		"Швейные машины"			=> "Կարի մեքենաներ",
		"Пылесосы"					=> "Փոշեկուլներ",
		"Кухонная мебель"			=> "Խոհանոցի կահույք",
		"Утюги"						=> "Արդուկ",
		"Кондиционеры и вентиляция"	=> "Կոնդիցիոներներ և օդօրակիչներ",
		"Отопительные приборы"		=> "Տաքացուցիչներ",	
		"Кухонная мебель"			=> "Խոհանոցային կահույք",
		"Мебель для спальни"		=> "Ննջարանի կահույք",
		"Столы и стулья"			=> "Սեղաններ և աթոռներ",
		"Шкафы"						=> "Պահարաններ",
		"Диваны и кресла"			=> "Բազմոցներ և բազկաթոռներ",
		"Компьютерная мебель"		=> "Համակարգչային կահույք",
		"Подставки"					=> "Հենարաններ",
		"Тумбочки"					=> "Պահարաններ",
		"Цвет"						=> "Գույն",
		"Женская одежда"			=> "Կանացի հագուստ",
		"Женская обувь"				=> "Կանացի կոշիկներ",
		"Мужская одежда"			=> "Տղամարդկանց հագուստ",
		"Мужская обувь"				=> "Տղամարդկանց կոշիկներ",
		"Детская одежда"			=> "Մանկական հագուստ",
		"Детская обувь"				=> "Մանկական կոշիկներ",
		"Косметика и бижутерия"		=> "Կոսմետիկա եւ զարդեր",
		"Духи"						=> "Օծանելիք",
		"Часы"						=> "Ժամացույցների",
		"Сумки"						=> "Պայուսակներ",
		"Товары для детей"			=> "Մանկական ապրանքներ",
		"Красота и здоровье"		=> "Առողջություն եւ գեղեցկության",
		"Тренажёры"						=> "Տրինաժորներ",
		"Детские спортивные комплексы"	=> "Մանկական սպորտային համալիրներ",
		"Лыжи/Сноуборды и защита"		=> "Դահուկ/Սնոուբորդ եւ պաշտպանության",
		"Обувь"							=> "Կոշիկ",
		"Одежда"						=> "Հագուստ",
		"Спортивное питание"			=> "Սպորտային սնունդ",
		"Хоккей"						=> "Հոկեյ",
		"Велосипеды"					=> "Հեծանիվներ",
		"Гимнастика"					=> "Մարմնամարզություն",
		"Единоборства"					=> "Մարտարվեստ",
		"Мячи"							=> "Գնդակներ",
		"Туризм"						=> "Տուրիզմ",
		"Теннис"						=> "Թենիս",
		"Тяжелая атлетика"				=> "Ծանրամարտ",
		"Рюкзаки/Сумки"					=> "Ուսապարկեր/պայուսակներ",
		"Коньки/Ролики"					=> "Չմուշկներ/Ռոլիկներ",
		"Санки"							=> "Սահնակներ",
		"Спортивные приборы"			=> "Սպորտային սարքավորումներ",
		"Игри"							=> "Игри",
		"Возраст"						=> "Տարիք",
		"Собаки"						=> "Շներ",
		"Кошки"							=> "Կատուներ",
		"Рыбы"							=> "Ձկներ",
		"Птицы"							=> "Թռչուններ",
		"Сельскохозяйственные животные"	=> "Գյուղատնտեսական կենդանիներ",
		"Все для животных"				=> "Ամեն ինչ կենդանիների համար",
		"Ростения"						=> "Բույսեր",
		"Возраст от"					=> "Տարիքը սկսած",
		"Возраст до"					=> "Տարիքը մինչև",
		"Пол"							=> "Սեռ",
		"Опыт работы"					=> "Աշխատանքային փորց",
		"Офисная работа"				=> "Օֆիսային աշխատանք",
		"Торговля"						=> "Առեւտր",
		"Финансы и право"				=> "Ֆինանսներ և իրավունք",
		"Информационные технологии"		=> "Ինֆորմացիոն տեխնոլոգիաներ",
		"Mедиа и дизайн"				=> "Դիզայն և մեդիա",
		"Рестораны и кухня"				=> "Ռեստորաններ և խոհանոց",
		"Туризм и отели"				=> "Տուրիզմ և խոհանոց",
		"Танспорт и такси"				=> "Տրանսպորտ և տաքսի",
		"Бизнес и маркетинг"			=> "Բիզնես և մարկետինգ",
		"Строительство и архитектура"	=> "Շինարարություն եւ ճարտարապետություն",
		"Домашнее хозяйство"			=> "Տնային տնտեսություն",
		"Производство"					=> "Արտադրություն",
		"Образования"					=> "Կրթություն",
		"Здравоохранения"				=> "Առողջապահություն",
		"Мужской"						=> "Արական",
		"Женский"						=> "Իգական",
		"График"						=> "Գրաֆիկ",
		"Oбразование"					=> "Կրթություն",
		"Полный"						=> "Լռիվ",
		"Неполный"						=> "Ոչ լռիվ",
		"Профессиональное"				=> "Մասնագիտական",
		"Начальное"						=> "Նախնական",
		"Среднее"						=> "Միջին մասնագիտական",
		"Бакалавр"						=> "Բակալավր",
		"Магистр"						=> "Մագիստրոս",
		"Аспирант"						=> "Ասպիրանտ",
		"Кандидат"						=> "Թեքնածու",
		"Языки"							=> "Լեզուներ",
		"Зарплата"						=> "Աշխատավարձ",
		"Строительство и ремонт"		=> "Շինարարություն եւ վերանորոգում",
		"Транспорт"						=> "Տրանսպորտ",
		"Электроника"					=> "Էլեկտրոնիկա",
		"Компьютеры и Интернет"			=> "Համակարգիչներ և համացանց",
		"Образование"					=> "Կրթություն",
		"Торговые услуги"				=> "Բիզնես ծառայություններ",
		"Бытовые услуги"				=> "Կենցաղային ծառայություններ",
		"Красота и здоровье"			=> "Առողջություն եւ գեղեցկության",
		"События и праздники"			=> "Միջոցառումներ եւ հանդիսությունների",
		"Туризм и путешествия"			=> "Զբոսաշրջություն եւ ճանապարհորդություն",
		"СмсРегистрацияУспешно"			=> "<div>Գրանցումն հաջողությամբ կատարվել է:</div><div>Խնդրում ենք ստուգել Ձեր Էլ. հասցեն ակտիվացնելու համար Ձեր էջը:</div><div>Շնորհակալություն գրանցվելու համար:</div>",
		"СмсРегистрацияНеУспешно"		=> "<div>Ռեգիստրացիայի սխալ</div><div>Ռեգիստրացիան չի կատարվել. Կրկին փորձեք.</div>",
		"Регистрация на сайте"			=> "Գրանցում ForSale.am կայքում",
		"Ваш акаунт на сайте" 			=> "Ձեր էջը գրանցվել է ForSale.am կայքում:",
		"Если вы на самом деле регистрировались на нашем сайте, то для активации Вашего акаунта Вам нужно перейти по " => "Եթե Դուք իսկապես գրանցվել եք, ապա ձեր էջի ակտիվացման համար անցեք ",
		"В обратном случае просто проигнорируйте данное письмо." => "Հակառակ դեպքում հեռացրեք նամակը:",
		"ссылке" 						=> "հետևյալ էջ,",
		"С уважением" 					=> "Հարգանքներով <br/><strong>ForSale.am</strong>",
		"Перейти на главную"			=> "Գլխավոր էջ",
		"Перейти на страницу входа"		=> "Մուտքի էջ",
		"Активация прошла успешно! Теперь вы можете войти на сайт используя свой логин и пароль!" => "Ակտիվացումը կատարվել է: Այժմ դուք կարող եք մուտք գործել, օգտագործելով Ձեր ծածկանունը եւ գաղտնաբառը:",
		"Теперь Вы можете"				=> "Այժմ դուք կարող եք",
		"войти на сайт"					=> "մուտք գործել կայք,",
		", используя свои регистрационные данные."	=> ", օգտագործելով Ձեր գրանցված տվյալները:",
		"Последние"						=> "Վերջին Հայտարարությունները",
		"Скоро"							=> "Շուտով",
		"Модель"						=> "Մոդել",
		"Пробег от"						=> "Վազք սկսած",
		"Пробег до"						=> "Վազք մինչև",
		"Год выпуска с"					=> "Արտադ. տ/թ սկսած",
		"Год выпуска по"				=> "Արտադ. տ/թ մինչև",
		"Твой бесплатный онлайн брокер"	=> "Քո անվճար օնլայն բրոկերը",
		"Новое объявление"				=> "Նոր հայտարարություն",
		"Подписка"						=> "Բաժանորդագրվեք",
		"На новости"					=> "Նորություններին",
		"Для чего создан сайт ForSale.am ?"	=> "Ինչի՞ համար է ստեղծվել ForSale.am կայքը",
		"Как подать объявление ?"		=> "Ինչպե՞ս ավելացնել հայտարարություն",
		"Наши преимущества"				=> "Մեր առավելությունները",
		"Дополнительные услуги"			=> "Լրացուցիչ ծառայություններ",
		"ForSale.am это сайт для каждого, кому необходимо продать или купить недвижимость, транспортное средство или что-то другое, и хочет быстро и просто получить всю необходимую информацию." => "ForSale.am-ը կայքը է բոլոր այն մարդկանց համար, ովքեր ցանկանում են վաճառել կամ ձեռք բերել անշարժ գույք, տրանսպորտային միջոց կամ որևէ այլ բան, և ցանկանում են արագ ստանալ ողջ անհրաժեշտ ինֆորմացիան.",
		"Вы хотите подать объявление о продаже, обмене или аренде  и не знаете с чего начать? Начните с регистрации на ForSale.am! Мы постарались сделать так, чтобы процесс создания объявления был для Вас максимально быстр, прост и понятен." => "Դուք ցանկանում եք վաճառքի, փոխանակման կամ վարձակալման մասին հայտարարություն տեղադրել, բայց չգիտեք, թե ինչից սկսել: Սկսեք գրանցումից forsale.am-ում: Մենք արել ենք ամեն ինչ, որպեսզի հայտարության ստեղծման ընթացքը ձեզ համար լինի մաքսիմալ արագ, պարզ և հասկանալի:",
		"Мы поддерживаем базу в актуальном состоянии. Мы сделали удобный сайт. Совершенствуются и успешно внедряются полезные сервисы для продавцов, арендодателей и для покупателей." => "Մենք ստեղծել ենք հարմարավետ և արագագործ կայք: Այն շատ հարմար է  վաճառողների, գնորդների, վարձակալողների և փոխանակողների համար: Տվյալների բազան պահվում է ակտուալ վիճակում:",
		"Вы можете поставить рекламу на нашем сайте."	=> "Դուք կարող եք տեղադրել ձեր գովազդը մեր կայքում.",
		"Сортировать по:"				=> "Դասակարգել ըստ ",
		"Цене"							=> "Գումարի",
		"Названию"						=> "Անվանման",
		"Контакты"						=> "Կոնտակտներ",
		"Введите электронный адрес"		=> "Մուտքագրեք Ձեր էլ. փոստի հասցեն",
		"Забыли пароль?"				=> "Մոռացել եք գաղտնաբառը?",
		'Имя пользователя' 				=> 'Ծածկանուն',
		'Пароль' 						=> 'Գաղտնաբառ	',	
		'Повторите пароль' 				=> 'Գաղտնաբառի հաստատում',
		'Старый пароль' 				=> 'Հին գաղտնաբառ',
		'Новый пароль' 					=> 'Նոր գաղտնաբառ',
		'Имя' 							=> 'Անուն',
		'Фамилия'  						=> 'Ազգանուն',
		'Эл. почта' 					=> 'Էլ. հասցե',	
		'Логин' 						=> 'Ծածկանուն',
		'Забыли пароль?' 				=> 'Մոռացել ե՞ք գաղտնաբառը',
		'Воити' 						=> 'Մուտք',
		'Зарегистрировавшись, Вы соглашаетесь' => 'Գրանցվելով` Դուք համաձայնում եք ',
		'с условиями' 					=> 'պայմանների հետ:',	
		'Профиль' 						=> 'Պրոֆիլ',
		'Календарь' 					=> 'Оրացույց',
		'Сменить пароль' 				=> 'Փոխել գաղտնաբառը',
		'Удалить акаунт' 				=> 'Ջնջել պրոֆիլը',
		'Вы можете загрузить изображение в формате JPG, GIF или PNG.' => 'Դուք կարող եք ներբեռնել լուսանկար JPG, GIF և PNG. ձևաչափերի:',
		'Загрузить фотографию' 			=> 'Ներբեռնել լուսանկար',
		'Выбрать картинку' 				=> 'Ընտրել լուսանկար', 
		'Загружаемый файл не является картинкой' => 'Ներբեռված ֆայլը չի հանդիսանում լուսանկար', 
		'Смена пароля' 					=> 'Փոխել գաղտնաբառը',
		'Введите свой адрес эл. почты.' => 'Մուտքագրեք Ձեր Էլ. հասցեն',
		'Укажите новый пароль'			=> 'Մուտքագրեք Ձեր նոր գաղտնաբառը',
		"Активация прошла успешно! Теперь вы можете войти на сайт используя свой логин и пароль!" => "Ակտիվացումը կատարվել է: Այժմ դուք կարող եք մուտք գործել, օգտագործելով Ձեր ծածկանունը եւ գաղտնաբառը:",
		"Ваш пароль был изменен" 		=> "Ձեր գաղտնաբառը փոխվել է",
		"Изменить" 						=> "Փոխել",
		"Подождите"						=> "Սպասեք",
		"Перетащите картинки сюда"		=> "Քաշեք նկարը այստեղ",
		"Загрузить файл"				=> "Ներբեռնել լուսանկար",
		"Отмена"						=> "Չեղյալ համարել",
		"Повторить"						=> "Կրկնել",
		"Удалить"						=> "Ջնջել",
		"Ошибка загрузки"				=> "Բեռնման սխալ",
		"Следите за нами:"				=> "Հետեվեք մեզ",
		"Отправить"						=> "Ուղարկել",
		"Тема"							=> "Թեմա",
		"Мой профиль"					=> "Իմ էջը",
		"Мои объявления"				=> "Իմ հայտարարություններ",
		"Посмотреть позже"				=> "Դիտել ավելի ուշ",
		"Избранные"						=> "Առանձնացված հայտարարությունները",
		"Отчество"						=> "Հայրանուն",
		"Здравствуй, посетитель Forsale.am !"					=> "Ողջույն, Forsale.am-ի այցելու :",
		"О нас - текст"					=> "Forsale.am-ը հանրային կայք է, որտեղ դու կարող ես գտնել այն ամենն, ինչ կապված է առք ու վաճառքի հետ:                                                                                                                                                                           Մեր կայքն առաջարկում է քեզ մի շարք հետաքրքիր ու օգտակար ծառայություններ: 
Forsale.am-ում դու կարող ես գնել, վաճառել կամ վարձակալել ամենատարբեր ապրանքներ:
Forsale.am-ը հարթակ է յուրաքանչյուր հետաքրքրության ու ճաշակի մարդու համար: Մեր կայքում տեղ են գտել ամենամանր ապրանքներից մինչև ամենամեծերը, սակայն, շեշտն ավելի շատ դրված է մեքենաների և բնակարանների վրա: Պարզ խորագրերը քեզ կօգնեն ճիշտ և արագ կողմնորոշվել, իսկ կայքի դիզայնն առավելագույնս հարմարեցված է  յուրաքանչյուրի համար: 
Forsale.am կայքը ստեղծել է բոլոր անհրաժեշտ լծակները , որպեսզի խնայի քո ժամանակն ու դարձնի քո կյանքն առավել հարմարավետ: 
Տեղադրեք   ձեր հայտարարությունը, և այն կտեսնեն  մեր կայքի հազարավոր  այցելուներ:",
		"Желаем  приятных покупок, выгодных продаж."	=> "Մաղթում ենք հաճելի գնումներ, շահավետ վաճառք:",
		"Нет результатов"				=> "Ձեր որոնմանը համապատասխանող արդյունք չկա:",
		"Сброс пароля" 					=> "Գաղտնաբառի փոփոխություն",
		"Для сброса пароля на сайте " 	=> "Գաղտնաբառը փոխելու համար ",
		"Нажмите для сброса пароля" 	=> "փոխել գաղտնաբառը",
		"Пожалуйста проверьте почту" 	=>  "Խնդրում ենք ստուգել Ձեր Էլ. հասցեն",
		"Поменять"						=> "Փոխել",
		'Смена пароля' 					=> 'Փոխել գաղտնաբառը',
		"Сброс пароля на сайте {site}!" => "Գաղտնաբառի վերագործարկում {site} կայքում",
		"Пароль успешно изменен"		=> "Գաղտնաբառը հաջողությամբ փոխվել է",
		"Укажите свой email"			=> "Մուտքագրեք Ձեր էլ. հասցեն",
		"нажмите на следующую ссылку"	=> "սեղմեք հետեւյալ հղման վրա",
		"Ваше письмо отправлено"		=> "Շնորհակալություն: Ձեր նամակը ուղարկված է",
		"Вы указали не верный пароль!"	=> "Գաղտնաբառը սխալ է",
		"Такой пользователь не существует!"	=> "Այդպիսի օգտատեր գոյություն չունի",
		"Пользователь заблокирован"		=> " Օգտատերը արգելափակված է",
		"Имя пользователя не может быть пустым"	=> "Օգտատիրոջ անունը չի կարող լինել դատարկ",
		"Пароль пользователя не может быть пустым"	=> "Օգտատիրոջ գաղտնաբառը չի կարող լինել դատարկ",
		"Логин нужен для входа на сайт"	=> "Ծածկանունը պահանջվում է կայք մուտք գործելու համար",
		"Укажите пароль"				=> "Մուտքագրեք գաղտնաբառը",
		"Пожалуйста, введите Ваше имя"	=> "Խնդրում ենք մուտքագրել Ձեր անունը",
		"Укажите эл. почту"				=> "Մուտքագրեք Ձեր էլ. փոստը",
		"Укажите телефон"				=> "Մուտքագրեք հեռախոսը",
		"Некорректный формат эл. почты"	=> "Սխալ էլ. փոստի հասցե",
		"Поиск"							=> "Փնտրել",
		"Без цены"						=> "Պայմանագրային",
		"Нет результата"				=> "Արդյունք չկա",
		"{attribute} может содержать только латинские символы, цифры, и знаки -, _"	=> "{attribute} կարող է պարունակել միայն լատինական տառեր, թվեր, եւ կետադրական նշաններ -, _",
	);
?>