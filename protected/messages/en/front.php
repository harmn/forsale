<?php
	return array(
		"Регистрация" 			=> "Registration",
		"Выход" 				=> "Exit",
		"Вход" 					=> "Login",
		"Дом" 					=> "Realty",
		"Авто" 					=> "Car",
		"Электроника" 			=> "Electronic",
		"Техника" 				=> "Technique",
		"Мебель" 				=> "Furniture",
		"Мода" 					=> "Fashion",
		"Спорт и Игры" 			=> "Sport and Games",
		"Животное и растения" 	=> "Animals and plants",
		"Работа" 				=> "Job",
		"Службы" 				=> "Services",
		"Главная" 				=> "Home",
		"О нас" 				=> "About us",
		"Контакты" 				=> "Contacts",
		"Область"				=> "Region",
		"Город"					=> "City",
		"Быстрый просмотр"		=> "Quick review",
		"Объявления"			=> "Advertisement",
		"Условия"               => "Registering, you agree to the conditions.",
		"Войти"					=> "Login",
		"Добавить"				=> "Add",
		"Категория"				=> "Category",
		"Продается"				=> "For sale",
		"Обменивается"			=> "Exchanges",
		"Аренда"				=> "Rent",
		"Тип"					=> "Type",
		"Квартира"				=> "Apartment",
		"Офис"					=> "Office",
		"Земельный участок"		=> "Plot",
		"Гараж"					=> "Garage",
		"Валюта"				=> "Value",
		"Регион"				=> "Region",
		"Город/Район"			=> "City/Region",
		"Цена"					=> "Price",
		"Количество комнат"		=> "Number of rooms",
		"Плошадь"				=> "Area",
		"Этаж"					=> "Floor",
		"Количество этажей"		=> "Number of floors",
		"Название"				=> "Title",
		"Описание"				=> "Description",
		"Автомобиль"			=> "Car",
		"Автозапчасти"			=> "Auto parts",
		"Аксессуары"			=> "Accessories",
		"Уникальная"			=> "The unique things",
		"Марка"					=> "Brand",
		"Состояние"				=> "Status",
		"Новое"					=> "New",
		"С пробегом"			=> "With mileage",
		"Битые"					=> "Beaten",
		"Пробег"				=> "Mileage",
		"Кор/п"					=> "Transmission",
		"Автомат"				=> "Automatic",
		"Механический"			=> "Mechanical",
		"Типтроник"				=> "Tiptronic",
		"Кол/с"					=> "Number of seats",
		"Кузов"					=> "Corps",
		"Руль"					=> "Steering wheel",
		"Левый"					=> "Left",
		"Правый"				=> "Right",
		"Топливо"				=> "Fuel type",
		"Бензин"				=> "Petrol",
		"Дизель"				=> "Diesel",
		"Гибрид"				=> "Hybrid",
		"Газ/Бензин"			=> "Gas/petrol",
		"белый"					=> "White",
		"желтый"				=> "Yellow",
		"зеленый"				=> "Green",
		"золотой"				=> "Gold",
		"коричневый"			=> "Brown",
		"красный"				=> "Red",
		"серебристый"			=> "Silver",
		"серый"					=> "Grey",
		"синий"					=> "Blue",
		"фиолетовый"			=> "Purple",
		"черный"				=> "Black",
		"Год выпуска"			=> "Year",
		"Сохранить"				=> "Save",
		"седан"					=> "Sedan",
		"хэтчбек"				=> "Hatchback",
		"универсал"				=> "Universal",
		"внедорожник"			=> "SUV",
		"кроссовер"				=> "Crossover",
		"пикап"					=> "Pickup",
		"купе"					=> "Coupe",
		"кабриолет"				=> "Cabriolet",
		"минивен"				=> "Minivan",
		"фургон"				=> "Van",
		"микроавтобус"			=> "Minibus",
		"Телефоны"					=> "Telephone",
		"Телефонные аксессуары"		=> "Telephone Accessories",
		"SIM карты"					=> "SIM Card",
		"Ноутбуки"					=> "Notebooks",
		"Планшеты"                  => "Tablets",
		"Компьютеры"				=> "Computers",
		"Компьютерные устройства"	=> "Devices for computers",
		"Компьютерные аксессуары"	=> "Accessories for computers",
		"Телевизоры"				=> "Televisions",
		"Другое"					=> "Other",
		"Использованные"			=> "Used",
		"Холодильники"				=> "Refrigerators",
		"Газовые плиты"				=> "Cookers",
		"Стиральные машины"			=> "Washing Machines",
		"Швейные машины"			=> "Sewing Machines",
		"Пылесосы"					=> "Vacuum cleaners",
		"Кухонная мебель"			=> "Kitchen furniture",
		"Утюги"						=> "Irons",
		"Кондиционеры и вентиляция"	=> "Air-conditioning and ventilation",
		"Отопительные приборы"		=> "Space Heaters",
		"Кухонная мебель"			=> "Kitchen furniture",
		"Мебель для спальни"		=> "Bedroom furniture",
		"Столы и стулья"			=> "Tables and chairs",
		"Шкафы"						=> "Built-in",
		"Диваны и кресла"			=> "Sofas and armchairs",
		"Компьютерная мебель"		=> "Computer furniture",
		"Подставки"					=> "Stands",
		"Тумбочки"					=> "Cupboards",
		"Цвет"						=> "Colour",
		"Женская одежда"			=> "Women's clothing",
		"Женская обувь"				=> "Women's shoes",
		"Мужская одежда"			=> "Men's clothing",
		"Мужская обувь"				=> "Men's shoes",
		"Детская одежда"			=> "Baby's clothes",
		"Детская обувь"				=> "Baby's shoes",
		"Косметика и бижутерия"		=> "Cosmetics and jewelry",
		"Духи"						=> "Perfume",
		"Часы"						=> "Watches",
		"Сумки"						=> "Bags",
		"Товары для детей"			=> "Goods for kids",
		"Красота и здоровье"		=> "Health and beauty",
		"Тренажёры"						=> "Training apparatuses",
		"Детские спортивные комплексы"	=> "Children's sports complexes",
		"Лыжи/Сноуборды и защита"		=> "Ski / snowboard and security",
		"Обувь"							=> "Shoes",
		"Одежда"						=> "Clothes",
		"Спортивное питание"			=> "Sports nutrition",
		"Хоккей"						=> "Hockey",
		"Велосипеды"					=> "Bicycles",
		"Гимнастика"					=> "Gymnastics",
		"Единоборства"					=> "Martial Arts",
		"Мячи"							=> "Balls",
		"Туризм"						=> "Tourism",
		"Теннис"						=> "Tennis",
		"Тяжелая атлетика"				=> "Weight lifting",
		"Рюкзаки/Сумки"					=> "Backpacks/Bags",
		"Коньки/Ролики"					=> "Skates/Clips",
		"Санки"							=> "Sledge",
		"Спортивные приборы"			=> "Sports equipment",
		"Игри"							=> "Games",
		"Возраст"						=> "Age",
		"Собаки"						=> "Dogs",
		"Кошки"							=> "Cats",
		"Рыбы"							=> "Fishes",
		"Птицы"							=> "Birds",
		"Сельскохозяйственные животные"	=> "Farm animals",
		"Все для животных"				=> "All for animals",
		"Ростения"						=> "Plantes",
		"Возраст от"					=> "Age from",
		"Возраст до"					=> "Age to",
		"Пол"							=> "Sex",
		"Опыт работы"					=> "Experience",
		"Офисная работа"				=> "Office Work",
		"Торговля"						=> "Trading",
		"Финансы и право"				=> "Finance and Law",
		"Информационные технологии"		=> "Information technology",
		"Mедиа и дизайн"				=> "Media and Design",
		"Рестораны и кухня"				=> "Restaurants and kitchen",
		"Туризм и отели"				=> "Tourism and kitchen",
		"Танспорт и такси"				=> "Transportn and taxi",
		"Бизнес и маркетинг"			=> "Business and marketing",
		"Строительство и архитектура"	=> "Construction and architecture",
		"Домашнее хозяйство"			=> "Housekeeping",
		"Производство"					=> "Production",
		"Образования"					=> "Education",
		"Здравоохранения"				=> "Health",
		"Мужской"						=> "Male",
		"Женский"						=> "Female",
		"График"						=> "Graph",
		"Oбразование"					=> "Education",
		"Полный"						=> "Complete",
		"Неполный"						=> "Incomplete",
		"Профессиональное"				=> "Professional",
		"Начальное"						=> "Preliminary",
		"Среднее"						=> "Secondary education",
		"Бакалавр"						=> "Bachelor",
		"Магистр"						=> "Master",
		"Аспирант"						=> "Graduate",
		"Кандидат"						=> "Candidate of sciences",
		"Языки"							=> "Languages",
		"Зарплата"						=> "Salary",
		"Строительство и ремонт"		=> "Construction and repair",
		"Транспорт"						=> "Transport",
		"Электроника"					=> "Electronics",
		"Компьютеры и Интернет"			=> "Computers and Internet",
		"Образование"					=> "Education",
		"Торговые услуги"				=> "Business services",
		"Бытовые услуги"				=> "Household services",
		"Красота и здоровье"			=> "Beauty and health",
		"События и праздники"			=> "Events and celebrations",
		"Туризм и путешествия"			=> "Tourism and Travel",
		"СмсРегистрацияУспешно"			=> "<div>SMS registration succeeded</div><div>Please check the email to activate your account.</div><div>Thank you for registering.</div>",
		"СмсРегистрацияНеУспешно"		=> "<div>SMS registration failed</div><div>For some reason, the registration failed. Try again later.</div>",
		"Регистрация на сайте"			=> "Registration on the site",
		"Ваш акаунт на сайте" 			=> "Your Account on the site",
		"Если вы на самом деле регистрировались на нашем сайте, то для активации Вашего акаунта Вам нужно перейти по " => "If you really signed on our site, to activate your yccount you need to go to ",
		"В обратном случае просто проигнорируйте данное письмо." => "Otherwise, just ignore this letter",
		"ссылке" 						=> "link",
		"С уважением" 					=> "With respect <br/><strong>ForSale.am</strong>",
		"Перейти на главную"			=> "Home page",
		"Перейти на страницу входа"		=> "Entry page",
		"Активация прошла успешно! Теперь вы можете войти на сайт используя свой логин и пароль!" => "Activation is succeeded: Now you can log in using your username and password!",
		"Теперь Вы можете"				=> "Now you can",
		"войти на сайт"					=> "Log in",
		", используя свои регистрационные данные."	=> ", using your registration information",
		"Последние"						=> "Recent Statements",
		"Скоро"							=> "Soon",
		"Модель"						=> "Model",
		"Пробег от"						=> "Mileage from",
		"Пробег до"						=> "Mileage to",
		"Год выпуска с"					=> "Year to",
		"Год выпуска по"				=> "By year",
		"Твой бесплатный онлайн брокер"	=> "Your free online broker",
		"Новое объявление"				=> "New advert",
		"Подписка"						=> "Subscribe",
		"На новости"					=> "our news",
		"Для чего создан сайт ForSale.am ?"	=> "The Reason of creation the site ForSale.am",
		"Как подать объявление ?"		=> "How to add Ad",
		"Наши преимущества"				=> "Our advantages",
		"Дополнительные услуги"			=> "Additional services",
		"ForSale.am это сайт для каждого, кому необходимо продать или купить недвижимость, транспортное средство или что-то другое, и хочет быстро и просто получить всю необходимую информацию." => "ForSale.am is a site for everyone who need to sell or buy a realty, vehicle or something else, and want to quickly and easily obtain all the necessary information",
		"Вы хотите подать объявление о продаже, обмене или аренде  и не знаете с чего начать? Начните с регистрации на ForSale.am! Мы постарались сделать так, чтобы процесс создания объявления был для Вас максимально быстр, прост и понятен." => "Do you want to place an ad for the sale, exchange or rent and do not know where to start? Start by registering ForSale.am! We have tried to make the process of creating the ad was for you as quickly as possible, simple and straightforward",
		"Мы поддерживаем базу в актуальном состоянии. Мы сделали удобный сайт. Совершенствуются и успешно внедряются полезные сервисы для продавцов, арендодателей и для покупателей." => "We support base up to date. We did comfortable site. Improved and successfully implemented useful services for sellers, landlords and customers.",
		"Вы можете поставить рекламу на нашем сайте."	=> "You can put advertising on our site",
		"Сортировать по:"				=> "Sort by",
		"Цене"							=> "Price",
		"Названию"						=> "Name",
		"Контакты"						=> "Contacts",
		"Введите электронный адрес"		=> "Enter the email address",
		"Забыли пароль?"				=> "Forgot your password",
		'Имя пользователя' 				=> "Username",
		'Пароль' 						=> "Password",
		'Повторите пароль' 				=> "Repeat password",
		'Старый пароль' 				=> "Pld password",
		'Новый пароль' 					=> "New password",
		'Имя' 							=> "First name",
		'Фамилия'  						=> "Last name",
		'Эл. почта' 					=> "Email",
		'Логин' 						=> "Login",
		'Забыли пароль?' 				=> "Forgot your password",
		'Воити' 						=> "Login",
		'Зарегистрировавшись, Вы соглашаетесь' => "By registering, you agree to",
		'с условиями' 					=> "with the terms",
		'Профиль' 						=> "Profile",
		'Календарь' 					=> "Calendar",
		'Сменить пароль' 				=> "Change password",
		'Удалить акаунт' 				=> "Delete account",
		'Вы можете загрузить изображение в формате JPG, GIF или PNG.' => "You can download the image in JPG, GIF, or PNG",
		'Загрузить фотографию' 			=> "Upload a photo",
		'Выбрать картинку' 				=> "Select an image ",
		'Загружаемый файл не является картинкой' => "The uploaded file is not an image",
		"Смена пароля" 					=> "Change password",
		"Введите свой адрес эл. почты." => "Please enter your email address",
		'Укажите новый пароль'			=> "Enter a new password",
		"Активация прошла успешно! Теперь вы можете войти на сайт используя свой логин и пароль!" => "Activation is successful! Now you can log in using your username and password!",
		"Ваш пароль был изменен" 		=> "Your password has been changed",
		"Изменить" 						=> "Change",
		"Подождите"						=> "Wait",
		"Перетащите картинки сюда"		=> "Drag the picture here",
		"Загрузить файл"				=> "Upload file",
		"Отмена"						=> "Cancel",
		"Повторить"						=> "Repeat",
		"Удалить"						=> "Delete",
		"Ошибка загрузки"				=> "Loading error",
		"Следите за нами:"				=> "Follow us",
		"Отправить"						=> "Send",
		"Тема"							=> "Topic",
		"Мой профиль"					=> "My profile",
		"Мои объявления"				=> "My ads",
		"Посмотреть позже"				=> "Watch later",
		"Избранные"						=> "Favorites",
		"Отчество"						=> "Patronymic name",
		"Здравствуй, посетитель Forsale.am !"	=> "Hello dear visitor",
		"О нас - текст"					=> "Forsale.am is a self-service advertising platform that gives our customers a way to reach thousands of potential clients and buyers, who visit us every day. Our site offers you a variety of interesting and useful features, which may help you to buy, sell or rent any product or service! Cars and apartments are most popular titles; however, there is no limit for your imagination! You may sell, buy or rent a pen or even an elephant! Simple navigation and titles will help you quickly adapt to our website and become an active user. Forsale.am will save you lots of time and make your life as simple and comfortable, as our service is!",
		"Желаем  приятных покупок, выгодных продаж."	=> "We wish you a pleasant shopping, profitable sales",
		"Нет результатов"				=> "Results not found",
		"Сброс пароля" 					=> "Reset password",
		"Для сброса пароля на сайте " 	=> "To reset the password on the site",
		"Нажмите для сброса пароля" 	=> "Click to reset the password",
		"Пожалуйста проверьте почту" 	=> "Please check your mails",	
		"Поменять"						=> "Change",
		"Смена пароля" 					=> "Change password",
		"Сброс пароля на сайте {site}!" => "Password Reset on the site {site} ",
		"Пароль успешно изменен"		=> "Password successfully changed",
		"Укажите свой email"			=> "Enter your email",
		"нажмите на следующую ссылку"	=> "click on the following link",
		"Ваше письмо отправлено"		=> "Your message sent",
		"Вы указали не верный пароль!"	=> "You entered an invalid password",
		"Такой пользователь не существует!"	=> "Such a user does not exist",
		"Пользователь заблокирован"		=> "User is blocked",
		"Имя пользователя не может быть пустым"	=> "Username can not be empty",
		"Пароль пользователя не может быть пустым"	=> "User password can not be empty",
		"Логин нужен для входа на сайт"	=> "Login is required to access the site",
		"Укажите пароль"				=> "Enter the password",
		"Пожалуйста, введите Ваше имя"	=> "Please enter your name",
		"Укажите эл. почту"				=> "Enter e-mail",
		"Укажите телефон"				=> "Enter the phone.",
		"Некорректный формат эл. почты"	=> "Incorrect e-mail format",
		"Поиск"							=> "Find",
		"Без цены"						=> "Without price",
		"Нет результата"				=> "No result found",
		"{attribute} может содержать только латинские символы, цифры, и знаки -, _"	=> "{attribute} may contain only Latin characters, numbers, and symbols -, _",
	);
?>