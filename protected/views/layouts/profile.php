<?$this->beginContent('//layouts/main')?>
	<div class="mb20" id='wrap'>
		<div class="profile wrapper">
			<? 
			$this->beginWidget('UITabs', [
			'tabs' =>  [
				            'profile'     => [
				            	'label' => t('front', 'Мой профиль'), 
				            	'url' => ['/account/profile'],
				            	'visible' => !user()->isGuest
				            ],
				            'myAdverts'       => [
				            	'label' => t('front', 'Мои объявления'), 
				            	'url' => ['/account/myAdverts'],
				            ],
				            'later'       => [
				            	'label' => t('front', 'Посмотреть позже'), 
				            	'url' => ['/account/later'],
				            ],
			 	            'favorite'    => [
				            	'label' => t('front', 'Избранные'), 
				            	'url' => ['/account/favorites'],
				            ]
				        ],
			'ajax' => false]); ?>

			<div class="tab-content bg-lighten-gray clearfix p20">
				<?=$content?>
			</div>
			<?$this->endWidget();?>
		</div>
	</div>
<?$this->endContent()?>