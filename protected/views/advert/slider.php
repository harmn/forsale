<?php 
	if(count($alternativeAdverts) > 4){
		$result = array_chunk($alternativeAdverts, 4);
	}else{
		$result[] = $alternativeAdverts;
	}

	$controller = ($this instanceof BaseController) ? $this : $this->controller;
	$counter = 0;
	if($result){
?>
<div class="span12">
	<div class="carousel slide rel" id="myItemCarousel">
		<div class="carousel-inner">
			<?php foreach($result as $item){
				$counter ++;
				?>
				<div class="item <?=($counter == 1) ? "active" : "";?>">
					<ul class="thumbnails">
						<?php 
						if($item)
						foreach($item as $i){ 
							$photos = $i->photos;
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";

							if($photos){ 
								$src = $imageUrl.$i->relatedphotos[0]->id_model."/"."customBig"."/".$photos[0]->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 

							$favorites = [];
							if($i->favorites && $i->favorites[0]){
								$favorites[] = $i->favorites[0]->attributes['type'];
								if(isset($i->favorites[1])){
									$favorites[] = $i->favorites[1]->attributes['type'];
								}
							}

							$favoritChecker = ($favorites && in_array('1', $favorites)) ? " activeFavorit" : "";
							$laterChecker = ($favorites && in_array('2', $favorites)) ? " activeFavorit" : "";
						?>
							<li class="span3" data-model="<?=get_class($model);?>" data-id="<?=$i->id;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $i->id, 'type' => get_class($model)])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=get_class($model);?>" data-id="<?=$i->id;?>" href="/Site/getItem">
										<a href="#" class="quick-view"><i class="fa fa-search-plus mr5"></i><?=t('front', 'Быстрый просмотр');?></a>
										<?=CHtml::link('<i class="fa fa-heart mr5"></i>', ['/account/favorites'], ["class" => "c-main-gray fsize18 transition favorites ".$favoritChecker])?>
										<?=CHtml::link('<i class="fa fa-clock-o"></i>', ['/account/later'], ["class" => "c-main-gray fsize18 transition later ".$laterChecker])?>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($i->title) > 20) ? mb_substr($i->title,0,20) . "..." : $i->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($i->salary)) ? ($i->price > 0) ? $i->price : t('front', 'Без цены') : $i->salary?> 
										<?php 
											if($i->valuta && (isset($i->salary) AND $i->salary > 0 OR isset($i->price) AND $i->price > 0)) echo Home::$valuta_select[$i->valuta];
										?>
									</div>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		</div>

		<div class="control-box <?=($counter <= 1) ? "hidden" : "";?>">
			<a data-slide="prev" href="#myItemCarousel" class="carousel-control left">‹</a>
			<a data-slide="next" href="#myItemCarousel" class="carousel-control right">›</a>
		</div>
	</div>
</div>
<?php } ?>
