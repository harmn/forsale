<?//модальное окно "Буструй просмотр"
// $this->renderPartial('_itemModal', ['model' => $this->loadModel('Product')]);

//Для предварительной загрузки скриптов
// ProductCompareWidget::loadModalAndScripts(['loadModal' => true]);

//АЛЬТЕРНАТИВНЫЙ ВАРИАНТ
$alternativeAdverts = AdvertController::getAlternativeAdverts($model->id, $type, 8);

$infoBlocks = Listing::getAdvertFieldsByType($type);

$this->renderPartial('/site/_itemModal');

?>


<div class="page-info mt30 mb20">
	<div class="wrapper">
		<? $this->widget('SBreadcrumbs', ['homeLink' => CHtml::link(t('front', 'Главная'), ['/site/index']), 'links' => $this->breadcrumbs]) ?>
	</div>
</div>
<div id="advertPageGalleria" class="wrapper">
	<?$this->renderPartial('//site/_itemSection', compact('model'))?>
</div>

<br class="clear "></br>

<div class="wrapper item-description clearfix mb50 mt30">
	<?$this->beginWidget('UITabs',['tabs' => $this->tabs, 'paramName' => 'tab']);?>
		<div id="myTabContent" class="tab-content pl0 pr0">
			<div class="tab-pane active" id="description">
				<ul>
					<?php foreach($infoBlocks as $k => $v){ 
						?>
							<li>
								<span><?=$k;?>:</span>
								<span>
									<?php 
										if($v["is_select"] == "1"){
											echo (!is_array($model->$v["name"]) && $model->$v["name"] && $v["select"][$model->$v["name"]]) ? $v["select"][$model->$v["name"]] : "";
										}else{
											echo ($model->$v["name"]) ? $model->$v["name"] : "";	
										}
									?>
								</span>
							</li>
					<?php	}	?>
				</ul>
			</div>
			<div class="tab-pane hidden" id="reviews">
				<div class="p20 clearfix">
					<?
					// comments
					$comments = SiteComment::model()->provider(SiteComment::TYPE_ADVERT, $model->id);

					$this->widget('comment.widgets.SiteComments',
						['dataProvider'=>$comments, 'type' => SiteComment::TYPE_ADVERT, 'objectId' => $model->id]);
					?>
				</div>
			</div>
		</div>
	<?$this->endWidget()?>
</div>

<?=CHtml::hiddenField('' , $model->lng, ['class' => 'lngAddress']);?>
<?=CHtml::hiddenField('' , $model->ltd, ['class' => 'latAddress']);?>
<div id="map_update" class="wrapper"></div>

<div class="wrapper clearfix mt50 mb50">
	<?$this->renderPartial('slider', compact('model', 'alternativeAdverts'))?>
</div>


