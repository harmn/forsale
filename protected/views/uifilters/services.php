<? 
/**
 * блок фильтров в списке Службы
 */
?>
<div class="clearfix " style="height: 160px;">
	<div class="fl mr10 posrel mt10" style="width: 200px">
		<!-- Тип-->		
		<?=$model->dropDownList('type_filter', Services::$type_select, 
			['id' => 'services_type_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
		<!-- Цена-->		
		<?=$model->textField('price_filter',['id' => 'services_price_filter', 'class' => "w100p posrel  mb5 t0", 'data-filter' => true, "placeholder" => "Цена"])?>
		<!-- Опыт работы -->		
		<?=$model->textField('experience',['id' => 'services_experience', 'class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Опыт работы"])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'services_region_filter',
		 		'class' => 'w100p small',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'id'		  => 'services_city_filter',
				'class' 	  => "w100p small mb5", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="fl posrel mt10" style="width: 200px" >
		<div class="posabs w100p mb5">
			<!-- Цена от-->		
			<?=$model->textField('age_filter_from',['id' => 'services_age_filter_from', 'class' => " fl w50p mb5", 'data-filter' => true, "placeholder" => "Возраст от"])?>
			<!-- Цена до-->		
			<?=$model->textField('age_filter_to',['id' => 'services_age_filter_to', 'class' => "posabs fl w50p ml100", 'data-filter' => true, "placeholder" => "Возраст до"])?>
		</div>
		<div style="margin-top: 31px;">
			<!--Пол-->		
			<?=$model->dropDownList('sex_filter', Services::$sex_select, 
				['id' => 'services_sex_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Пол'])?>
			<!-- Имя пользователья -->		
			<?=$model->textField('fullname',['id' => 'services_fullname', 'class' => "w100p mt0", 'data-filter' => true, "placeholder" => "Имя пользователья"])?>
		</div>
	</div>
</div>