<? 
/**
 * блок фильтров в списке мебель
 */
?>

<div class="clearfix " style="height: 170px;">
	<div class="fl mr10 posrel mt25" style="width: 200px">
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Home::$category_select, 
			['id' => 'category_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Категория'])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Furniture::$type_select, 
			['id' => 'type_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
		<div class="posrel w100p mb5 t0">
			<!-- Цена от-->		
			<?=$model->textField('price_filter_from',['class' => " fl w50p mb5", 'data-filter' => true, "placeholder" => "Цена от"])?>
			<!-- Цена до-->		
			<?=$model->textField('price_filter_to',['class' => "posabs fl t0 w50p mb5 ml100", 'data-filter' => true, "placeholder" => "Цена до"])?>
		</div>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'class' => 'w100p small',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'class' 	  => "w100p small mb5", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="fl mr10 w200 posrel" >
		<label data-reset="true">Год выпуска с</label>
		<?=$model->datePicker('release_year_from', ['pluginOptions'=>[
            'format'=>'yyyy',
            'data-width'    => '100%',
            'viewMode' => "years", 
            'minViewMode' => "years"
        ]],
        false,
        ['class' => 'block mb5 mt5'])?>
		<div class="checkbox posabs" style="width: 209px;">
			<!-- Цвет -->
			<?=$model->dropDownList('color_filter', Furniture::$color_select, 
				['id' => 'color_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Цвет'])?>
			<!-- Состояние -->
			<?=$model->dropDownList('condition_filter', Furniture::$condition_select, 
			['id' => 'condition_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Состояние'])?>
			<!-- Имя пользователья -->		
			<?=$model->textField('fullname',['class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Имя пользователья"])?>
		</div>
	</div>

	<div class="fl  w200">
		<label data-reset="true">Год выпуска по</label>
		<?=$model->datePicker('release_year_to', ['pluginOptions'=>[
            'format'=>'yyyy',
            'data-width'    => '100%',
            'viewMode' => "years", 
            'minViewMode' => "years"
        ]],
        false,
        ['class' => 'block mb5 mt5'])?>
	</div>
	
</div>