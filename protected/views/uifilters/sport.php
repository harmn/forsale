<? 
/**
 * блок фильтров в списке Спорт
 */
?>

<div class="clearfix " style="height: 160px;">
	<div class="fl mr10 posrel mt10" style="width: 200px">
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Home::$category_select, 
			['id' => 'sport_category_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Категория'])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Sport::$type_select, 
			['id' => 'sport_type_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
		<div class="posrel w100p mb5 t0">
			<!-- Цена от-->		
			<?=$model->textField('price_filter_from',['id' => 'sport_price_filter_from', 'class' => " fl w50p mb5", 'data-filter' => true, "placeholder" => "Цена от"])?>
			<!-- Цена до-->		
			<?=$model->textField('price_filter_to',['id' => 'sport_price_filter_to', 'class' => "posabs fl t0 w50p ml100", 'data-filter' => true, "placeholder" => "Цена до"])?>
		</div>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'sport_region_filter', 
		 		'class' => 'w100p small',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'id' 		  => 'sport_city_filter', 
				'class' 	  => "w100p small mb5", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="fl posrel mt10" style="width: 200px" >
		<!-- Цвет -->
		<?=$model->dropDownList('color_filter', Sport::$color_select, 
			['id' => 'sport_color_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Цвет'])?>
		<!-- Состояние -->
		<?=$model->dropDownList('condition_filter', Sport::$condition_select, 
		['id' => 'sport_condition_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Состояние'])?>
		<!-- Имя пользователья -->		
		<?=$model->textField('fullname',['id' => 'sport_fullname', 'class' => "w100p ", 'data-filter' => true, "placeholder" => "Имя пользователья"])?>
	</div>
</div>