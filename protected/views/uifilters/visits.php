<? 
/**
 * блок фильтров в списке карточек
 */
?>

<div class="clearfix">
	<div class="fl mr10 mt25 " style="width: 200px">
		<!-- Имя -->		
		<?=$model->textField('fullname',['class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Имя"])?>	
		<!-- Номер -->		
		<?=$model->textField('number',['class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Номер Карточки"])?>
		<!-- Тариф -->		
		<?=$model->dropDownList('tarif', Card::$tarifs,
	 		['id' => 'tarifSelect','class' => 'w100p small', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тариф'])?>
	</div>

	<div class="fl mr10 w200 posrel" >
		<label data-reset="true">Начало сеанса</label>
		<?//=$model->textField('begin_date_from',['id' => 'begin_date_from','class' => 'block mb5 mt5 w100'])?>	
		<?=$model->datePicker('begin_date_from', isset($params['begin_date_from']) ? ['default' => $params['begin_date_from']] : [], false, ['class' => 'block mb5 mt5']);?>
		<?//=$model->datePicker('begin_date_to', isset($params['begin_date_to']) ? ['default' => $params['begin_date_to']] : [], false, ['class' => 'block']);?>
		<div class="checkbox posabs mt0" style="width: 209px;">
			<!-- "В зале" -->
			<?=$model->dropDownList('inHall', Visits::$inHallSelect, 
				['id' => 'inHallSelect', 'class' => 'w100p small', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Присутствие'])?>
		</div>
		<div class="checkbox posabs" style="width: 209px; margin-top: 31px">
			<!-- "В зале" -->
			<?=$model->dropDownList('hasPause', Visits::$hasPauseList, 
				['id' => 'hasPause', 'class' => 'w100p small', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Паузa'])?>
		</div>
	</div>

	<div class="fl  w200">
		<label data-reset="true">Конец сеанса</label>
		<?//=$model->datePicker('end_date_from', [], false, ['class' => 'block mb5 mt5']);?>
		<?=$model->datePicker('end_date_to', isset($params['end_date_to']) ? ['default' => $params['end_date_to']] : [], false, ['class' => 'block mb5 mt5']);?>
		<?//=$model->textField('end_date_to',['id' => 'end_date_to','class' => 'block mb5 mt5 w100'])?>
		<?php 
			// $this->widget('ext.YiiDateTimePicker.jqueryDateTime', array(
		 //        'model' => $model,
		 //        'attribute' => ['end_date_to'],
		 //        'options' => array('format' => 'd-m-Y H:i:s'), //DateTimePicker options
		 //        'htmlOptions' => ['class' => 'block mb5 mt5 w100', 'name' => 'begin_date_from'],
		 //    ));
		?>
		<?php 
			// $this->widget('ext.YiiDateTimePicker.jqueryDateTime', array(
		 //        'model' => $model,
		 //        'attribute' => [],
		 //        'options' => array('format' => 'd-m-Y H:i:s'), //DateTimePicker options
		 //        'htmlOptions' => ['class' => 'block mb5 mt5 w100', 'name' => 'end_date_to'],
		 //    ));
		?>
	</div>
	
</div>