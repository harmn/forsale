<?  
/**
 * блок фильтров в списке Автомобилей
 */
?>

<div class="clearfix row w800">
	<div class="col-md-4">
		<?=CHtml::hiddenField('Car[model]', 'true')?>
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Listing::getHomeCategory(), 
			['id' => 'car_category_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Категория")])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Listing::getCarType(), 
			['id' => 'car_type_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Тип")])?>
		<!-- Марка -->
		<?=$model->dropDownList('brand_filter', Car::$brand_select, 
			['id' => 'car_brand_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Марка")])?>
		<!-- Модель -->
		<?=$model->dropDownList('model_filter', [], 
			['id' => 'car_model_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Модель")])?>
		
		<div class="posrel w100p mb15 " >
			<!-- Пробег от -->
			<?=$model->dropDownList('mileage_from_filter', Car::$mileage_select, 
				['id' => 'car_mileage_from_filter', 'class' => 'fl w50p mb5 small', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Пробег от")])?>
			<div class="posabs fl t0 w50p mb5 ml100">
				<!-- Пробег до -->
				<?=$model->dropDownList('mileage_to_filter', Car::$mileage_select, 
					['id' => 'car_mileage_to_filter', 'class' => '  small', 'style' => 'top: 31px;', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Пробег до")])?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<!-- Кузов -->
		<?=$model->dropDownList('body_filter', Listing::getBodySelect(), 
			['id' => 'car_body_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Кузов")])?>
		<!-- Руль -->
		<?=$model->dropDownList('rudder_filter', Listing::getRudderSelect(), 
			['id' => 'car_rudder_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Руль")])?>
		<!-- Цвет -->
		<?=$model->dropDownList('color_filter', Listing::getColorSelect(), 
			['id' => 'car_color_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Цвет")])?>
		<!-- Коробка передач -->
		<?=$model->dropDownList('transmission_filter', Listing::getTransmissionSelect(), 
			['id' => 'car_transmission_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Кор/п")])?>
		<!-- Количество сидений -->
		<?=$model->dropDownList('seats_count_filter', Car::$seats_count_select, 
			['id' => 'car_seats_count_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Кол/с")])?>
		
	</div>
	<div class="col-md-4" >
		<div class="posrel mb15 w300">
			<div class="fl mr15">
				<label data-reset="true"><?=t("front", "Год выпуска с");?></label>
				<?=$model->datePicker('release_year_from', ['pluginOptions'=>[
		            'format'=>'yyyy',
		            'data-width'    => '100%',
		            'id' => 'car_release_year_from',
		            'viewMode' => "years", 
		            'minViewMode' => "years"
		        ]],
		        false,
		        ['class' => 'block mb5 mt5'])?>
	        </div>
	        <div>
				<label data-reset="true"><?=t("front", "Год выпуска по");?></label>
				<?=$model->datePicker('release_year_to', ['pluginOptions'=>[
		            'format'=>'yyyy',
		            'data-width'    => '100%',
		            'id' => 'car_release_year_to',
		            'viewMode' => "years", 
		            'minViewMode' => "years"
		        ]],
		        false,
		        ['class' => 'block mb5 mt5'])?>
			</div>
		</div>
		<!-- Состояние -->
		<?=$model->dropDownList('condition_filter', Listing::getConditionSelect(), 
			['id' => 'car_condition_filter', 'class' => 'w100p small mb15 ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Состояние")])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'car_region_filter', 
		 		'class' => 'w100p small mb15 ',  
		 		'empty'=>t("front", "Регион"),
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => t("front", "Город/Район"),
				'id' 		  => 'car_city_filter', 
				'class' 	  => "w100p small mb15 ", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
		
        <div class="mb15 mt8 ml10">
        	<label class="mr20 block mb3 c-gray"><?=t("front", "Цена");?></label>	
			<?php 
				$step = 5000;
				$priceFrom = 0; 
				$priceTo = 100000;
				if($model->owner->price)
					list($priceFrom, $priceTo) = explode(',', $model->owner->price);
				
				echo $model->textField('price', [
					'class' => 'slider-element form-control ',
					'style'	=> 'width: 225px',
					'data-slider-value' => '['.(float) $priceFrom.', '.(float) $priceTo.']',
					'data-slider-step' => $step,
					'data-slider-min' => 0,
					'data-slider-max' => '100000',
					'data-slider-selection' => "after",
				]);
			?>
		</div>
	</div>
</div>