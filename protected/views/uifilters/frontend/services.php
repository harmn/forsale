<? 
/**
 * блок фильтров в списке Службы
 */
?>
<div class="clearfix row w800">
	<div class="col-md-5">
		<?=CHtml::hiddenField('Services[model]', 'true')?>
		<!-- Тип-->		
		<?=$model->dropDownList('type_filter', Listing::getServicesType(), 
			['id' => 'services_type_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Тип")])?>
		<!-- Опыт работы -->		
		<?=$model->textField('experience',['id' => 'services_experience', 'class' => "w100p mb15", 'data-filter' => true, "placeholder" => t("front", "Опыт работы")])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'services_region_filter',
		 		'class' => 'w100p small mb15',  
		 		'empty'=>t("front", "Регион"),
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => t("front", "Город/Район"),
				'id'		  => 'services_city_filter',
				'class' 	  => "w100p small mb15", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="col-md-5">
		<div class="posrel mb15">
			<!-- Цена от-->		
			<?=$model->textField('age_filter_from',['id' => 'services_age_filter_from', 'class' => " w47p", 'data-filter' => true, "placeholder" => t("front", "Возраст от"), "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'');"])?>
			<div style="  position: absolute;  top: 0px;  margin-left: 146px;  width: 100%;">
				<!-- Цена до-->		
				<?=$model->textField('age_filter_to',['id' => 'services_age_filter_to', 'class' => "w47p", 'data-filter' => true, "placeholder" => t("front", "Возраст до"), "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'');"])?>
			</div>
		</div>
		<!--Пол-->		
		<?=$model->dropDownList('sex_filter', Listing::getWorkSexSelect(), 
			['id' => 'services_sex_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Пол")])?>
		<div class="fl ml10 mb15 mt8">	
			<label class="mr20 block mb3 c-gray"><?=t("front", "Цена");?></label>
			<?php 
				$step = 5000;
				$priceFrom = 0; 
				$priceTo = 100000;
				if($model->owner->price)
					list($priceFrom, $priceTo) = explode(',', $model->owner->price);
				
				echo $model->textField('price', [
					'class' => 'slider-element form-control ',
					'style'	=> 'width: 290px',
					'data-slider-value' => '['.(float) $priceFrom.', '.(float) $priceTo.']',
					'data-slider-step' => $step,
					'data-slider-min' => 0,
					'data-slider-max' => '100000',
					'data-slider-selection' => "after",
				]);
			?>
		</div>
	</div>
	<div class="col-md-2">
	</div>
</div>