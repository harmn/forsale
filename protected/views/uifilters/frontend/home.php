<? 
/**
 * блок фильтров в списке домов
 */
?>

<div class="clearfix row w800">
	<div class="col-md-3">
		<?=CHtml::hiddenField('Home[model]', 'true')?>
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Listing::getHomeCategory(), 
			['id' => 'home_category_filter', 'class' => 'w180 small mb10', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Категория")])?>
		<!-- Тип строения -->
		<?=$model->dropDownList('type_filter', Listing::getHomeType(), 
			['id' => 'home_type_filter', 'class' => 'w180 small ', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Тип")])?>
	</div>

	<div class="col-md-3">
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'home_region_filter',
		 		'class' => 'w180 small mb10',  
		 		'empty'=>	t("front", "Регион"),
				'data-depend'=>'job',
		 	])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => t("front", "Город/Район"),
				'id' 		  => 'home_city_filter',
				'class' 	  => "w180 small", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>

	<div class="col-md-3">
		<div class="w180 mb10">
			<!-- Этаж -->		
			<?=$model->textField('floor_filter',['id' => 'home_floor_filter', 'class' => "w200p ", 'data-filter' => true, "placeholder" => t("front", "Этаж"), "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'');"])?>	
		</div>

		<div class="w180">	
			<!-- Количество комнат -->		
			<?=$model->textField('room_count_filter',['id' => 'home_room_count_filter', 'class' => "w200p ", 'data-filter' => true, "placeholder" => t("front", "Количество комнат"), "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'');"])?>	
		</div>	
	</div>

	<div class="col-md-3">
		<!-- Цена -->	
		<div class="mt53 ml10">	
			<label class="mr20 block mb3 c-gray"><?=t("front", "Цена");?></label>	
			<?php 
				$step = 5000;
				$priceFrom = 0; 
				$priceTo = 100000;
				if($model->owner->price)
					list($priceFrom, $priceTo) = explode(',', $model->owner->price);
				
				echo $model->textField('price', [
					'class' => 'slider-element form-control w250',
					'data-slider-value' => '['.(float) $priceFrom.', '.(float) $priceTo.']',
					'data-slider-step' => $step,
					'data-slider-min' => 0,
					'data-slider-max' => '100000',
					'data-slider-selection' => "after",
				]);
			?>
		</div>
	</div>
</div>