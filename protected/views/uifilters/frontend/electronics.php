<? 
/**
 * блок фильтров в списке Электроники
 */
?>

<div class="clearfix row w800">
	<div class="col-md-4">
		<?=CHtml::hiddenField('Electronics[model]', 'true')?>
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Listing::getHomeCategory(), 
			['id' => 'electronics_category_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Категория")])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Listing::getElectronicsType(), 
			['id' => 'electronics_type_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Тип")])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'electronics_region_filter',
		 		'class' => 'w100p small mb15',  
		 		'empty'=>t("front", "Регион"),
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->
		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => t("front", "Город/Район"),
				'id' 		  => 'electronics_city_filter',
				'class' 	  => "w100p small mb15", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="col-md-5">
		<div class="posrel mb15 ">
			<div class="fl mr50 ml10">
				<label data-reset="true"><?=t("front", "Год выпуска с");?></label>
				<?=$model->datePicker('release_year_from', ['pluginOptions'=>[
		            'format'=>'yyyy',
		            'data-width'    => '100%',
		            'id' => 'electronics_release_year_from',
		            'viewMode' => "years", 
		            'minViewMode' => "years"
		        ]],
		        false,
		        ['class' => 'block mb5 mt5'])?>
	        </div>
		
			<div>
				<label data-reset="true"><?=t("front", "Год выпуска по");?></label>
				<?=$model->datePicker('release_year_to', ['pluginOptions'=>[
		            'format'=>'yyyy',
		            'data-width'    => '100%',
		            'id' => 'electronics_release_year_to',
		            'viewMode' => "years", 
		            'minViewMode' => "years"
		        ]],
		        false,
		        ['class' => 'block mb5 mt5'])?>
	        </div>
        </div>
		<!-- Марка -->
		<?=$model->dropDownList('brand_filter', [], 
			['id' => 'electronics_brand_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Марка")])?>
		
		<!-- Состояние -->
		<?=$model->dropDownList('condition_filter', Listing::getElectronicsConditionSelect(), 
		['id' => 'electronics_condition_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Состояние")])?>
		
		<!-- Цвет -->
		<?=$model->dropDownList('color_filter', Listing::getColorSelect(), 
			['id' => 'electronics_color_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Цвет")])?>
		
	</div>

	<div class="col-md-3">
		<div class="fl ml20 " style="margin-top: 160px;">	
			<label class="mr20 block mb3 c-gray"><?=t("front", "Цена");?></label>
			<?php 
				$step = 5000;
				$priceFrom = 0; 
				$priceTo = 100000;
				if($model->owner->price)
					list($priceFrom, $priceTo) = explode(',', $model->owner->price);
				
				echo $model->textField('price', [
					'class' => 'slider-element form-control w240',
					'data-slider-value' => '['.(float) $priceFrom.', '.(float) $priceTo.']',
					'data-slider-step' => $step,
					'data-slider-min' => 0,
					'data-slider-max' => '100000',
					'data-slider-selection' => "after",
				]);
			?>
		</div>
	</div>
	
</div>