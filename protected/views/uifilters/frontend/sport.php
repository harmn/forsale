<? 
/**
 * блок фильтров в списке Спорт
 */
?>

<div class="clearfix row w800">
	<div class="col-md-5">
		<?=CHtml::hiddenField('Sport[model]', 'true')?>
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Listing::getHomeCategory(), 
			['id' => 'sport_category_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Категория")])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Listing::getSportType(), 
			['id' => 'sport_type_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Тип")])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'sport_region_filter', 
		 		'class' => 'w100p small mb15',  
		 		'empty'=>t("front", "Регион"),
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => t("front", "Город/Район"),
				'id' 		  => 'sport_city_filter', 
				'class' 	  => "w100p small mb15", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="col-md-5">
		<!-- Цвет -->
		<?=$model->dropDownList('color_filter', Listing::getColorSelect(), 
			['id' => 'sport_color_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Цвет")])?>
		<!-- Состояние -->
		<?=$model->dropDownList('condition_filter', Listing::getElectronicsConditionSelect(), 
		['id' => 'sport_condition_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Состояние")])?>
		<div class="fl ml10 mb15 mt5">	
			<label class="mr20 block mb3 c-gray"><?=t("front", "Цена");?></label>
			<?php 
				$step = 5000;
				$priceFrom = 0; 
				$priceTo = 100000;
				if($model->owner->price)
					list($priceFrom, $priceTo) = explode(',', $model->owner->price);
				
				echo $model->textField('price', [
					'class' => 'slider-element form-control ',
					'style'	=> 'width: 290px',
					'data-slider-value' => '['.(float) $priceFrom.', '.(float) $priceTo.']',
					'data-slider-step' => $step,
					'data-slider-min' => 0,
					'data-slider-max' => '100000',
					'data-slider-selection' => "after",
				]);
			?>
		</div>
	</div>
	<div class="col-md-2">
	</div>
</div>