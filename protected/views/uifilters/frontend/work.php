<? 
/**
 * блок фильтров в списке Спорт
 */
?>

<div class="clearfix row w800">
	<div class="col-md-5">
		<?=CHtml::hiddenField('Work[model]', 'true')?>
		<!-- Тип-->		
		<?=$model->dropDownList('type_filter', Listing::getWorkType(), 
			['id' => 'work_type_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Тип")])?>
		<!-- График-->		
		<?=$model->dropDownList('graph_filter', Listing::getWorkGraphSelect(), 
			['id' => 'work_graph_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "График")])?>
		<!-- Образование-->		
		<?=$model->dropDownList('education_filter', Listing::getWorkEducationSelect(), 
			['id' => 'work_education_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Образование")])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'work_region_filter',
		 		'class' => 'w100p small mb15',  
		 		'empty'=>t("front", "Регион"),
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => t("front", "Город/Район"),
				'id' 		  => 'work_city_filter',
				'class' 	  => "w100p small mb15", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="col-md-5">
		<!--Языки-->		
		<?=$model->dropDownList('languages_filter', Languages::listData(), 
			['id' => 'work_languages_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'title'=>t("front", "Языки"), 'multiple' => 'multiple'])?>
		<div class="posrel mb15">
			<!-- Возраст от-->		
			<?=$model->textField('age_filter_from',['id' => 'work_age_filter_from', 'class' => " w47p ", 'data-filter' => true, "placeholder" => t("front", "Возраст от"), "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'');"])?>
			<div style="  position: absolute;  top: 50px;  margin-left: 146px;  width: 100%;">
				<!-- Возраст до-->		
				<?=$model->textField('age_filter_to',['id' => 'work_age_filter_to', 'class' => "w47p ", 'data-filter' => true, "placeholder" => t("front", "Возраст до"), "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'');"])?>
			</div>
		</div>
		<!-- Опыт работы -->		
		<?=$model->textField('experience',['id' => 'work_experience', 'class' => "w100p mt0 mb15", 'data-filter' => true, "placeholder" => t("front", "Опыт работы")])?>
		<!--Пол-->		
		<?=$model->dropDownList('sex_filter', Listing::getWorkSexSelect(), 
			['id' => 'work_sex_filter', 'class' => 'w100p small mb15', 'data-width' => '100%', 'data-filter' => true, 'empty'=>t("front", "Пол")])?>
		<div class="fl ml10 mb15 mt8">	
			<label class="mr20 block mb3 c-gray"><?=t("front", "Цена");?></label>
			<?php 
				$step = 5000;
				$salaryFrom = 0; 
				$salaryTo = 100000;
				if($model->owner->salary)
					list($salaryFrom, $salaryTo) = explode(',', $model->owner->salary);
				
				echo $model->textField('salary', [
					'class' => 'slider-element form-control ',
					'style'	=> 'width: 290px',
					'data-slider-value' => '['.(float) $salaryFrom.', '.(float) $salaryTo.']',
					'data-slider-step' => $step,
					'data-slider-min' => 0,
					'data-slider-max' => '100000',
					'data-slider-selection' => "after",
				]);
			?>
		</div>
	</div>
	<div class="col-md-2">
	</div>
</div>