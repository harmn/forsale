<? 
/**
 * блок фильтров в списке пользователей
 */
?>

<div class="clearfix">
	<div class="fl mr10 " style="width: 200px">
		<!-- ФИО -->		
		<?=$model->textField('fullname',['class' => "mb5 w100p", 'data-filter' => true, "placeholder" => "ФИО"])?>

		<!-- Логин -->		
		<?=$model->textField('username',['class' => "w100p", 'data-filter' => true, "placeholder" => "Логин"])?>	
	</div>

	<div class="fl mr10 w100">
		<!-- Допущенные-->
		<?=$model->dropDownList('status', Client::$filter_status,
		 	['class' => 'mb5 small', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Допущенные'])?>
	</div>

</div>