<? 
/**
 * блок фильтров в списке Спорт
 */
?>

<div class="clearfix " style="height: 200px;">
	<div class="fl mr10 posrel mt10" style="width: 200px">
		<!-- Тип-->		
		<?=$model->dropDownList('type_filter', Work::$type_select, 
			['id' => 'work_type_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
		<!-- Зарплата-->		
		<?=$model->textField('salary_filter',['id' => 'work_salary_filter', 'class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Зарплата"])?>
		<!-- Тип-->		
		<?=$model->dropDownList('graph_filter', Work::$graph_select, 
			['id' => 'work_graph_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'График'])?>
		<!-- Образование-->		
		<?=$model->dropDownList('education_filter', Work::$education_select, 
			['id' => 'work_education_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Образование'])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'work_region_filter',
		 		'class' => 'w100p small',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'id' 		  => 'work_city_filter',
				'class' 	  => "w100p small mb5", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="fl posrel mt10" style="width: 200px" >
		<!--Языки-->		
		<?=$model->dropDownList('languages_filter', Languages::listData(), 
			['id' => 'work_languages_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'title'=>'Языки', 'multiple' => 'multiple'])?>
		<div class="posabs w100p mb5">
			<!-- Цена от-->		
			<?=$model->textField('age_filter_from',['id' => 'work_age_filter_from', 'class' => " fl w50p mb5", 'data-filter' => true, "placeholder" => "Возраст от"])?>
			<!-- Цена до-->		
			<?=$model->textField('age_filter_to',['id' => 'work_age_filter_to', 'class' => "posabs fl w50p ml100", 'data-filter' => true, "placeholder" => "Возраст до"])?>
		</div>
		<!-- Опыт работы -->		
		<?=$model->textField('experience',['id' => 'work_experience', 'class' => "w100p mt0 mb5", 'data-filter' => true, "placeholder" => "Опыт работы"])?>
		<!--Пол-->		
		<?=$model->dropDownList('sex_filter', Work::$sex_select, 
			['id' => 'work_sex_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Пол'])?>
		<!-- Имя пользователья -->		
		<?=$model->textField('fullname',['id' => 'work_fullname', 'class' => "w100p mt0", 'data-filter' => true, "placeholder" => "Имя пользователья"])?>
	</div>
</div>