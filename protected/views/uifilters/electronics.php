<? 
/**
 * блок фильтров в списке Электроники
 */
?>

<div class="clearfix " style="height: 210px;">
	<div class="fl mr10 posrel mt25" style="width: 200px">
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Home::$category_select, 
			['id' => 'electronics_category_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Категория'])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Electronics::$type_select, 
			['id' => 'electronics_type_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
		<!-- Марка -->
		<?=$model->dropDownList('brand_filter', [], 
			['id' => 'electronics_brand_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Марка'])?>
		<div class="posrel w100p mb5 t0">
			<!-- Цена от-->		
			<?=$model->textField('price_filter_from',['id' => 'electronics_price_filter_from', 'class' => " fl w50p mb5", 'data-filter' => true, "placeholder" => "Цена от"])?>
			<!-- Цена до-->		
			<?=$model->textField('price_filter_to',['id' => 'electronics_price_filter_to', 'class' => "posabs fl t0 w50p mb5 ml100", 'data-filter' => true, "placeholder" => "Цена до"])?>
		</div>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'electronics_region_filter',
		 		'class' => 'w100p small',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'id' 		  => 'electronics_city_filter',
				'class' 	  => "w100p small mb5", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="fl mr10 w200 posrel" >
		<label data-reset="true">Год выпуска с</label>
		<?=$model->datePicker('release_year_from', ['pluginOptions'=>[
            'format'=>'yyyy',
            'data-width'    => '100%',
            'id' => 'electronics_release_year_from',
            'viewMode' => "years", 
            'minViewMode' => "years"
        ]],
        false,
        ['class' => 'block mb5 mt5'])?>
		<div class="checkbox posabs" style="width: 209px;">
			<!-- Цвет -->
			<?=$model->dropDownList('color_filter', Electronics::$color_select, 
				['id' => 'electronics_color_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Цвет'])?>
			<!-- Состояние -->
			<?=$model->dropDownList('condition_filter', Electronics::$condition_select, 
			['id' => 'electronics_condition_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Состояние'])?>
			<!-- Имя пользователья -->		
			<?=$model->textField('fullname',['id' => 'electronics_fullname', 'class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Имя пользователья"])?>
		</div>
	</div>

	<div class="fl  w200">
		<label data-reset="true">Год выпуска по</label>
		<?=$model->datePicker('release_year_to', ['pluginOptions'=>[
            'format'=>'yyyy',
            'data-width'    => '100%',
            'id' => 'electronics_release_year_to',
            'viewMode' => "years", 
            'minViewMode' => "years"
        ]],
        false,
        ['class' => 'block mb5 mt5'])?>
	</div>
	
</div>