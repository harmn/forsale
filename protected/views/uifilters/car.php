<?  
/**
 * блок фильтров в списке Автомобилей
 */
?>

<div class="clearfix " style="height: 210px;">
	<div class="fl mr10 posrel mt25" style="width: 200px">
		<!-- Марка -->
		<?=$model->dropDownList('brand_filter', Car::$brand_select, 
			['id' => 'car_brand_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Марка'])?>
		<!-- Модель -->
		<?=$model->dropDownList('model_filter', [], 
			['id' => 'car_model_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Модель'])?>
		<div class="posrel w100p mb5 t0">
			<!-- Цена от-->		
			<?=$model->textField('price_filter_from',['id' => 'car_price_filter_from', 'class' => " fl w50p mb5", 'data-filter' => true, "placeholder" => "Цена от"])?>
			<!-- Цена до-->		
			<?=$model->textField('price_filter_to',['id' => 'car_price_filter_to', 'class' => "posabs fl t0 w50p mb5 ml100", 'data-filter' => true, "placeholder" => "Цена до"])?>
		</div>
		<div class="posrel w100p mb5 t0" style="margin-top: 36px;">
			<!-- Пробег от -->
			<?=$model->dropDownList('mileage_from_filter', Car::$mileage_select, 
				['id' => 'car_mileage_from_filter', 'class' => 'fl w50p mb5 small', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Пробег от'])?>
			<div class="posabs fl t0 w50p mb5 ml100">
				<!-- Пробег до -->
				<?=$model->dropDownList('mileage_to_filter', Car::$mileage_select, 
					['id' => 'car_mileage_to_filter', 'class' => '  small', 'style' => 'top: 31px;', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Пробег до'])?>
			</div>
		</div>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'car_region_filter', 
		 		'class' => 'w100p small',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 		])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'id' 		  => 'car_city_filter', 
				'class' 	  => "w100p small mb5", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
	</div>
	<div class="fl mr10 posrel mt25" style="width: 200px">
		<!-- Кузов -->
		<?=$model->dropDownList('body_filter', Car::$body_select, 
			['id' => 'car_body_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Кузов'])?>
		<!-- Руль -->
		<?=$model->dropDownList('rudder_filter', Car::$rudder_select, 
			['id' => 'car_rudder_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Руль'])?>
		<!-- Цвет -->
		<?=$model->dropDownList('color_filter', Car::$color_select, 
			['id' => 'car_color_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Цвет'])?>
		<!-- Состояние -->
		<?=$model->dropDownList('condition_filter', Car::$condition_select, 
			['id' => 'car_condition_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Состояние'])?>
		<!-- Тип  -->
		<?=$model->dropDownList('type_filter', Car::$type_select, 
			['id' => 'car_type_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
	</div>
	<div class="fl mr10 w200 posrel" >

		<label data-reset="true">Год выпуска с</label>
		<?=$model->datePicker('release_year_from', ['pluginOptions'=>[
            'format'=>'yyyy',
            'data-width'    => '100%',
            'id' => 'car_release_year_from',
            'viewMode' => "years", 
            'minViewMode' => "years"
        ]],
        false,
        ['class' => 'block mb5 mt5'])?>
		<div class="checkbox posabs mt0" style="width: 209px;">
			<!-- Коробка передач -->
			<?=$model->dropDownList('transmission_filter', Car::$transmission_select, 
				['id' => 'car_transmission_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Коробка передач'])?>
		</div>
		<div class="checkbox posabs" style="width: 209px; margin-top: 31px">
			<!-- Количество сидений -->
			<?=$model->dropDownList('seats_count_filter', Car::$seats_count_select, 
				['id' => 'car_seats_count_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Количество сидений'])?>
		</div>
		<div class="checkbox posabs" style="width: 209px; margin-top: 62px">
			<!-- Категория -->
			<?=$model->dropDownList('category_filter', Home::$category_select, 
				['id' => 'car_category_filter', 'class' => 'w100p small mb5', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Категория'])?>
		</div>
		<div class="checkbox posabs" style="width: 209px; margin-top: 93px">
			<!-- Имя пользователья -->		
			<?=$model->textField('fullname',['id' => 'car_fullname','class' => "w100p mb5", 'data-filter' => true, "placeholder" => "Имя пользователья"])?>
		</div>
	</div>

	<div class="fl  w200">
		<label data-reset="true">Год выпуска по</label>
		<?=$model->datePicker('release_year_to', ['pluginOptions'=>[
            'format'=>'yyyy',
            'data-width'    => '100%',
            'id' => 'car_release_year_to',
            'viewMode' => "years", 
            'minViewMode' => "years"
        ]],
        false,
        ['class' => 'block mb5 mt5'])?>
	</div>
	
</div>