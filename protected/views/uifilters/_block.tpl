<div class="filter-block grid simple {$name} {$float} {$hidden}" id="{$id}">
	<div class="grid-title">
		<!-- <h4>{$title}</h4> -->
		<div class="tools"> 
			<a href="#" class="reload" rel="tooltip" title="Сброс полей"></a> 
			<a href="#" class="remove" rel="tooltip" title="Скрыть блок"></a> 
		</div>
	</div>
	<div class="grid-body">
		{$content}
	</div>

</div>