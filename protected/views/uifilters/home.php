<? 
/**
 * блок фильтров в списке домов
 */
?>

<div class="clearfix">
	<div class="clearfix mb10">
		<!-- Категория -->
		<?=$model->dropDownList('category_filter', Home::$category_select, 
			['id' => 'home_category_filter', 'class' => 'w125 small fl mr10', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Категория'])?>
		<!-- Тип строения -->
		<?=$model->dropDownList('type_filter', Home::$type_select, 
			['id' => 'home_type_filter', 'class' => 'w180 small fl mr10', 'data-width' => '100%', 'data-filter' => true, 'empty'=>'Тип'])?>
		<!-- Регион -->
		<?=$model->dropDownList('region_filter', Region::listData(),
		 	[	'data-search' => true,
		 		'data-filter' => true, 
		 		'id' => 'home_region_filter',
		 		'class' => 'w150 small mr10 fl',  
		 		'empty'=>'Регион',
				'data-depend'=>'job',
		 	])?>

		<!-- Район -->

		<?
			$cachedUsers = City::listData();
			$dependOptions = City::listDataCustom(false, false, ['depend' => true]);
		?>
		<?=$model->dropDownList('city_filter',
			$cachedUsers,
			[
				'empty'		  => 'Район',
				'id' 		  => 'home_city_filter',
				'class' 	  => "w180 small fl mr10", 
				'data-depend' =>'city',
				'data-search' => true,
				'data-filter' => true, 
				'options'	  => $dependOptions
		]);?>
		
		<div class="fl w60">
			<!-- Этаж -->		
			<?=$model->textField('floor_filter',['id' => 'home_floor_filter', 'class' => "w200p mb5", 'data-filter' => true, "placeholder" => "Этаж"])?>	
		</div>
	</div>

	<div class="clearfix">
		<!-- Цена -->		
		<div class="fl w100 mr10">
			<?=$model->textField('price_filter_from',['id' => 'home_price_filter_from', 'class' => "w50p mb5", 'data-filter' => true, "placeholder" => "Цена от"])?>
		</div>
		<!-- Цена -->
		<div class="fl mr10 w100">
			<?=$model->textField('price_filter_to',['id' => 'home_price_filter_to', 'class' => "posabs t0 w50p mb5 ml100", 'data-filter' => true, "placeholder" => "Цена до"])?>
		</div>
	
		<div class="fl mr10 w100">	
			<!-- Количество комнат -->		
			<?=$model->textField('room_count_filter',['id' => 'home_room_count_filter', 'class' => "w200p mb5", 'data-filter' => true, "placeholder" => "Кол. комнат"])?>	
		</div>	
	</div>
	
</div>