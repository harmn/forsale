<?php
$modal = $this->beginWidget('UIModal',[
	 	'id' => 'login-modal',
		'width' => 400,
		'title' => t('front', 'Вход'),
		'draggable' => false,
 	]);
	?>

<div class="bg-lighten-gray p20 clearfix">
	<?php $form = $this->beginWidget('SActiveForm', array(
			'id'=>'login-form-modal',
			'modal' => true,
			'action' => ['/auth/ajaxlogin'],
			'focus'=>array($model, 'username'),
			'enableAjaxValidation' => true,
			'afterModalClose' => 'function(form, data){
				 window.location = "/site/index";
			}'

		)); 
		echo CHtml::hiddenField('scenario', 'flogin');
		?>
		<div class="clearfix">
			<div class="mb10">
				<div class="inputDiv">
					<?=$form->textField($model, 'username', ['class' => "form-control big", 'placeholder'=>t('front', 'Логин')])?>
				</div>
				<?=$form->error($model, 'username')?>
			</div>

			<div class="mb10">
				<div class="inputDiv">
					<?=$form->passwordField($model, 'password', ['class' => "form-control big", 'placeholder'=>t('front', 'Пароль')])?>
				</div>
				<?=$form->error($model, 'password')?>
			</div>

			<div class="clearfix mb10">
				<!-- <div class="fl"><input type="checkbox"> <label class="ml5 fnormal fsize13"><?=t('front', 'Запомнить');?></label></div> -->
				<div class="fr"><a href="<?=$this->createUrl('/auth/forgotpassword')?>" class="link c-gray fsize13"><?=t('front', 'Забыли пароль?');?></a></div>
			</div>

			<?= CHtml::htmlButton(t('front', 'Войти'),['class'=>'btn btn-primary bg-blue block fr mb10', 'type' => 'submit', 'data-form'=>'login-form-modal']) ?>
		</div>
	<?php $this->endWidget(); ?>

	<div class="social-wrap clearfix mt10 hidden">
		<span class="c-gray fl ml10 fsize14" style="line-height: 30px">Войти через аккаунт</span>
		<!-- <ul class="social">
			<li><a href="#"><i class="fa fa-vk"></i></a></li>
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-google"></i></a></li>
		</ul> -->
		<?//$this->widget('application.modules.core.modules.social.extensions.eauth.EAuthWidget', ['action' => '/auth/login']);?>
	</div>
</div>
<?$this->endWidget();?>
