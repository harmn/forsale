<div class="wrap tcenter">
    <? if(user()->hasFlash('error')): ?>
    	<!-- ошибка активации -->
    	<div class="success_message">
            <?=user()->getFlash('error')?>
            <div class="gradient mt10">
            <?=CHtml::link(t('front', 'Перейти на главную'), array('/site/index'), ["class"=> "btn btn-ms bg-red c-white"])?>
            </div>
        </div>

    <? endif ?>

    <? if(user()->hasFlash('success')): ?>
        <!-- активация прошла успешно -->
        <div class="success_message">
            <?=user()->getFlash('success')?>
            <div class="gradient mt10">
            <?=CHtml::link(t('front', 'Перейти на страницу входа'), array('login'), ["class"=> "btn btn-ms bg-red c-white"])?>
        	</div>
        </div>
    <? endif ?>
</div>