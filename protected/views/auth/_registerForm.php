<div id="register" class="rel">
	<div>
		<img src="<?=$this->assetsUrl?>/images/placeholders/register.png" alt="" height="50">
	</div>
	<div class="formDiv">

		<?if(user()->hasFlash('success')):?>
			<div class="success_message">
				<?=user()->getFlash('success');?>
			</div>

		<?elseif(user()->hasFlash('error')):?>
			<div class="error_message">
				<?=user()->getFlash('error');?>
			</div>
		<?else: ?>

			<?php $form = $this->beginWidget('SActiveForm', array(
				'id' => 'register-form',
				'action' => ['login'],
				'enableAjaxValidation' => true,
			)); 

				echo CHtml::hiddenField('scenario', $model->scenario);
			?>
			<div class="clearfix mt20">
				<div class="clearfix ">
					<div class="fl mb10 pr10 mr10" style="width:49%">
						<div class="inputDiv">
							<?=$form->textField($model, 'username',['class' => 'form-control big', 'placeholder'=> t('front', 'Логин')])?>
						</div>
						<?=$form->error($model, 'username')?>
					</div>

					<div class="fl" style="width:49%">
						<div class="inputDiv">
							<?=$form->textField($model, 'email', ['class' => 'form-control big', 'placeholder'=> t('front', 'E-mail')])?>
						</div>
						<?=$form->error($model, 'email')?>
					</div>
				</div>

				<div class="clearfix ">
					<div class="fl mb10 pr10 mr10" style="width:49%">
						<div class="inputDiv">
							<?=$form->passwordField($model, 'password', ['class' => 'form-control big', 'placeholder'=> t('front', 'Пароль')])?>
						</div>
						<?=$form->error($model, 'password')?>
					</div>

					<div class="fl " style="width:49%">
						<div class="inputDiv">
							<?=$form->passwordField($model, 'password2', ['class' => 'form-control big', 'placeholder'=> t('front', 'Повторите пароль')])?>
						</div>
						<?=$form->error($model, 'password2')?>
					</div>
				</div>

				<div class="clearfix ">
					<div class="fl mb10 pr10 mr10" style="width:49%">
						<div class="inputDiv">
							<?=$form->textField($model, 'firstname', ['class' => 'form-control big', 'placeholder'=> t('front', 'Имя')])?>
						</div>
						<?=$form->error($model, 'firstname')?>
					</div>

					<div class="fl " style="width:49%">
						<div class="inputDiv">
							<?=$form->textField($model, 'phone', ['class' => 'form-control big', 'placeholder'=> t('front', 'Телефон')])?>
						</div>
						<?=$form->error($model, 'phone')?>
					</div>
				</div>

				<div class="clearfix mb20 mt10" style="font-size: 12px !important; color: #c2c1bd !important;">
					<span class="fr mr3"><?=t('front', 'Условия');?></span>
				</div>

				<?$this->widget('UIButtons', ['buttons' => [
					'custom' => [
						'value'		=> t('front', 'Регистрация'),
						'icon'		=> '',
						'options'	=> [
							'class'			=> 'btn btn-danger bg-red block fr w20p',
							'data-form' 	=> 'register-form',
							'type'			=> 'submit',
							'onclick'		=> 'UIButtons.save(this); return false;'
						]
					]]
				]);?>
				<?php $this->endWidget(); ?>
			</div>
		<?endif?>
	</div>
</div>
