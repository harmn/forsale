<?php
$modal = $this->beginWidget('UIModal',[
	 	'id' => 'change-password-modal',
		'width' => 400,
		'title' => t('front', 'Изменить пароль'),
		'draggable' => false,
 	]);
	?>

<div class="bg-lighten-gray p20 clearfix">
	<?php $form = $this->beginWidget('SActiveForm', array(
			'id'=>'change-password',
			'modal' => true,
			'action' => ['/auth/changepassword'],
			'focus'=>array($model, 'old_password'),
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => true,
			),
			'afterModalClose' => 'function(form, data){
				location.reload();
			}'

		)); 
		echo CHtml::hiddenField('scenario', "changePassword");
		?>
		<div class="clearfix">
			<div class="mb10">
				<div class="inputDiv">
					<?=$form->passwordField($model, 'old_password', ['class' => "form-control big", 'placeholder'=>t('front', 'Старый пароль')])?>
				</div>
				<?=$form->error($model, 'old_password')?>
			</div>

			<div class="mb10">
				<div class="inputDiv">
					<?=$form->passwordField($model, 'password', ['class' => "form-control big", 'placeholder'=>t('front', 'Новый пароль')])?>
				</div>
				<?=$form->error($model, 'password')?>
			</div>

			<div class="mb10">
				<div class="inputDiv">
					<?=$form->passwordField($model, 'password2', ['class' => "form-control big", 'placeholder'=>t('front', 'Повторите пароль')])?>
				</div>
				<?=$form->error($model, 'password2')?>
			</div>

			<?= CHtml::htmlButton(t('front', 'Изменить'),['class'=>'btn btn-primary bg-blue block fr mb10', 'type' => 'submit', 'data-form'=>'change-password']) ?>
		</div>
	<?php $this->endWidget(); ?>
</div>
<?$this->endWidget();?>
