<?php $this->renderPartial('_changePassword', ['model'=> $changePasswordModel]);?>

<div class="wrapper mt50 mb50"> 
	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#profile" data-toggle="tab"><?=t('admin', 'Мой профиль');?></a></li>
        <li><a href="#adverts" data-toggle="tab"><?=t('admin', 'Обявления');?></a></li>
        <li><a href="#later" data-toggle="tab"><?=t('admin', 'Посмотреть позже');?></a></li>
        <li><a href="#favorite" data-toggle="tab"><?=t('admin', 'Избранные');?></a></li>
    </ul>
    <div id="my-tab-content" class="tab-content mb50 mt30">
        <div class="tab-pane active " id="profile">
           <?$form = $this->beginWidget('SActiveForm', [
				'id' => 'profile-form', //объязательно для работы кнопок сохранения, удаления
				'enableAjaxValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true,
					'validateOnChange' => true,
				),
				'action' => ['profile'],
				'htmlOptions' => ['enctype'=>'multipart/form-data'],
			]); ?>
			<?=CHtml::hiddenField('scenario', "update");?>
			<div class="col-md-5">
				<div class="col-md-5">
					<div class="control-group fleft">
						<span class="field">
							<?$this->widget('Avatar', [
								'form' 			=> $form,
								'model' 		=> $model,
								'field' 		=> 'image',
								'image' 		=> 'avatar',
								'size'			=> 'big',
								'hiddenFile' 	=> true,
								'hiddenLink'	=> true,
								'thumbWidth' 	=> param('images/user/sizes/big/width'),
								'thumbHeight' 	=> param('images/user/sizes/big/height'),
								'bigSize'		=> 'big',
								'alt' 			=> $model->fullname,
							]);?>
						</span>
					</div>
				</div>
				<div class="col-md-7">
					<div>
						<p class="fsize18 c-blue bold mb10">Ներբեռնել լուսանկար</p>
						<p class="mb10">
							<?=CHtml::htmlButton(t('front', "Выбрать файл").CHtml::tag('i', ['class'=>'fa fa-camera ml10'], ''), [
								'class'=>'uploadAvatarImage btn btn-success btn-mini bg-blue',
							]);?>
						</p>
						<p class="fsize14 c-black mb10 bold" style="line-height: 16px;">Դուք կարող եք ներբեռնել լուսանկար JPG, GIF և PNG. ձևաչափերի:</p>
					</div>

					<div class="control-group">
						<span class="field">
							<?=CHtml::htmlButton(CHtml::tag('i', ['class'=>'fa fa-key mr5'], '').t('front', "Сменить пароль"), [
								'class'=>'btn btn-mini bg-red c-white',
								'data-toggle'=>"domodal",
								'data-target'=>"#change-password-modal"
							]);?>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="mb20">
					<div class="inputDiv">
						<?=$form->textField($model, 'firstname', ['class' => 'form-control big', 'placeholder'=> t('front', 'Ваше имя')])?>
					</div>
					<?=$form->error($model, 'firstname')?>
				</div>

				<div class="mb20">
					<div class="inputDiv">
						<?=$form->textField($model, 'lastname', ['class' => 'form-control big', 'placeholder'=> t('front', 'Ваша фамилия')])?>
					</div>
					<?=$form->error($model, 'lastname')?>
				</div>

				<div class="mb20">
					<div class="inputDiv">
						<?=$form->textField($model, 'middlename', ['class' => 'form-control big', 'placeholder'=> t('front', 'Ваше отчество')])?>
					</div>
					<?=$form->error($model, 'middlename')?>
				</div>
			</div>
			<div class="col-md-4">
				<div class="mb20">
					<div class="inputDiv">
						<?=$form->textField($client->profile, 'phone', ['class' => 'form-control big', 'placeholder'=> t('front', 'Телефон')])?>
					</div>
					<?=$form->error($client->profile, 'phone')?>
				</div>
				<div class="mb20">
					<div class="inputDiv">
						<?=$form->textField($model, 'email', ['class' => 'form-control big', 'placeholder'=> t('front', 'E-mail')])?>
					</div>
					<?=$form->error($model, 'email')?>
				</div>
				<div class="mb20">
					<div class="save w100p">
	                    <!-- Сохранить -->
	                    <?$this->widget('UIButtons', ['buttons' => [
	                            'custom' => [
	                            'value'     => t("front", "Изменить"),
	                            'icon'      => '',
	                            'options'   => [
	                            	'id' 			=> $model->id,
	                                'class'         => 'btn btn-submit btn-icon fr c-white bg-red uppercase fsize14 w100p',
	                                'data-form'     => 'profile-form',
	                                'type'          => 'submit',
	                                'onclick'       => 'UIButtons.save(this); return false;'
	                            ]
	                        ]]
	                    ]);?>
	                </div>
				</div>
			</div>
			
			<?$this->endWidget()?>
        </div>
        <div class="tab-pane" id="adverts">
            2
        </div>
        <div class="tab-pane" id="later">
            3
        </div>
        <div class="tab-pane" id="favorite">
            4
        </div>
    </div>
</div>