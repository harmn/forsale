<?
$criteria = new CDbCriteria;
$criteria->compare('id_owner', $id);
$model = new Product('search');
$this->widget('SGridView', [
		'id' => 'grid',
		'dataProvider'=>$model->search($criteria),
		'filter'=>$model,
		'showCheckBoxColumn' => false,
		'template' => '{items}',
		'ajaxUrl' => $this->createUrl('profile'),
		'columns'=>[
			[
				'class'	 => 'SDateColumn',
				'name'	 => 'created',
				'rangeFilter' => false,
				'headerHtmlOptions' => ['width' => 90],
				'value'	 => function($data){
					return date('d/m/Y', strtotime($data->created));
				}
			],
			[
				'name'	=> 'title',
			],
			[
				'name'	=> 'price',
				'headerHtmlOptions' => ['width'=>100],
				'value'	=> function($data){
					return $data->price ? $data->price." ".Product::CURRENCY_ARM : '';
				}
			],
			[
				'class'  => 'SButtonColumn',
				'headerHtmlOptions' => ['width' => 60],
				'buttons' 	  => [
					'delete' => ['url' => 'url("/site/deleteproduct", ["id"=>$data->id])'],
					'update' => ['url' => 'url("/site/editproduct", ["id"=>$data->id])'],
				],
			]
		],
	]); 
?>