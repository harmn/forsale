<div id="login">	
	<div>
		<img src="<?=$this->assetsUrl?>/images/placeholders/user.png" alt="" height="50">
	</div>
	<div class="formDiv">
		<?php $form = $this->beginWidget('SActiveForm', array(
				'id'=>'login-form',
				'action' => ['login'],
				// 'focus'=>array($model, 'username'),
				'enableAjaxValidation' => true,
			)); 
			echo CHtml::hiddenField('scenario', $model->scenario);
			?>
			<div class="clearfix mt20">
				<div class="mb10">
					<div class="inputDiv">
						<?=$form->textField($model, 'username', ['class' => "form-control big", 'placeholder'=>t('front', 'Логин')])?>
					</div>
					<?=$form->error($model, 'username')?>
				</div>

				<div class="mb10">
					<div class="inputDiv">
						<?=$form->passwordField($model, 'password', ['class' => "form-control big", 'placeholder'=>t('front', 'Пароль')])?>
					</div>
					<?=$form->error($model, 'password')?>
				</div>

				<div class="clearfix mb10 mt20">
					<!-- <div class="fl"><input type="checkbox"> <label class="ml5 fnormal fsize13"><?=t('front', 'Запомнить');?></label></div> -->
					<div class="fr getPassDiv"><a href="<?=$this->createUrl('/auth/forgotpassword')?>" class="link c-gray fsize13"><?=t('front', 'Забыли пароль?');?></a></div>
				</div>
			
				<?$this->widget('UIButtons', ['buttons' => [
						'custom' => [
							'value'		=> t('front', 'Войти'),
							'icon'		=> '',
							'options'	=> [
								'class'			=> 'btn btn-primary bg-blue block fr mt10 w150',
								'data-form' 	=> 'login-form',
								'type'			=> 'submit',
								'onclick'		=> 'UIButtons.save(this); return false;'
							]
					]]
				]);?>

				<?php $this->endWidget(); ?>
			</div>
	</div>
</div>
