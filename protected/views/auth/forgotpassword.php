<div class="wrapper success_message mt50" style="width: 400px; margin: auto">
	<h1 class="headers hl bb tcenter" style="border-bottom: 2px solid #0a558a; width: 400px;  padding-bottom: 8px;"><?=$this->pageTitle?></h1>
	
	<?if(user()->hasFlash('email-sent')):?>
		<?=user()->getFlash('email-sent'); ?>
    <?elseif(user()->hasFlash('password-changed')):?>
        <p style="  text-align: center;  margin-top: 15px;"><?=user()->getFlash('password-changed');?></p>
	<?else:?>
		<!-- показ формы -->
		<?$form=$this->beginWidget('SActiveForm',[
			'id' =>	'forgot-password',
			'htmlOptions' => [
				'class' => 'changepassword'
			]
		]); // echo $form->errorSummary($model);
		?>

			<?if(!request()->getParam('key')): //указание почтового ящика?>
				<div id="email" class="clearfix mt20 mb40 bg-lighten-gray">
					<h2 class='mt10 tcenter'><?=t('front', 'Укажите свой email')?></h2>
					<div class="left w300 mt10" style='margin: auto'>
						<?=$form->textField($model, 'email', ['class'=>'form-control big']);?>
	    				<?=$form->error($model, 'email',['class' => 'c-red']);?>
					</div>

					<?=CHtml::submitButton(t('front', 'Сменить пароль'), [
						'class' => 'tcenter btn btn-primary bg-blue block mt10 mb20',
						'style' => 'margin: auto',
					])?>
				</div>
			<?else: //установка пароля?>
				<div id="password" class="clearfix mt10 mb40 bg-lighten-gray">

					<h2 class="mt10 mb10 tcenter"><?=t('front', 'Укажите новый пароль')?></h2>

					<div class="left w300 mb20" style='margin: auto'>
						<?=$form->passwordField($model, 'password', [
							'placeholder'	=>	t('front', 'Пароль'),
							'class'			=>	'form-control big'
						]);?>
	    				<?=$form->error($model, 'password', ['class' => 'c-red'])?>
					</div>
					
					<div class="left w300 mb20" style='margin: auto'>
						<?=$form->passwordField($model, 'password2', [
							'placeholder'	=>	t('front', 'Повторите пароль'),
							'class'			=>	'form-control big'
						])?>
	    				<?=$form->error($model, 'password2', ['class' => 'c-red'])?>
					</div>

					<?=CHtml::submitButton(t('front', 'Поменять'), [
						'class'	=>	'tcenter btn btn-primary bg-blue block mt10 mb20',
						'style' => 'margin: auto',
					])?>
				</div>
			<?endif?>
		<?$this->endWidget();?>
	<?endif?>
</div>