<?$modal = $this->beginWidget('UIModal',[
	'id' 	=> 'change-user-settings-frontend',
	'width' => 345,
	'title' => 'Сменить пароль',
	'draggable' => false
	]);?>

	<!-- change password -->
	<?$form = $this->beginWidget('SActiveForm', [
		'id' => 'reset-password-form', //объязательно для работы кнопок сохранения, удаления
		'action' => ['/auth/changepassword', 'id'=>Yii::app()->user->id],
		'modal'	=> true,
		'enableAjaxValidation'  => true,
		// 'clientOptions' => [
		// 	'validateOnChange' => false,
		// 	'afterValidate' => 'js:function(form, data, hasError){
		// 		var form = $("#reset-password-form");
		// 		if(!hasError){
		// 			form.closest(".modal-overlay").hide();
		// 			form.get(0).reset();
		// 		}

		// 		Forms.enableFormSubmit(form);
		// 	}'
		// ],
		'afterModalClose' => 'function(form, data){
			form.get(0).reset();
			Forms.enableFormSubmit(form);
		}'
		])?>

		<div class="clearfix p20">
			<!-- Старый пароль -->
			<div class="mb10">
				<div class="mb10">
					<?=$form->passwordField($model, 'old_password',['class'=>'w100p','placeholder'=>'Старый пароль'])?>
				</div>
				<?=$form->error($model, 'old_password', ['class'=>'c-red mb10'])?>
			</div>

			<!-- Пароль -->
			<div class="mb10">
				<div class="mb10">
					<?=$form->passwordField($model, 'password',['class'=>'w100p','placeholder'=>'Новый пароль'])?>
				</div>
				<?=$form->error($model, 'password', ['class'=>'c-red mb10'])?>
			</div>

			<!-- Повторите пароль -->
			<div class="mb10">
				<div class="mb10">
					<?=$form->passwordField($model, 'password2',['class'=>'w100p','placeholder'=>'Повторите новый пароль'])?>
				</div>
				<?=$form->error($model, 'password2', ['class'=>'c-red mb10'])?>
			</div>

			<?=CHtml::submitButton('Поменять',[
				'class'		=>	'btn btn-primary fr bg-blue c-white ',
				'style'		=>	'width:100%',
				'data-wait' =>	'Подождите',
				'data-form' =>  'reset-password-form',
				'tupe'		=> 	'submit'
			])?>
		</div>

	<?$this->endWidget()// form ?>
<?$this->endWidget(); // modal ?>