<h2><?=t("front", "Регистрация на сайте")?></h2>

<p><?=t("front", "Ваш акаунт на сайте")?></p>

<p><?=t('front', 'Теперь Вы можете')?> <?=CHtml::link(t('front', 'войти на сайт'), $this->createAbsoluteUrl(app()->getHomeUrl()))?> <?=t('front', ', используя свои регистрационные данные.')?></p>
