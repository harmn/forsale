<h2><?=Yii::t('front', "Сброс пароля")?></h2>
<p><?=Yii::t('front', "Для сброса пароля на сайте ")?><?=CHtml::encode(Yii::app()->name)?> <?=Yii::t('front', "нажмите на следующую ссылку")?></p>
<p>
<?=CHtml::link(Yii::t('front', "Нажмите для сброса пароля"), 
	$this->createAbsoluteUrl('/auth/resetpassword', array('username'=>$username, 'key'=>$key)));?></p>
