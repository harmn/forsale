<?//Для предварительной загрузки скриптов
//ProductCompareWidget::loadModalAndScripts(['loadModal' => true]);?>
<?if(isset($model) && $model):?>
	<div class="tab-pane active" id="order">
		
		<? $active = [Order::STATUS_NEW, Order::STATUS_APPROVED, Order::STATUS_READY, Order::STATUS_APPOINTED, Order::STATUS_ADDRESS_A, Order::STATUS_ON_WAY, Order::STATUS_ADDRESS_B]?>
		<? $canceled = [Order::STATUS_CANCELED]?>
		<? $closed = [Order::STATUS_CLOSED]?>
		<?$index = 0;?>
		<!-- Nav tabs -->
		<ul class="clearfix ml10 mb5">
		  <li class="active fl mr15"><a href="#this-order" data-toggle="tab" class="c-gray">Текущий</a></li>
		  <li class="fl mr15"><a href="#canceled-order" data-toggle="tab" class="c-gray">Отменен</a></li>
		  <li class="fl"><a href="#closed-order" data-toggle="tab" class="c-gray">Закрытий</a></li>
		</ul>

		<?php 
			$modelView = [];
			//$view = [];
			$view['active'] = '';
			$view['canceled'] = '';
			$view['closed'] = '';
		?>
		<?foreach ($model->orders as $order) : ?>
			<?if(in_array($order->status, $active)){
				$modelView['active'] = $order;
				$view['active'] .= $this->renderPartial('myOrdersView', ['model' => $modelView['active']], true);
			}?>

			<?if(in_array($order->status, $canceled)){
				$modelView['canceled'] = $order;
				$view['canceled'] .= $this->renderPartial('myOrdersView', ['model' => $modelView['canceled']], true);
			}?>

			<?if(in_array($order->status, $closed)){
				$modelView['closed'] = $order;
				$view['closed'] .= $this->renderPartial('myOrdersView', ['model' => $modelView['closed']], true);
			}?>
		<?endforeach;?>	
		
		<!-- Tab panes -->
		<div class="tab-content">	
		  	<div class="tab-pane active" id="this-order">
		  		<div class="panel-group mb0" id="accordion">
					<?if($view['active']):?>	
						<?= $view['active'];?>
					<?else:?>
						<div>
							<p class="tcenter italic fbold">Нет результатов</p> 
						</div>
					<?endif;?>
				</div>
		  	</div>
		  	<div class="tab-pane" id="canceled-order">
		  		<div class="panel-group mb0" id="accordion">
		  			<?if($view['canceled']):?>
						<?= $view['canceled'];?>
					<?else:?>
						<div>
							<p class="tcenter italic fbold">Нет результатов</p> 
						</div>
					<?endif;?>	
				</div>
		  	</div>
		  	<div class="tab-pane" id="closed-order">
				<div class="panel-group mb0" id="accordion">
					<?if($view['closed']):?>	
						<?= $view['closed'];?>
					<?else:?>
						<div>
							<p class="tcenter italic fbold">Нет результатов</p> 
						</div>
					<?endif;?>
				</div>
		  	</div>
		</div>		
	</div>
<? else : ?>
	<div>
		<p class="tcenter italic fbold">Нет результатов</p> 
	</div>
<? endif ?>
