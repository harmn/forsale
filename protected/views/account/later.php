<div class="tab-pane active list-view" id="global-list">
	<div class="p20">
		<?php 
			if($results){ ?>
				<ul class="item-list-descr blocks pt10 main">
			<?php	$this->renderPartial('/site/_itemModal');
				foreach($results as $key => $value){
					$model = $key;
					foreach($value as $i){
						$i = $i[0];
						$photos = $i->photos; 
						$imageUrl = Yii::app()->baseUrl.DS.'storage/images/parts'.DS;

						$favorites = [];
						if($i->favorites && $i->favorites[0]){
							$favorites[] = $i->favorites[0]->attributes['type'];
							if(isset($i->favorites[1])){
								$favorites[] = $i->favorites[1]->attributes['type'];
							}
						}

						$favoritChecker = ($favorites && in_array('1', $favorites)) ? " activeFavorit" : "";
						$laterChecker = ($favorites && in_array('2', $favorites)) ? " activeFavorit" : "";
					?>
						<li class="clearfix rel" data-model="<?=$model;?>" data-id="<?=$i->id;?>">
							<div class="fl w150 datePart rel">
								<div class="fl mt10 ml1"><i class="fa fa-calendar-o mr5"></i><?=date("d.m.Y", strtotime($i->created));?></div>
							</div>
							<a href="<?=url('advert/item', ['id' => $i->id, 'type' => $model])?>" class="fl w200">
								<?if($photos){ ?>
									<img src="<?=$imageUrl.$i->relatedphotos[0]->id_model.DS."custom".DS.$photos[0]->filename?>">
								<? }else{ ?>
									<img src="<?=$this->assetsUrl.'/images/nophoto/big.png'?>">
								<? } ?>
							</a>

							<div class="fl">
								<div class="fl ml10 mt10 w300">
									<div class="fsize22 bold c-dark-gray"><?=(strlen($i->title) > 20) ? mb_substr($i->title,0,20) . "..." : $i->title;?></div>
									<div class="mt30 "><?=mb_substr($i->description,0,40) . "...";?></div>
									<div class="mt30">
										<?=CHtml::link('<i class="fa fa-heart mr5"></i>', ['/account/favorites'], ["class" => "c-main-gray fsize18 transition favorites ".$favoritChecker])?>
										<?=CHtml::link('<i class="fa fa-clock-o"></i>', ['/account/later'], ["class" => "c-main-gray fsize18 transition later ".$laterChecker])?>
									</div>
								</div>
								<div class="fl w250 pricePart rel">
									<div class="fl mt10 bold c-red">
										<?=(!isset($i->salary)) ? ($i->price > 0) ? $i->price : t('front', 'Без цены') : $i->salary?> 
										<?php 
											if($i->valuta && (isset($i->salary) AND $i->salary > 0 OR isset($i->price) AND $i->price > 0)) echo Home::$valuta_select[$i->valuta];
										?>
									</div>
								</div>
								<div class="fl fastView link-to-item" data-title="" data-model="<?=$model;?>" data-id="<?=$i->id;?>" href="<?=url('Site/getItem')?>">
									<a href="#" class="quick-view">&rarr;</a>
								</div>
							</div>
						</li>
				<?php	}
				} ?>
				</ul>
			<?php }else{ ?>
				<span class="empty"><?=t('front', 'Нет результатов');?></span>
		<?php	}	?>
	</div>
</div>