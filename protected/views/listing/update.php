<div>
    <?=CHtml::hiddenField('' , $model->id, array('id' => 'idGlobal'));?>
    <?php 

         $form = $this->beginWidget('SActiveForm',[
            'id'    =>'update-statement',
            'modal' => false,
            'action' => ['update'],
            'enableAjaxValidation' => true,
            'htmlOptions' => [
                'class' => 'mt30 w800'
            ],
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
            ),
            'afterModalClose' => 'function(form, data){
               
            }',
            
        ]);

    ?>
        <div class="body">
            <div class="grid simple mb0">
                <div class="grid-body pb10 no-border clearfix">
                    <div class="fl" style="width: 300px;margin-left: 15px;">
                        <?=$form->hiddenField($model,'id',array('type'=>"hidden"), $model->id); ?>
                        <div class="row mb10">
                                <?=$form->labelEx($model, 'category')?>
                                <?=$form->dropDownList($model, 'category', Home::$category_select, [
                                            'empty'         => 'Не выбрано',
                                            'data-width'    => '100%',
                                        ] );?>
                                <?=$form->error($model,'category')?>
                        </div>
                        <div class="row mb10">
                                <?=$form->labelEx($model, 'type')?>
                                <?=$form->dropDownList($model, 'type', Home::$type_select, [
                                            'empty'         => 'Не выбрано',
                                            'data-width'    => '100%',
                                        ] );?>
                                <?=$form->error($model,'type')?>
                        </div>
                        <div class="row mb10">
                                <?=$form->labelEx($model,'price'); ?>
                                <?=$form->textField($model,'price', ['class' => 'w100p']); ?>
                                <?=$form->error($model,'price'); ?>
                        </div>
                       <div class="row mb10">
                                <?=$form->labelEx($model, 'valuta')?>
                                <?=$form->dropDownList($model, 'valuta', Home::$valuta_select, [
                                            'empty'         => 'Не выбрано',
                                            'data-width'    => '100%',
                                        ] );?>
                                <?=$form->error($model,'valuta')?>
                        </div>
                        <div class="row mb10">
                                <?php 
                                    $dependOptionsRegion = Region::listDataCustom(false, false, ['depend' => true]);
                                ?>
                                <?=$form->labelEx($model, 'id_region')?>
                                <?=$form->dropDownList($model, 'id_region', Region::listData(), [
                                            'empty'         => 'Район',
                                            'id'          => 'Home_update_id_region',
                                            'data-search' => true,
                                            'data-filter' => true, 
                                            'data-width'    => '100%', 
                                            'empty'=>'Регион',
                                            'data-depend'=>'job',
                                            'options'     => $dependOptionsRegion
                                        ] );?>
                                <?=$form->error($model,'id_region')?>
                        </div>
                    </div>    
                    <div class="fr mb10" style="width: 300px;margin-right: 15px;">
                        <div class="row mb10">
                                <?=$form->labelEx($model,'room_count'); ?>
                                <?=$form->textField($model,'room_count', ['class' => 'w100p']); ?>
                                <?=$form->error($model,'room_count'); ?>
                        </div>
                        <div class="row mb10">
                                <?=$form->labelEx($model,'area'); ?>
                                <?=$form->textField($model,'area', ['class' => 'w100p']); ?>
                                <?=$form->error($model,'area'); ?>
                        </div>
                        <div class="row mb10">
                                <?=$form->labelEx($model,'floor'); ?>
                                <?=$form->textField($model,'floor', ['class' => 'w100p']); ?>
                                <?=$form->error($model,'floor'); ?>
                        </div>
                        <div class="row mb10">
                                <?=$form->labelEx($model,'floor_count'); ?>
                                <?=$form->textField($model,'floor_count', ['class' => 'w100p']); ?>
                                <?=$form->error($model,'floor_count'); ?>
                        </div>
                        <div class="row mb10">
                            <?
                                $cachedUsers = City::listData();
                                $dependOptions = City::listDataCustom(false, false, ['depend' => true]);
                            ?>
                            <?=$form->labelEx($model, 'id_city')?>
                            <?=$form->dropDownList($model, 'id_city', $cachedUsers, [
                                        'empty'       => 'Район',
                                        'id'          => 'Home_update_id_city',
                                        'data-width'  => '100%',  
                                        'data-depend' =>'city',
                                        'data-search' => true,
                                        'data-filter' => true, 
                                        'options'     => $dependOptions
                                    ] );?>
                            <?=$form->error($model,'id_city')?>
                        </div>
                    </div>   
                    <div class="fl mb10 w100p">
                        <?=$form->hiddenField($model,'lng',array('type'=>"hidden", "id" => "Home_update_lng"), $model->lng); ?>
                        <?=$form->hiddenField($model,'ltd',array('type'=>"hidden", "id" => "Home_update_lat"), $model->ltd); ?>
                        <div id="map_update"></div>
                    </div> 
                    <div class="fl mb10 w100p">
                            <?=$form->labelEx($model,'title'); ?>
                            <?=$form->textField($model, 'title', array('class' => 'w100p')); ?>
                            <?=$form->error($model,'title'); ?>
                    </div>                      
                    <div class="fl mb10 w100p">
                            <?=$form->labelEx($model,'description'); ?>
                            <?=$form->textArea($model, 'description', array('class' => 'w100p h100')); ?>
                            <?=$form->error($model,'description'); ?>
                    </div>
                    <div class="fl mb10 w100p">
                    <? 
                        $params = "parts";
                        $files = $model->photos;
                        $model->id = 0;
                        $showTypeLink = false;
                        $this->widget('Uploader', compact('files', 'model', 'params', 'showTypeLink'));
                    ?>
                    </div>
                    
                    <div class="fl mb10 w100p">
                        <!-- Сохранить -->
                        <?$this->widget('UIButtons', ['buttons' => [
                                'custom' => [
                                'value'     => 'Сохранить',
                                'icon'      => '',
                                'options'   => [
                                    'class'         => 'btn btn-primary btn-lg bg-blue w200 fr',
                                    'data-form'     => 'update-statement',
                                    'type'          => 'submit',
                                    'onclick'       => 'UIButtons.save(this); return false;'
                                ]
                            ]]
                        ]);?>
                    </div>
                    
                </div> 
            </div>
        </div>
    <?
        $this->endWidget();
    ?>
</div>