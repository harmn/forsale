<div class="wrapper parrentDiv">
    <?=CHtml::hiddenField('' , $model->id, array('id' => 'idGlobal'));?>

<?php 
    $form = $this->beginWidget('SActiveForm',[
        'id'    =>'update-statement',
        'modal' => false,
        'action' => ['update'],
        'enableAjaxValidation' => true,
        'htmlOptions' => [
            'class' => 'mt30 w800'
        ],
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
    ]);
?>
   <div class="body">
        <div class="grid simple mb0"> 
            <div class="grid-body pb10 no-border clearfix">
                <div class="fl" style="width: 300px;margin-left: 15px;">
                    <?=$form->hiddenField($model,'id',array('type'=>"hidden"), $model->id); ?>
                    <?=CHtml::hiddenField('modelName' , $type);?>
                    <?=$form->hiddenField($model,'active',array('type'=>"hidden", "value" => "1")); ?>
                    <div class="row mb10">
                            <?=$form->dropDownList($model, 'type', Listing::getWorkType(), [
                                        'empty'         => t("front", "Тип"),
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'type')?>
                    </div>
                    <div class="row mb10 fl mr10">
                            <?=$form->textField($model,'age_from', ['style' => 'width: 160px', 'placeholder' => t("front", "Возраст от")]); ?>
                            <?=$form->error($model,'age_from')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->textField($model,'age_to', ['style' => 'width: 160px', 'placeholder' => t("front", "Возраст до")]); ?>
                            <?=$form->error($model,'age_to')?>
                    </div>
                    <div class="row mb10">
                        <?=$form->dropDownList($model, 'sex', Listing::getWorkSexSelect(), [
                                    'empty'         => t("front", "Пол"),
                                    'data-width'    => '100%',
                                ] );?>
                        <?=$form->error($model,'sex')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->textField($model,'experience', ['class' => 'w100p', 'placeholder' => t("front", "Опыт работы")]); ?>
                            <?=$form->error($model,'experience'); ?>
                    </div>
                    <div class="row mb10">
                            <?php 
                                $dependOptionsRegion = Region::listDataCustom(false, false, ['depend' => true]);
                            ?>
                            <?=$form->dropDownList($model, 'id_region', Region::listData(), [
                                        'empty'        => t("front", "Регион"),
                                        'id'          => 'Work_update_id_region',
                                        'class'        => 'regionAddress',
                                        'data-search' => true,
                                        'data-filter' => true, 
                                        'data-width'    => '100%', 
                                        'data-depend'=>'job',
                                        'options'     => $dependOptionsRegion
                                    ] );?>
                            <?=$form->error($model,'id_region')?>
                    </div>
                </div>    
                <div class="fr mb10" style="width: 300px;margin-right: 15px;">
                    <div class="row mb10">
                            <?=$form->dropDownList($model, 'graph', Listing::getWorkGraphSelect(), [
                                        'empty'         => t("front", "График"),
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'graph')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->dropDownList($model, 'education', Listing::getWorkEducationSelect(), [
                                        'empty'         => t("front", "Oбразование"),
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'education')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->dropDownList($model, 'languages', Languages::listData(), [
                                        'title'         => t("front", "Языки"),
                                        'data-width'    => '100%',
                                        'multiple' => 'multiple'
                                    ] );?>
                            <?=$form->error($model,'languages')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->textField($model,'salary', ['class' => 'w100p', 'placeholder' => t("front", "Зарплата")]); ?>
                            <?=$form->error($model,'salary'); ?>
                    </div>
                    <div class="row mb10">
                        <?=$form->dropDownList($model, 'valuta', Technics::$valuta_select, [
                                    'empty'         => t("front", "Валюта"),
                                    'data-width'    => '100%',
                                ] );?>
                        <?=$form->error($model,'valuta')?>
                    </div>
                    <div class="row mb10">
                        <?
                            $cachedUsers = City::listData();
                            $dependOptions = City::listDataCustom(false, false, ['depend' => true]);
                        ?>
                        <?=$form->dropDownList($model, 'id_city', $cachedUsers, [
                                    'empty'       =>  t("front", "Город/Район"),
                                    'id'          => 'Work_update_id_city',
                                    'class'       => 'cityAddress',
                                    'data-width'    => '100%',  
                                    'data-depend' =>'city',
                                    'data-search' => true,
                                    'data-filter' => true, 
                                    'options'     => $dependOptions
                                ] );?>
                        <?=$form->error($model,'id_city')?>
                    </div>
                </div>
                <div class="fl mb10 w100p">
                    <?=$form->hiddenField($model,'lng',array('type'=>"hidden", "id" => "Work_update_lng", "class" => "lngAddress"), $model->lng); ?>
                    <?=$form->hiddenField($model,'ltd',array('type'=>"hidden", "id" => "Work_update_lat", "class" => "latAddress"), $model->ltd); ?>
                    <div id="map_update"></div>
                </div>
                <div class="fl mb20 w100p">
                        <?=$form->textField($model, 'title', array('class' => 'w100p', 'placeholder' => t("front", "Название"))); ?>
                        <?=$form->error($model,'title'); ?>
                </div>             
                <div class="fl mb10 w100p">
                        <?=$form->textArea($model, 'description', array('class' => 'w100p h150', 'placeholder' => t("front", "Описание"))); ?>
                        <?=$form->error($model,'description'); ?>
                </div>    
                <div class="fl mb10 w100p">
                <? 
                    $params = "parts";
                    $files = $model->photos;
                    $model->id = "0";
                    $showTypeLink = false;
                    $this->widget('Uploader', compact('files', 'model', 'params', 'showTypeLink'));
                ?>
                </div>
                <div class="fl mb10 w100p">
                        <!-- Сохранить -->
                        <?$this->widget('UIButtons', ['buttons' => [
                                'custom' => [
                                'value'     => t("front", "Сохранить"),
                                'icon'      => '',
                                'options'   => [
                                    'class'         => 'btn btn-submit btn-icon fr c-white bg-red uppercase fsize14 w100p',
                                    'data-form'     => 'update-statement',
                                    'type'          => 'submit',
                                    'onclick'       => 'UIButtons.save(this); return false;'
                                ]
                            ]]
                        ]);?>
                </div>
            </div> 
        </div>
    </div>

<?
    $this->endWidget();
?>
</div>