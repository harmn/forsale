<?php 
	$controller = ($this instanceof BaseController) ? $this : $this->controller;
	$photos = $data->photos;
	$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";

	if($photos){ 
		$src = $imageUrl.$data->relatedphotos[0]->id_model."/"."customBig"."/".$photos[0]->filename;
	}else{ 
		$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
	} 

	$favorites = [];
	if($data->favorites && $data->favorites[0]){
		$favorites[] = $data->favorites[0]->attributes['type'];
		if(isset($data->favorites[1])){
			$favorites[] = $data->favorites[1]->attributes['type'];
		}
	}

	$favoritChecker = ($favorites && in_array('1', $favorites)) ? " activeFavorit" : "";
	$laterChecker = ($favorites && in_array('2', $favorites)) ? " activeFavorit" : "";
?>
<li class="view rel pointer thumbLink" data-href="<?=url('advert/item', ['id' => $data->id, 'type' => get_class($model)])?>" data-model="<?=get_class($model);?>" data-id="<?=$data->id;?>">
	<div class="view-back link-to-item" data-title="" data-model="<?=get_class($model);?>" data-id="<?=$data->id;?>" href="<?=url('Site/getItem')?>">
		<span data-icon="A">
			<?=CHtml::link('<i class="fa fa-heart"></i>', ['/account/favorites'], ["class" => "pointer c-gray border0 favorites ".$favoritChecker, "style" => "position: relative; right: 0px;"])?>
		</span>
		<span data-icon="B">
			<?=CHtml::link('<i class="fa fa-clock-o"></i>', ['/account/later'], ["class" => "pointer c-gray border0 fsize16 later ".$laterChecker, "style" => "position: relative; right: 2px;"])?>
		</span>
		<a href="#" class="quick-view">&rarr;</a>
		<img src="<?=$src;?>">
	</div>
	<div class="infoPart">
		<div class="mt10 fsize20 "><?=(strlen($data->title) > 20) ? mb_substr($data->title,0,20) . "..." : $data->title;?></div>
		<div class="mt10 fsize18 c-blue">
			<?=(!isset($data->salary)) ? ($data->price > 0) ? $data->price : t('front', 'Без цены') : $data->salary?> 
			<?php 
				if($data->valuta && (isset($data->salary) AND $data->salary > 0 OR isset($data->price) AND $data->price > 0)) echo Home::$valuta_select[$data->valuta];
			?>
		</div>
	</div>
</li>




