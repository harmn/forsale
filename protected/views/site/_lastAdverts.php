<div class="hits mt30">
	<h2 class="title"><a href="#"><?=t('front', 'Последние');?></a></h2>

	<ul id="myTab" class="nav nav-tabs">
		<li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-home c-gray mr5"></i><?=t('front', 'Дом');?></a></li>
        <li ><a href="#tab2" data-toggle="tab"><i class="fa fa-car c-gray mr5"></i><?=t('front', 'Авто');?></a></li>
        <li><a href="#tab3" data-toggle="tab"><i class="fa fa-mobile fsize-18 c-gray mr5"></i><?=t('front', 'Электроника');?></a></li>
        <li><a href="#tab4" data-toggle="tab"><i class="fa fa-hdd-o c-gray mr5"></i><?=t('front', 'Техника');?></a></li>
        <li><a href="#tab5" data-toggle="tab"><i class="fa fa-female c-gray mr5"></i><?=t('front', 'Мода');?></a></li>
        <li><a href="#tab6" data-toggle="tab"><i class="fa fa-briefcase c-gray mr5"></i><?=t('front', 'Работа');?></a></li>
        <li><a href="#tab7" data-toggle="tab"><i class="fa fa-umbrella c-gray mr5"></i><?=t('front', 'Службы');?></a></li>
	</ul>
   	
	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade active in carousel mt10" id="tab1">
			<ul class="item-list thumbnails">
				<?php 
					if($advertsList['Home'])
					foreach($advertsList['Home'] as $v){
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];

							if(isset($v->filename) && $v->filename){ 
								$src = $imageUrl.$v->id_item."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>

		<div class="tab-pane fade  in carousel mt10" id="tab2">
			<ul class="item-list thumbnails">
				<?php 
					if(isset($advertsList['Car']) && $advertsList['Car'])
					foreach($advertsList['Car'] as $v){ 
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];

							if(isset($v->filename) && $v->filename){  
								$src = $imageUrl.$v->id_model."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>

		<div class="tab-pane tab-pane fade  in carousel mt10" id="tab3">
			<ul class="item-list thumbnails">
				<?php 
					if(isset($advertsList['Electronics']) && $advertsList['Electronics'])
					foreach($advertsList['Electronics'] as $v){ 
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];

							if(isset($v->filename) && $v->filename){  
								$src = $imageUrl.$v->id_model."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>

		<div class="tab-pane tab-pane fade  in carousel mt10" id="tab4">
			<ul class="item-list thumbnails">
				<?php 
					if(isset($advertsList['Technics']) && $advertsList['Technics'])
					foreach($advertsList['Technics'] as $v){ 
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];

							if(isset($v->filename) && $v->filename){ 
								$src = $imageUrl.$v->id_model."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>
		<div class="tab-pane tab-pane fade  in carousel mt10" id="tab5">
			<ul class="item-list thumbnails">
				<?php 
					if(isset($advertsList['Fashion']) && $advertsList['Fashion'])
					foreach($advertsList['Fashion'] as $v){ 
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];

							if(isset($v->filename) && $v->filename){ 
								$src = $imageUrl.$v->id_model."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>
		<div class="tab-pane tab-pane fade  in carousel mt10" id="tab6">
			<ul class="item-list thumbnails">
				<?php 
					if(isset($advertsList['Work']) && $advertsList['Work'])
					foreach($advertsList['Work'] as $v){ 
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];

							if(isset($v->filename) && $v->filename){  
								$src = $imageUrl.$v->id_model."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>
		<div class="tab-pane tab-pane fade  in carousel mt10" id="tab7">
			<ul class="item-list thumbnails">
				<?php 
					if(isset($advertsList['Services']) && $advertsList['Services'])
					foreach($advertsList['Services'] as $v){ 
							$imageUrl = Yii::app()->baseUrl."/"."storage/images/parts"."/";
							$favorites = [];
							if(isset($v['favorites']) && $v['favorites']){
								$favorites = $v['favorites'];
							}
							$v = $v['data'];
							
							if(isset($v->filename) && $v->filename){ 
								$src = $imageUrl.$v->id_model."/"."customBig"."/".$v->filename;
							}else{ 
								$src = $this->assetsUrl.'/images/nophoto/rsz_big.png';
							} 
						?>
							<li class="span3" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>">
								<div class="thumbnail rel">
									<a href="<?=url('advert/item', ['id' => $v->id_item, 'type' => $v->model])?>">
										<img src="<?=$src;?>">
									</a>
									<div class="functional-buttons link-to-item" data-model="<?=$v->model;?>" data-id="<?=$v->id_item;?>" href="<?=url('Site/getItem')?>">
										<a href="#" class="quick-view border0"><i class="fa fa-search-plus mr5 first "></i><?=t('front', 'Быстрый просмотр');?></a>
										<a href="#" class="fa fa-heart mr5 c-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
										<a href="#" class="fa fa-clock-o c-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
									</div>
								</div>
								<div class="caption tcenter">
									<span class="fsize16 c-dark-gray"><?=(strlen($v->title) > 20) ? mb_substr($v->title,0,20) . "..." : $v->title;?></span>
									<div class="mt5 c-red fsize16">
										<?=(!isset($v->salary)) ? ($v->price > 0) ? $v->price : t('front', 'Без цены') : $v->salary?> 
										<?php 
											if($v->valuta && (isset($v->salary) AND $v->salary > 0 OR isset($v->price) AND $v->price > 0)) echo Home::$valuta_select[$v->valuta];
										?>
									</div>
								</div>
							</li>
				<?php	}		?>
			</ul>
		</div>
	</div>
</div>