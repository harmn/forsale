<?php 
    $modelName = get_class($model);
?>
<div class="tab-content">
    <div class="tab-pane <?=($modelName == "" || $modelName == "Home") ? "active" : ""; ?>" id="home">
          <?$this->renderPartial('filters/_home')?>
    </div>
    <div class="tab-pane <?=($modelName == "Car") ? "active" : ""; ?>" id="Car">
         <?$this->renderPartial('filters/_car')?>
    </div>
    <div class="tab-pane <?=($modelName == "Electronics") ? "active" : ""; ?>" id="Electronics">
        <?$this->renderPartial('filters/_electronics')?>
    </div>
    <div class="tab-pane <?=($modelName == "Technics") ? "active" : ""; ?>" id="Technics">
        <?$this->renderPartial('filters/_technics')?>
    </div>
    <div class="tab-pane <?=($modelName == "Furniture") ? "active" : ""; ?>" id="Furniture">
        <?$this->renderPartial('filters/_furniture')?>
    </div>
    <div class="tab-pane <?=($modelName == "Fashon") ? "active" : ""; ?>" id="Fashon">
        <?$this->renderPartial('filters/_fashion')?>
    </div>
    <div class="tab-pane <?=($modelName == "Sport") ? "active" : ""; ?>" id="Sport">
        <?$this->renderPartial('filters/_sport')?>
    </div>
    <div class="tab-pane <?=($modelName == "Animal") ? "active" : ""; ?>" id="Animal">
        <?$this->renderPartial('filters/_animal')?>
    </div>
    <div class="tab-pane <?=($modelName == "Work") ? "active" : ""; ?>" id="Work">
        <?$this->renderPartial('filters/_work')?>
    </div>
    <div class="tab-pane <?=($modelName == "Services") ? "active" : ""; ?>" id="Services">
        <?$this->renderPartial('filters/_services')?>
    </div>
</div>