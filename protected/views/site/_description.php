<?php 
	$photos = $data->photos; 
	$imageUrl = Yii::app()->baseUrl.DS.'storage/images/parts'.DS;
	$favorites = [];
	if($data->favorites && $data->favorites[0]){
		$favorites[] = $data->favorites[0]->attributes['type'];
		if(isset($data->favorites[1])){
			$favorites[] = $data->favorites[1]->attributes['type'];
		}
	}
?>

<li class="clearfix rel" data-model="<?=get_class($model);?>" data-id="<?=$data->id;?>">
	<div class="fl w150 datePart rel">
		<div class="fl mt10 ml1"><i class="fa fa-calendar-o mr5"></i><?=date("d.m.Y", strtotime($data->created));?></div>
	</div>
	<a href="<?=url('advert/item', ['id' => $data->id, 'type' => get_class($model)])?>" class="fl w200">
		<?if($photos){ ?>
			<img src="<?=$imageUrl.$data->relatedphotos[0]->id_model.DS."custom".DS.$photos[0]->filename?>">
		<? }else{ ?>
			<img src="<?=$this->assetsUrl.'/images/nophoto/big.png'?>">
		<? } ?>
	</a>

	<div class="fl">
		<div class="fl ml10 mt10 w340">
			<div class="fsize22 bold c-dark-gray"><?=(strlen($data->title) > 20) ? mb_substr($data->title,0,20) . "..." : $data->title;?></div>
			<div class="mt30 "><?=mb_substr($data->description,0,40) . "...";?></div>
			<div class="mt30">
				<a href="#" class="fa fa-heart mr5 c-main-gray fsize18 transition favorites <?=($favorites && in_array('1', $favorites)) ? "activeFavorit" : ""?>"></a>
				<a href="#" class="fa fa-clock-o c-main-gray fsize18 transition later <?=($favorites && in_array('2', $favorites)) ? "activeFavorit" : ""?>"></a>
			</div>
		</div>
		<div class="fl w250 pricePart rel"> 
			<div class="fl mt10 bold c-red">
				<?=(!isset($data->salary)) ? ($data->price > 0) ? $data->price : t('front', 'Без цены') : $data->salary?> 
				<?php 
					if($data->valuta && (isset($data->salary) AND $data->salary > 0 OR isset($data->price) AND $data->price > 0)) echo Home::$valuta_select[$data->valuta];
				?>
			</div>
		</div>
		<div class="fl fastView link-to-item" data-title="" data-model="<?=get_class($model);?>" data-id="<?=$data->id;?>" href="<?=url('Site/getItem')?>">
			<a href="#" class="quick-view">&rarr;</a>
		</div>
	</div>
</li>
