<div class="wrapper mt20 mb20 rel">
	<div id="contactInfoBlock">
		<div>
			<img src="<?=$this->assetsUrl?>/images/placeholders/book.jpg" alt="" height="50">
		</div>
		<div id="infoDivContacts">
			<span class="bold block mb10 fsize14"><i class="fa fa-phone fsize18 c-blue mr10"></i>(+374 55) 108 180</span>
			<span class="bold block fsize14"><i class="fa fa-envelope c-blue fsize16 mr10"></i>info@forsale.am</span>
			<div class="socialBlock mt20">	
				<span><?=t('front', 'Следите за нами:');?> </span>
				<ul class="social fl">
					<li><a href="#" class="fa fa-facebook"></a></li>
					<li><a href="#" class="fa fa-twitter"></a></li>
					<li><a href="#" class="fa fa-vk fsize13"></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="contactInfoFormBlock" class="rel"> 
		<div>
			<img src="<?=$this->assetsUrl?>/images/placeholders/mail.jpg" alt="" height="50">
		</div>
		<div class="formDiv">
			<?php 
			if(isset($action) && $action == 'sendedMail'){ ?>
					<div class="clearfix mt20 mb10 bold">
						<p><?=t("front", "Ваше письмо отправлено");?></p>
					</div>
			<?php }else{

				$form = $this->beginWidget('SActiveForm', array(
					'id' => 'sendMail-form',
					'action' => ['sendMail'],
					'enableAjaxValidation' => true,
					'clientOptions' => [
						'validateOnSubmit' => true,
						'validateOnChange' => true,
					],
				)); 
			?>
				<div class="clearfix mt20">
					<div class="clearfix mb10">
						<div class="fl mr20" style="width:31%">
							<div class="inputDiv">
								<?=$form->textField($model, 'name',['class' => 'form-control big', 'placeholder'=> t('front', 'Имя')])?>
							</div>
							<?=$form->error($model, 'name')?>
						</div>

						<div class="fl mr20" style="width:31%">
							<div class="inputDiv">
								<?=$form->textField($model, 'email', ['class' => 'form-control big', 'placeholder'=> t('front', 'E-mail')])?>
							</div>
							<?=$form->error($model, 'email')?>
						</div>
					
						<div class="fl " style="width:31%">
							<div class="inputDiv">
								<?=$form->textField($model, 'title', ['class' => 'form-control big', 'placeholder'=> t('front', 'Тема')])?>
							</div>
							<?=$form->error($model, 'title')?>
						</div>
					</div>
					<div class="clearfix mb10">
						<?=$form->textArea($model, 'comment'); ?>
					</div>

					<?$this->widget('UIButtons', ['buttons' => [
						'custom' => [
							'value'		=> t('front', 'Отправить'),
							'icon'		=> '',
							'options'	=> [
								'class'			=> 'btn btn-danger bg-red block fr w20p',
								'data-form' 	=> 'sendMail-form',
								'type'			=> 'submit',
								'onclick'		=> 'UIButtons.save(this); return false;'
							]
						]]
					]);?>
					<?php $this->endWidget(); ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>