<? 
$this->renderPartial('_itemModal');

$this->beginWidget('UITabs', [
    'paramName' => 'type',
    'tabs' => $this->tabs,
]); ?>
    <div class="wrapper row mb50">
        <?=$this->renderPartial('_filterMenu', compact('model'));?>
        <div class="col-md-10  mb20 pr0">
            <?=$this->renderPartial('_filters', compact('model'));?>
            <div class="advert-list mb20">
                <?=(isset($provider) && $provider) ? $this->renderPartial('_list', compact('provider', 'model')) : '';?>
            </div>
            <div class="mb50">
                <?=$this->renderPartial('_lastAdverts', compact('model', 'advertsList'));?> 
            </div>
            <div class="clearfix row infoPart">
                <?=$this->renderPartial('_info', compact('model'));?>
            </div>
        </div>
    </div>
<? $this->endWidget(); ?>
