<?php 
   $modelName = get_class($model);
?>
<div class="col-md-2 pl0">
  <!-- tabs left -->
  <div class="tabbable tabs-left">
    <ul class="left-menu nav nav-tabs">
        <li class="blue <?=($modelName == "" || $modelName == "Home") ? "active" : ""; ?>"><a href="#home" data-toggle="tab"><?=t('front', 'Дом');?><i class="fa fa-home c-white fr fsize14 mt5"></i></a></li>
        <li class="red <?=($modelName == "Car") ? "active" : ""; ?>"><a href="#Car" data-toggle="tab"><?=t('front', 'Авто');?><i class="fa fa-car c-white fr fsize14 mt5"></i></a></li>
        <li class="yellow <?=($modelName == "Electronics") ? "active" : ""; ?>"><a href="#Electronics" data-toggle="tab"><?=t('front', 'Электроника');?><i class="fa fa-mobile c-white fr fsize18 mt5"></i></a></li>
        <li class="green <?=($modelName == "Technics") ? "active" : ""; ?>"><a href="#Technics" data-toggle="tab"><?=t('front', 'Техника');?><i class="fa fa-hdd-o c-white fr fsize14 mt5"></i></a></li>
        <li class="blue <?=($modelName == "Furniture") ? "active" : ""; ?>"><a href="#Furniture" data-toggle="tab"><?=t('front', 'Мебель');?><i class="fa fa fa-bed c-white fr fsize14 mt5"></i></a></li>
        <li class="red <?=($modelName == "Fashon") ? "active" : ""; ?>"><a href="#Fashon" data-toggle="tab"><?=t('front', 'Мода');?><i class="fa fa-female c-white fr fsize14 mt5"></i></a></li>
        <li class="yellow <?=($modelName == "Sport") ? "active" : ""; ?>"><a href="#Sport" data-toggle="tab"><?=t('front', 'Спорт и Игры');?><i class="fa fa-bicycle c-white fr fsize14 mt5"></i></a></li>
        <li class="green <?=($modelName == "Animal") ? "active" : ""; ?>"><a href="#Animal" data-toggle="tab"><?=t('front', 'Животное и растения');?><i class="fa fa-paw c-white fr fsize14 mt5"></i></a></li>
        <li class="blue <?=($modelName == "Work") ? "active" : ""; ?>"><a href="#Work" data-toggle="tab"><?=t('front', 'Работа');?><i class="fa fa-briefcase c-white fr fsize14 mt5"></i></a></li>
        <li class="red <?=($modelName == "Services") ? "active" : ""; ?>"><a href="#Services" data-toggle="tab"><?=t('front', 'Службы');?><i class="fa fa-umbrella c-white fr fsize14 mt5"></i></a></li>
    </ul>
  </div>
  <!-- /tabs -->
</div>

