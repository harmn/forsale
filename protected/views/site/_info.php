<div class="col-sm-8">
	<dl class="accordion">
		<dt class="active"><i></i><?=t('front', 'Для чего создан сайт ForSale.am ?');?></dt>
			<dd style="display: block;"><?=t('front', 'ForSale.am это сайт для каждого, кому необходимо продать или купить недвижимость, транспортное средство или что-то другое, и хочет быстро и просто получить всю необходимую информацию.');?></dd>
		<dt><i></i><?=t('front', 'Как подать объявление ?');?></dt>
			<dd ><?=t('front', 'Вы хотите подать объявление о продаже, обмене или аренде  и не знаете с чего начать? Начните с регистрации на ForSale.am! Мы постарались сделать так, чтобы процесс создания объявления был для Вас максимально быстр, прост и понятен.');?></dd>
		<dt><i></i><?=t('front', 'Наши преимущества');?></dt>
			<dd ><?=t('front', 'Мы поддерживаем базу в актуальном состоянии. Мы сделали удобный сайт. Совершенствуются и успешно внедряются полезные сервисы для продавцов, арендодателей и для покупателей.');?></dd>
		<dt><i></i><?=t('front', 'Дополнительные услуги');?></dt>
			<dd ><?=t('front', 'Вы можете поставить рекламу на нашем сайте.');?></dd>
	</dl>
</div>
<div class="col-sm-4">
	<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FForSaleam%2F1601694066728233%3Fref%3Daymt_homepage_panel&amp;width&amp;height=250&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=472794509408769" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:250px;" allowTransparency="true"></iframe>
</div>