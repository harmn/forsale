<?php
$modal = $this->beginWidget('UIModal',[
	 	'id' => 'addListing-modal',
		'width' => 450,
		'title' => t('front', 'Добавить'),
		'draggable' => false,
 	]);
	?>

<div class="add-listing-category">
	<div class="addListBlock red"  data-model="Home">
		<i class="fa fa-home fl fsize22"></i>
		<?=t('front', 'Дом');?>
	</div>

	<div class="addListBlock yellow" data-model="Car">
		<i class="fa fa-car fl fsize22"></i>
		<?=t('front', 'Авто');?>
	</div>
	
	<div class="addListBlock green" data-model="Electronics">
		<i class="fa fa-mobile fl fsize28"></i>
		<?=t('front', 'Электроника');?>
	</div>

	<div class="addListBlock blue" data-model="Technics">
		<i class="fa fa-hdd-o fl fsize22"></i>
		<?=t('front', 'Техника');?>
	</div>

	<div class="addListBlock red" data-model="Furniture">
		<i class="fa fa-bed fl fsize22"></i>
		<?=t('front', 'Мебель');?>
	</div>

	<div class="addListBlock yellow" data-model="Fashion">
		<i class="fa fa-female fl fsize22"></i>
		<?=t('front', 'Мода');?>
	</div>

	<div class="addListBlock green" data-model="Sport">
		<i class="fa fa-bicycle fl fsize22"></i>
		<?=t('front', 'Спорт и Игры');?>
	</div>

	<div class="addListBlock blue" data-model="Animal">
		<i class="fa fa-paw fl fsize22"></i>
		<?=t('front', 'Животное и растения');?>
	</div>

	<div class="addListBlock red" data-model="Work">
		<i class="fa fa-briefcase fl fsize22"></i>
		<?=t('front', 'Работа');?>
	</div>

	<div class="addListBlock yellow" data-model="Services">
		<i class="fa fa-umbrella fl fsize22"></i>
		<?=t('front', 'Службы');?>
	</div>
</div>
<?$this->endWidget();?>
