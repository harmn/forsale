<section class="item" id="advert-gallery">
	<?
		$photos = $model->photos;
		if($photos){
			$imageUrl = Yii::app()->baseUrl.DS.'storage/images/parts'.DS.$model->relatedphotos[0]->id_model.DS;
		}
		$regionData = Region::listData();
		$cityData = City::listData();

		$favorites = [];
		if($model->favorites && $model->favorites[0]){
			$favorites[] = $model->favorites[0]->attributes['type'];
			if(isset($model->favorites[1])){
				$favorites[] = $model->favorites[1]->attributes['type'];
			}
		}

		$favoritChecker = ($favorites && in_array('1', $favorites)) ? " activeFavorit" : "";
		$laterChecker = ($favorites && in_array('2', $favorites)) ? " activeFavorit" : "";

		Yii::app()->clientScript->registerMetaTag($model->title, '', null, array('id'=>'meta_og_title', 'property' => 'og:title'), 'meta_og_title');
		Yii::app()->clientScript->registerMetaTag('ForSale', '', null, array('id'=>'meta_og_site_name', 'property' => 'og:site_name'), 'meta_og_site_name');
		Yii::app()->clientScript->registerMetaTag($model->description, '', null, array('id'=>'meta_og_description', 'property' => 'og:description'), 'meta_og_description');
	?>

	<div class="full-image">
		<div id="big-image" data-gal="single">
			<a href="#" 
				<?if($photos) { 
					echo "data-id='{$photos[0]->id}' ";
					echo 'class="zoom"'; 
				}?>>
				<?php if($photos){ 
						Yii::app()->clientScript->registerMetaTag($imageUrl."big".DS.$photos[0]->filename, '', null, array('id'=>'meta_og_image', 'property' => 'og:image'), 'meta_og_image');
					?>
					<img src="<?=$imageUrl."big".DS.$photos[0]->filename?>">
				<?php }else{ 
						Yii::app()->clientScript->registerMetaTag($this->assetsUrl.'/images/nophoto/big.png', '', null, array('id'=>'meta_og_image', 'property' => 'og:image'), 'meta_og_image');
					?>
					<img src="<?=$this->assetsUrl.'/images/nophoto/big.png'?>">
				<?php } ?>
				<div class="glass"></div>
			</a>
		</div>

		<div id="small-images" class="jThumbnailScroller">
		    <div class="jTscrollerContainer">
		        <div class="jTscroller">
					<? foreach ($photos as  $photo) :?>
						<a href="<?=$imageUrl."big".DS.$photo->filename?>" class="zoom"
							alt="<?=$photo->title?>" data-id="<?=$photo->id?>">
						<img src="<?=$imageUrl."small".DS.$photo->filename?>" 
							data-big="<?=$imageUrl."big".DS.$photo->filename?>"
							data-title="<?//=setActual($photo->title, $model->title, 'encode')?>"></a>
					<? endforeach ?>

					<?if(count($photos)<5) :?>
					<? for ($i =0; $i < 5-count($photos); $i++) :?>
						<a href="#"><img src="<?=$this->assetsUrl.'/images/nophoto/smallest.png'?>"></a>
					<? endfor ?>
					<? endif ?>
				</div>
			</div>
		</div>
		
	</div>

	<div class="fl ml20 item-info mr20">
		<div class="firstBlock clearfix">
			<div class="fl fsize22 bold c-dark-gray mb10 w85p"><?=$model->title?></div>
			<div class="fr">
				<ul>
					<li data-model="<?=get_class($model);?>" data-id="<?=$model->id;?>">
						<?=CHtml::link('<i class="fa fa-heart mr5"></i>', ['/account/favorites'], ["class" => "c-main-gray fsize18 transition favorites ".$favoritChecker])?>
						<?=CHtml::link('<i class="fa fa-clock-o"></i>', ['/account/later'], ["class" => "c-main-gray fsize18 transition later ".$laterChecker])?>
					</li>
				</ul>
			</div>
		</div>
		<div class="secondBlock bold clearfix">
			<div class="fl mt10 ml1"><i class="fa fa-calendar-o mr5"></i><?=date("d.m.Y", strtotime($model->created));?></div>
			<div class="fl mt10 ml20 rightBorder"><i class="fa fa-map-marker mr5"></i><span class="mr20"><?=($model->id_city) ? $cityData[$model->id_city] : 'Не выбрано';?>,<?=($model->id_region) ? $regionData[$model->id_region] : 'Не выбрано';?></span></div>
			<div class="fl mt10 ml20"><i class="fa fa-user mr5"></i><?=$model->id_creator ? User::getUserFromCache($model->id_creator) : "";?></div>
			<div class="fl mt10 ml20"><i class="fa fa-envelope  mr5"></i><?=($model->client) ? $model->client->email : "";?></div>
		</div>
		<div class="therdBlock">
			<div class="fl mt10 ml1"><?=mb_substr($model->description,0,300) . "...";?></div>
			<div class="phone"><i class="fa fa-phone mr5"></i><?=($model->client &&  $model->client->profile) ? $model->client->profile->phone : "";?></div>
		</div>
		<div class="firthBlock">
			<div class="fl mt10 fsize30 bold c-red">
				<?=(!isset($model->salary)) ? ($model->price > 0) ? $model->price : t('front', 'Без цены') : $model->salary?> 
				<?php 
					if($model->valuta && (isset($model->salary) AND $model->salary > 0 OR isset($model->price) AND $model->price > 0)) echo Home::$valuta_select[$model->valuta];
				?>
			</div>
			<div class="fr mt10">
				<div class="fl mt8 fsize14 mr10 c-main-gray"><?=t('front', 'Следите за нами:');?></div>
				<ul class="fr itemSocial">
					<li><a href="#" class="fa fa-facebook"></a></li>
					<li><a href="#" class="fa fa-twitter"></a></li>
					<li><a href="#" class="fa fa-vk fsize13"></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>