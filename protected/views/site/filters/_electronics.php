<? 
echo $this->widget("UIManyFilters", [
    'blocks'=>[
        'Электроника' => ['name'=>'frontend.electronics'],
    ], 
    'model' => new Electronics('search'),
    'class' => 'pull-left mt20 ',
    'onSearch' => 'function(form, url, data){
		$.fn.global("search", data);
	}'
], true); ?>