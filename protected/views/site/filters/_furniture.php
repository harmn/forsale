 <?php
echo $this->widget("UIManyFilters", [
    'blocks'=>[
        'Мебель' => ['name'=>'frontend.furniture'],
    ], 
    'model' => new Furniture('search'),
    'class' => 'pull-left mt20 ',
    'onSearch' => 'function(form, url, data){
		$.fn.global("search", data);
	}'
], true); ?>