<?php


return CMap::mergeArray(
    // наследуемся от main.php
    require(dirname(__FILE__).'/main.php'),

    array(
        'preload' => array(
            'debug'
        ),
        
        'components'=>array(

           // кеширование
            'cache'=>array('class'=>'CFileCache'),

            // кеширование сессий
            'sessionCache' => array('class' => 'CFileCache'),

            'session' => array(
                'class' =>'CCacheHttpSession',
                'cacheID' => 'sessionCache',
            ),

            //дебагер
            'debug' => array(
                'class' => 'core.extensions.yii2-debug.Yii2Debug',
            ),
            
            'db'=> array( 
                // настройка соединения с базой
                'connectionString' => 'mysql:host=localhost;dbname=bh57137_forsale',
                'username' => 'bh57137_master',
                'password' => 'fors108180',
                // включаем профайлер
                'enableProfiling'=>true,
                // показываем значения параметров
                'enableParamLogging' =>true,
            ), 

            'eauth' => array(
                'services' => array(
                    'facebook' => array(
                        'class' => 'CustomFacebookService',
                        'client_id' => '443241905775604', 
                        'client_secret' => 'bba1c81fab12562a4e223c38e1b5001e',
                        'scope' => 'email', 
                    )
                )
            ),

            'facebook'=>array(
                'class' => 'ext.facebook.SFacebook',
                'appId'=>'443241905775604', // needed for JS SDK, Social Plugins and PHP SDK
                'secret'=>'bba1c81fab12562a4e223c38e1b5001e', // needed for the PHP SDK
                //'fileUpload'=>false, // needed to support API POST requests which send files
                //'trustForwarded'=>false, // trust HTTP_X_FORWARDED_* headers ?
                //'locale'=>'en_US', // override locale setting (defaults to en_US)
                //'jsSdk'=>true, // don't include JS SDK
                //'async'=>true, // load JS SDK asynchronously
                //'jsCallback'=>false, // declare if you are going to be inserting any JS callbacks to the async JS SDK loader
                //'status'=>true, // JS SDK - check login status
                //'cookie'=>true, // JS SDK - enable cookies to allow the server to access the session
                //'oauth'=>true,  // JS SDK - enable OAuth 2.0
                //'xfbml'=>true,  // JS SDK - parse XFBML / html5 Social Plugins
                //'frictionlessRequests'=>true, // JS SDK - enable frictionless requests for request dialogs
                //'html5'=>true,  // use html5 Social Plugins instead of XFBML
                //'ogTags'=>array(  // set default OG tags
                    //'title'=>'MY_WEBSITE_NAME',
                    //'description'=>'MY_WEBSITE_DESCRIPTION',
                    //'image'=>'URL_TO_WEBSITE_LOGO',
                //),
            ),

            'mail' => array(
                "host" => "mail.forsale.am", //smtp сервер
                "debug" => 0, //отображение информации дебаггера (0 - нет вообще)
                "auth" => true, //сервер требует авторизации
                "port" => 25, //порт (по-умолчанию - 25)
                "secure" => "tls",
                "username" => "bh57137", //имя пользователя на сервере
                "password" => "m9Ja1kdDekHEFYm", //пароль
                "fromname" => "", //имя
                "from" => "info@forsale.am", //от кого
                "charset" => "utf-8",
                "layout" => "main"
            ),

            'messages'=>array(
                // 'onMissingTranslation' => array('MissingMessages', 'toTable')
            ),

            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'logFile'=> 'info.log',
                        'class'  => 'CFileLogRoute',
                        'levels' => 'info', // error, warning
                    ),
                    array(
                        'logFile'=> 'error.log',
                        'class'  => 'CFileLogRoute',
                        'levels' => 'error', // error, warning
                    ),
                    array(
                        'logFile'=> 'warning.log',
                        'class'  => 'CFileLogRoute',
                        'levels' => 'warning', // error, warning
                    ),
                ),
            ),

            'csv' => array(
                'class' => 'modules.market.components.CsvReader'
            ),
        ),

        'behaviors' => array(
            'core.components.urlManager.LanguageBehavior',
        ),
        
    )
);