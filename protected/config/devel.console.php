<?php

$files = glob(dirname(dirname(__FILE__)).'/modules/*/config.php');
$modulePaths = array();

foreach($files as $file){
    $name = preg_replace('#^.*/modules/([^\.]*)/config\.php$#', '$1', $file);
    $config = require($file);
    
    if(!isset($config['disabled']) || $config['disabled'] !== false){
        $modulePaths[$name] = "application.modules.{$name}.migrations";
    }
}


return CMap::mergeArray(
    // наследуемся от main.php
    require(dirname(__FILE__).'/main.php'),

    array(
        'components'=>array(

            'cache'=>array('class'=>'CFileCache'),

            'db'=> array( 
                // настройка соединения с базой
                'connectionString' => 'mysql:host=localhost;dbname=bh57137_forsale',
                'username' => 'bh57137_master',
                'password' => 'fors108180',
                // включаем профайлер
                'enableProfiling'=>true,
                // показываем значения параметров
                'enableParamLogging' =>true,
            ), 

            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'logFile'=> 'info.log',
                        'class'  => 'CFileLogRoute',
                        'levels' => 'info', 
                    ),
                    array(
                        'logFile'=> 'error.log',
                        'class'  => 'CFileLogRoute',
                        'levels' => 'error', 
                    ),
                    array(
                        'logFile'=> 'warning.log',
                        'class'  => 'CFileLogRoute',
                        'levels' => 'warning', 
                    ),
                ),
            ),
           
        ), 

        'commandMap' => array(
            
            'migrate' => array(
                // alias of the path where you extracted the zip file
                'class' => 'modules.core.components.migrate.EMigrateCommand',
                // this is the path where you want your core application migrations to be created
                'migrationPath' => 'application.db.migrations',
                // the name of the table created in your database to save versioning information
                'migrationTable' => 'tbl_migration',
                // the application migrations are in a pseudo-module called "core" by default
                'applicationModuleName' => 'root',
                // define all available modules
                // 'modulePaths' => $modulePaths,
                // // here you can configrue which modules should be active, you can disable a module by adding its name to this array
                // 'disabledModules' => array(
                //     'admin', 'anOtherModule', // ...
                // ),
                // // the name of the application component that should be used to connect to the database
                // 'connectionID'=>'db',
                // // alias of the template file used to create new migrations
                // 'templateFile'=>'application.db.migration_template',
            ),
        ),

        'params' => array(
            'upload_max_filesize' => '512M',
        )
        
    )
);