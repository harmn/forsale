<?php 

/**
 * Sendmail
 */
class SendContactMail extends Client {
	
	public $name, $title, $email, $comment;

	const SELF_EMAIL = 'info@forsale.am';

	/**
	 * Rules
	 * @return array
	 */
	public function rules(){
		return [
			//email
			array('email', 'email', 'message' => t('front', 'Некорректный формат эл. почты')),
			//filter trim
			array('name, title, comment', 'filter', 'filter'=>'trim'),
			//max, min
			array('name', 'length', 'max'=>50),

			['name', 'required', 'message' => Yii::t('front', 'Пожалуйста, введите Ваше имя')],
			['email', 'required', 'message' => Yii::t('front', 'Укажите эл. почту')],
			// ['id_user, id_advert, model, type', 'ECompositeUniqueValidator', 'message' => t('front', 'Объявление уже было добавлено')],
		];
	}

}
?>
