<?php 

/**
 * LoginForm
 */
class Listing extends AR{
	// home
	public static function getHomeCategory(){
		return [1 => t("front", "Продается"), 2 => t("front", "Обменивается"), 3 => t("front", "Аренда")];
	}

	public static function getHomeType(){
		return [1 => t("front", "Квартира"), 2 => t("front", "Офис"), 3=>t("front", "Гараж"), 4=>t("front", "Земельный участок"),  5=>t("front", "Дом")];
	}

	// car
	public static function getCarType(){
		return [1 => t("front", "Автомобиль"), 2 => t("front", "Автозапчасти"), 3=>t("front", "Аксессуары"), 4=>t("front", "Уникальная")];
	}

	public static function getConditionSelect(){
		return [1 => t("front", "Новое"), 2 => t("front", "С пробегом"), 3=>t("front", "Битые")];
	}

	public static function getTransmissionSelect(){
		return [1 => t("front", "Автомат"), 2 => t("front", "Механический"), 3=>t("front", "Типтроник")];
	}

	public static function getRudderSelect(){
		return [1 => t("front", "Левый"), 2 => t("front", "Правый")];
	}

	public static function getFuelsSelect(){
		return [1 => t("front", "Бензин"), 2 => t("front", "Дизель"), 3=>t("front", "Гибрид"), 4=>t("front", "Газ/Бензин")];
	}

	public static function getColorSelect(){
		return [	1 => t("front", "белый"), 
					2 => t("front", "желтый"), 
					3 => t("front", "зеленый"), 
					4 => t("front", "золотой"),
					5 => t("front", "коричневый"),
					6 => t("front", "красный"),
					7 => t("front", "серебристый"),
					8 => t("front", "серый"),
					9 => t("front", "синий"),
					10 => t("front", "фиолетовый"),
					11 => t("front", "черный"),
				];
	}

	public static function getBodySelect(){
		return [	1 => t("front", "седан"), 
					2 => t("front", "хэтчбек"), 
					3 => t("front", "универсал"), 
					4 => t("front", "внедорожник"),
					5 => t("front", "кроссовер"),
					6 => t("front", "пикап"),
					7 => t("front", "купе"),
					8 => t("front", "кабриолет"),
					9 => t("front", "минивен"),
					10 => t("front", "фургон"),
					11 => t("front", "микроавтобус"),
				];
	}

	// Electronics
	public static function getElectronicsType(){
		return [	1 => t("front", "Телефоны"), 
					2 => t("front", "Телефонные аксессуары"), 
					3 => t("front", "SIM карты"), 
					4 => t("front", "Ноутбуки"),
					5 => t("front", "Планшеты"),
					6 => t("front", "Компьютеры"),
					7 => t("front", "Компьютерные устройства"),
					8 => t("front", "Компьютерные аксессуары"),
					9 => t("front", "Телевизоры"),
					10 => t("front", "Другое"),
				];
	}

	public static function getElectronicsConditionSelect(){
		return [1 => t("front", "Новое"), 2 => t("front", "Использованные")];
	}

	// Technics
	public static function getTechnicsType(){
		return [	1 => t("front", "Холодильники"), 
					2 => t("front", "Газовые плиты"),
					3 => t("front", "Стиральные машины"), 
					4 => t("front", "Швейные машины"), 
					5 => t("front", "Пылесосы"),
					6 => t("front", "Кухонная мебель"),
					7 => t("front", "Утюги"),
					8 => t("front", "Кондиционеры и вентиляция"),
					9 => t("front", "Отопительные приборы"),
					10 => t("front", "Другое"),
				];
	}

	// Furniture
	public static function getFurnitureType(){
		return [	1 => t("front", "Кухонная мебель"), 
					2 => t("front", "Мебель для спальни"),
					3 => t("front", "Столы и стулья"),
					4 => t("front", "Шкафы"), 
					5 => t("front", "Диваны и кресла"), 
					6 => t("front", "Компьютерная мебель"),
					7 => t("front", "Подставки"),
					8 => t("front", "Тумбочки"),
					9 => t("front", "Другое"),
				];
	}

	// Fashion
	public static function getFashionType(){
		return [	1 => t("front", "Женская одежда"), 
					2 => t("front", "Женская обувь"),
					3 => t("front", "Мужская одежда"),
					4 => t("front", "Мужская обувь"),
					5 => t("front", "Детская одежда"), 
					6 => t("front", "Детская обувь"), 
					7 => t("front", "Косметика и бижутерия"),
					8 => t("front", "Духи"),
					9 => t("front", "Часы"),
					10 => t("front", "Сумки"),
					11 => t("front", "Товары для детей"),
					12 => t("front", "Красота и здоровье"),
					13 => t("front", "Другое"),
				];
	}

	// Sport
	public static function getSportType(){
		return [	1 => t("front", "Тренажёры"), 
					2 => t("front", "Детские спортивные комплексы"),
					3 => t("front", "Лыжи/Сноуборды и защита"),
					4 => t("front", "Обувь"),
					5 => t("front", "Одежда"), 
					6 => t("front", "Спортивное питание"), 
					7 => t("front", "Хоккей"),
					8 => t("front", "Велосипеды"),
					9 => t("front", "Гимнастика"),
					10 => t("front", "Единоборства"),
					11 => t("front", "Мячи"),
					12 => t("front", "Туризм"),
					13 => t("front", "Теннис"),
					14 => t("front", "Тяжелая атлетика"),
					15 => t("front", "Рюкзаки/Сумки"),
					16 => t("front", "Коньки/Ролики"),
					17 => t("front", "Санки"),
					18 => t("front", "Спортивные приборы"),
					19 => t("front", "Игри"),
					20 => t("front", "Другое"),
				];
	}


	// Animal
	public static function getAnimalType(){
		return [	1 => t("front", "Собаки"), 
					2 => t("front", "Кошки"),
					3 => t("front", "Рыбы"),
					4 => t("front", "Птицы"),
					5 => t("front", "Сельскохозяйственные животные"), 
					6 => t("front", "Все для животных"), 
					7 => t("front", "Ростения"),
					8 => t("front", "Другое"),
				];
	}

	// Work
	public static function getWorkType(){
		return [	1 => t("front", "Офисная работа"), 
					2 => t("front", "Торговля"),
					3 => t("front", "Финансы и право"),
					4 => t("front", "Информационные технологии"),
					5 => t("front", "Mедиа и дизайн"), 
					6 => t("front", "Рестораны и кухня"), 
					7 => t("front", "Туризм и отели"),
					8 => t("front", "Танспорт и такси"),
					9 => t("front", "Бизнес и маркетинг"),
					10 => t("front", "Строительство и архитектура"),
					11 => t("front", "Домашнее хозяйство"),
					12 => t("front", "Производство"),
					13 => t("front", "Образования"),
					14 => t("front", "Здравоохранения"),
					15 => t("front", "Другое"),
				];
	}

	public static function getWorkSexSelect(){
		return [1 => t("front", "Мужской"), 2 => t("front", "Женский")];
	}

	public static function getWorkGraphSelect(){
		return [1 => t("front", "Полный"), 2 => t("front", "Неполный")];
	}

	public static function getWorkEducationSelect(){
		return [	1 => t("front", "Профессиональное"), 
					2 => t("front", "Начальное"),
					3 => t("front", "Среднее"),
					4 => t("front", "Бакалавр"),
					5 => t("front", "Магистр"),
					6 => t("front", "Аспирант"),
					7 => t("front", "Кандидат"),
					8 => t("front", "Другое"),
				];
	}

	// Services
	public static function getServicesType(){
		return [	1 => t("front", "Строительство и ремонт"), 
					2 => t("front", "Транспорт"),
					3 => t("front", "Электроника"),
					4 => t("front", "Компьютеры и Интернет"),
					5 => t("front", "Образование"), 
					6 => t("front", "Торговые услуги"), 
					7 => t("front", "Бытовые услуги"),
					8 => t("front", "Красота и здоровье"),
					9 => t("front", "События и праздники"),
					10 => t("front", "Туризм и путешествия"),
					11 => t("front", "Медиа и дизайн"),
					12 => t("front", "Производство"),
					13 => t("front", "Здравоохранения"),
					14 => t("front", "Другое"),
				];
	}

	//  For Advert info
	public static function getAdvertFieldsByType($type){
		$result = 	[
						"Home" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeType(),
																				],
										t("front", "Количество комнат")		=> 	[
																					"name" => "room_count",
																					"is_select"	=> "0",
																				],
										t("front", "Плошадь")				=> 	[
																					"name" => "area",
																					"is_select"	=> "0",
																				],
										t("front", "Этаж")					=> 	[
																					"name" => "floor",
																					"is_select"	=> "0",
																				],
										t("front", "Количество этажей")		=> 	[
																					"name" => "floor_count",
																					"is_select"	=> "0",
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Car" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getCarType(),
																				],
										t("front", "Марка")					=> 	[
																					"name" => "brand",
																					"is_select"	=> "1",
																					"select"	=> Car::$brand_select
																				],
										t("front", "Состояние")				=> 	[
																					"name" => "condition",
																					"is_select"	=> "1",
																					"select"	=> Listing::getConditionSelect()
																				],	
										t("front", "Пробег")				=> 	[
																					"name" => "mileage",
																					"is_select"	=> "1",
																					"select"	=> Car::$mileage_select
																				],									
										t("front", "Кор/п")					=> 	[
																					"name" => "transmission",
																					"is_select"	=> "1",
																					"select"	=> Listing::getTransmissionSelect()
																				],
										t("front", "Кол/с")					=> 	[
																					"name" => "seats_count",
																					"is_select"	=> "1",
																					"select"	=> Car::$seats_count_select
																				],
										t("front", "Кузов")					=> 	[
																					"name" => "body",
																					"is_select"	=> "1",
																					"select"	=> Listing::getBodySelect()
																				],
										t("front", "Руль")					=> 	[
																					"name" => "rudder",
																					"is_select"	=> "1",
																					"select"	=> Listing::getRudderSelect()
																				],
										t("front", "Топливо")				=> 	[
																					"name" => "fuels",
																					"is_select"	=> "1",
																					"select"	=> Listing::getFuelsSelect()
																				],
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Electronics" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getElectronicsType(),
																				],
										t("front", "Состояние")				=> 	[
																					"name" => "condition",
																					"is_select"	=> "1",
																					"select"	=> Listing::getElectronicsConditionSelect()
																				],	
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Год выпуска")			=>	[
																					"name" => "release_year",
																					"is_select"	=> "0",
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Technics" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getTechnicsType(),
																				],
										t("front", "Состояние")				=> 	[
																					"name" => "condition",
																					"is_select"	=> "1",
																					"select"	=> Listing::getElectronicsConditionSelect()
																				],	
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Год выпуска")			=>	[
																					"name" => "release_year",
																					"is_select"	=> "0",
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Furniture" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getFurnitureType(),
																				],
										t("front", "Состояние")				=> 	[
																					"name" => "condition",
																					"is_select"	=> "1",
																					"select"	=> Listing::getElectronicsConditionSelect()
																				],	
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Год выпуска")			=>	[
																					"name" => "release_year",
																					"is_select"	=> "0",
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Fashion" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getFashionType(),
																				],
										t("front", "Состояние")				=> 	[
																					"name" => "condition",
																					"is_select"	=> "1",
																					"select"	=> Listing::getElectronicsConditionSelect()
																				],	
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Sport" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getSportType(),
																				],
										t("front", "Состояние")				=> 	[
																					"name" => "condition",
																					"is_select"	=> "1",
																					"select"	=> Listing::getElectronicsConditionSelect()
																				],	
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Animal" => 	[
										t("front", "Категория") 			=> 	[
																					"name" => "category",
																					"is_select"	=> "1",
																					"select" => Listing::getHomeCategory(),
																				],
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getAnimalType(),
																				],
										t("front", "Возраст")				=> 	[
																					"name" => "age",
																					"is_select"	=> "0",
																				],
										t("front", "Цвет")					=> 	[
																					"name" => "color",
																					"is_select"	=> "1",
																					"select"	=> Listing::getColorSelect()
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Work" => 	[
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" => Listing::getWorkType(),
																				],
										t("front", "Возраст от")			=> 	[
																					"name" => "age_from",
																					"is_select"	=> "0",
																				],
										t("front", "Возраст до")			=> 	[
																					"name" => "age_to",
																					"is_select"	=> "0",
																				],
										t("front", "Пол")					=> 	[
																					"name" => "sex",
																					"is_select"	=> "1",
																					"select"	=> Listing::getWorkSexSelect()
																				],
										t("front", "Опыт работы")			=> 	[
																					"name" => "experience",
																					"is_select"	=> "0",
																				],
										t("front", "График")				=> 	[
																					"name" => "graph",
																					"is_select"	=> "1",
																					"select"	=> Listing::getWorkGraphSelect()
																				],
										t("front", "Oбразование")			=> 	[
																					"name" => "education",
																					"is_select"	=> "1",
																					"select"	=> Listing::getWorkEducationSelect()
																				],
										t("front", "Языки")					=> 	[
																					"name" => "languages",
																					"is_select"	=> "1",
																					"select"	=> Languages::listData()
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
						"Services" => 	[
										t("front", "Тип") 					=> 	[
																					"name" => "type",
																					"is_select"	=> "1",
																					"select" =>Listing::getServicesType(),
																				],
										t("front", "Возраст от")			=> 	[
																					"name" => "age_from",
																					"is_select"	=> "0",
																				],
										t("front", "Возраст до")			=> 	[
																					"name" => "age_to",
																					"is_select"	=> "0",
																				],
										t("front", "Пол")					=> 	[
																					"name" => "sex",
																					"is_select"	=> "1",
																					"select"	=> Listing::getWorkSexSelect()
																				],
										t("front", "Опыт работы")			=> 	[
																					"name" => "experience",
																					"is_select"	=> "0",
																				],
										t("front", "Описание")				=> 	[
																					"name" => "description",
																					"is_select"	=> "0",
																				],
									],
					];


		return $result[$type];
	}
}
?>
