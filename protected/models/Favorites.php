<?php

/**
 * Favorites
 *
 * @property integer	$id
 * @property datetime	$id_advert
 */

class Favorites extends AR
{
	private static $myFavorites, $watchLaters; //для хранения кеша
	const FAVORITES   = 1; // type favorite
	const WATCH_LATER = 2; // type watch later
	const COOKIE_NAME_FAVORITE    = "favorite-adverts";
	const COOKIE_NAME_WATCH_LATER = "watch-later-adverts";

	const COOKIE_NAME_RESENTLY_VIEW_ITEMS 	= 'Recently-Viewed-Items';
	const RESENTLY_VIEW_ITEMS_LIMIT 		= 4;
	const RESENTLY_VIEW_ITEMS_TIME	 		= 17280; // 2 day

	public static $myAdvertRelations = 	[	
											1 => "Home", 
											2 => "Car",
											3 => "Electronics",
											4 => "Technics",
											5 => "Furniture",
											6 => "Fashion",
											7 => "Sport",
											8 => "Animal",
											9 => "Work",
											10 => "Services",
										];

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($classname = __CLASS__){
		return parent::model($classname);
	}

	/**
	 * @return string
	 */
	public function tableName(){
		return "favorites";
	}

	/**
	 * Rules
	 * @return array
	 */
	public function rules(){
		return [
			['id_user, type', 'numerical', 'integerOnly'=>true],
			// ['id_user, id_advert, model, type', 'ECompositeUniqueValidator', 'message' => t('front', 'Объявление уже было добавлено')],
		];
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations()
	{
		return 	[
					'Home' 			=> [self::HAS_MANY, 'Home', ['id' => 'id_advert'], 'on'=>'t.model = "Home"'],
					'Car' 	  		=> [self::HAS_MANY, 'Car', ['id' => 'id_advert'], 'on'=>'t.model = "Car"'],
					'Electronics' 	=> [self::HAS_MANY, 'Electronics', ['id' => 'id_advert'], 'on'=>'t.model = "Electronics"'],
					'Technics' 	  	=> [self::HAS_MANY, 'Technics', ['id' => 'id_advert'], 'on'=>'t.model = "Technics"'],
					'Furniture' 	=> [self::HAS_MANY, 'Furniture', ['id' => 'id_advert'], 'on'=>'t.model = "Furniture"'],
					'Fashion' 	  	=> [self::HAS_MANY, 'Fashion', ['id' => 'id_advert'], 'on'=>'t.model = "Fashion"'],
					'Sport' 	  	=> [self::HAS_MANY, 'Sport', ['id' => 'id_advert'], 'on'=>'t.model = "Sport"'],
					'Animal' 	  	=> [self::HAS_MANY, 'Animal', ['id' => 'id_advert'], 'on'=>'t.model = "Animal"'],
					'Work' 	  		=> [self::HAS_MANY, 'Work', ['id' => 'id_advert'], 'on'=>'t.model = "Work"'],
					'Services' 	  	=> [self::HAS_MANY, 'Services', ['id' => 'id_advert'], 'on'=>'t.model = "Services"'],
				];
	}

	/**
	 * AttributeLabels
	 * @return array
	 */
	public function attributeLabels(){
		return [];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => [
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			],
		));
	}

	public function searchMyFavorites(){
		if(self::$myFavorites) return self::$myFavorites;

		$cacheKey= "listdata.myFavorites".user()->id;

		if(self::$myFavorites = Yii::app()->cache->get($cacheKey))
			return self::$myFavorites;

		$criteria = new CDbCriteria;

		$criteria->with = ['Home', 'Car', 'Electronics', 'Technics', 'Furniture', 'Fashion', 'Sport', 'Animal', 'Work', 'Services'];

		$criteria->compare('t.id_user', user()->id);
		$criteria->compare('t.type', 1);
		
	  	$data = Favorites::model()->findAll($criteria);

	  	if($data){
  			$results = [];
		  	foreach($data as $item){
		  		$key = $item->model;
		  		if(isset($results[$key]) && !is_array($results[$key])){
		  			if($item->$key)
		  				$results[$key] = [];
		  		}
		  		if($item->$key)
		  			$results[$key][] = $item->$key;
		  	}
  			self::$myFavorites = $results;
	  	}else{
	  		self::$myFavorites = [];
	  	}
		Yii::app()->cache->set($cacheKey, self::$myFavorites, 60*60*24*30);

		return self::$myFavorites;
	}

	public function searchMyWatchLaters(){
		if(self::$watchLaters) return self::$watchLaters;

		$cacheKey= "listdata.watchLaters".user()->id;

		if(self::$watchLaters = Yii::app()->cache->get($cacheKey))
			return self::$watchLaters;

		$criteria = new CDbCriteria;

		$criteria->with = ['Home', 'Car', 'Electronics', 'Technics', 'Furniture', 'Fashion', 'Sport', 'Animal', 'Work', 'Services'];

		$criteria->compare('t.id_user', user()->id);
		$criteria->compare('t.type', 2);
		
	  	$data = Favorites::model()->findAll($criteria);

	  	if($data){
  			$results = [];
		  	foreach($data as $item){
		  		$key = $item->model;
		  		if(isset($results[$key]) && !is_array($results[$key])){
		  			if($item->$key)
		  				$results[$key] = [];
		  		}
		  		if($item->$key)
		  			$results[$key][] = $item->$key;
		  	}
  			self::$watchLaters = $results;
	  	}else{
	  		self::$watchLaters = [];
	  	}
		Yii::app()->cache->set($cacheKey, self::$watchLaters, 60*60*24*30);

		return self::$watchLaters;
	}
}
