<?php


/**
* AdvertController
*/

Yii::import('app.controllers.FrontController');
class AdvertController extends FrontController
{

	const PRICE_PERCENT = 20;
	const PRICE_MORE_PERCENT = 40;

	public static $pageTypes =  [
									"Home" => "Дом",
									"Car" => "Авто",
									"Electronics" => "Электроника",
									"Technics" => "Техника",
									"Furniture" => "Мебель",
									"Fashion" => "Мода",
									"Sport" => "Спорт и Игры",
									"Animal" => "Животное и растения",
									"Work" => "Работа",
									"Services" => "Службы",
								];

	public function init(){
		parent::init();
	 	cs()->registerCssFile($this->rootAssetsUrl.'/plugins/bottom-slider/css/style.css');
	 	cs()->registerScriptFile($this->assetsUrl.'/js/advert.js');
		cs()->registerScriptFile("http://maps.google.com/maps/api/js?sensor=false");
		// cs()->registerPackage('jalerts'); //alerts

		// ВЫ УЖЕ СМОТРЕЛИ widget //Для предварительной загрузки скриптов
		// RecentlyViewedItemsWidget::loadScriptFiles();
	}
	
	public function behaviors(){
		return [
			// // для работы Favorites widget
			// 'FavoriteActions',
			// // для работы recently viewed items widget
			// 'RecentlyViewedItemsActions',
			// для работы комментариев 
			'comment.widgets.behaviors.CommentActions'
		];
	}

	public function actionItem($id, $type){
		// ВЫ УЖЕ СМОТРЕЛИ
		// RecentlyViewedItemsActions::actionRecently($id);
		$model  = $this->loadModel($type, $id);
		
		$this->breadcrumbs[] = t('front', 'Объявления');
		$this->breadcrumbs[] = t('front', self::$pageTypes[$type]);

		// $this->breadcrumbs = array_reverse($this->breadcrumbs, true);

		$tabs['description'] = t('front', 'Описание');	
		// $tabs['reviews'] = 'Отзывы покупателей';
		$this->pageTitle   = t('front', self::$pageTypes[$type]);
		$this->tabs = $tabs; 
		$this->render('item', compact('model', 'type'));
	}

	//АЛЬТЕРНАТИВНЫЙ ВАРИАНТ
	public static function getAlternativeAdverts($id, $type, $limit = 4){

		$advert = $type::model()->findByPk($id);

		if(isset($advert->price)){
			$price = $advert->price;
			$up = $price + ($price/100)*self::PRICE_PERCENT;
			$down = $price - ($price/100)*self::PRICE_PERCENT;
		}else if(isset($advert->salary)){
			$salary = $advert->salary;
			$up = $salary + ($salary/100)*self::PRICE_PERCENT;
			$down = $salary - ($salary/100)*self::PRICE_PERCENT;
		}


		$criteria = new SDbCriteria;
		$criteria->addNotInCondition('id', [$id]);
		if(isset($advert->price)){
			$criteria->compare('t.price', '<'.$up);
			$criteria->compare('t.price', '>'.$down);
		}else{
			$criteria->compare('t.salary', '<'.$up);
			$criteria->compare('t.salary', '>'.$down);
		}

		$criteria->compare('t.id_region', $advert->id_region);
		$criteria->compare('t.id_city', $advert->id_city);
		$criteria->compare('t.active', '1');
		$criteria->limit = $limit;

		$adverts = $type::model()->findAll($criteria);

		$count = count($adverts);

		if($count < 4){
			if(isset($advert->price)){
				$price = $advert->price;
				$up = $price + ($price/100)*self::PRICE_MORE_PERCENT;
				$down = $price - ($price/100)*self::PRICE_MORE_PERCENT;
			}else if(isset($advert->salary)){
				$salary = $advert->salary;
				$up = $salary + ($salary/100)*self::PRICE_MORE_PERCENT;
				$down = $salary - ($salary/100)*self::PRICE_MORE_PERCENT;
			}
			$criteria = new SDbCriteria;
			$criteria->addNotInCondition('id', [$id]);
			$criteria->compare('t.active', '1');
			$criteria->limit = $limit;
			if(isset($advert->price)){
				$criteria->compare('t.price', '<'.$up);
				$criteria->compare('t.price', '>'.$down);
			}else{
				$criteria->compare('t.salary', '<'.$up);
				$criteria->compare('t.salary', '>'.$down);
			}

			$criteria->compare('t.id_region', $advert->id_region);

			$adverts = $type::model()->findAll($criteria);

			$count = count($adverts);
			if($count < 4){
				$criteria = new SDbCriteria;
				$criteria->addNotInCondition('id', [$id]);
				$criteria->compare('t.active', '1');
				$criteria->limit = $limit;
				$adverts = $type::model()->findAll($criteria);
			}
		}

		if($adverts){
			$ids = array_slice($adverts, 0, $limit);

			return $ids;
		}

		return false;
	}

	/**
	 * AJAX ACTIONS
	 * ----------------
	 */

	// Функция для сохранение рейтингов
	public function actionFavorites(){
		$id_advert 	= request()->getParam('id_advert');
		$modelName 	= request()->getParam('model');
		$type 		= request()->getParam('type');
		$id_user	= user()->id;
		
		if($id_advert && $modelName && $type && $id_user){
			$criteria = new SDbCriteria;
			$criteria->compare('t.id_user', $id_user);
			$criteria->compare('t.id_advert', $id_advert);
			$criteria->compare('t.model', $modelName);
			$criteria->compare('t.type', $type);
			$model = Favorites::model()->find($criteria);
			if($model){ // remove favorite 
				$model->delete();
				$add = '0';
				if($type == 1)
					$cacheKey= "listdata.myFavorites".user()->id;
				else if($type == 2)
					$cacheKey= "listdata.watchLaters".user()->id;
				Yii::app()->cache->delete($cacheKey);
				Common::jsonSuccess(true, compact('add'));
			}
			else{// add favorite 
				$model = $this->loadModel('Favorites');
				$model->id_advert 	= $id_advert;
				$model->model 		= $modelName;
				$model->type 		= $type;
				$model->id_user 	= $id_user;
				if($model->save()){
					$add = '1';
					if($type == 1)
						$cacheKey= "listdata.myFavorites".user()->id;
					else if($type == 2)
						$cacheKey= "listdata.watchLaters".user()->id;
					Yii::app()->cache->delete($cacheKey);
					Common::jsonSuccess(true, compact('add'));
				}
			}
		}
		Common::jsonSuccess(true,['success' => false, 'errors' => $model->getErrors()]);
	}

}

