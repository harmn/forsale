<?php
Yii::import('app.controllers.FrontController');
class SiteController extends FrontController 
{
	public $model;
	public $tabs = [];

	public function init(){
		parent::init();
	}

	public function allowedActions(){
		return "install, error, backenderror";
	}

	/**
	 * Главная страница
	 */
	public function actionIndex(){
		
		$criteria = new CDbCriteria;

		if(Yii::app()->request->getQuery('Home')){
			$model = new Home('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Car')){
			$model = new Car('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Electronics')){
			$model = new Electronics('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Technics')){
			$model = new Technics('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Furniture')){
			$model = new Furniture('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Fashion')){
			$model = new Fashion('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Sport')){
			$model = new Sport('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Animal')){
			$model = new Animal('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Work')){
			$model = new Work('search');
			$provider = $model->search($criteria);
		}else if(Yii::app()->request->getQuery('Services')){
			$model = new Services('search');
			$provider = $model->search($criteria);
		}else{
			$model = new Home('search');
			$provider = $model->search($criteria);
		}
		
		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('filter')){
			Common::jsonSuccess(true, [
				'list' => $this->renderPartial('_list', compact('model', 'provider'), true)
			]);
		}

		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('_list', compact('model', 'provider'));
		}
        else{
        	$advertsList = LastAdverts::listData();
        	$this->pageTitle   = t('front', 'Главная');
       		$this->render('index', compact('provider', 'model', 'advertsList'));
        }
	}

	/**
	 * 
	*/
	public function actionGetInfo(){
		if($page = request()->getPost('page')){
			if($page == 1){
				$model = new Home('search');
			}
			$res = [];
			$res['filters'] = $this->renderPartial('homeFilters', compact('model'), true);
			Common::jsonSuccess(true,$res);
		}
	}

	/**
	 *  get item info
	*/
	public function actionGetItem(){
		if($model = request()->getParam('model')){
			$id = request()->getParam('id');
			$model  = $this->loadModel($model, $id);
			Common::jsonSuccess(true, ['item' => $this->renderPartial('_itemSection', compact('model'), true)]);
		}
	}

	public function actionAbout(){
		$this->pageTitle  = t('front', 'О нас');
		$this->render('about');
	}
	
	public function actionContacts(){
		$model = new SendContactMail;
	 	$this->performAjaxValidation($model);
	 	$this->pageTitle  = t('front', 'Контакты');
		$this->render('contacts', array('model' => $model, 'action' => 'sendForm'));
	}

	public function actionSendMail(){
		$model = new SendContactMail;
	 	$this->performAjaxValidation($model);
	 	$post = request()->getPost('SendContactMail');
	 	$text = $post['comment'].' <br/> From '.$post['name'];
	 	app()->mail->send(SendContactMail::SELF_EMAIL, $post['title'], $text, $post['email']); 
	 	$this->pageTitle  = t('front', 'Контакты');
		$this->render('contacts', array('model' => $model, 'action' => 'sendedMail'));
	}

	public function actionError(){
		
		// $this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{
				/*if($error['code'] == 404) 
					$this->render('404');
				else
					$this->render('error', $error);*/

				echo "<h4>Ошибка ".$error['code']."</h4>";
				echo "<p>".$error['message']."</p>";
			}
				
		}
	}

	public function actionBackendError(){
		app()->theme = 'backend';
		$this->layout = '/layouts/error';
        
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('modules.admin.views.errors.'.$error['code'], $error);
        }
	}

	/**
	 * СТАТИЧЕСКИЕ СТРАНИЦЫ
	 */
	public function actionPage($route){

		$page = Page::model()->find('route =:route', array(':route'=>$route));

		if(!$page) exception(404);

		$this->pageTitle = actual($page->meta_title, $this->pageTitle);
		$this->pageKeywords = actual($page->meta_keywords, $this->pageKeywords);
		$this->pageDesc = actual($page->meta_description, $this->pageDesc);

		$this->render('page', compact('page'));
	}

	
}

?>