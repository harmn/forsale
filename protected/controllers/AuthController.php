<?php
/**
* AuthController
* 
* Контроллер для наших пользователей. Содержит в себе следующие функции:
* - авторизация
* - регистрация
* - выход
* - редактирование профиля [в будущем]
*/
Yii::import('app.controllers.FrontController');
class AuthController extends FrontController
{     
    public $model;

    public function init(){
        parent::init();

        $this->model = user()->userModel;
    }

    public function behaviors(){
        return CMap::mergeArray(parent::behaviors(), [

        ]);
    }

    public function filters()
    {
        return array(
            'accessControl + profile, changepassword',
        );
    }

    //Правила доступа, используемые фильтром определяются переопределением метода
    public function accessRules()
    {
        return array(
            //даем доступ только зарегистрированным пользователям
            array(
                'allow',
                'users'=> array('@'),
            ), 
            array(
                'deny',
                'users'=>array('*')
            )
        );      
    }

    public function actions()
    {
        return array(
            // Создаем actions captcha.
            // Он понадобиться нам для формы регистрации (да и авторизации)
             'captcha'=>array(
                'class'=>'core.actions.SCaptchaAction',
                'height'=>42,
                'backend' => 'gd',
            ),
        );
    }

    /**
    * Метод входа на сайт
    * 
    * Метод в котором мы выводим форму авторизации
    * и обрабатываем её на правильность.
    */
    public function actionLogin() {

        // Проверяем является ли пользователь гостем
        // ведь если он уже зарегистрирован - формы он не должен увидеть.
        if (!user()->isGuest) 
            $this->redirect(param('home'));

        $mdlLogin = $this->loadModel('LoginForm', false, ['profile']);
        $mdlLogin->setScenario('flogin');

        $mdlReg = $this->loadModel('RegisterForm', false, ['profile']);
        $mdlReg->setScenario('fregistration');

        $scenario = request()->getParam('scenario');

        if($scenario == 'flogin')
            $this->performAjaxValidation($mdlLogin);
        elseif($scenario == 'fregistration')
            $this->performAjaxValidation($mdlReg);

        
        if(request()->isPostRequest) {
            
            //login
            if($scenario == 'flogin'){
                if($this->login($mdlLogin)){
                    // если всё ок - либо идет туда откуда запрос забросил нас сюда, либо на страницу админ
                    $this->redirect($this->createUrl('site/index'));
                }

            } //registration
            elseif($scenario == 'fregistration'){

                $mdlReg->attributes = $_POST[ 'RegisterForm' ];
                $mdlReg->is_social_user = User::SOCIAL_USER_NO;

                $profile = new ClientProfile;
                $mdlReg->profile = $profile;
                $mdlReg->profile->attributes = $_POST[ 'RegisterForm' ];

                if($mdlReg->withRelated->save(true, array('profile'))){
                    // если всё ок - либо идет туда откуда запрос забросил нас сюда, либо на страницу админ

                    // отправка email с просьбой активировать аккаунт
                    $status = 
                        app()->mail->send( $mdlReg->email, 
                            t("front", "Регистрация на сайте"),
                            app()->mail->getView('auth.register-confirmation', array(
                                    'username'=>$mdlReg->username, 
                                    'key'=>$mdlReg->activation_key
                                    ))); 

                    if($status){
                        //set flash message about registration complete
                        setFlash('success', t("front", "СмсРегистрацияУспешно"));
                    }
                    else{
                        setFlash('error', t("front", "СмсРегистрацияНеУспешно"));
                    }

                    $this->refresh();
                }
            }
        } 

        $this->render('login', array('mdlLogin' => $mdlLogin, 'mdlReg'=>$mdlReg));
    }   

    /**
     * Фунция авторизации через ajax
     * @return bool
     */
    public function actionAjaxLogin(){
        
        $model = new LoginForm('flogin');

        $this->performAjaxValidation($model);

        if($this->login($model)){
            Common::jsonSuccess();
        }
        else{
            Common::jsonError('Ошибка авторизации');
        }
    }  

    public function login($model){
        //проверяем вход через социальные сервисы
        $service = Yii::app()->request->getQuery('service');

        if (isset($service) && $service) {
            $authIdentity = Yii::app()->eauth->getIdentity($service);

            $authIdentity->redirectUrl = user()->getReturnUrl( bu() );
            $authIdentity->cancelUrl = $this->createAbsoluteUrl('/auth/login');

            if ($authIdentity->authenticate()) {
                
                $identity = new EAuthUserIdentity($authIdentity);

                // successful authentication
                if ($identity->authenticate()) {
                    Yii::app()->user->login($identity);

                    // special redirect with closing popup window
                    $authIdentity->redirect();

                    return true;
                }
                else {
                    // close popup window and redirect to cancelUrl
                    $authIdentity->cancel();

                    return false;
                }
            }            
        }
        else{
            $model->attributes = $_POST[ 'LoginForm' ];
            // Проверяем правильность данных
            $ui = user()->frontUserIdentity;
            $identity = new $ui($model->username, $model->password);
            $identity->authenticate();

            if($identity->errorCode == CBaseUserIdentity::ERROR_NONE) {
               
                // Данная строчка говорит что надо выдать пользователю
                // соответствующие куки о том что он зарегистрирован, срок действий
                 // у которых указан вторым параметром. 
                $duration = $model->rememberMe ? 3600*24*30 : 0; // 30 days
               
                user()->login($identity, $duration);

                return true;
            } 
        }
    }


   /**
    * Метод выхода с сайта
    * 
    * Данный метод описывает в себе выход пользователя с сайта
    * Т.е. кнопочка "выход"
    * @return 
    */
    public function actionLogout(){
        // Выходим
        user()->logout();
        // Перезагружаем страницу
        $this->redirect($this->createUrl('site/index'));
    }
    
    /**
     * Метод активации пользователя
     * он доступен по переходу по ссылке, которую пользователь получил 
     * по емаил.
     * 
     * ссылка имеет следующий вид http://host/activate/username/activationcode
     * @param  String $username Имя пользователя
     * @param  String $key      Ключ активации, который был выслан ему по почте
     * @return 
     */
    public function actionActivate($username, $key) {

        $this->pageTitle = t('front', "Активация");
        
        $user = AR::model($this->model)->notActivated()->find('username = :username && activation_key =:key', 
                        array(':username'=>$username, ':key' => $key));

        if (!$user) {
            //user not found, or blocked, or already activated
            setFlash('error', "<h3>Ошибка активации</h3><p>Активация не прошла! Возможно ваш аккаунт уже активирован! Попробуйте позже.</p>");

            $this->render('activation');
            return;
        }

        if($user->activate()) // процедура активации
        {
            // отправить сообщение о активации аккаунта
            $status = app()->mail->send( $user->email, 
                        t('front', 'Ваш акаунт на сайте'),
                        app()->mail->getView('auth.account-activated', array(
                                'username'=>$user->username, 
                                'key'=>$user->activation_key
                                ))); 
            
            //set flash message for success
            setFlash('success', t("front", "Активация прошла успешно! Теперь вы можете войти на сайт используя свой логин и пароль!"));
            
        }
        else { //error on activation process
            
            //set flash for activation error
            user()->setFlash('error', t("front", "Ошибка активации"));
        }

        $this->render('activation');
    }

    /**
     * редактирование профиля
     *
     * но в данном проекте данная функция не работает, 
     * а вместо нее работает функция из контроллера AccountController
     */
    public function actionProfile(){
        
        // Проверяем является ли пользователь гостем
        if (user()->isGuest)
            $this->redirect(['/auth/login']);

        $model = $this->loadModel('User', user()->id);
        $client = $this->loadModel('Client', user()->id, ['profile']);

        $model->scenario = 'update';
        $client->scenario = 'update';
        $this->performAjaxValidation($model);
        $this->performAjaxValidation($client);
        
        if(request()->getPost('User')){
            
            $model->setAttributes($_POST);

            $client->profile->attributes = $_POST['ClientProfile'];



            if($model->save()){
               $client->profile->save();
                //сохранение настроек профиля
                if($model->id == user()->id)
                    user()->UpdateInfo($model); //обновить данные текущего пользователя

                $this->refresh();
            }
        }

        $changePasswordModel = $this->loadModel('User', user()->id);
        $changePasswordModel->scenario = 'changePassword';
        $changePasswordModel->password = '';
        
        $this->render('/auth/profile', compact('model', 'changePasswordModel', 'client'));
    }

    //смена пароля
    public function actionChangePassword(){

        $model = $this->loadModel($this->model, user()->id);
        $model->scenario = 'changePassword';
        $model->password = null;
 
        $this->performAjaxValidation($model);

        if(isset($_POST[$this->model])){

            $model->attributes = $_POST;

            //save new password and salt
            if($model->validate()){

                if($model->save(array('password')))
                    Common::jsonSuccess(true);
            }  
            else{
                 Common::jsonError('Ошибка авторизации');
            }
        }
    
    }

    //удаление акаунта
    public function actionDelete(){
        $user = $this->loadModel($this->model, user()->id);

        $user->delete();

        user()->logout();
        $this->redirect( array('/login') );
    }   

    /**
     * Удаляет из куки данные фавориты и посм.позже и добавляет ш базу
     * 
     */

    public function deleteCookieFavoriteProducts(){
        $cookieKeys = [Favorites::COOKIE_NAME_FAVORITE, Favorites::COOKIE_NAME_WATCH_LATER];

        foreach ($cookieKeys as $cookieKey) {
    
            if(Cookie::get($cookieKey)){

                $criteria = new CDbCriteria;
                $criteria->compare('t.id_user', user()->id);
                $criteria->select = 'CONCAT("_" ,id_product, type) as concid, id_product, type';
                $models =  CHtml::listData(Favorites::model()->findAll($criteria), 'id', 'concid');

                $cookies = CJSON::decode(Cookie::get($cookieKey));

                foreach ($cookies as $id_product => $type) {
                    if(!in_array($id_product."_".$type, $models)){
                        $newModel = new Favorites;
                        $newModel->id_user = user()->id;
                        $newModel->id_product = $id_product;
                        $newModel->type = $type;
                        $newModel->save();
                    }
                }

                // bazayum ave enq anum ev jnjum enq cookie-n
                setcookie($cookieKey, '', 0, '/');
            }
        }
    }

    public function  actionForgotPassword(){
        
        $model = $this->loadModel($this->model);
        $model->scenario = 'resetPassword';

        $this->performAjaxValidation($model);

        if(isset($_POST[$this->model])){

            $model->attributes = $_POST;

            if(!empty($model->email)){ //посылка письма для сброса

                if($model->validate(array('email'))){
                    $client = Client::model($this->model)->find('email = :email', array(':email'=>$model->email));
                    $username = $client->username;
                    $key = String::getUniqueString($this->model, 'reset_key', 12);

                    // отправить сообщение для сброса пароля
                    $status = 
                        app()->mail->send( $client->email, 
                            t('front', 'Сброс пароля на сайте {site}!', array('{site}' => app()->name)),
                            app()->mail->getView('auth.reset-password', array(
                                    'username'=>$username, 
                                    'key'=>$key
                                    ))); 

                    //save reset key in db
                    Client::model()->updateByPk($client->id, array('reset_key'=>$key));

                    $cs = Yii::app()->clientScript;

                    $cs->registerCSSFile($this->assetsUrl . '/stylesheets/main.css');
                    $cs->registerCSSFile($this->assetsUrl . '/stylesheets/Bootstrap/bootstrap-theme.css');
                    $cs->registerCSSFile($this->assetsUrl . '/stylesheets/Bootstrap/bootstrap.min.css');
                    //set user flash message
                    $this->render('forgot-password-confirm');
                }
            }
        }else{
            $this->pageTitle = t('front', 'Смена пароля');
            $this->render('forgotpassword', compact('model'));
        }
    }

  /**
     * Функция которая сбрасывает пароль на новый
     * @param  String $username Имя пользователя
     * @param  String $key      Ключ для сброса пароля, который был выслан ему по почте
     * @return 
     */
    public function actionResetPassword($username, $key){
       
        $criteria = new CDbCriteria;
        $criteria->compare('username', $username);
        $criteria->compare('is_social_user', User::SOCIAL_USER_NO);
        $criteria->compare('reset_key', $key);


        $model = Client::model($this->model)->find($criteria);
        if(!$model) exception(404);
        $model->scenario = 'resetPassword';
        $model->password = '';
 
        $this->performAjaxValidation($model);
        if(isset($_POST[$this->model])){

            $model->attributes = $_POST;

            //save new password and salt
            if($model->save()){
                //set flash message
                setFlash('password-changed', t('front', 'Пароль успешно изменен'));
            }

        }

        $this->pageTitle = t('front', 'Смена пароля');

        $this->render('forgotpassword', compact('model'));
    }

    public function actionIsGuest(){
        if(user()->isGuest) Common::jsonSuccess(true);
        else Common::jsonSuccess(true, ['success' => false]);
    }
}