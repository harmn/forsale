<?php 

/**
* FrontController
*/
class FrontController extends FController
{

    public $tabs = array();

    public function init(){
        parent::init();
      	$this->registerStyles();
      	$this->registerScripts();
    }


    private function registerStyles(){  
        $cs = Yii::app()->clientScript;     
        
        //{* ЭТО НУЖНО ОБЪЯЗАТЕЛЪНО *}
        $cs->registerCssFile($this->rootAssetsUrl."/plugins/font-awesome/css/font-awesome.css");

        cs()->registerCssFile($this->rootAssetsUrl.'/plugins/bootstrap-slider/css/slider.css');
        //мои стили
        cs()->registerCssFile($this->assetsUrl."/stylesheets/slider.css");
        cs()->registerCssFile($this->assetsUrl."/stylesheets/main.css");
        cs()->registerCssFile($this->assetsUrl."/stylesheets/helpers.css");
        //мои стили
        cs()->registerCssFile($this->assetsUrl."/stylesheets/gridset.css");
        cs()->registerCssFile($this->assetsUrl."/stylesheets/custom-style.css");
        // cs()->registerPackage('inputstyler'); //select-styler
        cs()->registerCssFile($this->assetsUrl."/stylesheets/style.css");

        $cs->registerCssFile($this->rootAssetsUrl .'/plugins/jquery-slider/css/jquery.sidr.light.css'); //важно 
        $cs->registerPackage('jalerts'); //alerts
        $cs->registerPackage('selectstyler-backend'); //select-styler
        $cs->registerPackage("messenger"); //вывод сообщений

        // carusel css
        $cs->registerCssFile($this->assetsUrl."/js/jQuery-carousel/jquery-carousel1.css");

        cs()->registerPackage('bootstrap'); 

        //galleria
        $cs->registerCssFile($this->rootAssetsUrl."/plugins/galleria/css/jquery.thumbnailScroller.css");
        $cs->registerCssFile($this->rootAssetsUrl."/plugins/galleria/css/custom.css");

        // item-hover 
        cs()->registerCssFile($this->rootAssetsUrl.'/plugins/item-hover/css/style.css');
        cs()->registerCssFile($this->rootAssetsUrl.'/plugins/item-hover/css/style_common.css');
    }

    public function registerScripts(){
        $cs = Yii::app()->clientScript;

        cs()->registerCoreScript('jquery.ui');
        cs()->registerScriptFile($this->coreAssetsUrl .'/js/main/main-functions.js');
        cs()->registerScriptFile($this->rootAssetsUrl.'/plugins/bootstrap-slider/js/bootstrap-slider.js');
        cs()->registerScriptFile(app()->getModule('core')->assetsUrl.'/js/main/small-plugins.js');
        cs()->registerScriptFile($this->assetsUrl.'/js/main.js');
        cs()->registerScriptFile($this->assetsUrl.'/js/global.js');
        cs()->registerScriptFile($this->assetsUrl.'/js/googleAnalitics.js');
        cs()->registerScriptFile($this->assetsUrl.'/js/mapper.js');
        $cs->registerScriptFile($this->rootAssetsUrl .'/plugins/breakpoints/breakpoints.js'); //TODO: не знаю 
        $cs->registerScriptFile($this->rootAssetsUrl .'/plugins/jquery-slider/jquery.sidr.min.js');
        $cs->registerScriptFile($this->rootAssetsUrl .'/plugins/jquery-unveil/jquery.unveil.min.js');
        $cs->registerScriptFile($this->rootAssetsUrl .'/plugins/jquery-slimscroll/jquery.slimscroll.min.js');

        //для history навигации
        $cs->registerCoreScript('bbq');
        $cs->registerCoreScript('history');

        // slider
        cs()->registerScriptFile($this->rootAssetsUrl.'/plugins/sequence/js/jquery.sequence.js');
        cs()->registerScriptFile($this->rootAssetsUrl.'/plugins/sequence/js/jquery.sequence-min.js');
        cs()->registerScriptFile($this->rootAssetsUrl.'/plugins/sequence/js/jquery.ba-hashchange.min.js');

        // carusel js
        $cs->registerScriptFile($this->assetsUrl .'/js/jQuery-carousel/jquery.carousel1.js');

        // galleria
        $cs->registerScriptFile($this->rootAssetsUrl."/plugins/galleria/js/galleria-1.2.9.min.js"); 
        $cs->registerScriptFile($this->rootAssetsUrl."/plugins/galleria/js/myplugin.js");
        $cs->registerScriptFile($this->rootAssetsUrl."/plugins/galleria/js/jquery.thumbnailScroller.js");

        $cs->registerScript('load_galleria', "
            Galleria.loadTheme('{$this->rootAssetsUrl}/plugins/galleria/themes/folio/galleria.folio.min.js');
        ", CClientScript::POS_READY); 

        $cs->registerScriptFile($this->rootAssetsUrl.'/plugins/bootstrap-slider/js/bootstrap-slider.js');

        // js language change
        Yii::import('core.extensions.JsTrans.JsTrans');
        new JsTrans(['admin','user','property','front'], ['ru','en', 'fr', 'hy']);
    }
}