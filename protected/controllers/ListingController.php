<?php


/**
* ListingController
*/

Yii::import('app.controllers.FrontController');
class ListingController extends FrontController
{

	public static $pageTypes =  [
									"Home" => "Дом",
									"Car" => "Авто",
									"Electronics" => "Электроника",
									"Technics" => "Техника",
									"Furniture" => "Мебель",
									"Fashion" => "Мода",
									"Sport" => "Спорт и Игры",
									"Animal" => "Животное и растения",
									"Work" => "Работа",
									"Services" => "Службы",
								];
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->assetsUrl.'/js/listing.js');
		cs()->registerScriptFile("http://maps.google.com/maps/api/js?sensor=false");
		// ВЫ УЖЕ СМОТРЕЛИ widget //Для предварительной загрузки скриптов
		// RecentlyViewedItemsWidget::loadScriptFiles();
	}
	
	public function actionAddListing(){
		$typeModel = request()->getPost('model');
		$model = new $typeModel('addListing');
		$model->save();
		$cacheKey= "listdata.myAdverts".user()->id;
		Yii::app()->cache->delete($cacheKey);
		Common::jsonSuccess(true, ['id' => $model->id]);
	}

	/**
	 * изменение заявления
	 */
	public function actionEdit(){
		$id = request()->getParam('id');
		$type = request()->getParam('type');
		$model = $this->loadModel($type, $id);
		if(!isset(user()->id) || $model->id_creator != user()->id){
			return false;
		}

		$this->pageTitle   = t('front', self::$pageTypes[$type]);
		$this->render(strtolower($type), compact('model', 'type'));
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$modelName = $_POST['modelName'];
		$post = request()->getPost($modelName);
		$model = $this->loadModel($modelName, $post['id']);
		$model->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($model);

		if(isset($post['languages'])){
			$model->languages = $post['languages'];
			$model->manyMany->save(true, null, array('languages'));
		}

		if($model->save()){
			$cacheKey= "listdata.myAdverts".user()->id;
			Yii::app()->cache->delete($cacheKey);
			$url = Yii::app()->createUrl('listing/edit',array( 'id' => $post['id'], 'type' => $modelName));   
			$this->redirect($url);
		}
	}
}

