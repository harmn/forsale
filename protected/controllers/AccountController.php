<?php
/**
* AccountController
*
*/
Yii::import('app.controllers.FrontController');
class AccountController extends FrontController
{
    public $model;
    public $layout = '/layouts/profile';

    public function init(){
        parent::init();

        $this->model = user()->userModel;
    }


    public function behaviors(){
        return CMap::mergeArray(parent::behaviors(), [
            //действия для сравнения продуктов
            // 'ProductCompareActions',
            // для работы Favorites widget
            // 'FavoriteActions'
        ]);
    }


    public function filters(){
        return array(
            'accessControl + profile, changepassword, orders, checkout',
        );
    }

    //Правила доступа, используемые фильтром определяются переопределением метода
    public function accessRules(){
        return array(
            //даем доступ только зарегистрированным пользователям
            array(
                'allow',
                'users'=> array('@'),
            ),
            array(
                'deny',
                'users'=>array('*')
            )
        );
    }

    public function actions(){
        return array(
            // Создаем actions captcha.
            // Он понадобиться нам для формы регистрации (да и авторизации)
             'captcha'=>array(
                'class'=>'core.actions.SCaptchaAction',
                'height'=>42,
                'backend' => 'gd',
            ),
        );
    }

    //редактирование профиля
    public function actionProfile(){

         // Проверяем является ли пользователь гостем
        if (user()->isGuest)
            $this->redirect(['/auth/login']);

        $model = $this->loadModel('User', user()->id);
        $client = $this->loadModel('Client', user()->id, ['profile']);

        $model->scenario = 'update';
        $client->scenario = 'update';
        $this->performAjaxValidation($model);
        $this->performAjaxValidation($client);
        
        if(request()->getPost('User')){
            
            $model->setAttributes($_POST);

            $client->profile->attributes = $_POST['ClientProfile'];
            
            if($model->save()){
               $client->profile->save();
                //сохранение настроек профиля
                if($model->id == user()->id)
                    user()->UpdateInfo($model); //обновить данные текущего пользователя

                $this->refresh();
            }
        }


        // MetaDefault::setSeo('profile-default');
        
        $changePasswordModel = $this->loadModel('User', user()->id);
        $changePasswordModel->scenario = 'changePassword';
        $changePasswordModel->password = '';
        $this->pageTitle   = t('front', 'Мой профиль');
        $this->render('profile', compact('model', 'changePasswordModel', 'client'));
    }

     //Посмотреть позже
    public function actionMyAdverts(){
        $model    = new Client('search');
        $results = $model->searchMyAdverts();
        $this->pageTitle   = t('front', 'Мои объявления');
        $this->render('myAdverts', compact('results'));
    }

    //Посмотреть позже
    public function actionLater(){

        if (user()->isGuest)
           $this->redirect(['/auth/login']);

        $model    = new Favorites('search');
        $results = $model->searchMyWatchLaters();
        $this->pageTitle   = t('front', 'Посмотреть позже');
        $this->render('later', compact('results'));
    }

    //Избранные
    public function actionFavorites(){

        if (user()->isGuest)
           $this->redirect(['/auth/login']);

        $model    = new Favorites('search');
        $results = $model->searchMyFavorites();
        $this->pageTitle   = t('front', 'Избранные');
        $this->render('favorites', compact('results'));
    }

    //Корзина
    public function actionCart(){
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($this->assetsUrl .'/js/orderFront.js');

        $orderModel = new NewOrder('search');//для заказов

        MetaDefault::setSeo('cart-default');

        if(request()->isAjaxRequest && request()->getParam('ajax'))
            $this->renderPartial('cart', compact('orderModel'));
        else
            $this->render('cart', compact('orderModel'));
    }

    //Заказы
    public function actionOrders(){

        MetaDefault::setSeo('order-default');

        $model = Buyer::model()->findByPk(user()->id);
       
        return $this->render('orders', compact('model'));
    }

    /**
     * Создание быстрый заказа
     */
    public function actionFastCreateOrder(){
        $model = $this->loadModel('NewOrder', false, ['positions']);
        $model->scenario = 'frontend-fast-create';

        $this->performAjaxValidation($model);

        if(!empty($_POST)){
            $model->setAttributes($_POST);

            //если введенный телефон не существует, то создать нового пользователя
            $model->id_buyer = Buyer::findBuyerByPhone($model->buyerPhone, $model->buyerAddPhone, $model->buyerName);

            if($model->save()) Common::jsonSuccess(true, ['message' => $model->id]);
            else Common::jsonSuccess(true, ['message' => false]);
        }
    }

    /**
     * Сохранить заказ
     */
    public function actionCreateOrder(){
        $id = 'error';//message после заказа
        $model = $this->loadModel('NewOrder', false, ['positions']);

        $model->scenario = 'frontend-create';
        $this->performAjaxValidation($model);

        if(!empty($_POST)){
            $model->setAttributes($_POST);

            //если введенный телефон не существует, то создать нового пользователя
            $model->id_buyer = Buyer::findBuyerByPhone($model->buyerPhone, $model->buyerAddPhone, $model->buyerName);
            if($model->save())
                $id = $model->id; 

            $this->redirect('/account/checkout/'.$id);
        }
    }

    //checkout, создание заказа
    public function actionCheckout($id = false){

        if($id)
            $url = '/order/success';
        else
            $url = '/order/orderForm';

        $model = $this->loadModel('NewOrder');
        $modelCart = app()->shoppingCart;

        $model->scenario = 'frontend-create';
        $this->performAjaxValidation($model);

        if(!user()->isGuest && user()->getState('webuserModel') == 'Buyer'){
            $buyer = Buyer::model()->findByPk(user()->id);
            if($buyer){
                $model->buyerPhone = $buyer->phone;
                $model->buyerAddPhone = $buyer->phone_additional;
                $model->buyerName = $buyer->firstname;
                $model->buyerAddName = $buyer->additional_name;
                $model->buyerEmail = $buyer->email;
            }   
        }

        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile($this->assetsUrl .'/js/orderFront.js');

        $this->render($url, compact('model', 'modelCart', 'id'));
    }

    // public function actionTestRobokassa()
    // {
    //     /**
    //      * Создадим или используем уже существующий в базе инвойс
    //      * @var Invoice $invoice
    //      */
    //     $NewOrder = new NewOrder;
    //     $NewOrder->id = 1;
    //     $NewOrder->amount = 10.25;
    //     $NewOrder->description = 'Test payment';
    //     // Дополнительные параметры
    //     //$NewOrder->shpClientId = 1;

    //     // Показываем форму оплаты
    //     $this->render('/testrobokassa/index', array(
    //         'invoice' => $NewOrder,
    //     ));
    // }
}