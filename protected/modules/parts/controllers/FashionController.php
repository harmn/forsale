<?php
/**
* Parts Controller FashionController
* 
* 
*/
class FashionController extends BController
{    
	public $model = 'Fashion'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/fashion.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Fashion('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Мода и одежда';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetFashionInfo(){
		if($idFashion = request()->getPost('idFashion')){
			$model = $this->loadModel($this->model, $idFashion);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idFashion);
			$Criteria->compare('`related`.`model`', 'Fashion');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idFashion', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Fashion = $this->loadModel('Fashion');
		$this->performAjaxValidation($Fashion);
		$post = request()->getPost('Fashion');
		$Fashion->setAttributes($post);

		if($Fashion->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Fashion');
		$Fashion = $this->loadModel($this->model, $post['id']);
		$Fashion->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($Fashion);

		if($Fashion->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
