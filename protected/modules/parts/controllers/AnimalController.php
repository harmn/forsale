<?php
/**
* Parts Controller AnimalController
* 
* 
*/
class AnimalController extends BController
{    
	public $model = 'Animal'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/animal.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Animal('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Животное и растения';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetAnimalInfo(){
		if($idAnimal = request()->getPost('idAnimal')){
			$model = $this->loadModel($this->model, $idAnimal);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idAnimal);
			$Criteria->compare('`related`.`model`', 'Animal');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idAnimal', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Animal = $this->loadModel('Animal');
		$this->performAjaxValidation($Animal);
		$post = request()->getPost('Animal');
		$Animal->setAttributes($post);

		if($Animal->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Animal');
		$Animal = $this->loadModel($this->model, $post['id']);
		$Animal->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($Animal);

		if($Animal->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
