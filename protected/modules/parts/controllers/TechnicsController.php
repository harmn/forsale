<?php
/**
* Parts Module TechnicsController
* 
* 
*/
class TechnicsController extends BController
{    
	public $model = 'Technics'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/technics.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Technics('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Техника';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetTechnicsInfo(){
		if($idTechnics = request()->getPost('idTechnics')){
			$model = $this->loadModel($this->model, $idTechnics);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idTechnics);
			$Criteria->compare('`related`.`model`', 'Technics');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idTechnics', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Technics = $this->loadModel('Technics');
		$this->performAjaxValidation($Technics);
		$post = request()->getPost('Technics');
		$Technics->setAttributes($post);

		if($Technics->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Technics');
		$Technics = $this->loadModel($this->model, $post['id']);
		$Technics->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($Technics);

		if($Technics->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
