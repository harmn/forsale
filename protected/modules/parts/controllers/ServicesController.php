<?php
/**
* Parts Controller ServicesController
* 
* 
*/
class ServicesController extends BController
{    
	public $model = 'Services'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/services.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Services('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Службы';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetServicesInfo(){
		if($idServices = request()->getPost('idServices')){
			$model = $this->loadModel($this->model, $idServices);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idServices);
			$Criteria->compare('`related`.`model`', 'Services');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idServices', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Services = $this->loadModel('Services');
		$this->performAjaxValidation($Services);
		$post = request()->getPost('Services');
		$Services->setAttributes($post);

		if($Services->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Services');
		$Services = $this->loadModel($this->model, $post['id']);
		$Services->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($Services);

		if($Services->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
