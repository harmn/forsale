<?php
/**
* Parts Module CarController
* 
* 
*/
class CarController extends BController
{    
	public $model = 'Car'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/car.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Car('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Автомобили';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetCarInfo(){
		if($idCar = request()->getPost('idCar')){
			$model = $this->loadModel($this->model, $idCar);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idCar);
			$Criteria->compare('`related`.`model`', 'Car');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idCar', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$car = $this->loadModel('Car');
		$this->performAjaxValidation($car);
		$post = request()->getPost('Car');
		$car->setAttributes($post);

		if($car->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Car');
		$car = $this->loadModel($this->model, $post['id']);
		$car->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($car);

		if($car->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
