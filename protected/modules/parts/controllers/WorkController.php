<?php
/**
* Parts Controller WorkController
* 
* 
*/
class WorkController extends BController
{    
	public $model = 'Work'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/work.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Work('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Работа';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetWorkInfo(){
		if($idWork = request()->getPost('idWork')){
			$model = $this->loadModel($this->model, $idWork);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idWork);
			$Criteria->compare('`related`.`model`', 'Work');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idWork', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Work = $this->loadModel('Work');
		$this->performAjaxValidation($Work);
		$post = request()->getPost('Work');

		$Work->setAttributes($post);
		if(isset($post['languages'])){
			$Work->languages = $post['languages'];
			$Work->manyMany->save(true, null, array('languages'));
		}

		if($Work->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Work');
		$Work = $this->loadModel($this->model, $post['id']);

		//ajax validation
		$this->performAjaxValidation($Work);

		$Work->setAttributes($post);

		if(isset($post['languages'])){
			$Work->languages = $post['languages'];
			$Work->manyMany->save(true, null, array('languages'));
		}

		if($Work->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
