<?php
/**
* Parts Module ElectronicsController
* 
* 
*/
class ElectronicsController extends BController
{    
	public $model = 'Electronics'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/electronics.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Electronics('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Электроника';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetElectronicsInfo(){
		if($idElectronics = request()->getPost('idElectronics')){
			$model = $this->loadModel($this->model, $idElectronics);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idElectronics);
			$Criteria->compare('`related`.`model`', 'Electronics');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idElectronics', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Electronics = $this->loadModel('Electronics');
		$this->performAjaxValidation($Electronics);
		$post = request()->getPost('Electronics');
		$Electronics->setAttributes($post);

		if($Electronics->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Electronics');
		$Electronics = $this->loadModel($this->model, $post['id']);
		$Electronics->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($Electronics);

		if($Electronics->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
