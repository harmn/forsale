<?php
/**
* Parts Module HomeController
* 
* 
*/
class HomeController extends BController
{    
	public $model = 'Home'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/home.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Home('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Дома';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetHomeInfo(){
		if($idHome = request()->getPost('idHome')){
			$model = $this->loadModel($this->model, $idHome);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idHome);
			$Criteria->compare('`related`.`model`', 'Home');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idHome', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$home = $this->loadModel('Home');
		$this->performAjaxValidation($home);
		$post = request()->getPost('Home');
		$home->setAttributes($post);

		if($home->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Home');
		$home = $this->loadModel($this->model, $post['id']);
		$home->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($home);

		if($home->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
