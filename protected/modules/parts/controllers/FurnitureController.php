<?php
/**
* Parts Module FurnitureController
* 
* 
*/
class FurnitureController extends BController
{    
	public $model = 'Furniture'; //for loadModel function

	// Прикрепляем файлы
	public function init(){
		parent::init();
		cs()->registerScriptFile($this->module->assetsUrl.'/js/furniture.js');
		cs()->registerCssFile($this->module->assetsUrl.'/css/home.css');
	}

	/**
	 * index
	 */
	public function actionIndex(){

		$model = new Furniture('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		$provider = $model->search($criteria);


		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider'));
		}
        else{
        	$this->pageTitle   = 'Мебель';
       		$this->render('index', compact('provider'));
        }
	}

	/**
	 *  берем инфу по ID
	 */
	public function actionGetFurnitureInfo(){
		if($idFurniture = request()->getPost('idFurniture')){
			$model = $this->loadModel($this->model, $idFurniture);
			$Criteria = new CDbCriteria();
			$Criteria->with = ['related'];
			$Criteria->compare('`related`.`id_model`', $idFurniture);
			$Criteria->compare('`related`.`model`', 'Furniture');
			$photos = Photo::model()->findAll($Criteria);
			Common::jsonSuccess(true, ['attributes' => $model->attributes, 'photos' => $this->renderPartial('photos', compact('idFurniture', 'photos'), true)]);
		}
	}

	// регистрация заявления
	public function actionRegistration() {
		
		$Furniture = $this->loadModel('Furniture');
		$this->performAjaxValidation($Furniture);
		$post = request()->getPost('Furniture');
		$Furniture->setAttributes($post);

		if($Furniture->save()) {
			echo CJSON::encode(['success'=>true]);
		}
	}

	/**
	 * изменение заявления
	 */
	public function actionUpdate(){
		$post = request()->getPost('Furniture');
		$Furniture = $this->loadModel($this->model, $post['id']);
		$Furniture->setAttributes($post);

		//ajax validation
		$this->performAjaxValidation($Furniture);

		if($Furniture->save()) Common::jsonSuccess(true);
		Common::jsonSuccess(true, ['success' => false]);
	}

}
