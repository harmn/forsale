<?php 
/** 
* Region Modul
*
* 	"name" 				
*   "id_region"         
*/

class Region extends AR
{
	public $name;
	private static $listData; //для хранения кеша
	private static $listDataCustom; 	//для хранения кеша custom

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return 'region';
	}

	public function behaviors() {
	    return CMap::mergeArray(parent::behaviors(), [
	        'ml' => [
	        	'class' => 'core.behaviors.Ml',
                'langTableName' => 'region_lang',
                'langForeignKey' => 'id_region',
                'localizedAttributes' => array('name'), //attributes of the model to be translated
            ]
	    ]);
	}

	/**
     * list Data
     * возвращяет масив ['id' => 'name'] для dropDownList и сохраняет в кеш
     */
	public static function listData(){
		if(self::$listData) return self::$listData;

		$cacheKey = "listdata.Region".lang();
		
		if(self::$listData = Yii::app()->cache->get($cacheKey))
			return self::$listData;

		$table = self::model()->tableName();

		self::$listData =  CHtml::listData(self::model()->findAll(),'id', 'name');

		$dependency = new CDbCacheDependency($sql = "SELECT MAX(changed) FROM ".$table);
		Yii::app()->cache->set($cacheKey, self::$listData, 0, $dependency);
	
		return self::$listData;
	}

	/**
	 * list Data CUSTOM
	 * возвращяет масив ['id' => **, 'name' => **, 'id_job' => **, 'username' =>**] для dropDownList и сохраняет в кеш
	 */
	public static function listDataCustom($type = false, $id = false, $options = []){
		$cacheKey = "listdata.region.custom";

		$inshift = (isset($options['inshift']) && $options['inshift']) ? true : false;
		$depend  = (isset($options['depend']) && $options['depend']) ? true : false;
		
		// в случает eсли нужны те ползователи, которые находятся в смене
		$cacheKey = $inshift ? $cacheKey.".inshift" : $cacheKey;
		
		// в случает eсли ползователи нужны для зависимости от ролей
		$cacheKey = $depend ? $cacheKey.".depend" : $cacheKey;

		$criteria = ""; 
		$params = array();
		// $condition = " WHERE true";
		$select = "c.id, cl.name, c.lng, c.ltd"; //u.driver_color
		$condition = " "; //активные
		$join = "";
		$order = "c.id";
		$all = false;
	   	
		switch($type){
			case 'job':
				$cacheKey .= ".region.{$id}";
				$criteria = 'c.id_region = :id';
				$params[':id'] = $id;
				$condition .= " AND c.id_region = {$id}";
				break;
			case 'group':
				$select .= ', u.name';
				$cacheKey .= ".group.{$id}";
				$criteria .= 'j.group = :group';
				$params[':group'] = $id;

				if(isset($options['order']))
					$order = $options['order'];
				break;

			default:
				$cacheKey .= ".all";
				break;
		}

		//проверка кеша
		if((isset(self::$listDataCustom[$cacheKey]) && self::$listDataCustom[$cacheKey]) || (self::$listDataCustom[$cacheKey] = Yii::app()->cache->get($cacheKey))) {
			//Only users in shift
			if($inshift) $this->leaveInShift($cacheKey, self::$listDataCustom);

			return self::$listDataCustom[$cacheKey];
		}

		// collect command
		$command = Yii::app()->db->createCommand()
							->select($select)
							->leftJoin('region_lang cl', 'cl.id_region = c.id')
							->from(self::model()->tableName()." c")
							->order($order);
							// ->join("user_status us", 'u.id=us.id_user');
		if($type == 'group')
			$command->join("region j", "c.id_region = j.id");

		$query = $command->queryAll();

		// set id jobs as data attributes in options for depending with users
		if($depend)
			foreach ($query as $key => $value)
				self::$listDataCustom[$cacheKey][$value['id']] = [
					'data-lng' => $value['lng'],
					'data-ltd' => $value['ltd']
				];
		else
			foreach ($query as $key => $value) {
				self::$listDataCustom[$cacheKey][$value['id']] = [
					'id'		 => $value['id'],
					'name'		 => $value['name'],
					'data-lng' => $value['lng'],
					'data-ltd' => $value['ltd']
				];
				
				if(isset($options['display']) && !$options['display'])
					self::$listDataCustom[$cacheKey][$value['id']]['style'] = 'display: none;';
			}

		$dependency = new CDbCacheDependency($sql = "SELECT MAX(r.changed) FROM {{region}} r LEFT JOIN {{city}} c on r.id = c.id_region".$condition);
		Yii::app()->cache->set($cacheKey, self::$listDataCustom[$cacheKey], 0, $dependency);

		//Only users in shift
		if($inshift) $this->leaveInShift($cacheKey, self::$listDataCustom);
		
		return self::$listDataCustom[$cacheKey];
	}
}
