<?php 
/** 
* Car Modul
*
* 	"category" 				=> "int UNSIGNED", //vacharvum e, poxanakvum e
*	"type"					=>	"int UNSIGNED",	//Тип строения,
*	"price"		   			=> "int UNSIGNED", //	цена
*	"valuta"           		=> "int UNSIGNED", // 	валюта
*	"brand"					=> "int UNSIGNED", //	марка
*	"model"					=> "int UNSIGNED", //	модель
*	"condition"				=> "int UNSIGNED", // состояние
*	"release_year"	        => "datetime DEFAULT NULL", // Год выпуска
*	"mileage"			   	=> "int UNSIGNED", // Пробег
*	"transmission"	        => "int UNSIGNED", // Коробка передач
*	"seats_count"			=> "int UNSIGNED", // Количество сидений
*	"body"        			=> "int UNSIGNED", // Кузов
*	"rudder"				=> "int UNSIGNED", // Руль
*	"fuels"					=> "int UNSIGNED", // топлива
*	"color"					=> "int UNSIGNED", // цвет
* 	"title" 				=> "varchar(255) CHARACTER SET UTF8", Название
*	"description"			=> "TEXT NOT NULL", // описание
*	"created"              	=> "datetime DEFAULT NULL",
*	"id_creator"           	=> "int UNSIGNED",
*	"changed"              	=> "datetime DEFAULT NULL",
*	"id_changer"           	=> "int UNSIGNED",
*/

class Car extends AR
{
	// Тип 
	public static $type_select = [1 => "Автомобиль", 2 => "Автозапчасти", 3=>"Аксессуары", 4=>"Уникальная техника"]; 

	// марка
	public static $brand_select = [	
		1 => 	"AC Cars", 
		2 => 	"Acura",
		3 =>	"Agrale",
		4 =>	"Aixam",
		5 =>	"Alfa Romeo",
		6 =>	"Alpina",
		7 =>	"Ariel",
		8 =>	"ARO",
		9 =>	"Asia",
		10 =>	"Ashok Leyland",
		11 =>	"Aston Martin",
		12 =>	"Audi",
		13 =>	"Austin",
		14 =>	"AZLK (Москвич)",
		15 =>	"Beifang Benchi",
		16 =>	"BelAZ",
		17 =>	"Bentley",
		18 =>	"BMC",
		19 =>	"BMW",
		20 =>	"Brilliance",
		21 =>	"Bristol",
		22 =>	"Bugatti",
		23 =>	"Buick",
		24 =>	"BYD",
		25 =>	"Cadillac",
		26 =>	"Callaway",
		27 =>	"САМС",
		28 =>	"Caterham",
		29 =>	"Chana",
		30 =>	"Changfeng",
		31 =>	"Chery",
		32 =>	"Chevrolet",
		33 =>	"Chrysler",
		34 =>	"Citroen",
		35 =>	"Cizeta",
		36 =>	"Dacia",
		37 =>	"Dadi",
		38 =>	"Daewoo",
		39 =>	"DAF",
		40 =>	"Daihatsu",
		41 =>	"De Lorean",
		42 =>	"De Tomaso",
		43 =>	"Derways",
		44 =>	"Dodge",
		45 =>	"DONGFENG",
		46 =>	"Eagle",
		47 =>	"Eicher",
		48 =>	"ErAZ",
		49 =>	"Excalibur",
		50 =>	"FAW",
		51 =>	"Ferrari",
		52 =>	"Fiat",
		53 =>	"Ford",
		54 =>	"GAZ (ГАЗ)",
		55 =>	"Geely",
		56 =>	"Geo",
		57 =>	"Ginetta",
		58 =>	"GMC",
		59 =>	"Great Wall",
		60 =>	"HAFEI",
		61 =>	"Harley Davidson",
		62 =>	"Hino",
		63 =>	"Holden",
		64 =>	"Honda",
		65 =>	"HOWO",
		66 =>	"Huanghai",
		67 =>	"Hummer",
		68 =>	"Hyundai",
		69 =>	"IKCO",
		70 =>	"IJ (Иж)",
		71 =>	"Infiniti",
		72 =>	"Innocenti",
		73 =>	"Iran Khodro",
		74 =>	"Isuzu",
		75 =>	"Iveco",
		76 =>	"Jaguar",
		77 =>	"Jeep",
		78 =>	"Jensen",
		79 =>	"JMC",
		80 =>	"Kamaz",
		81 =>	"KAZ",
		82 =>	"Kenworth Truck",
		83 =>	"Kia",
		84 =>	"Koenigseegg",
		85 =>	"Lamborghini",
		86 =>	"Lancia",
		87 =>	"Land Rover",
		88 =>	"Landwind",
		89 =>	"LAZ",
		90 =>	"Lexus",
		91 =>	"LiAZ",
		92 =>	"Lincoln",
		93 =>	"Lotus",
		94 =>	"LUAZ (ЛуАЗ)",
		95 =>	"Mahindra",
		96 =>	"MAN",
		97 =>	"Maple (SMA)",
		98 =>	"Marcos",
		99 =>	"Maruti",
		100 =>	"Maserati",
		101 =>	"Maybach",
		102 =>	"MAZ",
		103 =>	"Mazda",
		104 =>	"McLaren",
		105 =>	"Mega",
		106 =>	"Mercedes",
		107 =>	"Mercury",
		108 =>	"MG",
		109 =>	"Mini",
		110 =>	"Mitsubishi",
		111 =>	"Morgan",
		112 =>	"Mustang",
		113 =>	"Nissan",
		114 =>	"Oldsmobile",
		115 =>	"Opel",
		116 =>	"Packard",
		117 =>	"Pagani",
		118 =>	"PAZ",
		119 =>	"Peterbilt",
		120 =>	"Peugeot",
		121 =>	"Plymouth",
		122 =>	"Pontiac",
		123 =>	"Porsche",
		124 =>	"Proton",
		125 =>	"RAF",
		126 =>	"REUANT",
		127 =>	"Renault",
		128 =>	"Rolls Royce",
		129 =>	"Rover",
		130 =>	"Saab",
		131 =>	"Saleen",
		132 =>	"Saturn",
		133 =>	"Scania",
		134 =>	"Scion",
		135 =>	"Seat",
		136 =>	"Setra",
		137 =>	"SHAANXI",
		138 =>	"Shelby",
		139 =>	"Shuanghuan",
		140 =>	"Sisu",
		141 =>	"Skoda",
		142 =>	"SMART",
		143 =>	"Soueast",
		144 =>	"Spyker",
		145 =>	"Ssang Young",
		146 =>	"Studebekker",
		147 =>	"Subaru",
		148 =>	"Suzuki",
		149 =>	"TagAZ",
		150 =>	"Tata",
		151 =>	"Tatra",
		152 =>	"Toyota",
		153 =>	"Trabant",
		154 =>	"Tramontana",
		155 =>	"Triumph",
		156 =>	"TVR",
		157 =>	"UAZ",
		158 =>	"Ural (Урал)",
		160 =>	"Vauxhall",
		161 =>	"VAZ",
		162 =>	"Venturi",
		163 =>	"VIS (ВАЗинвестСервис)",
		164 =>	"Volkswagen",
		165 =>	"Volvo",
		166 =>	"Western Star",
		167 =>	"Wiesmann",
		168 =>	"Xinkai",
		169 =>	"Zastava",
		170 =>	"ZAZ",
		171 =>	"ZIL (Зил)",
		172 =>	"ZXAuto",
		];
	// состояние
	public static $condition_select = [1 => "Новое", 2 => "С пробегом", 3=>"Битые"]; 
	// Валюта
	public static $valuta_select = [1 => "AMD", 2 => "USD", 3=>"RUB"];

	// Коробка передач
	public static $transmission_select = [1 => "Автомат", 2 => "Механический", 3=>"Типтроник"];

	// Количество сидений
	public static $seats_count_select = [1 => "1", 2 => "2", 3=>"4", 4=>"5", 5=>"6-8", 6=>" >= 9"];

	// Руль
	public static $rudder_select = [1 => "Левый", 2 => "Правый"];

	// Топлива
	public static $fuels_select = [1 => "Бензин", 2 => "Дизель", 3 => "Гибрид", 4 => "Газ/Бензин"];

	// Цвет
	public static $color_select = [
		1 	=> 	"белый", 
		2 	=> 	"желтый", 
		3 	=>	"зеленый",
		4 	=>	"золотой",
		5 	=>	"коричневый",
		6 	=>	"красный",
		7 	=>	"серебристый",
		8 	=>	"серый",
		9 	=>	"синий",
		10 	=>	"фиолетовый",
		11 	=>	"черный",
	];

	// Кузов
	public static $body_select = [
		1 	=> 	"Седан", 
		2 	=> 	"хэтчбек", 
		3 	=>	"универсал",
		4 	=>	"внедорожник",
		5 	=>	"кроссовер",
		6 	=>	"пикап",
		7 	=>	"купе",
		8 	=>	"кабриолет",
		9 	=>	"минивен",
		10 	=>	"фургон",
		11 	=>	"микроавтобус",
	];
	

	// Пробег
	public static $mileage_select = [	
		1 	=> 	"5000", 
		2 	=> 	"10000", 
		3 	=>	"20000",
		4 	=>	"30000",
		5 	=>	"40000",
		6 	=>	"50000",
		7 	=>	"60000",
		8 	=>	"70000",
		9 	=>	"80000",
		10 	=>	"90000",
		11 	=>	"100000",
		12	=>	"110000",
		13	=>	"120000",
		14	=>	"130000",
		15 	=>	"140000",
		16 	=>	"150000",
		17 	=>	"160000",
		18 	=>	"170000",
		19 	=>	"180000",
		20 	=>	"190000",
		21 	=>	"200000",
		22 	=>	"210000",
		23 	=>	"220000",
		24 	=>	"230000",
		25 	=>	"240000",
		26 	=>	"250000",
		27 	=>	"260000",
		28 	=>	"270000",
		29 	=>	"280000",
		30 	=>	"290000",
		31 	=>	"300000",
	]; 

	public $type_filter, $fullname, $price_filter_from, $price_filter_to, $brand_filter, $mileage_from_filter, $mileage_to_filter, $body_filter, $rudder_filter, $release_year_from, $release_year_to, $transmission_filter, $seats_count_filter, $color_filter, $condition_filter, $category_filter, $region_filter, $city_filter;

	public $searchAttributes = ['type_filter', 'brand_filter', 'mileage_from_filter', 'mileage_to_filter', 'body_filter', 'rudder_filter', 'release_year_from', 'release_year_to', 'transmission_filter', 'seats_count_filter', 'color_filter', 'condition_filter', 'category_filter', 'region_filter', 'city_filter'];
	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return '{{car}}';
	}

	/**
	 * Правила валидации
	 */
	public function rules() {
		return CMap::mergeArray(parent::rules(), array(
			array('valuta, brand_filter, body_filter, rudder_filter, transmission_filter, seats_count_filter, color_filter, condition_filter, condition, category, category_filter,  type, type_filter', 'numerical', 'integerOnly' => true, 'allowEmpty'=>true),
			array('price_filter_from, price_filter_to, price, mileage, mileage_to_filter, mileage_from_filter, id_city, id_region, region_filter, city_filter, active', 'numerical'),
			array('description, fullname, title, lng, ltd', 'filter', 'filter'=>'trim'),
			array('description, title', 'required', 'on' => 'create, update'),
			array('release_year, release_year_from, release_year_to', 'safe'),
			array('brand, release_year, transmission, seats_count, body, rudder, fuels, color, category, type', 'required', 'on' => 'create, update'),
		));
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations(){
		return [
			'client' 		=> [self::BELONGS_TO, 'Client', 'id_creator'],
			'favorites'		=> [self::HAS_MANY, 'Favorites', 'id_advert', 'on'=>'model = :model AND id_user = :idUser', 'params'=>[':model'=>__CLASS__, ':idUser' => user()->id]],
			'relatedphotos' => [self::HAS_MANY, 'RelatedPhoto', 'id_model', 'condition'=>'model = :model', 'params'=>[':model'=>__CLASS__]],
			'photos' 		=> [self::HAS_MANY, 'Photo', ['id_photo'=>'id'], 'through'=>'relatedphotos', 'deleteBehavior' => true],
		];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => array(
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			),
			'filters'         => ['class'=>'core.behaviors.FilterBehavior'],
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filteњr conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = false, $group = false){

		if(!$criteria) $criteria = new CDbCriteria;

		$criteria->with = ['client'];
		
		if($this->fullname){
			$criteria->addCondition("client.firstname LIKE :fullname OR client.lastname LIKE :fullname OR client.middlename LIKE :fullname");
			$criteria->params[':fullname'] = "%".$this->fullname."%";
		}

		if($this->price){
			list($priceFrom, $priceTo) = explode(',', $this->price);
			$criteria->addCondition("t.price >= :priceFrom AND t.price <= :priceTo");
			$criteria->params += [':priceFrom' => $priceFrom, ':priceTo' => $priceTo];
		}

		if($this->price_filter_from && $this->price_filter_to){
			$criteria->condition = 't.price >= :price_filter_from AND t.price <= :price_filter_to';
   			$criteria->params = array(':price_filter_from' => $this->price_filter_from, ':price_filter_to' => $this->price_filter_to);
		}
		else if($this->price_filter_from){
			$criteria->condition = 't.price >= :price_filter_from';
			$criteria->params = array(':price_filter_from' => $this->price_filter_from);
		}
		else if($this->price_filter_to){
			$criteria->condition = 't.price <= :price_filter_to';
			$criteria->params = array(':price_filter_to' => $this->price_filter_to);
		}

		$criteria->compare('t.brand', $this->brand_filter);

		if($this->mileage_from_filter && $this->mileage_to_filter){
			$criteria->condition = 't.mileage >= :mileage_from_filter AND t.mileage <= :mileage_to_filter';
   			$criteria->params = array(':mileage_from_filter' => $this->mileage_from_filter, ':mileage_to_filter' => $this->mileage_to_filter);
		}
		else if($this->mileage_from_filter){
			$criteria->condition = 't.mileage >= :mileage_from_filter';
			$criteria->params = array(':mileage_from_filter' => $this->mileage_from_filter);
		}
		else if($this->mileage_to_filter){
			$criteria->condition = 't.mileage <= :mileage_to_filter';
			$criteria->params = array(':mileage_to_filter' => $this->mileage_to_filter);
		}

		$criteria->compare('t.body', $this->body_filter);
		$criteria->compare('t.rudder', $this->rudder_filter);

		$criteria->compare('t.transmission', $this->transmission_filter);
		$criteria->compare('t.seats_count', $this->seats_count_filter);
		$criteria->compare('t.color', $this->color_filter);
		$criteria->compare('t.condition', $this->condition_filter);
		$criteria->compare('t.category', $this->category_filter);
		$criteria->compare('t.type', $this->type_filter);
		$criteria->compare('t.id_region', $this->region_filter);
		$criteria->compare('t.id_city', $this->city_filter);
		$criteria->compare('t.active', "1");

		if($this->release_year_from && $this->release_year_to){
			$criteria->condition = 't.release_year >= :release_year_from AND t.release_year <= :release_year_to';
   			$criteria->params = array(':release_year_from' => $this->release_year_from, ':release_year_to' => $this->release_year_to);
		}
		else if($this->release_year_from){
			$criteria->condition = 't.release_year >= :release_year_from';
			$criteria->params = array(':release_year_from' => $this->release_year_from);
		}
		else if($this->release_year_to){
			$criteria->condition = 't.release_year <= :release_year_to';
			$criteria->params = array(':release_year_to' => $this->release_year_to);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			  'pageSize'=>Common::getPagerSize(__CLASS__),
			  'pageVar' => 'page'
			),
			'sort'=>array(
				'defaultOrder'=>'t.id DESC',
				'attributes'=>array(
					'id',
					'price',
					'title',
					// 'number'=>array(
		   //              'asc'=>'card.id',
		   //              'desc'=>'card.id DESC',
		   //          ),
					// 'begin_date',
					// 'end_date',
				)
			),
		));
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return CMap::mergeArray(parent::attributeLabels(), array(
			'price'    			=> 'Цена',
			'valuta'    		=> 'Валюта',
			'brand'    			=> 'Марка',
			'model'    			=> 'Модель',
			'condition'    		=> 'Состояние',
			'release_year'    	=> 'Год выпуска',
			'mileage'    		=> 'Пробег',
			'transmission'    	=> 'Кор/п',
			'seats_count'    	=> 'Кол/с',
			'body'    			=> 'Кузов',
			'rudder'    		=> 'Руль',
			'fuels'    			=> 'Топливо',
			'color'    			=> 'Цвет',
			'id_creator' 		=> 'Пользователь',
			'description' 		=> 'Описание',
			'fullname'			=> 'Пользователь',
			'valuta'			=> 'Валюта',
			'category'			=> 'Категория',
			'type' 				=> 'Тип',
			'title'  			=> 'Название',
			'id_region'  		=> 'Регион',
			'id_city'  			=> 'Район'
		));
		
	}
}

