<?php 
/** 
* Languages Modul
*
* 	"id" 				
*   "name"         
*/

class Languages extends AR
{
	public $name;
	private static $listData; //для хранения кеша

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return 'languages';
	}

	public function behaviors() {
	    return CMap::mergeArray(parent::behaviors(), [
	        'ml' => [
	        	'class' => 'core.behaviors.Ml',
                'langTableName' => 'languages_lang',
                'langForeignKey' => 'id_language',
                'localizedAttributes' => array('name'), //attributes of the model to be translated
            ]
	    ]);
	}
	
	/**
     * list Data
     * возвращяет масив ['id' => 'name'] для dropDownList и сохраняет в кеш
     */
	public static function listData(){
		// if(self::$listData) return self::$listData;

		$cacheKey = "listdata.languages".lang();
		
		// if(self::$listData = Yii::app()->cache->get($cacheKey))
		// 	return self::$listData;

		$table = self::model()->tableName();

		self::$listData =  CHtml::listData(self::model()->findAll(),'id', 'name');

		$dependency = new CDbCacheDependency($sql = "SELECT MAX(changed) FROM ".$table);
		Yii::app()->cache->set($cacheKey, self::$listData, 0, $dependency);
	
		return self::$listData;
	}
}
