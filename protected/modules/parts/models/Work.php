<?php 
/** 
* Work Model 
*
*	"salary"		   		=> "int UNSIGNED",
*	"valuta"           		=> "int UNSIGNED",
*	"age_from"				=> "int UNSIGNED", // Возраст От
*	"age_to"				=> "int UNSIGNED", // Возраст До
*	"graph" 				=> "int UNSIGNED", // график
*	"education" 			=> "int UNSIGNED", // образование
*	"languages" 			=> "int UNSIGNED", // языки
*	"sex" 					=> "int UNSIGNED", // Пол
*	"experience" 			=> "varchar(255) CHARACTER SET UTF8", // опыт работы
* 	"title" 				=> "varchar(255) CHARACTER SET UTF8", Название
*	"description"			=> "TEXT NOT NULL",
*	"created"              	=> "datetime DEFAULT NULL",
*	"id_creator"           	=> "int UNSIGNED",
*	"changed"              	=> "datetime DEFAULT NULL",
*	"id_changer"           	=> "int UNSIGNED",
*/



class Work extends AR
{
	// Тип 
	public static $type_select = [
				1 	=> "Офисная работа", 
				2 	=> "Торговля",
				3 	=> "Финансы и право", 
				4 	=> "Информационные технологии",
				5 	=> "Mедиа и дизайн", 
				6 	=> "Рестораны и кухня",
				7 	=> "Туризм и отели",
				8 	=> "Танспорт и такси",
				9 	=> "Бизнес и маркетинг",
				10 	=> "Строительство и архитектура",
				11 	=> "Домашнее хозяйство",
				12 	=> "Производство",
				13 	=> "Образования",
				14 	=> "Здравоохранения",
				15 	=> "Другое",
	]; 

	// Валюта
	public static $valuta_select = [1 => "AMD", 2 => "USD", 3=>"RUB"];

	// график
	public static $graph_select = [
		1 	=> 	"Полный", 
		2 	=> 	"Неполный", 
	];

	// education
	public static $education_select = [
		1 	=> 	"Профессиональное", 
		2 	=> 	"Начальное", 
		3 	=> 	"Среднее", 
		4 	=> 	"Бакалавр", 
		5 	=> 	"Магистр", 
		6 	=> 	"Аспирант",
		7 	=> 	"Кандидат",
		8 	=> 	"Доктор",
		9   =>  "Другое"
	];

	// sex
	public static $sex_select = [
		1 	=> 	"Мужской", 
		2 	=> 	"Женский", 
	];

	public $type_filter, $fullname, $salary_filter, $age_filter_from, $age_filter_to, $work_filter, 
			$graph_filter, $education_filter, $languages_filter, $sex_filter, $region_filter, $city_filter;

	public $searchAttributes = ['type_filter', 'work_filter', 'graph_filter', 'education_filter', 'languages_filter', 'sex_filter', 'age_filter_from', 'age_filter_to', 'region_filter', 'city_filter'];

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return '{{work}}';
	}

	/**
	 * Правила валидации
	 */
	public function rules() {
		return CMap::mergeArray(parent::rules(), array(
			array('valuta, age_from, age_to, type, type_filter, graph, education, 
					languages_filter, sex, education_filter, graph_filter, sex_filter,
					age_filter_from, age_filter_to, region_filter, city_filter, id_city, id_region, active', 'numerical', 'integerOnly' => true, 'allowEmpty'=>true),
			array('description, fullname, experience, title, salary_filter, salary, lng, ltd', 'filter', 'filter'=>'trim'),
			array('description, title', 'required', 'on' => 'create, update'),
			array('type', 'required', 'on' => 'create, update'),
		));
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations(){
		return [
			'client' 			=> [self::BELONGS_TO, 'Client', 'id_creator'],
			'favorites'			=> [self::HAS_MANY, 'Favorites', 'id_advert', 'on'=>'model = :model AND id_user = :idUser', 'params'=>[':model'=>__CLASS__, ':idUser' => user()->id]],
			'relatedphotos'  	=> [self::HAS_MANY, 'RelatedPhoto', 'id_model', 'condition'=>'model = :model', 'params'=>[':model'=>__CLASS__]],
			'photos' 			=> [self::HAS_MANY, 'Photo', ['id_photo'=>'id'], 'through'=>'relatedphotos', 'deleteBehavior' => true],
			'languages' 		=> [self::MANY_MANY, 'Languages','work_languages(id_work, id_language)']
		];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => array(
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			),
			'withRelated' => array('class'=>'core.behaviors.WithRelatedBehavior'),
			'manyMany' => [
				//этот вариант отлично решает вопрос с MANY_MANY
				'class' => 'core.behaviors.CAdvancedArBehavior'
			],
			'filters'         => ['class'=>'core.behaviors.FilterBehavior'],
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filteњr conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = false, $group = false){

		if(!$criteria) $criteria = new CDbCriteria;

		$criteria->with = ['client', 'languages'];
		$criteria->together = true;
		
		if($this->fullname){
			$criteria->addCondition("client.firstname LIKE :fullname OR client.lastname LIKE :fullname OR client.middlename LIKE :fullname");
			$criteria->params[':fullname'] = "%".$this->fullname."%";
		}

		if($this->salary){
			list($salaryFrom, $salaryTo) = explode(',', $this->salary);
			$criteria->addCondition("t.salary >= :salaryFrom AND t.salary <= :salaryTo");
			$criteria->params += [':salaryFrom' => $salaryFrom, ':salaryTo' => $salaryTo];
		}

		$criteria->compare('t.type', $this->type_filter);
		$criteria->compare('t.work', $this->work_filter);
		$criteria->compare('t.education', $this->education_filter);
		$criteria->compare('t.graph', $this->graph_filter);
		$criteria->compare('t.experience', $this->experience, true);
		$criteria->compare('t.salary', $this->salary_filter, true);
		$criteria->compare('languages.id', $this->languages_filter);
		$criteria->compare('t.sex', $this->sex_filter);
		$criteria->compare('t.id_region', $this->region_filter);
		$criteria->compare('t.id_city', $this->city_filter);
		$criteria->compare('t.active', "1");
		
		if($this->age_filter_from && $this->age_filter_to){
			$criteria->condition = 't.age_from >= :age_filter_from AND t.age_to <= :age_filter_to';
   			$criteria->params = array(':age_filter_from' => $this->age_filter_from, ':age_filter_to' => $this->age_filter_to);
		}else if($this->age_filter_from){
			$criteria->condition = 't.age_from >= :age_filter_from OR :age_filter_from <= t.age_to';
			$criteria->params = array(':age_filter_from' => $this->age_filter_from);
		}else if($this->age_filter_to){
			$criteria->condition = 't.age_to <= :age_filter_to OR :age_filter_to <= t.age_from';
			$criteria->params = array(':age_filter_to' => $this->age_filter_to);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			  'pageSize'=>Common::getPagerSize(__CLASS__),
			  'pageVar' => 'page'
			),
			'sort'=>array(
				'defaultOrder'=>'t.id DESC',
				'attributes'=>array(
					'id',
					'price',
					'title',
					// 'number'=>array(
		   //              'asc'=>'card.id',
		   //              'desc'=>'card.id DESC',
		   //          ),
					// 'begin_date',
					// 'end_date',
				)
			),
		));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return CMap::mergeArray(parent::attributeLabels(), array(
			'type' 				=> 'Тип',
			'salary'    		=> 'Зарплата',
			'valuta'    		=> 'Валюта',
			'age_from'    		=> 'Возраст от',
			'age_to'    		=> 'Возраст до',
			'graph'    			=> 'График',
			'education'    		=> 'Oбразование',
			'languages'    		=> 'Языки',
			'sex'    			=> 'Пол',
			'experience'    	=> 'Опыт работы',
			'description'    	=> 'Описание',
			'id_creator' 		=> 'Пользователь',
			'fullname'			=> 'Пользователь',
			'title'  			=> 'Название',
			'id_region'  		=> 'Регион',
			'id_city'  			=> 'Район'
		));
	}
}
