<?php 
/** 
* WorkLanguages Modul
*
* 	"id" 				
*   "id_work"
* 	"id_language"         
*/

class WorkLanguages extends AR
{

	private static $listData; //для хранения кеша

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return 'work_languages';
	}
}
