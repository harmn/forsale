<?php 
/** 
* Electronics Modul
*
* 	"category" 				=> "int UNSIGNED", //vacharvum e, poxanakvum e
*	"type"					=>	"int UNSIGNED",	//Тип строения,
*	"brand"					=> "int UNSIGNED", //	марка
*	"price"		   			=> "int UNSIGNED", //	цена
*	"valuta"           		=> "int UNSIGNED", // 	валюта
*	"condition"				=> "int UNSIGNED", // состояние
*	"release_year"	        => "DATE", // Год выпуска
*	"color"					=> "int UNSIGNED", // цвет
* 	"title" 				=> "varchar(255) CHARACTER SET UTF8", Название
*	"description"			=> "TEXT NOT NULL", // описание
*	"created"              	=> "datetime DEFAULT NULL",
*	"id_creator"           	=> "int UNSIGNED",
*	"changed"              	=> "datetime DEFAULT NULL",
*	"id_changer"           	=> "int UNSIGNED",
*/

class Electronics extends AR
{
	// Тип 
	public static $type_select = [
				1 => "Телефоны", 
				2 => "Телефонные аксессуары", 
				3 => "SIM карты", 
				4 => "Ноутбуки",
				5 => "Планшеты",
				6 => "Компьютеры",
				7 => "Компьютерные устройства",
				8 => "Компьютерные аксессуары",
				9 => "Телевизоры",
				10 => "Другое",
	]; 

	// состояние
	public static $condition_select = [1 => "Новое", 2 => "Использованные"]; 

	// Валюта
	public static $valuta_select = [1 => "AMD", 2 => "USD", 3=>"RUB"];

	// Цвет
	public static $color_select = [
		1 	=> 	"белый", 
		2 	=> 	"желтый", 
		3 	=>	"зеленый",
		4 	=>	"золотой",
		5 	=>	"коричневый",
		6 	=>	"красный",
		7 	=>	"серебристый",
		8 	=>	"серый",
		9 	=>	"синий",
		10 	=>	"фиолетовый",
		11 	=>	"черный",
	];

	public $type_filter, $fullname, $price_filter_from, $price_filter_to, $brand_filter, $release_year_from, $release_year_to, $color_filter, $condition_filter, $category_filter, $region_filter, $city_filter;

	public $searchAttributes = ['type_filter', 'brand_filter',  'release_year_from', 'release_year_to', 'color_filter', 'condition_filter', 'category_filter', 'region_filter', 'city_filter'];

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return '{{electronics}}';
	}

	/**
	 * Правила валидации
	 */
	public function rules() {
		return CMap::mergeArray(parent::rules(), array(
			array('valuta, brand_filter, color_filter, condition_filter, condition, category, category_filter,  type, type_filter, color', 'numerical', 'integerOnly' => true, 'allowEmpty'=>true),
			array('price_filter_from, price_filter_to, price, region_filter, city_filter, id_city, id_region, active', 'numerical'),
			array('description, fullname, title, lng, ltd', 'filter', 'filter'=>'trim'),
			array('description, title', 'required', 'on' => 'create, update'),
			array('release_year, release_year_from, release_year_to', 'safe'),
			array('category, type', 'required', 'on' => 'create, update'),
		));
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations(){
		return [
			'client' 		=> [self::BELONGS_TO, 'Client', 'id_creator'],
			'favorites'		=> [self::HAS_MANY, 'Favorites', 'id_advert', 'on'=>'model = :model AND id_user = :idUser', 'params'=>[':model'=>__CLASS__, ':idUser' => user()->id]],
			'relatedphotos' => [self::HAS_MANY, 'RelatedPhoto', 'id_model', 'condition'=>'model = :model', 'params'=>[':model'=>__CLASS__]],
			'photos' 		=> [self::HAS_MANY, 'Photo', ['id_photo'=>'id'], 'through'=>'relatedphotos', 'deleteBehavior' => true],
		];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => array(
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			),
			'filters'         => ['class'=>'core.behaviors.FilterBehavior'],
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filteњr conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = false, $group = false){

		if(!$criteria) $criteria = new CDbCriteria;

		$criteria->with = ['client'];
		
		if($this->fullname){
			$criteria->addCondition("client.firstname LIKE :fullname OR client.lastname LIKE :fullname OR client.middlename LIKE :fullname");
			$criteria->params[':fullname'] = "%".$this->fullname."%";
		}

		if($this->price){
			list($priceFrom, $priceTo) = explode(',', $this->price);
			$criteria->addCondition("t.price >= :priceFrom AND t.price <= :priceTo");
			$criteria->params += [':priceFrom' => $priceFrom, ':priceTo' => $priceTo];
		}
		
		if($this->price_filter_from && $this->price_filter_to){
			$criteria->condition = 't.price >= :price_filter_from AND t.price <= :price_filter_to';
   			$criteria->params = array(':price_filter_from' => $this->price_filter_from, ':price_filter_to' => $this->price_filter_to);
		}
		else if($this->price_filter_from){
			$criteria->condition = 't.price >= :price_filter_from';
			$criteria->params = array(':price_filter_from' => $this->price_filter_from);
		}
		else if($this->price_filter_to){
			$criteria->condition = 't.price <= :price_filter_to';
			$criteria->params = array(':price_filter_to' => $this->price_filter_to);
		}

		$criteria->compare('t.brand', $this->brand_filter);
		$criteria->compare('t.color', $this->color_filter);
		$criteria->compare('t.condition', $this->condition_filter);
		$criteria->compare('t.category', $this->category_filter);
		$criteria->compare('t.type', $this->type_filter);
		$criteria->compare('t.id_region', $this->region_filter);
		$criteria->compare('t.id_city', $this->city_filter);
		$criteria->compare('t.active', "1");

		if($this->release_year_from && $this->release_year_to){
			$criteria->condition = 't.release_year >= :release_year_from AND t.release_year <= :release_year_to';
   			$criteria->params = array(':release_year_from' => $this->release_year_from, ':release_year_to' => $this->release_year_to);
		}
		else if($this->release_year_from){
			$criteria->condition = 't.release_year >= :release_year_from';
			$criteria->params = array(':release_year_from' => $this->release_year_from);
		}
		else if($this->release_year_to){
			$criteria->condition = 't.release_year <= :release_year_to';
			$criteria->params = array(':release_year_to' => $this->release_year_to);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			  'pageSize'=>Common::getPagerSize(__CLASS__),
			  'pageVar' => 'page'
			),
			'sort'=>array(
				'defaultOrder'=>'t.id DESC',
				'attributes'=>array(
					'id',
					'price',
					'title',
					// 'number'=>array(
		   //              'asc'=>'card.id',
		   //              'desc'=>'card.id DESC',
		   //          ),
					// 'begin_date',
					// 'end_date',
				)
			),
		));
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return CMap::mergeArray(parent::attributeLabels(), array(
			'category'			=> 'Категория',
			'type' 				=> 'Тип',
			'price'    			=> 'Цена',
			'valuta'    		=> 'Валюта',
			'brand'    			=> 'Марка',
			'condition'    		=> 'Состояние',
			'release_year'    	=> 'Год выпуска',
			'color'    			=> 'Цвет',
			'id_creator' 		=> 'Пользователь',
			'description' 		=> 'Описание',
			'fullname'			=> 'Пользователь',
			'title'  			=> 'Название',
			'id_region'  		=> 'Регион',
			'id_city'  			=> 'Район'
		));
	}
}
