<?php 
/** 
* LastAdverts Modul
*
* 	"name" 				
*   "id_region"         
*/

class LastAdverts extends AR
{
	public $filename, $id_model, $id_user, $type, $favorites, $later, $data;
	private static $listData; //для хранения кеша

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return 'last_adverts';
	}

	public function behaviors() {
	    return CMap::mergeArray(parent::behaviors(), [
	        'dateBehavior' => array(
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			),
	    ]);
	}

	/**
     * list Data
     * возвращяет масив ['id' => 'name'] для dropDownList и сохраняет в кеш
     */
	public static function listData(){
		if(self::$listData) return self::$listData;

		$cacheKey = "listdata.lastAdverts".user()->id;
		if(self::$listData = Yii::app()->cache->get($cacheKey))
			return self::$listData;

		$table = self::model()->tableName();

	 	$criteria = new CDbCriteria;
	 	$criteria->select  	= 't.*, p.filename, rf.id_model, p.id as id_rf';
	 	$criteria->order 	= 't.created DESC, p.pos ASC';
 	  	$criteria->join 	= 'LEFT JOIN related_photo rf ON rf.model=t.model AND rf.id_model=t.id_item ';
 	  	$criteria->join    .= 'LEFT JOIN photo p ON p.id= rf.id_photo ';
 	  	if(user()->id){
	 		$criteria->select  	.= ', f.id_user, f.type, t.model';
 	  		$criteria->join    .= 'LEFT JOIN favorites f ON t.id_item= f.id_advert AND t.model = f.model AND f.id_user = '.user()->id;
 	  	}

		$result = self::model()->findAll($criteria);

		self::$listData = [];
		foreach($result as $item){
			if(!isset(self::$listData[$item->model]) || !is_array(self::$listData[$item->model])){
				self::$listData[$item->model] = [];
			}

			if(count(self::$listData[$item->model]) < 4 && !in_array($item->id_item, self::$listData[$item->model])){
				self::$listData[$item->model][$item->id_item]['data'] = $item;
				if(isset($item->type) && $item->type){
					if(!isset(self::$listData[$item->model][$item->id_item]['favorites']) || !is_array(self::$listData[$item->model][$item->id_item]['favorites'])){
						self::$listData[$item->model][$item->id_item]['favorites'] = [];
					}
					if(!in_array($item->type, self::$listData[$item->model][$item->id_item]['favorites']))
						self::$listData[$item->model][$item->id_item]['favorites'][] = $item->type;
				}
			}
		}
		$dependency = new CDbCacheDependency($sql = "SELECT MAX(changed) FROM ".$table);
		Yii::app()->cache->set($cacheKey, self::$listData, 0, $dependency);
	
		return self::$listData;
	}
}
