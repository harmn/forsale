<?php 
/**
* Home Modul
*
* 	category 		- int UNSIGNED,  						vacharvum e, poxanakvum e
* 	price 			- int UNSIGNED,  						цена
* 	valuta 			- int UNSIGNED,  						Валюта
* 	room_count 		- int UNSIGNED,							Количество комнат
* 	area 			- varchar(255) CHARACTER SET UTF8,		плошадь
* 	floor 			- int UNSIGNED,							этаж
* 	floor_count 	- int UNSIGNED,							количество этажей
* 	type 			- int UNSIGNED,							Тип строения
* 	id_creator 		- int UNSIGNED,							Кто создовал заявление, // anhat , gorcakal
* 	title 			- varchar(255) CHARACTER SET UTF8,		Название
* 	description 	- TEXT NOT NULL,						Описание
*/

class Home extends AR
{
	// Для чего предназначено строение
	public static $category_select = [1 => "Продается", 2 => "Обменивается", 3 => "Аренда"];
	// Тип 
	public static $type_select = [1 => "Квартира", 2 => "Офис", 3=>"Гараж", 4=>"Земельный участок",  5=>"Дом"]; 
	// Валюта
	public static $valuta_select = [1 => "AMD", 2 => "USD", 3=>"RUB"];

	public $fullname, $room_count_filter, $type_filter, $price_filter_from, $price_filter_to, $category_filter, $floor_filter, $region_filter, $city_filter, $modelName;

	public $searchAttributes = ['room_count_filter', 'type_filter', 'price_filter_from', 'category_filter', 'price_filter_to', 'floor_filter', 'region_filter', 'city_filter'];
	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return '{{home}}';
	}

	/**
	 * Правила валидации
	 */
	public function rules() {
		return CMap::mergeArray(parent::rules(), array(
			array('room_count, room_count_filter,floor, floor_count, type, valuta, type_filter, category_filter, floor_filter, 
				category, region_filter, city_filter, id_city, id_region', 'numerical', 'integerOnly' => true, 'allowEmpty'=>true),
			array('price_filter_from, price_filter_to, price, active', 'numerical'),
			array('area, description, fullname, title, lng, ltd, modelName', 'filter', 'filter'=>'trim'),
			array('room_count, type, description, area, title', 'required', 'on' => 'create, update'),
		));
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations(){
		return [
			'client' 		=> [self::BELONGS_TO, 'Client', 'id_creator'],
			'favorites'		=> [self::HAS_MANY, 'Favorites', 'id_advert', 'on'=>'model = :model AND id_user = :idUser', 'params'=>[':model'=>__CLASS__, ':idUser' => user()->id]],
			'relatedphotos' => [self::HAS_MANY, 'RelatedPhoto', 'id_model', 'condition'=>'model = :model', 'params'=>[':model'=>__CLASS__]],
			'photos' 		=> [self::HAS_MANY, 'Photo', ['id_photo'=>'id'], 'through'=>'relatedphotos', 'deleteBehavior' => true],
		];
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => array(
				'class'           => 'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
			),
			'filters'         => ['class'=>'core.behaviors.FilterBehavior'],
		));
	}

	/**
	 * Retrieves a list of models based on the current search/filteњr conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = false, $group = false){

		if(!$criteria) $criteria = new CDbCriteria;

		$criteria->with = ['client'];
		$criteria->together = true;
		
		if($this->fullname){
			$criteria->addCondition("client.firstname LIKE :fullname OR client.lastname LIKE :fullname OR client.middlename LIKE :fullname");
			$criteria->params[':fullname'] = "%".$this->fullname."%";
		}

		if($this->price){
			list($priceFrom, $priceTo) = explode(',', $this->price);
			$criteria->addCondition("t.price >= :priceFrom AND t.price <= :priceTo");
			$criteria->params += [':priceFrom' => $priceFrom, ':priceTo' => $priceTo];
		}

		if($this->price_filter_from && $this->price_filter_to){
			$criteria->condition = 't.price >= :price_filter_from AND t.price <= :price_filter_to';
   			$criteria->params = array(':price_filter_from' => $this->price_filter_from, ':price_filter_to' => $this->price_filter_to);
		}
		else if($this->price_filter_from){
			$criteria->condition = 't.price >= :price_filter_from';
			$criteria->params = array(':price_filter_from' => $this->price_filter_from);
		}
		else if($this->price_filter_to){
			$criteria->condition = 't.price <= :price_filter_to';
			$criteria->params = array(':price_filter_to' => $this->price_filter_to);
		}
		$criteria->compare('t.floor', $this->floor_filter);
		$criteria->compare('t.room_count', $this->room_count_filter);
		$criteria->compare('t.type', $this->type_filter);
		$criteria->compare('t.category', $this->category_filter);
		$criteria->compare('t.id_region', $this->region_filter);
		$criteria->compare('t.id_city', $this->city_filter);
		$criteria->compare('t.active', "1");
		

		// $this->compareDate($criteria, "t.begin_date", ">{$this->begin_date_from}");
  // 		$this->compareDate($criteria, "t.begin_date", "<{$this->begin_date_to}");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			  'pageSize'=>Common::getPagerSize(__CLASS__),
			  'pageVar' => 'page'
			),
			'sort'=>array(
				'defaultOrder'=>'t.id DESC',
				'attributes'=>array(
					'id',
					'price',
					'title',
					// 'number'=>array(
		   //              'asc'=>'card.id',
		   //              'desc'=>'card.id DESC',
		   //          ),
					// 'begin_date',
					// 'end_date',
				)
			),
		));
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return CMap::mergeArray(parent::attributeLabels(), array(
			'category'   		=> 'Категория',
			'price'    			=> 'Цена',
			'room_count'   		=> 'Количество комнат',
			'area' 				=> 'плошадь',
			'floor' 			=> 'Этаж',
			'floor_count' 		=> 'Количество этажей',
			'type' 				=> 'Тип',
			'id_creator' 		=> 'Пользователь',
			'description' 		=> 'Описание',
			'fullname'			=> 'Пользователь',
			'valuta'			=> 'Валюта',
			'title'  			=> 'Название',
			'id_region'  		=> 'Регион',
			'id_city'  			=> 'Район'
		));
		
	}
}

