<?php
return array(
	'modules' => array(),
	'import' => array(
		//user module
		'application.modules.parts.models.*',
		'application.modules.parts.components.*',
        'application.modules.parts.behaviors.*',
	),
    'params' => array(
        'images'=>array(
            'parts' => array(
                'path' => 'storage/images/parts/',
                'placeholder' => 'img/placeholders/parts/',
                'sizes'=>array(
                    'original'  => array(),
                    'thumb'     => array('width'    => 100,  'height'    => 100, 'crop' => true),
                    'big'       => array('width'    => 450,  'height'    => 280, 'crop' => true /*'watermark' => true*/),
                    'customBig' => array('width'    => 300,  'height'    => 200, 'crop' => true /*'watermark' => true*/),
                    'small'     => array('width'    => 80,   'height'    => 60,  'crop' => true),
                    'custom'    => array('width'    => 200,  'height'    => 133, 'crop' =>true),
                )
            )
        ),
    ),
	'components' => array(
    ),
    'rules' => array(

        ADMIN_PATH.'/parts' => 'parts/home/index',
        
        ADMIN_PATH.'/parts/<action:\w+>/<id:\d+>' => 'parts/back/<action>',
        ADMIN_PATH.'/parts/<action:\w+>/*' => 'parts/back/<action>',

    )
);