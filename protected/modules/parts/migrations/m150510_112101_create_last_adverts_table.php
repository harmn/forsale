<?php

class m150510_112101_create_last_adverts_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{last_adverts}}")){
			$this->dropTable("{{last_adverts}}");
		}

		$this->createTable("{{last_adverts}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"model"              	=> "varchar(255) CHARACTER SET UTF8", 
			"id_item"		   		=> "int UNSIGNED",
			"title"		   			=> "varchar(500) CHARACTER SET UTF8",
			"price"		   			=> "int UNSIGNED DEFAULT 0",
			"valuta"		   		=> "int UNSIGNED DEFAULT 1",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{last_adverts}}")){
			$this->dropTable("{{last_adverts}}");
		}
	}
}