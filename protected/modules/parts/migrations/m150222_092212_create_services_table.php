<?php

class m150222_092212_create_services_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->dropTable("{{services}}");
		}

		$this->createTable("{{services}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"type"        			=> "int UNSIGNED", 
			"salary"		   		=> "int UNSIGNED",
			"valuta"           		=> "int UNSIGNED",
			"age_from"				=> "int UNSIGNED", // Возраст От
			"age_to"				=> "int UNSIGNED", // Возраст До
			"sex" 					=> "int UNSIGNED", // Пол
			"experience" 			=> "varchar(255) CHARACTER SET UTF8", // опыт работы
			"description"			=> "TEXT NOT NULL",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->dropTable("{{services}}");
		}
	}
}