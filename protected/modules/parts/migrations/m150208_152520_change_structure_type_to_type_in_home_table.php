<?php

class m150208_152520_change_structure_type_to_type_in_home_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{home}}")){
			$this->renameColumn("home", "structure_type", "type");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{home}}")){
			$this->renameColumn("home", "type", "structure_type");
		}
	}
}