<?php

class m150222_110852_create_title_in_animal_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{animal}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{animal}}', 'title');
	}
}