<?php

class m150222_155106_add_fields_in_city_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{city}}', 'created', 'datetime DEFAULT NULL');
		$this->addColumn('{{city}}', 'id_creator', 'int UNSIGNED');
		$this->addColumn('{{city}}', 'changed', 'datetime DEFAULT NULL');
		$this->addColumn('{{city}}', 'id_changer', 'int UNSIGNED');
	}

	public function safeDown()
	{
		$this->dropColumn('{{city}}', 'created');
		$this->dropColumn('{{city}}', 'id_creator');
		$this->dropColumn('{{city}}', 'changed');
		$this->dropColumn('{{city}}', 'id_changer');
	}
}