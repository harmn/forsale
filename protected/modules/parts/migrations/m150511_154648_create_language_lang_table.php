<?php

class m150511_154648_create_language_lang_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{languages_lang}}")){
			$this->dropTable("{{languages_lang}}");
		}

		$this->createTable("{{languages_lang}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"id_language"        	=> "int UNSIGNED", 
			"language"		   		=> "varchar(10) CHARACTER SET UTF8",
			"name"           		=> "varchar(200) CHARACTER SET UTF8", 
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{languages_lang}}")){
			$this->dropTable("{{languages_lang}}");
		}
	}
}