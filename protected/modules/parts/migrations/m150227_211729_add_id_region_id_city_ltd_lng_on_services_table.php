<?php

class m150227_211729_add_id_region_id_city_ltd_lng_on_services_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{services}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{services}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{services}}', 'id_region',	'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{services}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{services}}', 'lng');
		$this->dropColumn('{{services}}', 'ltd');
		$this->dropColumn('{{services}}', 'id_region');
		$this->dropColumn('{{services}}', 'id_city');
	}
}