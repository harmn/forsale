<?php

class m150514_190723_change_descriptions_to_utf8 extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{home}}")){
			$this->alterColumn("home", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->alterColumn("car", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{electronics}}")){
			$this->alterColumn("electronics", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
			$this->alterColumn("technics", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
			$this->alterColumn("furniture", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{fashion}}")){
			$this->alterColumn("fashion", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{sport}}")){
			$this->alterColumn("sport", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{animal}}")){
			$this->alterColumn("animal", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->alterColumn("work", "description", "text CHARACTER SET utf8");
		}
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->alterColumn("services", "description", "text CHARACTER SET utf8");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{home}}")){
			$this->alterColumn("home", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->alterColumn("car", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{electronics}}")){
			$this->alterColumn("electronics", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
			$this->alterColumn("technics", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
			$this->alterColumn("furniture", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{fashion}}")){
			$this->alterColumn("fashion", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{sport}}")){
			$this->alterColumn("sport", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{animal}}")){
			$this->alterColumn("animal", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->alterColumn("work", "description", "TEXT NOT NULL");
		}
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->alterColumn("services", "description", "TEXT NOT NULL");
		}
	}
}