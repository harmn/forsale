<?php

class m150227_201608_add_id_region_id_city_ltd_lng_on_sport_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{sport}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{sport}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{sport}}', 'id_region',		'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{sport}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{sport}}', 'lng');
		$this->dropColumn('{{sport}}', 'ltd');
		$this->dropColumn('{{sport}}', 'id_region');
		$this->dropColumn('{{sport}}', 'id_city');
	}
}