<?php

class m150227_204239_add_id_region_id_city_ltd_lng_on_animal_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{animal}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{animal}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{animal}}', 'id_region',		'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{animal}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{animal}}', 'lng');
		$this->dropColumn('{{animal}}', 'ltd');
		$this->dropColumn('{{animal}}', 'id_region');
		$this->dropColumn('{{animal}}', 'id_city');
	}
}