<?php

class m150222_111321_create_title_in_work_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{work}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `experience` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{work}}', 'title');
	}
}