<?php

class m150218_174926_change_reakays_year_type_in_technics_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
			$this->alterColumn("technics", "release_year", "int UNSIGNED");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
			$this->alterColumn("technics", "release_year", "DATE");
		}
	}
}