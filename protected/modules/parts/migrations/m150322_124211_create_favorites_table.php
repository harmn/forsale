<?php

class m150322_124211_create_favorites_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{favorites}}")){
			$this->dropTable("{{favorites}}");
		}


		$this->createTable("{{favorites}}", array(
			"id" 				  => "int UNSIGNED AUTO_INCREMENT",
			"id_user"			  => "int(10)",
			"id_advert" 		  => "int(10)",
			"model"	              => "varchar(255) CHARACTER SET UTF8", 
			"type" 		  		  => "int(10)",
			"created"		      => "datetime",
			"id_creator"	      => "int(10)",
			"changed"		      => "datetime",
			"id_changer"	      => "int(10)",
			"PRIMARY KEY (id)",
		));
	}

	public function down(){
		if(Yii::app()->db->getSchema()->getTable("{{favorites}}")){
			$this->dropTable("{{favorites}}");
		}
	}
}