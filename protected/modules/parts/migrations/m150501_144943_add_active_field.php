<?php

class m150501_144943_add_active_field extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{home}}', 		'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{car}}', 		'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{electronics}}', 'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{fashion}}', 	'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{animal}}', 		'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{sport}}', 		'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{technics}}', 	'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{work}}', 		'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{furniture}}', 	'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
		$this->addColumn('{{services}}', 	'active',	'int UNSIGNED DEFAULT 0	AFTER  `description` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{home}}', 			'active');
		$this->dropColumn('{{car}}', 			'active');
		$this->dropColumn('{{electronics}}', 	'active');
		$this->dropColumn('{{fashion}}', 		'active');
		$this->dropColumn('{{animal}}', 		'active');
		$this->dropColumn('{{sport}}', 			'active');
		$this->dropColumn('{{technics}}', 		'active');
		$this->dropColumn('{{work}}', 			'active');
		$this->dropColumn('{{furniture}}', 		'active');
		$this->dropColumn('{{services}}', 		'active');
	}
}