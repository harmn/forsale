<?php

class m150507_194412_create_region_lang_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{region_lang}}")){
			$this->dropTable("{{region_lang}}");
		}

		$this->createTable("{{region_lang}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"id_region"        		=> "int UNSIGNED", 
			"language"		   		=> "varchar(10) CHARACTER SET UTF8",
			"name"           		=> "varchar(200) CHARACTER SET UTF8", 
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{region_lang}}")){
			$this->dropTable("{{region_lang}}");
		}
	}
}