<?php

class m150223_161823_add_region_city_in_home_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{home}}', 'id_region',	'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{home}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{home}}', 'id_region');
		$this->dropColumn('{{home}}', 'id_city');
	}
}