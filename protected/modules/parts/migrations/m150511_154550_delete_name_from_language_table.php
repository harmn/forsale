<?php

class m150511_154550_delete_name_from_language_table extends EDbMigration
{
	public function up(){
		$this->dropColumn('languages', 'name');
	}

	public function down(){
		$this->addColumn('languages', 'name', 'varchar(255) CHARACTER SET UTF8');
	}
}