<?php

class m150227_205850_add_id_region_id_city_ltd_lng_on_work_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{work}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{work}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{work}}', 'id_region',		'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{work}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{work}}', 'lng');
		$this->dropColumn('{{work}}', 'ltd');
		$this->dropColumn('{{work}}', 'id_region');
		$this->dropColumn('{{work}}', 'id_city');
	}
}