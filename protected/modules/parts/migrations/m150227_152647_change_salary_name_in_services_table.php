<?php

class m150227_152647_change_salary_name_in_services_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->renameColumn("services", "salary", "price");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->renameColumn("services", "price", "salary");
		}
	}
}