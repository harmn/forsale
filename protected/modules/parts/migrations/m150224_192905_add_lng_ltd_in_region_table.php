<?php

class m150224_192905_add_lng_ltd_in_region_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{region}}', 'lng',	'varchar(255) CHARACTER SET UTF8	AFTER  `name` ');
		$this->addColumn('{{region}}', 'ltd',	'varchar(255) CHARACTER SET UTF8	AFTER  `name` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{region}}', 'lng');
		$this->dropColumn('{{region}}', 'ltd');
	}
}