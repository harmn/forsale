<?php

class m150221_114207_create_work_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->dropTable("{{work}}");
		}

		$this->createTable("{{work}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"type"        			=> "int UNSIGNED", 
			"salary"		   		=> "int UNSIGNED",
			"valuta"           		=> "int UNSIGNED",
			"age_from"				=> "int UNSIGNED", // Возраст От
			"age_to"				=> "int UNSIGNED", // Возраст До
			"graph" 				=> "int UNSIGNED", // график
			"education" 			=> "int UNSIGNED", // образование
			"languages" 			=> "int UNSIGNED", // языки
			"sex" 					=> "int UNSIGNED", // Пол
			"experience" 			=> "varchar(255) CHARACTER SET UTF8", // опыт работы
			"description"			=> "TEXT NOT NULL",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->dropTable("{{work}}");
		}
	}
}