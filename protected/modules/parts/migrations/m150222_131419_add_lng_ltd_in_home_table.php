<?php

class m150222_131419_add_lng_ltd_in_home_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{home}}', 'lng',	'varchar(255) CHARACTER SET UTF8	AFTER  `type` ');
		$this->addColumn('{{home}}', 'ltd',	'varchar(255) CHARACTER SET UTF8	AFTER  `type` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{home}}', 'title');
		$this->dropColumn('{{home}}', 'title');
	}
}
