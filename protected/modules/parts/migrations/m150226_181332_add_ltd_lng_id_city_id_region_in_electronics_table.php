<?php

class m150226_181332_add_ltd_lng_id_city_id_region_in_electronics_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{electronics}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{electronics}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{electronics}}', 'id_region',	'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{electronics}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{electronics}}', 'lng');
		$this->dropColumn('{{electronics}}', 'ltd');
		$this->dropColumn('{{electronics}}', 'id_region');
		$this->dropColumn('{{electronics}}', 'id_city');
	}
}