<?php

class m150222_104636_create_title_in_electronics_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{electronics}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{electronics}}', 'title');
	}
}