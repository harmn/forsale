<?php

class m150213_190818_create_technics_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
			$this->dropTable("{{technics}}");
		}

		$this->createTable("{{technics}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"category"              => "int UNSIGNED", // vacharvum e, poxanakvum e
			"type"        			=> "int UNSIGNED", 
			"brand"					=> "int UNSIGNED",
			"price"		   			=> "int UNSIGNED",
			"valuta"           		=> "int UNSIGNED", 
			"condition"				=> "int UNSIGNED", // состояние
			"release_year"	        => "DATE", // Год выпуска
			"color"					=> "int UNSIGNED", // цвет
			"description"			=> "TEXT NOT NULL",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
			$this->dropTable("{{technics}}");
		}
	}
}