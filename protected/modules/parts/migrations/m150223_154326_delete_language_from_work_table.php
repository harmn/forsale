<?php

class m150223_154326_delete_language_from_work_table extends EDbMigration
{
	public function up(){
		$this->dropColumn('work', 'languages');
	}

	public function down(){
		$this->addColumn('work', 'languages', 'int UNSIGNED');
	} 
}