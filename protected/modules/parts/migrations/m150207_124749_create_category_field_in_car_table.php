<?php

class m150207_124749_create_category_field_in_car_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{car}}', 'category',	'int UNSIGNED AFTER `id` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{car}}', 'category');
	}
}