<?php

class m150127_212155_create_home_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{home}}")){
			$this->dropTable("{{home}}");
		}

		$this->createTable("{{home}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"category"              => "int UNSIGNED", // vacharvum e, poxanakvum e
			"price"		   			=> "int UNSIGNED",
			"room_count"           	=> "int UNSIGNED", 
			"area"	            	=> "varchar(255) CHARACTER SET UTF8", // makeres
			"floor"			   		=> "int UNSIGNED", // hark
			"floor_count"	        => "int UNSIGNED", // harkeri qanak
			"structure_type"        => "int UNSIGNED", // bnakaran, grasenyak, avtotnak ev ayln
			"description"			=> "TEXT NOT NULL",
			"id_user"	            => "int UNSIGNED", // anhat , gorcakal
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{home}}")){
			$this->dropTable("{{home}}");
		}
	}
}