<?php

class m150218_181830_change_reakays_year_type_in_car_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->alterColumn("car", "release_year", "int UNSIGNED");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->alterColumn("car", "release_year", "DATE");
		}
	}
}