<?php

class m150222_110516_create_title_in_sport_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{sport}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{sport}}', 'title');
	}
}