<?php

class m150507_194754_delete_name_from_city_table extends EDbMigration
{
	public function up(){
		$this->dropColumn('city', 'name');
	}

	public function down(){
		$this->addColumn('city', 'name', 'varchar(255) CHARACTER SET UTF8');
	}
}