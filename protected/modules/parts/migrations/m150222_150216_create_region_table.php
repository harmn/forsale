<?php

class m150222_150216_create_region_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{region}}")){
			$this->dropTable("{{region}}");
		}

		$this->createTable("{{region}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"name"		   			=> "varchar(255) CHARACTER SET UTF8",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{region}}")){
			$this->dropTable("{{region}}");
		}
	}
}