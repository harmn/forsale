<?php

class m150227_193730_add_id_region_id_city_ltd_lng_on_fashion_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{fashion}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{fashion}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{fashion}}', 'id_region',	'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{fashion}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{fashion}}', 'lng');
		$this->dropColumn('{{fashion}}', 'ltd');
		$this->dropColumn('{{fashion}}', 'id_region');
		$this->dropColumn('{{fashion}}', 'id_city');
	}
}