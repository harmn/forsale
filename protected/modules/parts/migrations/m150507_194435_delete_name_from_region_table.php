<?php

class m150507_194435_delete_name_from_region_table extends EDbMigration
{
	public function up(){
		$this->dropColumn('region', 'name');
	}

	public function down(){
		$this->addColumn('region', 'name', 'varchar(255) CHARACTER SET UTF8');
	}
}