<?php

class m150218_182337_change_reakays_year_type_in_electronics_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{electronics}}")){
			$this->alterColumn("electronics", "release_year", "int UNSIGNED");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{electronics}}")){
			$this->alterColumn("electronics", "release_year", "DATE");
		}
	}
}