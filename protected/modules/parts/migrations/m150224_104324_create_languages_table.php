<?php

class m150224_104324_create_languages_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{languages}}")){
			$this->dropTable("{{languages}}");
		}

		$this->createTable("{{languages}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"name"		   			=> "varchar(255) CHARACTER SET UTF8",
			"created"   			=> "datetime DEFAULT NULL",
			"id_creator"   			=> "int UNSIGNED",
			"changed"   			=> "datetime DEFAULT NULL",
			"id_changer"   			=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{languages}}")){
			$this->dropTable("{{languages}}");
		}
	}
}