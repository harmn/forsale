<?php

class m150227_152037_change_salary_type_in_services_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{services}}")){
			$this->alterColumn("services", "salary", "varchar(255) CHARACTER SET UTF8");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->alterColumn("services", "salary", "int UNSIGNED");
		}
	}
}