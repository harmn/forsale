<?php

class m150222_105606_create_title_in_furniture_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{furniture}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{furniture}}', 'title');
	}
}