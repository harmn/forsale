<?php

class m150224_104726_create_work_languages_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{work_languages}}")){
			$this->dropTable("{{work_languages}}");
		}

		$this->createTable("{{work_languages}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"id_work"               => "int UNSIGNED ",
			"id_language"           => "int UNSIGNED ",
			"created"   			=> "datetime DEFAULT NULL",
			"id_creator"   			=> "int UNSIGNED",
			"changed"   			=> "datetime DEFAULT NULL",
			"id_changer"   			=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{work_languages}}")){
			$this->dropTable("{{work_languages}}");
		}
	}
}