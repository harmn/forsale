<?php

class m150208_152535_add_type_field_in_car_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{car}}', 'type',	'int UNSIGNED	AFTER `model` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{car}}', 'type');
	}
}