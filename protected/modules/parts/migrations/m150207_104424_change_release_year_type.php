<?php

class m150207_104424_change_release_year_type extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->alterColumn("car", "release_year", "DATE");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->alterColumn("car", "release_year", "datetime DEFAULT NULL");
		}
	}
}