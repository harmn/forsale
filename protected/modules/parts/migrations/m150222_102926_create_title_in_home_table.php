<?php

class m150222_102926_create_title_in_home_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{home}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `type` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{home}}', 'title');
	}
}