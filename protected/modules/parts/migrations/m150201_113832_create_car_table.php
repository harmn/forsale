<?php

class m150201_113832_create_car_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->dropTable("{{car}}");
		}

		$this->createTable("{{car}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"price"		   			=> "int UNSIGNED",
			"valuta"           		=> "int UNSIGNED", 
			"brand"					=> "int UNSIGNED",
			"model"					=> "int UNSIGNED",
			"condition"				=> "int UNSIGNED", // состояние
			"release_year"	        => "datetime DEFAULT NULL", // Год выпуска
			"mileage"			   	=> "int UNSIGNED", // Пробег
			"transmission"	        => "int UNSIGNED", // Коробка передач
			"seats_count"			=> "int UNSIGNED", // Количество сидений
			"body"        			=> "int UNSIGNED", // Кузов
			"rudder"				=> "int UNSIGNED", // Руль
			"fuels"					=> "int UNSIGNED", // топлива
			"color"					=> "int UNSIGNED", // цвет
			"description"			=> "TEXT NOT NULL",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{car}}")){
			$this->dropTable("{{car}}");
		}
	}
}