<?php

class m150222_111651_create_title_in_services_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{services}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `experience` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{services}}', 'title');
	}
}