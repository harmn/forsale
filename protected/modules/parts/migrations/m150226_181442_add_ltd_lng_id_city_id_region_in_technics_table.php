<?php

class m150226_181442_add_ltd_lng_id_city_id_region_in_technics_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{technics}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{technics}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{technics}}', 'id_region',	'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{technics}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{technics}}', 'lng');
		$this->dropColumn('{{technics}}', 'ltd');
		$this->dropColumn('{{technics}}', 'id_region');
		$this->dropColumn('{{technics}}', 'id_city');
	}
}