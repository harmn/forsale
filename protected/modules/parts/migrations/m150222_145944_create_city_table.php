<?php

class m150222_145944_create_city_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{city}}")){
			$this->dropTable("{{city}}");
		}

		$this->createTable("{{city}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"name"		   			=> "varchar(255) CHARACTER SET UTF8",
			"id_region" 			=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{city}}")){
			$this->dropTable("{{city}}");
		}
	}
}