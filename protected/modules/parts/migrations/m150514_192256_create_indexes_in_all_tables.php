<?php

class m150514_192256_create_indexes_in_all_tables extends EDbMigration
{
	public function up(){
		$transaction = Yii::app()->db->beginTransaction();
		try{

			if(Yii::app()->db->getSchema()->getTable("{{home}}")){
				$this->createIndex('id_city',			'home',	'id_city');
				$this->createIndex('id_region',			'home',	'id_region');
				$this->createIndex('id_creator',		'home',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{car}}")){
				$this->createIndex('id_city',			'car',	'id_city');
				$this->createIndex('id_region',			'car',	'id_region');
				$this->createIndex('id_creator',		'car',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{electronics}}")){
				$this->createIndex('id_city',			'electronics',	'id_city');
				$this->createIndex('id_region',			'electronics',	'id_region');
				$this->createIndex('id_creator',		'electronics',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
				$this->createIndex('id_city',			'technics',	'id_city');
				$this->createIndex('id_region',			'technics',	'id_region');
				$this->createIndex('id_creator',		'technics',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
				$this->createIndex('id_city',			'furniture',	'id_city');
				$this->createIndex('id_region',			'furniture',	'id_region');
				$this->createIndex('id_creator',		'furniture',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{fashion}}")){
				$this->createIndex('id_city',			'fashion',	'id_city');
				$this->createIndex('id_region',			'fashion',	'id_region');
				$this->createIndex('id_creator',		'fashion',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{sport}}")){
				$this->createIndex('id_city',			'sport',	'id_city');
				$this->createIndex('id_region',			'sport',	'id_region');
				$this->createIndex('id_creator',		'sport',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{animal}}")){
				$this->createIndex('id_city',			'animal',	'id_city');
				$this->createIndex('id_region',			'animal',	'id_region');
				$this->createIndex('id_creator',		'animal',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{work}}")){
				$this->createIndex('id_city',			'work',	'id_city');
				$this->createIndex('id_region',			'work',	'id_region');
				$this->createIndex('id_creator',		'work',	'id_creator');
			}
			if(Yii::app()->db->getSchema()->getTable("{{services}}")){
				$this->createIndex('id_city',			'services',	'id_city');
				$this->createIndex('id_region',			'services',	'id_region');
				$this->createIndex('id_creator',		'services',	'id_creator');
			}
			$transaction->commit();
		}
		catch(Exception $e){
			$transaction->rollback();
		}
	}

	public function down(){
		$transaction = Yii::app()->db->beginTransaction();
		try{

			if(Yii::app()->db->getSchema()->getTable("{{home}}")){
				$this->dropIndex('id_city',			'home');
				$this->dropIndex('id_region',		'home');
				$this->dropIndex('id_creator',		'home');
			}
			if(Yii::app()->db->getSchema()->getTable("{{car}}")){
				$this->dropIndex('id_city',			'car');
				$this->dropIndex('id_region',		'car');
				$this->dropIndex('id_creator',		'car');
			}
			if(Yii::app()->db->getSchema()->getTable("{{electronics}}")){
				$this->dropIndex('id_city',			'electronics');
				$this->dropIndex('id_region',		'electronics');
				$this->dropIndex('id_creator',		'electronics');
			}
			if(Yii::app()->db->getSchema()->getTable("{{technics}}")){
				$this->dropIndex('id_city',			'technics');
				$this->dropIndex('id_region',		'technics');
				$this->dropIndex('id_creator',		'technics');
			}
			if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
				$this->dropIndex('id_city',			'furniture');
				$this->dropIndex('id_region',		'furniture');
				$this->dropIndex('id_creator',		'furniture');
			}
			if(Yii::app()->db->getSchema()->getTable("{{fashion}}")){
				$this->dropIndex('id_city',			'fashion');
				$this->dropIndex('id_region',		'fashion');
				$this->dropIndex('id_creator',		'fashion');
			}
			if(Yii::app()->db->getSchema()->getTable("{{sport}}")){
				$this->dropIndex('id_city',			'sport');
				$this->dropIndex('id_region',		'sport');
				$this->dropIndex('id_creator',		'sport');
			}
			if(Yii::app()->db->getSchema()->getTable("{{animal}}")){
				$this->dropIndex('id_city',			'animal');
				$this->dropIndex('id_region',		'animal');
				$this->dropIndex('id_creator',		'animal');
			}
			if(Yii::app()->db->getSchema()->getTable("{{work}}")){
				$this->dropIndex('id_city',			'work');
				$this->dropIndex('id_region',		'work');
				$this->dropIndex('id_creator',		'work');
			}
			if(Yii::app()->db->getSchema()->getTable("{{services}}")){
				$this->dropIndex('id_city',			'services');
				$this->dropIndex('id_region',		'services');
				$this->dropIndex('id_creator',		'services');
			}
			$transaction->commit();
		}
		catch(Exception $e){
			$transaction->rollback();
		}
	}
}