<?php

class m150222_103943_create_title_in_car_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{car}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{car}}', 'title');
	}
}