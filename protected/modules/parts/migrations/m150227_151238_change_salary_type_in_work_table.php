<?php

class m150227_151238_change_salary_type_in_work_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->alterColumn("work", "salary", "varchar(255) CHARACTER SET UTF8");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{work}}")){
			$this->alterColumn("work", "salary", "int UNSIGNED");
		}
	}
}