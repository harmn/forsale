<?php

class m150201_105439_delete_id_user_row_in_home_table extends EDbMigration
{
	public function up(){
		$this->dropColumn('home', 'id_user');
	}

	public function down(){
		$this->addColumn('home', 'id_user', 'int(10)');
	} 
}