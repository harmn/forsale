<?php

class m150224_192759_add_lng_ltd_in_city_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{city}}', 'lng',	'varchar(255) CHARACTER SET UTF8	AFTER  `name` ');
		$this->addColumn('{{city}}', 'ltd',	'varchar(255) CHARACTER SET UTF8	AFTER  `name` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{city}}', 'lng');
		$this->dropColumn('{{city}}', 'ltd');
	}
}