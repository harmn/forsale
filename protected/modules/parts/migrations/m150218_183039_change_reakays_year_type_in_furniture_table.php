<?php

class m150218_183039_change_reakays_year_type_in_furniture_table extends EDbMigration
{
	public function safeUp()
	{
		if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
			$this->alterColumn("furniture", "release_year", "int UNSIGNED");
		}
	}

	public function safeDown()
	{
		if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
			$this->alterColumn("furniture", "release_year", "DATE");
		}
	}
}