<?php

class m150226_180419_add_ltd_lng_id_city_id_region_in_car_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{car}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{car}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{car}}', 'id_region',	'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{car}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{car}}', 'lng');
		$this->dropColumn('{{car}}', 'ltd');
		$this->dropColumn('{{car}}', 'id_region');
		$this->dropColumn('{{car}}', 'id_city');
	}
}