<?php

class m150222_105138_create_title_in_technics_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{technics}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{technics}}', 'title');
	}
}