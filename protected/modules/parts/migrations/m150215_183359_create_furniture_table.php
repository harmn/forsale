<?php

class m150215_183359_create_furniture_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
			$this->dropTable("{{furniture}}");
		}

		$this->createTable("{{furniture}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"category"              => "int UNSIGNED", // vacharvum e, poxanakvum e
			"type"        			=> "int UNSIGNED", 
			"price"		   			=> "int UNSIGNED",
			"valuta"           		=> "int UNSIGNED", 
			"condition"				=> "int UNSIGNED", // состояние
			"release_year"	        => "DATE", // Год выпуска
			"color"					=> "int UNSIGNED", // цвет
			"description"			=> "TEXT NOT NULL",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{furniture}}")){
			$this->dropTable("{{furniture}}");
		}
	}
}