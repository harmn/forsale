<?php

class m150507_194737_create_city_lang_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{city_lang}}")){
			$this->dropTable("{{city_lang}}");
		}

		$this->createTable("{{city_lang}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"id_city"        		=> "int UNSIGNED", 
			"language"		   		=> "varchar(10) CHARACTER SET UTF8",
			"name"           		=> "varchar(200) CHARACTER SET UTF8", 
			"PRIMARY KEY (id)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{city_lang}}")){
			$this->dropTable("{{city_lang}}");
		}
	}
}