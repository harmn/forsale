<?php

class m150227_185645_add_id_region_id_city_ltd_lng_on_furniture_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{furniture}}', 'lng',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{furniture}}', 'ltd',			'varchar(255) CHARACTER SET UTF8	AFTER  `description` ');
		$this->addColumn('{{furniture}}', 'id_region',		'int UNSIGNED	AFTER  `lng` ');
		$this->addColumn('{{furniture}}', 'id_city',		'int UNSIGNED	AFTER  `lng` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{furniture}}', 'lng');
		$this->dropColumn('{{furniture}}', 'ltd');
		$this->dropColumn('{{furniture}}', 'id_region');
		$this->dropColumn('{{furniture}}', 'id_city');
	}
}