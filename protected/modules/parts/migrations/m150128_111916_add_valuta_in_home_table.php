<?php

class m150128_111916_add_valuta_in_home_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{home}}', 'valuta',	'int UNSIGNED	AFTER `price` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{home}}', 'valuta');
	}
}