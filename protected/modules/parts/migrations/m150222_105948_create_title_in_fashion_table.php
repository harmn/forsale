<?php

class m150222_105948_create_title_in_fashion_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{fashion}}', 'title',	'varchar(255) CHARACTER SET UTF8	AFTER  `color` ');
	}

	public function safeDown()
	{
		$this->dropColumn('{{fashion}}', 'title');
	}
}