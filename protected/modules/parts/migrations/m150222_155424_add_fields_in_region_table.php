<?php

class m150222_155424_add_fields_in_region_table extends EDbMigration
{
		public function safeUp()
	{
		$this->addColumn('{{region}}', 'created', 'datetime DEFAULT NULL');
		$this->addColumn('{{region}}', 'id_creator', 'int UNSIGNED');
		$this->addColumn('{{region}}', 'changed', 'datetime DEFAULT NULL');
		$this->addColumn('{{region}}', 'id_changer', 'int UNSIGNED');
	}

	public function safeDown()
	{
		$this->dropColumn('{{region}}', 'created');
		$this->dropColumn('{{region}}', 'id_creator');
		$this->dropColumn('{{region}}', 'changed');
		$this->dropColumn('{{region}}', 'id_changer');
	}
}