<?
    // модал для создания карточки
    $modal = $this->beginWidget('UIModal',[
        'id' => 'register-modal',
        'width' => 800,
        'title' => 'Добавить обявление',
        'footerButtons' => []
    ]);
 
    $modal->header();
    
    $form = $this->beginWidget('SActiveForm',[
        'id'    =>'create-statement',
        'modal' => true,
        'action' => ['registration'],
        'enableAjaxValidation' => true,
        'htmlOptions' => [
            'class' => 'mt10'
        ],
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
        ),
        'afterModalClose' => 'function(form, data){
            $.fn.yiiGridView.update("work-table");
            form.get(0).reset();
            form.find("select").selectStyler("refresh");
        }',
        
    ]);
?>
    <div class="body">
        <div class="grid simple mb0">
            <div class="grid-body pb10 no-border clearfix">
                <div class="fl" style="width: 300px;margin-left: 15px;">
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'type')?>
                            <?=$form->dropDownList($model, 'type', Work::$type_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'type')?>
                    </div>
                    <div class="row fl mb10">
                            <?=$form->labelEx($model, 'age_from')?>
                            <?=$form->textField($model,'age_from', ['style' => 'width: 160px']); ?>
                            <?=$form->error($model,'age_from')?>
                    </div>
                    <div class="row mb10 fl ml20">
                            <?=$form->labelEx($model, 'age_to')?>
                            <?=$form->textField($model,'age_to', ['style' => 'width: 160px']); ?>
                            <?=$form->error($model,'age_to')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'sex')?>
                            <?=$form->dropDownList($model, 'sex', Work::$sex_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'sex')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model,'experience'); ?>
                            <?=$form->textField($model,'experience', ['class' => 'w100p']); ?>
                            <?=$form->error($model,'experience'); ?>
                    </div>
                    <div class="row mb10">
                            <?php 
                                $dependOptionsRegion = Region::listDataCustom(false, false, ['depend' => true]);
                            ?>
                            <?=$form->labelEx($model, 'id_region')?>
                            <?=$form->dropDownList($model, 'id_region', Region::listData(), [
                                        'empty'         => 'Район',
                                        'id'          => 'Work_registration_id_region',
                                        'data-search' => true,
                                        'data-filter' => true, 
                                        'data-width'    => '100%', 
                                        'empty'=>'Регион',
                                        'data-depend'=>'job',
                                        'options'     => $dependOptionsRegion
                                    ] );?>
                            <?=$form->error($model,'id_region')?>
                    </div>
                </div>    
                <div class="fr mb10" style="width: 300px;margin-right: 15px;">
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'graph')?>
                            <?=$form->dropDownList($model, 'graph', Work::$graph_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'graph')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'education')?>
                            <?=$form->dropDownList($model, 'education', Work::$education_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'education')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'languages')?>
                            <?=$form->dropDownList($model, 'languages', Languages::listData(), [
                                        'title'         => 'Языки',
                                        'data-width'    => '100%',
                                        'multiple' => 'multiple'
                                    ] );?>
                            <?=$form->error($model,'languages')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model,'salary'); ?>
                            <?=$form->textField($model,'salary', ['class' => 'w100p']); ?>
                            <?=$form->error($model,'salary'); ?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'valuta')?>
                            <?=$form->dropDownList($model, 'valuta', Work::$valuta_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'valuta')?>
                    </div>
                    <div class="row mb10">
                        <?
                            $cachedUsers = City::listData();
                            $dependOptions = City::listDataCustom(false, false, ['depend' => true]);
                        ?>
                        <?=$form->labelEx($model, 'id_city')?>
                        <?=$form->dropDownList($model, 'id_city', $cachedUsers, [
                                    'empty'       => 'Район',
                                    'id'          => 'Work_registration_id_city',
                                    'data-width'    => '100%',  
                                    'data-depend' =>'city',
                                    'data-search' => true,
                                    'data-filter' => true, 
                                    'options'     => $dependOptions
                                ] );?>
                        <?=$form->error($model,'id_city')?>
                    </div>
                </div>
                <div class="fl mb10 w100p mapContent">
                    <?=$form->hiddenField($model,'lng',array('type'=>"hidden", "id" => "Work_registration_lng"), $model->lng); ?>
                    <?=$form->hiddenField($model,'ltd',array('type'=>"hidden", "id" => "Work_registration_lat"), $model->ltd); ?>
                    <div id="map_registration"></div>
                </div>  
                <div class="fl mb10 w100p">
                        <?=$form->labelEx($model,'title'); ?>
                        <?=$form->textField($model, 'title', array('class' => 'w100p')); ?>
                        <?=$form->error($model,'title'); ?>
                </div>                      
                <div class="fl mb10 w100p">
                        <?=$form->labelEx($model,'description'); ?>
                        <?=$form->textArea($model, 'description', array('class' => 'w100p h100')); ?>
                        <?=$form->error($model,'description'); ?>
                </div>
            </div> 
        </div>
    </div>

    <?$modal->footer([
        'submit' => [
            'value' => 'Сохранить', 'icon' => false, 
              'htmlOptions' => [
                    'type'=>'submit', 'class'=>'btn btn-small btn-primary w100p',
                    'data-form' => 'create-statement'
               ]
            ]
    ]);?>
<?
    $this->endWidget();
    $this->endWidget(); 
?>
