<?
    // модал для создания карточки
    $modal = $this->beginWidget('UIModal',[
        'id' => 'update-modal',
        'width' => 800,
        'title' => 'Изменить обявление',
        'footerButtons' => []
    ]);

    $modal->header();
    
    $form = $this->beginWidget('SActiveForm',[
        'id'    =>'update-statement',
        'modal' => true,
        'action' => ['update'],
        'enableAjaxValidation' => true,
        'htmlOptions' => [
            'class' => 'mt10'
        ],
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
        'afterModalClose' => 'function(form, data){
            $.fn.yiiGridView.update("animal-table");
            form.get(0).reset();
            form.find("select").selectStyler("refresh");
        }',
        
    ]);
?>
    <div class="body">
        <div class="grid simple mb0">
            <div class="grid-body pb10 no-border clearfix">
                <div class="fl" style="width: 300px;margin-left: 15px;">
                    <?=$form->hiddenField($model,'id',array('type'=>"hidden"), $model->id); ?>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'category')?>
                            <?=$form->dropDownList($model, 'category', Home::$category_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'category')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'type')?>
                            <?=$form->dropDownList($model, 'type', Animal::$type_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'type')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'age')?>
                            <?=$form->textField($model,'age', ['class' => 'w100p']); ?>
                            <?=$form->error($model,'age')?>
                    </div>
                    <div class="row mb10">
                            <?php 
                                $dependOptionsRegion = Region::listDataCustom(false, false, ['depend' => true]);
                            ?>
                            <?=$form->labelEx($model, 'id_region')?>
                            <?=$form->dropDownList($model, 'id_region', Region::listData(), [
                                        'empty'         => 'Район',
                                        'id'          => 'Animal_update_id_region',
                                        'data-search' => true,
                                        'data-filter' => true, 
                                        'data-width'    => '100%', 
                                        'empty'=>'Регион',
                                        'data-depend'=>'job',
                                        'options'     => $dependOptionsRegion
                                    ] );?>
                            <?=$form->error($model,'id_region')?>
                    </div>
                </div>    
                <div class="fr mb10" style="width: 300px;margin-right: 15px;">
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'color')?>
                            <?=$form->dropDownList($model, 'color', Animal::$color_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'color')?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model,'price'); ?>
                            <?=$form->textField($model,'price', ['class' => 'w100p']); ?>
                            <?=$form->error($model,'price'); ?>
                    </div>
                    <div class="row mb10">
                            <?=$form->labelEx($model, 'valuta')?>
                            <?=$form->dropDownList($model, 'valuta', Animal::$valuta_select, [
                                        'empty'         => 'Не выбрано',
                                        'data-width'    => '100%',
                                    ] );?>
                            <?=$form->error($model,'valuta')?>
                    </div>
                    <div class="row mb10">
                        <?
                            $cachedUsers = City::listData();
                            $dependOptions = City::listDataCustom(false, false, ['depend' => true]);
                        ?>
                        <?=$form->labelEx($model, 'id_city')?>
                        <?=$form->dropDownList($model, 'id_city', $cachedUsers, [
                                    'empty'       => 'Район',
                                    'id'          => 'Animal_update_id_city',
                                    'data-width'    => '100%',  
                                    'data-depend' =>'city',
                                    'data-search' => true,
                                    'data-filter' => true, 
                                    'options'     => $dependOptions
                                ] );?>
                        <?=$form->error($model,'id_city')?>
                    </div>
                </div>
                <div class="fl mb10 w100p">
                    <?=$form->hiddenField($model,'lng',array('type'=>"hidden", "id" => "Animal_update_lng"), $model->lng); ?>
                    <?=$form->hiddenField($model,'ltd',array('type'=>"hidden", "id" => "Animal_update_lat"), $model->ltd); ?>
                    <div id="map_update"></div>
                </div> 
                <div class="fl mb10 w100p">
                        <?=$form->labelEx($model,'title'); ?>
                        <?=$form->textField($model, 'title', array('class' => 'w100p')); ?>
                        <?=$form->error($model,'title'); ?>
                </div>        
                <div class="fl mb10 w100p">
                        <?=$form->labelEx($model,'description'); ?>
                        <?=$form->textArea($model, 'description', array('class' => 'w100p h100')); ?>
                        <?=$form->error($model,'description'); ?>
                </div>  
                <div class="fl mb10 w100p">
                <? 
                    $params = "parts";
                    $files = $model->photos;
                    $model->id = "0";
                    $showTypeLink = false;
                    $this->widget('Uploader', compact('files', 'model', 'params', 'showTypeLink'));
                ?>
                </div>
            </div> 
        </div>
    </div>

    <?$modal->footer([
        'submit' => [
            'value' => 'Сохранить', 'icon' => false, 
              'htmlOptions' => [
                    'type'=>'submit', 'class'=>'btn btn-small btn-primary w100p',
                    'data-form' => 'update-statement'
               ]
            ]
    ]);?>
<?
    $this->endWidget();
    $this->endWidget(); 
?>
