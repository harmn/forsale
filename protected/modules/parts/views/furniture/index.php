<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?  
    $this->renderPartial('registration', ['model' => new Furniture('create')]);
    $this->renderPartial('update', ['model' => new Furniture('update')]);
?>

<?php
$this->filters = $this->widget("UIFilters", [
    'blocks'=>[
        'Мебель' => ['name'=>'furniture'],
    ], 
    'model' => $provider->model,
    'class' => 'pull-left mt20',
], true); ?>

<div class="buttons mb20">
    <? $this->widget('UIButtons', ['buttons'=>[
                'custom' => [
                    'value' => 'Добавить заявление', 
                    'icon'  => 'fa fa-plus',
                    'options'=>[
                        'id'            =>  'addFurniture',
                        'class'         =>  'btn btn-cons btn-small btn-primary',
                        'data-toggle'   =>  'domodal', 
                        'data-target'   =>  '#register-modal'
                    ]
                ],
            ], 'size' => 'small']) ?>
</div>
<? 
$this->widget('SGridView', array(
    'id'=>'furniture-table',
    'dataProvider'=>$provider,
    'filter' => null,
    'showButtonsColumn' => false,
    'showNumColumn' => false,
    // 'showCheckBoxColumn' => false,
    'flexible' => false,
    'columns'=>array(
                [
                    'name' => 'id',
                    'type' => 'raw',
                    'headerHtmlOptions' => ['width'=>20],
                    'htmlOptions' => ['align'=>'center', 'class'=>'clicker'],
                    'value' => function($data){
                         return CHtml::link($data->id, '#', 
                                 [
                                    'data-id'=>     $data->id, 
                                    'data-url'=>    App()->createUrl('/parts/furniture/getFurnitureInfo'),
                                    'data-target' => '#update-modal',
                                    'data-model' => 'Furniture',
                                    'data-title' => 'Изменить заявление',
                                    'data-action' => '/parts/furniture/update',
                                    'class'=>       'btn-small getFurnitureInfo'
                                ]);
                    }
                ],
                [
                    'name'  => 'title',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->title) ? $data->title : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'category',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->category) ? Home::$category_select[$data->category] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_region',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $regionData = Region::listData();
                        return ($data->id_region) ? $regionData[$data->id_region] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_city',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $cityData = City::listData();
                        return ($data->id_city) ? $cityData[$data->id_city] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'type',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->type) ? Furniture::$type_select[$data->type] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'price',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->price) ? $data->price : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'valuta',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                       return ($data->valuta) ? Furniture::$valuta_select[$data->valuta] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'condition',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>60],
                    'value' => function($data){
                        return ($data->condition) ? Furniture::$condition_select[$data->condition] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'release_year',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>220],
                    'value' => function($data){
                        return ($data->release_year) ? $data->release_year : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'color',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->color) ? Furniture::$color_select[$data->color] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'fullname',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return $data->client ? $data->client->getFullName() : '';
                    }
                ]
            ),
    'style' => 'blue',
    'type' => 'striped bordered',
)); ?>