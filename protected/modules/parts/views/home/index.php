<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<? 
    $this->renderPartial('registration', ['model' => new Home('create')]);
    $this->renderPartial('update', ['model' => new Home('update')]);
?>

<?php
$this->filters = $this->widget("UIFilters", [
    'blocks'=>[
        'Дома' => ['name'=>'home'],
    ], 
    'model' => $provider->model,
    'class' => 'pull-left mt20',
], true); ?>

<div class="buttons mb20">
    <? $this->widget('UIButtons', ['buttons'=>[
                'custom' => [
                    'value' => 'Добавить заявление', 
                    'icon'  => 'fa fa-plus',
                    'options'=>[
                        'id'            =>  'addStatement',
                        'class'         =>  'btn btn-cons btn-small btn-primary',
                        'data-toggle'   =>  'domodal', 
                        'data-target'   =>  '#register-modal'
                    ]
                ],
            ], 'size' => 'small']) ?>
</div>
<? 
$this->widget('SGridView', array(
    'id'=>'home-table',
    'dataProvider'=>$provider,
    'filter' => null,
    'showButtonsColumn' => false,
    'showNumColumn' => false,
    // 'showCheckBoxColumn' => false,
    'flexible' => false,
    'columns'=>array(
                [
                    'name' => 'id',
                    'type' => 'raw',
                    'headerHtmlOptions' => ['width'=>50],
                    'htmlOptions' => ['align'=>'center', 'class'=>'clicker'],
                    'value' => function($data){
                         return CHtml::link($data->id, '#', 
                                 [
                                    'data-id'=>     $data->id, 
                                    'data-url'=>    App()->createUrl('/parts/home/getHomeInfo'),
                                    'data-target' => '#update-modal',
                                    'data-model' => 'Home',
                                    'data-title' => 'Изменить заявление',
                                    'data-action' => '/parts/home/update',
                                    'class'=>       'btn-small getHomeInfo'
                                ]);
                    }
                ],
                [
                    'name'  => 'title',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->title) ? $data->title : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'category',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->category) ? Home::$category_select[$data->category] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_region',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $regionData = Region::listData();
                        return ($data->id_region) ? $regionData[$data->id_region] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_city',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $cityData = City::listData();
                        return ($data->id_city) ? $cityData[$data->id_city] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'type',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->type) ? Home::$type_select[$data->type] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'price',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->price) ? $data->price : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'valuta',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                       return ($data->valuta) ? Home::$valuta_select[$data->valuta] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'room_count',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return ($data->room_count) ? $data->room_count : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'floor',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->floor) ? $data->floor : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'floor_count',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return ($data->floor_count) ? $data->floor_count : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'area',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return ($data->area) ? $data->area : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'fullname',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return $data->client ? $data->client->getFullName() : '';
                    }
                ]
            ),
    'style' => 'blue',
    'type' => 'striped bordered',
)); ?>