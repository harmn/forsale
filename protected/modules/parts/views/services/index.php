<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?  
    $this->renderPartial('registration', ['model' => new Services('create')]);
    $this->renderPartial('update', ['model' => new Services('update')]);
?>

<?php
$this->filters = $this->widget("UIFilters", [
    'blocks'=>[
        'Службы' => ['name'=>'services'],
    ], 
    'model' => $provider->model,
    'class' => 'pull-left mt20',
], true); ?>

<div class="buttons mb20">
    <? $this->widget('UIButtons', ['buttons'=>[
                'custom' => [
                    'value' => 'Добавить заявление', 
                    'icon'  => 'fa fa-plus',
                    'options'=>[
                        'id'            =>  'addServices',
                        'class'         =>  'btn btn-cons btn-small btn-primary',
                        'data-toggle'   =>  'domodal', 
                        'data-target'   =>  '#register-modal'
                    ]
                ],
            ], 'size' => 'small']) ?>
</div>
<? 
$this->widget('SGridView', array(
    'id'=>'services-table',
    'dataProvider'=>$provider,
    'filter' => null,
    'showButtonsColumn' => false,
    'showNumColumn' => false,
    // 'showCheckBoxColumn' => false,
    'flexible' => false,
    'columns'=>array(
                [
                    'name' => 'id',
                    'type' => 'raw',
                    'headerHtmlOptions' => ['width'=>20],
                    'htmlOptions' => ['align'=>'center', 'class'=>'clicker'],
                    'value' => function($data){
                         return CHtml::link($data->id, '#', 
                                 [
                                    'data-id'=>     $data->id, 
                                    'data-url'=>    App()->createUrl('/parts/Services/getServicesInfo'),
                                    'data-target' => '#update-modal',
                                    'data-model' => 'Services',
                                    'data-title' => 'Изменить заявление',
                                    'data-action' => '/parts/services/update',
                                    'class'=>       'btn-small getServicesInfo'
                                ]);
                    }
                ],
                [
                    'name'  => 'title',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->title) ? $data->title : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'type',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->type) ? Services::$type_select[$data->type] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_region',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $regionData = Region::listData();
                        return ($data->id_region) ? $regionData[$data->id_region] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_city',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $cityData = City::listData();
                        return ($data->id_city) ? $cityData[$data->id_city] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'price',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->price) ? $data->price : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'valuta',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                       return ($data->valuta) ? Services::$valuta_select[$data->valuta] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'experience',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return $data->experience ? $data->experience : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'sex',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                       return ($data->sex) ? Services::$sex_select[$data->sex] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'age_from',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>100],
                    'value' => function($data){
                        return ($data->age_from) ? $data->age_from : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'age_to',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>100],
                    'value' => function($data){
                        return ($data->age_to) ? $data->age_to : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'fullname',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return $data->client ? $data->client->getFullName() : '';
                    }
                ]
            ),
    'style' => 'blue',
    'type' => 'striped bordered',
)); ?>