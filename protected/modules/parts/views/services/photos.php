<?php 
    if($photos)
    foreach($photos as $item){    ?>
        <li class="image image-container qq-upload-success" data-id="<?=$item->id;?>" data-size="thumb" draggable="true">
            <div class="tools"> 
                <span class="tool fa fa-rotate-left rotate-left-btn"></span>
                <span class="tool fa fa-rotate-right rotate-right-btn"></span>
                <span class="tool fa fa-crop crop-btn"></span>
                <span class="tool fa fa-trash-o delete-btn"></span>
                <span class="tool fa fa-arrows move-btn"></span>
            </div>
            <div class="qq-progress-bar" style="display: none; width: 100%;"></div>
            <a class="qq-upload-file fancybox" rel="gallery" data-big-size="big" href="<?="/".$item->path."thumb/".$item->filename;?>"><img class="image" src="<?="/".$item->path."thumb/".$item->filename;?>"></a>
            <span class="qq-upload-failed-text">Ошибка</span>
            <span class="qq-upload-status-text"></span>
        </li>
<?php  }  ?>

