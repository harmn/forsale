<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<?  
    $this->renderPartial('registration', ['model' => new Car('create')]);
    $this->renderPartial('update', ['model' => new Car('update')]);
?>

<?php
$this->filters = $this->widget("UIFilters", [
    'blocks'=>[
        'Автомобили' => ['name'=>'car'],
    ], 
    'model' => $provider->model,
    'class' => 'pull-left mt20',
], true); ?>

<div class="buttons mb20">
    <? $this->widget('UIButtons', ['buttons'=>[
                'custom' => [
                    'value' => 'Добавить заявление', 
                    'icon'  => 'fa fa-plus',
                    'options'=>[
                        'id'            =>  'addCar',
                        'class'         =>  'btn btn-cons btn-small btn-primary',
                        'data-toggle'   =>  'domodal', 
                        'data-target'   =>  '#register-modal'
                    ]
                ],
            ], 'size' => 'small']) ?>
</div>
<? 
$this->widget('SGridView', array(
    'id'=>'car-table',
    'dataProvider'=>$provider,
    'filter' => null,
    'showButtonsColumn' => false,
    'showNumColumn' => false,
    'showCheckBoxColumn' => false,
    'flexible' => false,
    'columns'=>array(
                [
                    'name' => 'id',
                    'type' => 'raw',
                    'headerHtmlOptions' => ['width'=>20],
                    'htmlOptions' => ['align'=>'center', 'class'=>'clicker'],
                    'value' => function($data){
                         return CHtml::link($data->id, '#', 
                                 [
                                    'data-id'=>     $data->id, 
                                    'data-url'=>    App()->createUrl('/parts/car/getCarInfo'),
                                    'data-target' => '#update-modal',
                                    'data-model' => 'Car',
                                    'data-title' => 'Изменить заявление',
                                    'data-action' => '/parts/car/update',
                                    'class'=>       'btn-small getCarInfo'
                                ]);
                    }
                ],
                [
                    'name'  => 'title',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->title) ? $data->title : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'category',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->category) ? Home::$category_select[$data->category] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_region',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $regionData = Region::listData();
                        return ($data->id_region) ? $regionData[$data->id_region] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'id_city',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        $cityData = City::listData();
                        return ($data->id_city) ? $cityData[$data->id_city] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'type',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->type) ? Car::$type_select[$data->type] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'brand',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->brand) ? Car::$brand_select[$data->brand] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'model',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>120],
                    'value' => function($data){
                        return ($data->model) ? Car::$model_select[$data->model] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'price',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->price) ? $data->price : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'valuta',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                       return ($data->valuta) ? Car::$valuta_select[$data->valuta] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'condition',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>60],
                    'value' => function($data){
                        return ($data->condition) ? Car::$condition_select[$data->condition] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'release_year',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>220],
                    'value' => function($data){
                        return ($data->release_year) ? $data->release_year : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'mileage',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->mileage) ? Car::$mileage_select[$data->mileage] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'transmission',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>150],
                    'value' => function($data){
                        return ($data->transmission) ? Car::$transmission_select[$data->transmission] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'seats_count',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->seats_count) ? Car::$seats_count_select[$data->seats_count] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'body',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->body) ? Car::$body_select[$data->body] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'rudder',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->rudder) ? Car::$rudder_select[$data->rudder] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'fuels',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>120],
                    'value' => function($data){
                        return ($data->fuels) ? Car::$fuels_select[$data->fuels] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'color',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>80],
                    'value' => function($data){
                        return ($data->color) ? Car::$color_select[$data->color] : 'Не выбрано';
                    }
                ],
                [
                    'name'  => 'fullname',
                    'type' => 'html',
                    'htmlOptions' => ['align'=>'center'],
                    'headerHtmlOptions' => ['width'=>200],
                    'value' => function($data){
                        return $data->client ? $data->client->getFullName() : '';
                    }
                ]
            ),
    'style' => 'blue',
    'type' => 'striped bordered',
)); ?>