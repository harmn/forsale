<?php

class m141108_174809_create_client_profile_table extends EDbMigration
{
	public function up()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{client_profile}}")){
			$this->dropTable("{{client_profile}}");
		}

		$this->createTable("{{client_profile}}", array(
			"id"                   	=> "int UNSIGNED AUTO_INCREMENT",
			"id_user"			   	=> "int UNSIGNED",
			"id_job"               	=> "int UNSIGNED",
			"is_employee"		   	=> "tinyint(1)",
			"status"	            => "varchar(50) CHARACTER SET UTF8",
			"phone"	              	=> "varchar(255) CHARACTER SET UTF8",
			"address"	            => "varchar(255) CHARACTER SET UTF8",
			"comment"             	=> "varchar(2000) CHARACTER SET UTF8",
			"skype_name"           	=> "varchar(255) CHARACTER SET UTF8",
			"created"              	=> "datetime DEFAULT NULL",
			"id_creator"           	=> "int UNSIGNED",
			"changed"              	=> "datetime DEFAULT NULL",
			"id_changer"           	=> "int UNSIGNED",
			"PRIMARY KEY (id)",
			"KEY `id_job` (`id_job`)",
		));
	}

	public function down()
	{
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{client_profile}}")){
			$this->dropTable("{{client_profile}}");
		}
	}
}

