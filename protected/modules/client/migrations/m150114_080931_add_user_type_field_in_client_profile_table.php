<?php

class m150114_080931_add_user_type_field_in_client_profile_table extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{client_profile}}', 'type',	'tinyint(1)');
	}

	public function safeDown()
	{
		$this->dropColumn('{{client_profile}}', 'type');
	}
}