<?php

class m150113_142506_add_facebook_link_field extends EDbMigration
{
	public function safeUp()
	{
		$this->addColumn('{{client_profile}}', 'facebook',	'varchar(255) CHARACTER SET UTF8');
	}

	public function safeDown()
	{
		$this->dropColumn('{{client_profile}}', 'facebook');
	}
}