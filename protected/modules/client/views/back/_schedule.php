<?if(empty($models)){
	for($i = 0; $i<7; $i++){
		$models[] = new UserSchedule;
	}

}?>
<? $week_day = ['0'=>'Пн','1'=>'Вт','2'=>'Ср','3'=>'Чт','4'=>'Пт','5'=>'Сб','6'=>'Вс'];?>

<div class="buttons mb20">
	<? $this->widget('UIButtons', ['buttons'=>[
			'custom' => [
				'value' => 'Назначить', 
				'icon' 	=> 'fa fa-plus',
				'options'=>[
					'id' 		=> 'assign-schedule',
					'class' 	=> 'btn btn-cons btn-small btn-success',
				]
			],
		], 'size' => 'small']) ?>
</div>


<table class="table table-bordered">
	<thead>
		<tr>
			<th class="tleft">
				<div class="checkbox check-default">
					<?=CHtml::checkbox('checkbox-all', '', ['class' => 'check-all-assign']);?>
					<?=CHtml::label('', 'checkbox-all');?>
				</div>
			</th>
			<th></th>
			<th>Работа</th>
			<th>Обед</th>
			<th>Выходной</th>
			<th>Лидер смены</th>
		</tr>
	</thead>
	<tbody>
		<? for($day = 0; $day<7; $day++) :
			$isNew = 'new';
			$id = $day;
			foreach($models as $model){
				if($model->week_day == $day + 1) {
					$isNew = 'old';
					$id = $model->primaryKey;
					break;
				}
			}
		?>
			<tr data-id="<?=$id?>">
				<td>
					<div class="checkbox check-default">
						<?=CHtml::checkbox('checkbox'.$day, '', ['class' => 'check-assign']);?>
						<?=CHtml::label('', 'checkbox'.$day);?>
					</div>
				</td>
				<td><? echo $week_day[$day]; echo CHtml::hiddenField("UserSchedule[$isNew][$id][week_day]", $day + 1)?></td>
				<td>
					<?=UIHelpers::dropDownList("UserSchedule[$isNew][$id][work_begin]", substr($model->work_begin, 0, strrpos($model->work_begin, ':')), 
						UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>5]);?>
					<?=UIHelpers::dropDownList("UserSchedule[$isNew][$id][work_end]", substr($model->work_end, 0, strrpos($model->work_end, ':')), 
						UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>5]);?>					
				</td>
				<td>
					<?=UIHelpers::dropDownList("UserSchedule[$isNew][$id][lunch_begin]", substr($model->lunch_begin, 0, strrpos($model->lunch_begin, ':')), 
						UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>5]);?>
					<?=UIHelpers::dropDownList("UserSchedule[$isNew][$id][lunch_end]", substr($model->lunch_end, 0, strrpos($model->lunch_end, ':')), 
						UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>5]);?>
				</td>
				<td>
					<?=UIHelpers::switcher("UserSchedule[$isNew][$id][day_off]", $model->day_off, ['class'=>'ios', 'id'=>"UserSchedule_{$day}_day_off"]);?>
				</td>
				<td>
					<?=UIHelpers::dropDownList("UserSchedule[$isNew][$id][shift_leader]", $model->shift_leader, 
						  ['1'=> '1 смена','2' => '2 смена'], ['empty'=>'Смена', 'data-width'=>'90']);?>
				</td>
			</tr>
		<?endfor?>
	</tbody>
</table>