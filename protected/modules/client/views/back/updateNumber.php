<?
	// модал для прикрепления карточки к user-у
	$modal = $this->beginWidget('UIModal',[
	  	'id' => 'edit-card-modal',
	 	'width' => 263,
	 	'title' => 'Прикрепить карточку',
	 	'footerButtons' => []
	]);

	$modal->header();
	$model->setScenario('update');

	$form = $this->beginWidget('SActiveForm',[
		'id'	=>'edit-card',
		'modal' => true,
		'action' => ['/admin/loft/update'],
		'enableAjaxValidation' => true,
		'htmlOptions' => [
			'class' => 'mt20'
		],
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => false,
		),
		'afterModalClose' => 'function(form, data){
			showSuccessMessage("Сохранено");
		}',
	]);
?>

<div class="body">
	<div class="grid simple mb0">
		<div class="grid-body no-border clearfix">
			<div class="">
				<?=$form->hiddenField($model,'id',array('type'=>"hidden"), $model->id); ?>
				<div class="row">
						<?=$form->labelEx($model,'number'); ?>
					    <?=$form->textField($model,'number', ['class' => 'w100p']); ?>
					    <?=$form->error($model,'number'); ?>
				</div>
				<div class="row">
						<?=$form->labelEx($model, 'id_user')?>
						<?=$form->dropDownList($model, 'id_user', User::listData(), [
									'empty'			=> 'Не выбрано',
									'style' 		=> 'width: 240px'
								] );?>
						<?=$form->error($model,'id_user')?>
				</div>
			</div>							
		</div> 
	</div>
</div>
	<?$modal->footer([
		'submit' => [
			'value' => 'Изменить', 'icon' => false, 
			  'htmlOptions' => [
					'type'=>'submit', 'class'=>'btn btn-small btn-success mt10',
					'data-form' => 'edit-card'
			   ]
			]
	]);?>
<?
	$this->endWidget();
	$this->endWidget(); 
?>
