
<div class="control-group">
	<label>Кандидат</label>
	<span class="field clearfix">
		<div class='fl mr10 mt25'>
			<?=UIHelpers::switcher("UserStatus[type]", $model->type == Employee::TYPE_CANDIDATE, ['class'=>'ios', 'id'=>"type_candidate", 'value' => Employee::TYPE_CANDIDATE])?>
		</div>
		<div class='fl mr10'>
			<div class="block mt20">	
				<label for="" class="w30 fl mt10">от</label>
				<?=$form->datePicker($model, 'candidate_date')?>
				<?=$form->error($model, 'candidate_date')?>
			</div>
		</div>

		<div class='fl' style="width: 76%">
			<?=$form->textArea($model, 'candidate_comment', ['class'=>'', "style" => 'width: 100%; height: 77px','placeholder' => 'Комментарий'])?>
			<?=$form->error($model, 'candidate_comment')?>
		</div>
	</span>
</div>

<div class="control-group">
	<label>Обучение</label>
	<span class="field clearfix">
		<div class='fl mr10 mt30'>
			<?=UIHelpers::switcher("UserStatus[type]", $model->type == Employee::TYPE_STUDY, ['class'=>'ios', 'id'=>"type_study", 'value' => Employee::TYPE_STUDY])?>
		</div>
		<div class='fl mr10'>
			<div class="block">
				<label for="" class="w30 fl mt10">от</label>
				<?=$form->datePicker($model, 'study_date_start')?>
				<?=$form->error($model,'study_date_start')?>
			</div>

			<div class='block'>
				<label for="" class="w30 fl mt10">до</label>
				<?=$form->datePicker($model, 'study_date_end')?>
				<?=$form->error($model,'study_date_end')?>
			</div>
		</div>
			
		<div class='fl' style="width: 76%">
			<?=$form->textArea($model, 'study_comment', ['class'=>'', "style" => 'width: 100%; height: 77px','placeholder' => 'Комментарий'])?>
			<?=$form->error($model, 'study_comment')?>
		</div>
	</span>
</div>

<div class="control-group">
	<label>Стажировка</label>
	<span class="field clearfix">
		<div class='fl mr10 mt30'>
			<?=UIHelpers::switcher("UserStatus[type]", $model->type == Employee::TYPE_PROBATION, ['class'=>'ios', 'id'=>"type_probation", 'value' => Employee::TYPE_PROBATION])?>
		</div>
		<div class='fl mr10'>
			<div class="block">
				<label for="" class="w30 fl mt10">от</label>
				<?=$form->datePicker($model, 'probation_date_start')?>
				<?=$form->error($model,'probation_date_start')?>
			</div>

			<div class='block'>
				<label for="" class="w30 fl mt10">до</label>
				<?=$form->datePicker($model, 'probation_date_end')?>
				<?=$form->error($model,'probation_date_end')?>
			</div>
		</div>
			
		<div class='fl' style="width: 76%">
			<?=$form->textArea($model, 'probation_comment', ['class'=>'', "style" => 'width: 100%; height: 77px','placeholder' => 'Комментарий'])?>
			<?=$form->error($model, 'probation_comment')?>
		</div>
	</span>
</div>

<div class="control-group">
	<label>Принят</label>
	<span class="field clearfix">
		<div class='fl mr10 mt25'>
			<?=UIHelpers::switcher("UserStatus[type]", $model->type == Employee::TYPE_HIRED, ['class'=>'ios', 'id'=>"type_hired", 'value' => Employee::TYPE_HIRED])?>
		</div>
		<div class='fl mr10'>
			<div class="block mt20">
				<label for="" class="w30 fl mt10">от</label>
				<?=$form->datePicker($model, 'hired_date')?>
				<?=$form->error($model,'hired_date')?>
			</div>
		</div>
			
		<div class='fl' style="width: 76%">
			<?=$form->textArea($model, 'hired_comment', ['class'=>'', "style" => 'width: 100%; height: 77px','placeholder' => 'Комментарий'])?>
			<?=$form->error($model, 'hired_comment')?>
		</div>
	</span>
</div>

<div class="control-group">
	<label>Уволен</label>
	<span class="field clearfix">
		<div class='fl mr10 mt25'>
			<?=UIHelpers::switcher("UserStatus[type]", $model->type == Employee::TYPE_RETIRED, ['class'=>'ios', 'id'=>"type_retired", 'value' => Employee::TYPE_RETIRED])?>
		</div>
		<div class='fl mr10'>
			<div class="block mt20">
				<label for="" class="w30 fl mt10">от</label>
				<?=$form->datePicker($model, 'fired_date')?>
				<?=$form->error($model,'fired_date')?>
			</div>

		</div>
			
		<div class='fl' style="width: 76%">
			<?=$form->textArea($model, 'fired_comment', ['class'=>'', "style" => 'width: 100%; height: 77px','placeholder' => 'Комментарий'])?>
			<?=$form->error($model, 'fired_comment')?>
		</div>
	</span>
</div>
