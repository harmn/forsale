<? 
$this->renderPartial('_registration', ['model' => new Client('registration')]);

$this->filters = $this->widget("UIFilters", [
    'blocks'=>[
        'Пользователи'   => ['name'=>'user'],
    ], 
    'model'=>$provider->model,
    'class'=>'pull-left',
    // 'gridId' => 'grid-pricelist' //это нужно потому что у нас на странице будет еще одна такая форма
], true); 

$this->beginWidget('UITabs', [
    'paramName' => 'type',
    'tabs' => $this->tabs,
    'ajax' => false
]); ?>
    <div class="tab-content">
        <div class="tab-pane active">
            
            <div class="buttons mb20">
                <? $buttons = []; 

                //добавить пользователя
                if (Yii::app()->request->getParam('type') != 'blocked'){
                    $buttons['add'] = ['data-toggle'=>"domodal", 'data-target'=>"#register-modal", 'class'=>'btn btn-cons btn-success btn-small '];
                    $buttons['DeleteSelected'] = ['class'=>'btn btn-cons btn-danger btn-small hidden'];
                }
                
                $buttons = array_merge($buttons, ['ShowImages']); 

                $this->widget('UIButtons', ['buttons' => $buttons, 'size'=>'small']); ?>
            </div>    

            <? 
            $columns = array(
                [
                    'name' => 'avatar',
                    'header' => '',
                    'type' => 'raw',
                    'filter' => false,
                    'visible'=> $provider->model->isShowThumbnail,
                    'headerHtmlOptions' => ['width'=>40],
                    'htmlOptions' => ['align'=>'center'],
                    'value' => function($data){
                        return CHtml::link($data->getThumbnail('thumb', 35, 35, $data->fullname), $data->backUrl, 
                            ['target' => '_blank', 'rel'=>'tooltip', 'title'=>'Открыть карту']);
                    }
                ],
                [
                    'name' => 'id',
                    'type' => 'raw',
                    'headerHtmlOptions' => ['width'=>50],
                    'htmlOptions' => ['align'=>'center'],
                    'value' => function($data){
                        return CHtml::link($data->id, $data->backUrl, 
                            ['target' => '_blank',  'rel'=>'tooltip', 'title'=>'Открыть карту']);
                    }
                ],
                [
                    'name'  => 'fullname',
                    'type' => 'html',
                    'value' => '$data->getFullName(true)',
                ],
                [
                    'name'  => 'username',
                    'type' => 'html',
                    'value' => '$data->username',
                ],
                [
                    'name'=>'email',
                    'type'=>'raw',
                    'value' => function($data){
                        return CHtml::mailto($data->email, $data->email);
                    }
                ],
                [
                    'class' => 'StatusButtonColumn', 
                    'name' => 'status',
                    'action' => 'status', //<--action for this button
                    'headerHtmlOptions' => ['width'=>70],
                    'header' => 'Активность',
                    'filter' => Lookup::items('StandartStatus'),
                    'value' => '$data->status',
                ]
            );

            if($type == 'blocked')
                $columns[] = [   
                    'class' => 'SButtonColumn',
                    'buttons' => [
                        'update' => ['visible' => 'false'],
                        'delete' => ['url' => 'url("/staff/back/delete/", ["id"=>$data->id, "type"=>"'.$type.'"])'],
                        'visible' => $type == 'block'
                    ],
                    'visible' => user()->id == 9502 || user()->id == 2
                ];

            $this->widget('SGridView', array(
                'id'=>'users-table',
                'dataProvider'=>$provider,
                'filter'=>$provider->model,
                'flexible'=>true,
                'showNumColumn' => false,
                'columns'=>$columns,
                'style' => 'blue',
                'type' => 'striped bordered',
                'showButtonsColumn' => $type == 'block'
                
            )); ?>
        </div>
    </div>
<? $this->endWidget(); ?>