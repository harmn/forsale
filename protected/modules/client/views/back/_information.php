<?php if(!$model) $model = new UserContract; ?>


<div class="w280 fl mr5">
	<h4>Паспорт</h4>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Дата рожд.</label>
		<span class="field" style="margin-left: 120px">
			<div class="custom-date-picker iblock">
				<a class="prev fa fa-caret-left" href="#"></a>
				<input class="w100p small custom-date-picker-input grd-white tcenter" autocomplete="off" id="action_date_from" name="action_date_from" type="text" value="">
				<a class="next fa fa-caret-right" href="#"></a>
			</div>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Место рожд.</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Серия</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Номер</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Кем выдан</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Когда выдан</label>
		<span class="field" style="margin-left: 120px">
			<div class="custom-date-picker iblock">
				<a class="prev fa fa-caret-left" href="#"></a>
				<input class="w100p small tcenter custom-date-picker-input grd-white" autocomplete="off" id="action_date_from" name="action_date_from" type="text" value="">
				<a class="next fa fa-caret-right" href="#"></a>
			</div>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Зарегистр.</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
		</span>
	</div>
</div>	
	
<div class="w280 fl mr5">
	<h4>Договор</h4>
	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">&#8470; договора</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small mb5">
			
			<div class="clearfix">
				<div class="fl  mr5">
					<div class="custom-date-picker iblock" style="width: 96px">
						<a class="prev fa fa-caret-left" href="#"></a>
						<input class="w100p small tcenter custom-date-picker-input grd-white" autocomplete="off" id="action_date_from" name="action_date_from" type="text" value="">
						<a class="next fa fa-caret-right" href="#"></a>
					</div>
				</div>
				
				<div class="fl">
					<select name="" id="" class="small" data-width="100%">
						<option value="">руб.</option>
						<option value="">руб.</option>
						<option value="">руб.</option>
					</select>
				</div>
			</div>
		</span>
		
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Фиксированная</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Мотивационная</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Почасовая</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Сдельная</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">

		<label for="Employee_fixed_price" style="width: 112px">Комментарии</label>
		<span class="field" style="margin-left: 120px">
			<a href="" class="fa fa-comments fsize18"></a>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Файлы</label>
		<span class="field" style="margin-left: 120px">
			<a href="" class="fa fa-cloud-download fsize18" rel="tooltip" title="Загрузить файл"></a>
		</span>
	</div>
</div>

<div class="w360 fl mr5">
	<h4>Банковские реквизиты</h4>
	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Назв. банка</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Р/С</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">ИНН</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">КПП</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">К/С</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>
</div>

<div class="w250 fl mr5">
	<h4>Учебное заведение</h4>
	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Наименование</label>
		<span class="field" style="margin-left: 120px">
			<input type="text" class="small">
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">Обучение</label>
		<span class="field" style="margin-left: 120px">
			<div class="custom-date-picker iblock">
				<a class="prev fa fa-caret-left" href="#"></a>
				<input class="w100p mb5 small tcenter custom-date-picker-input grd-white" autocomplete="off" id="action_date_from" name="action_date_from" type="text" value="">
				<a class="next fa fa-caret-right" href="#"></a>
			</div>

			<div class="custom-date-picker iblock">
				<a class="prev fa fa-caret-left" href="#"></a>
				<input class="w100p small tcenter custom-date-picker-input grd-white" autocomplete="off" id="action_date_from" name="action_date_from" type="text" value="">
				<a class="next fa fa-caret-right" href="#"></a>
			</div>
		</span>
	</div>

	<div class="control-group clearfix">
		<label for="Employee_fixed_price" style="width: 112px">С отличием</label>
		<span class="field" style="margin-left: 120px">
			<div class="checkbox">
				<label for="checkbox-inquiry800"></label>
				<input type="checkbox" value="1" id="checkbox-inquiry800">
			</div>
		</span>
	</div>
</div>

<div class="w280 fl">
	<h4>Другие</h4>
	<div class="control-group">
		<?=$form->labelEx($model, 'fixed_price', ['style'=>'width:112px'])?>
		<span class="field" style="margin-left: 120px">
			<?=$form->textField($model,'fixed_price', ['class'=>'small'])?>
			<?=$form->error($model,'fixed_price')?>
		</span>
	</div>

	<div class="control-group">
		<?=$form->labelEx($model, 'motivational_price', ['style'=>'width:112px'])?>
		<span class="field" style="margin-left: 120px">
			<?=$form->textField($model, 'motivational_price', ['class'=>'small'])?>
			<?=$form->error($model, 'motivational_price')?>
		</span>
	</div>

	<div class="control-group">
		<?=$form->labelEx($model, 'hourly_price', ['style'=>'width:112px'])?>
		<span class="field" style="margin-left: 120px">
			<?=$form->textField($model, 'hourly_price', ['class'=>'small'])?>
			<?=$form->error($model, 'hourly_price')?>
		</span>
	</div>

	<div class="control-group">
		<?=$form->labelEx($model, 'piecework_price', ['style'=>'width:112px'])?>
		<span class="field" style="margin-left: 120px">
			<?=$form->textField($model, 'piecework_price', ['class'=>'small'])?>
			<?=$form->error($model, 'piecework_price')?>
		</span>
	</div>
</div>
<br class="clear">

