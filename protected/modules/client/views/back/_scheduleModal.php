<?php

$modal = $this->beginWidget('UIModal',[
	'id' => 'schedule-modal',
	'width' => 370,
	'title' => 'Назначить график',
	'footerButtons' => [
		'assign' => [ 'value' => 'Назначить', 'icon' => false, 
			'htmlOptions' => ['type'=>'button', 'class'=>'btn btn-success assign-schedule', 'data-dismiss'=>'modal']]
	]
	]);

	$form = $this->beginWidget('SActiveForm',[
		'modal' => true,
		'action' => '',
	]);

	$modal->header(); ?>
		<div class="mt20 mb20">
			<table class="table table-bordered ma">
				<thead>
					<tr>
						<th></th>
						<th>От</th>
						<th>До</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><span class="pl10 pr10">Работа</span></td>
						<td>
							<?=CHtml::dropDownList("assign_work_begin", '', 
								UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>10]);?>			
						</td>
						<td>
							<?=CHtml::dropDownList("assign_work_end", '', 
								  UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>10]);?>
						</td>
					</tr>
					<tr>
						<td><span class="pl10 pr10">Обед</span></td>
						<td>
							<?=CHtml::dropDownList("assign_lunch_begin", '', 
								UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>10]);?>					
						</td>
						<td>
							<?=CHtml::dropDownList("assign_lunch_end", '', 
								UIHelpers::getTimeInterval(), ['empty'=>'&nbsp;', 'data-width'=>'90', 'data-size'=>10]);?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
<? 
	$modal->footer();
	$this->endWidget();
$this->endWidget();
?>