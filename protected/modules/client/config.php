<?php
return array(
	'modules' => array(),
	'import' => array(
		//user module
		'application.modules.client.models.*',
		'application.modules.client.components.*',
        'application.modules.client.behaviors.*',
	),
    'params' => array(
        'images'=>array(
            'client' => array(
                'path' => 'storage/images/user/',
                'placeholder' => 'storage/placeholders/user/frontend/',
                'sizes'=>array(
                    'original' => array(),
                    'big' => array('width'=>140, 'height'=>140, 'crop'=>true),
                    'thumb' => array('width'=>50, 'height'=>50, 'crop'=>true),
                )
            )
        ),
    ),
	'components' => array(
    ),
    'rules' => array(

        ADMIN_PATH.'/client' => 'client/back/index',
        
        ADMIN_PATH.'/client/<action:\w+>/<id:\d+>' => 'client/back/<action>',
        ADMIN_PATH.'/client/<action:\w+>/*' => 'client/back/<action>',

    )
);