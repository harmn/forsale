(function($) {
	var Staff = {

		init : function(options) {
			
		},

		/**
		 * Penalty checkboxes
		 */
		penalty : function(event) {
			event.preventDefault();
			var self = $(event.target);

			c_1_2 = {
				inactive: 'op4 bg-yellow c-yellow',
				active:   'bg-yellow c-yellow',
				approved: 'bg-yellow c-dark-gray'
			};

			c_3 = {
				inactive: 'op4 bg-red c-red',
				active  : 'bg-red c-red',
				approved: 'bg-red c-red c-dark-gray'
			};

			var click = parseInt(self.attr('data-click'));

			// недопускать следующий клик не одобряя предидущий
			if(self.prev().length == 0){
				//это первая карточка
				if(self.next().attr('data-click') != 0){
					showErrorMessage('Менять только по порядку');
					return false;
				}

			}
			else{
				//это вторая и третяя карточка
				if(self.prev().attr('data-click') != 2){
					showErrorMessage('Менять только по порядку');
					return false;
				}
				else if(self.next().length > 0 && self.next().attr('data-click') != 0){
					showErrorMessage('Менять только по порядку');
					return false;
				}
			}

			if($("#penalty-comments").is(":hidden"))
				$.fn.comments("open", "penalty-comments", self);

			// при клике
			switch(click) {
				case 0:
					self.addClass('op4');
					if(self.attr('id') == 'c_1' || self.attr('id') == 'c_2'){
						self.removeClass(c_1_2.inactive);
						self.addClass(c_1_2.active);
					}
					else{
						self.removeClass(c_3.inactive);
						self.addClass(c_3.active);
					}

					break;
				case 1:
					if(self.attr('id') == 'c_1' || self.attr('id') == 'c_2'){
						self.removeClass(c_1_2.active);
						self.addClass(c_1_2.approved);
					}
					else{
						self.removeClass(c_3.active);
						self.addClass(c_3.approved);
					}

					break;
				case 2:
					self.addClass('op4');
					if(self.attr('id') == 'c_1' || self.attr('id') == 'c_2'){
						self.removeClass(c_1_2.approved);
						self.addClass(c_1_2.inactive);
					}
					else{
						self.removeClass(c_3.approved);
						self.addClass(c_3.inactive);
					}

					break;
			}

			if(click < 2) click++;
			else click = 0;

			self.attr('data-click', click);

			var sum = 0;
			$.each(self.parent().find('a'), function(index){
				sum += parseInt($(this).attr('data-click'));
			});

			// for values 
			var level = 0;
			var approved = 0;
			
			if(sum != 0){
				if(sum%2 == 0){
					level = sum/2;
					approved = 1;
				}
				else{
					level = Math.ceil(sum/2);
					approved = 0;
				}
			}
				
			$("[name='Client[penalty_level]']").val(level);
			$("[name='Client[penalty_approved]']").val(approved);
		},

		/**
		 * assignSchedule1
		 */
		assignSchedule1 : function(event, target){
			var schedule = $("#schedule");

			if(schedule.length > 0){
				if(schedule.find("table input.check-assign:checked").length == 0){
					showErrorMessage('Нечего не выбрано');
					return false;
				}

				target.find('form select').val('').selectStyler('refresh');
				target.doModal('show');
			}
		},
		/**
		 * assignSchedule2
		 */
		assignSchedule2 : function(event){
			var self = $(event.target);
			
			var fields = ['work_begin', 'work_end', 'lunch_begin', 'lunch_end'];
			var result = {};

			for (var i = fields.length - 1; i >= 0; i--) {
				result[i] = self.closest('form').find("[name='assign_"+fields[i]+"']").val();
			};


			$.each($(".check-assign:checked"), function(index){
				var checkbox = $(this);
				$.each(fields, function(i){
					var num = checkbox.closest('tr').data('id');
					var field = checkbox.closest('tr').find("[name='UserSchedule[old]["+num+"]["+fields[i]+"]']");
					field.val(result[i]);
					field.selectStyler('refresh');
				});
			});
		},

		/**
		 * Check All In Schedule
		 */

		checkAll : function(event){
			var self = $(event.target);

			if(self.is(':checked'))
				$("input.check-assign").attr('checked', 'checked');
			else
				$("input.check-assign").removeAttr('checked');
		},

		// берем информацию и открываем модальное окно 
		getCardInfoByNumber: function(number){
			jPost('/admin/loft/registration', {number: number}, function(data){
				if(!data.success){
					jPost("/admin/loft/getCard", {idCard: data.number}, function(data){
						if(data.success){
							var additional = {
								"target" : "#edit-card-modal",
								"model"  : "Card",
								"title"	 : "Изменить карту",
								"action" : "/admin/loft/update"
							};
							$.fn.openModal(data, additional);
							$("#edit-card-modal").find('input:text:first').select();
						}
					});
				}
			});
		},
		
	};

	//events
	$(function(){
		var modal = $("#schedule-modal");
		$(document).on('click', '.check-all-assign', Staff.checkAll);
		$(document).on('click', 'span.penalty a', function(e){
			Staff.penalty(e);
		});

		$(document).on('click', '#schedule #assign-schedule', function(e){
			e.preventDefault();
			Staff.assignSchedule1(e, modal);
		});

		//показывает цвет когда ставится Прайс менеджер
		$('#Client_id_job').on('change', function(event){
			console.log($(this).val());
			if($(this).val() == 67){
				$('#user-update-price-manager-color').removeClass('hidden');
			}
			else{
				$('#user-update-price-manager-color').addClass('hidden');
			}
			
		});

		modal.on('click', '.assign-schedule', Staff.assignSchedule2);

		// идентификация карточки + переход в модал
		var KeyCodeArray = [];
		$(window)
		.on('keypress', function(event){
			var code = event.keyCode || event.charCode;
			if (code == '59' || KeyCodeArray[0] == ';'){
				var charName = String.fromCharCode(code);
				if(charName)
					KeyCodeArray.push(charName);

				if(KeyCodeArray.length == 12 && KeyCodeArray[KeyCodeArray.length-1] == '?'){
					var number = KeyCodeArray.join('');
					number = number.replace(/\D/g,'');
					$("#numberHiddenInput").val(number);
					if(number.length == 10){
						Staff.getCardInfoByNumber(number);
					}
					KeyCodeArray = [];
				}
			}
		});
	});

})( jQuery );