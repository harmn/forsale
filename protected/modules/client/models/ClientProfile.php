<?php 


/**
* ClientProfile
*
* 
* @property integer    $id_job  - должность
* 
*/
class ClientProfile extends AR
{
	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;
	
	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Имя таблицы вместе с именем БД
	 * @return string
	 */
	public function tableName(){
		return 'client_profile';
	}

	/**
	 * Правила валидации
	 */
	public function rules() {

		return CMap::mergeArray(parent::rules(), array(
			//max, min
			array('skype_name', 'length', 'max'=>50),
			//filter trim
			array('comment, facebook', 'filter', 'filter'=>'trim'),
			//filter purify
			array('comment, facebook', 'filter', 'filter' => [$this->purifier, 'purify']),
			//numerical
			array('id_job, type', 'numerical', 'integerOnly' => true, 'allowEmpty'=>true),
		));
	}

	/**
	 * Relations
	 * @return array
	 */
	public function relations(){
		return [
			'job' => [self::BELONGS_TO, 'Job', 'id_job'],
			'employee' => [self::BELONGS_TO, 'Employee', 'id_user']
		];
	}


	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
			'dateBehavior' => array(
				'class'           => 'DateBehavior',
				'dateAttribute'   => [
					'passport_birthday', 
					/*'candidate_date', 
					'study_date_start', 
					'study_date_end', 
					'probation_date_start', 
					'probation_date_end', 
					'hired_date', 
					'fired_date'*/],
				'calendar' => array('format' => 'd/m/Y', 'delimiter' => '/')
			),
		));
	}

	public function attributeLabels() {
		
		return CMap::mergeArray(parent::attributeLabels(), array(
			'skype_name'         =>  t('user', 'Логин Skype'),
			'id_job'             =>  t('user', 'Должность'),
			'facebook'           =>  t('user', 'Facebook'),
			'type'           	 =>  t('user', 'Тип электронной карточки'),
			
			//profile
			'phone'              =>  t('user', 'Телефон'),
			'comment'            =>  t('user', 'Заметки'),
		));
	}
}

