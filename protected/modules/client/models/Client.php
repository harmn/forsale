<?php 

/**
* Модель сотрудника
* Employee
*
* @property integer    $retired        			    - уволен ли сотрудник
* @property string     $phone             			- телефон
* @property integer    $passport_gender   			- пол
* @property date       $passport_birthday 			- дата рождения
* @property string     $comment           			- заметки
* @property string     $skype_name        			- имя в скайпе
* @property integer    $is_chief           		    -     
* @property integer    $is_chief_assistant 		    -     
* @property integer    $penalty_level     			- штрафная карточка
* @property integer    $penalty_approved  			- потверждение штрафа
* @property date 	   $penalty_level_1_date     	- 
* @property date 	   $penalty_level_2_date     	- 
* @property date 	   $penalty_level_3_date     	- 
* @property varchar    $color						- цвет сотрудника
*/

class Client extends User
{

	public static $filter_status = [0 => "Не допущенный", 1 => "Допущенный"]; //фильтр допущенные
	public static $filter_retired = [0 => "Не уволен", 1 => "Уволен"]; //фильтр уволен
	public static $myAdvertRelations = 	[	
											1 => "Home", 
											2 => "Car",
											3 => "Electronics",
											4 => "Technics",
											5 => "Furniture",
											6 => "Fashion",
											7 => "Sport",
											8 => "Animal",
											9 => "Work",
											10 => "Services",
										];

	private static $listData; 			//для хранения кеша
	private static $listDataCustom; 	//для хранения кеша custom
	private static $myAdverts; //для хранения кеша
	
	public $phone, $skype_name; //для фильтров (только те переменные которых нет в полях)

	public function init(){
		parent::init();

		$this->searchAttributes = CMap::mergeArray($this->searchAttributes, 
					['phone', 'skype_name']);
	}

	/**
	 * Model
	 * @param  $classname
	 * @return CModel
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function relations(){
		return CMap::mergeArray(parent::relations(), [
			'profile' 		=> [self::HAS_ONE, 'ClientProfile', 'id_user', 'deleteBehavior'=>true], //
			'Home' 			=> [self::HAS_MANY, 'Home', 'id_creator'],
			'Car' 	  		=> [self::HAS_MANY, 'Car', 'id_creator'],
			'Electronics' 	=> [self::HAS_MANY, 'Electronics', 'id_creator'],
			'Technics' 	  	=> [self::HAS_MANY, 'Technics', 'id_creator'],
			'Furniture' 	=> [self::HAS_MANY, 'Furniture', 'id_creator'],
			'Fashion' 	  	=> [self::HAS_MANY, 'Fashion', 'id_creator'],
			'Sport' 	  	=> [self::HAS_MANY, 'Sport', 'id_creator'],
			'Animal' 	  	=> [self::HAS_MANY, 'Animal', 'id_creator'],
			'Work' 	  		=> [self::HAS_MANY, 'Work', 'id_creator'],
			'Services' 	  	=> [self::HAS_MANY, 'Services', 'id_creator'],
		]);

	}

	public function rules(){
		return CMap::mergeArray(parent::rules(),[
			/**
			 * Общие правила
			 */
			//required
			['phone', 'required', 'on' => 'create, update, fregistration' , 'message' => Yii::t('front', 'Укажите телефон')],
			['is_social_user', 'numerical', 'integerOnly' => true, 'allowEmpty'=>true],
			['username', 'required', 'on' => 'fregistration' , 'message' => Yii::t('front', 'Логин нужен для входа на сайт')],
			['firstname', 'required', 'on' => 'fregistration' , 'message' => Yii::t('front', 'Пожалуйста, введите Ваше имя')],
			['username', 'required', 'on' => 'flogin' , 'message' => Yii::t('front', 'Имя пользователя не может быть пустым')],
			['password', 'required', 'on' => 'flogin' , 'message' => Yii::t('front', 'Пароль пользователя не может быть пустым')],
		]);
	}

	public function behaviors(){
		return CMap::mergeArray(parent::behaviors(), array(
		));
	}

	public function defaultScope(){
		return array(
	        'with' => array('profile'),
	        'together' => true,
	    );
	}

	public function getBackUrl(){
		return app()->createUrl('/client/back/update', array('id'=>$this->id));
	}

	/**
	 * Retrieves a list of models based on the current search/filteњr conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = false){

		if(!$criteria) $criteria = new CDbCriteria;
		
		$criteria->compare('t.id', $this->id);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('t.status', $this->status);
		$criteria->compare('is_social_user', $this->is_social_user);

		$criteria->compare('firstname', $this->firstname, true);
		$criteria->compare('lastname', $this->lastname, true);
		$criteria->compare('middlename', $this->middlename, true);

		$this->compareUser($criteria, $this->fullname, 't');
		

		//Роль
		// $criteria->compare('profile.id_job', $this->job);
		// уволен
		$criteria->compare('profile.retired', $this->retired);
		
		if(is_array($criteria->with)) $criteria->with = array_unique($criteria->with);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			  'pageSize'=>Common::getPagerSize(__CLASS__),
			  'pageVar' => 'page'
			),
			'sort'=>array(
				'defaultOrder'=>'t.id DESC',
				'attributes'=>array(
					'fullname'=>array(
						'asc' => 'firstname, lastname, middlename, username',
						'desc' => 'firstname DESC'
						),
					'username',
					'email',
					'status'
				)
			),
		));
	}

	/**
	 * list Data пользователи БЕЗ КУРЬЕРОВ
	 * возвращяет масив ['id' => 'name'] для dropDownList и сохраняет в кеш
	 */
	public static function listData($options = []){
		// кроме курьеров
		$cacheKey= "listdata.employee";
		
		// в случает eсли нужны те ползователи, которые находятся в смене
		if(isset($options['inshift']) && $options['inshift']) $cacheKey .= ".inshift";
		// в случает eсли нужны все ползователи
		if(isset($options['all']) && $options['all']) $cacheKey .= ".all";


		$activeUser = [user()->id => user()->fullname];

		if((isset(self::$listData[$cacheKey]) && self::$listData[$cacheKey]) || (self::$listData[$cacheKey] = Yii::app()->cache->get($cacheKey))){
			// current user
			self::$listData[$cacheKey] = $activeUser + self::$listData[$cacheKey];

			//Only users in shift
			if(isset($options['inshift']) && $options['inshift']) $this->leaveInShift($cacheKey, self::$listData);

			return self::$listData[$cacheKey];
		}

		$criteria = new CDbCriteria;
		$criteria->order = "CONCAT(firstname, ' ', lastname)";

		// если передан all показать уволенных тоже
		if(!isset($options['all']) || (isset($options['all']) && !$options['all'])){
			$criteria->compare('profile.retired', self::RETIRED_NO);
			$criteria->compare('t.status', self::STATUS_ACTIVE);
		}

		foreach (self::model()->findAll($criteria) as $courier)
			self::$listData[$cacheKey][$courier->id] = $courier->firstname.' '.$courier->lastname;

		$dependency = new CDbCacheDependency($sql = "SELECT MAX(changed) FROM ".self::model()->tableName());

		Yii::app()->cache->set($cacheKey, self::$listData[$cacheKey], 0, $dependency);
		
		//Only users in shift
		if(isset($options['inshift']) && $options['inshift']) $this->leaveInShift($cacheKey, self::$listData);

		self::$listData[$cacheKey] = $activeUser + self::$listData[$cacheKey];
		
		return self::$listData[$cacheKey];
	}

	/**
	 * list Data CUSTOM
	 * возвращяет масив ['id' => **, 'name' => **, 'id_job' => **, 'username' =>**] для dropDownList и сохраняет в кеш
	 */
	public static function listDataCustom($type = false, $id = false, $options = []){
		$cacheKey = "listdata.employee.custom";

		$inshift = (isset($options['inshift']) && $options['inshift']) ? true : false;
		$depend  = (isset($options['depend']) && $options['depend']) ? true : false;
		
		// в случает eсли нужны те ползователи, которые находятся в смене
		$cacheKey = $inshift ? $cacheKey.".inshift" : $cacheKey;
		
		// в случает eсли ползователи нужны для зависимости от ролей
		$cacheKey = $depend ? $cacheKey.".depend" : $cacheKey;

		$criteria = ""; 
		$params = array();
		// $condition = " WHERE true";
		$select = "u.id, p.id_job, u.username, CONCAT(firstname, ' ', lastname) as name"; //u.driver_color
		$condition = " WHERE status = ".self::STATUS_ACTIVE; //активные
		$join = "";
		$order = "name";
		$all = false;
	   	
		switch($type){
			case 'job':
				$cacheKey .= ".job.{$id}";
				$criteria = 'p.id_job = :id';
				$params[':id'] = $id;
				$condition .= " AND p.id_job = {$id}";
				break;
			case 'group':
				$select .= ', u.firstname, u.lastname';
				$cacheKey .= ".group.{$id}";
				$criteria .= 'j.group = :group';
				$params[':group'] = $id;

				if(isset($options['order']))
					$order = $options['order'];
				break;

			default:
				$cacheKey .= ".all";
				break;
		}

		//проверка кеша
		if((isset(self::$listDataCustom[$cacheKey]) && self::$listDataCustom[$cacheKey]) || (self::$listDataCustom[$cacheKey] = Yii::app()->cache->get($cacheKey))) {
			//Only users in shift
			if($inshift) $this->leaveInShift($cacheKey, self::$listDataCustom);

			return self::$listDataCustom[$cacheKey];
		}

		// collect command
		$command = Yii::app()->db->createCommand()
							->select($select)
							->from(self::model()->tableName()." u")
							->leftJoin('employee_profile p', 'p.id_user = u.id')
							->order($order);
							// ->join("user_status us", 'u.id=us.id_user');

		if($type == 'group')
			$command->join("job j", "p.id_job = j.id");

		if(!$all) $command->where($criteria, $params)
							->andWhere("p.retired = :retired and u.status = :status", 
							[':retired' => self::STATUS_INACTIVE, ':status' => self::STATUS_ACTIVE]); //не уволенные и активные

		$query = $command->queryAll();

		// set id jobs as data attributes in options for depending with users
		if($depend)
			foreach ($query as $key => $value)
				self::$listDataCustom[$cacheKey][$value['id']] = [
					'data-idjob' => $value['id_job']
				];
		else
			foreach ($query as $key => $value) {
				self::$listDataCustom[$cacheKey][$value['id']] = [
					'id'		 => $value['id'],
					'name'		 => $value['name'],
					'username'	 => $value['username'],
					'id_job'	 => $value['id_job'],
				];
				
				if(isset($options['display']) && !$options['display'])
					self::$listDataCustom[$cacheKey][$value['id']]['style'] = 'display: none;';
			}

		$dependency = new CDbCacheDependency($sql = "SELECT MAX(u.changed) FROM {{user}} u LEFT JOIN {{employee_profile}} p on u.id = p.id_user".$condition);
		Yii::app()->cache->set($cacheKey, self::$listDataCustom[$cacheKey], 0, $dependency);

		//Only users in shift
		if($inshift) $this->leaveInShift($cacheKey, self::$listDataCustom);
		
		return self::$listDataCustom[$cacheKey];
	}

	//
	public function searchMyAdverts($criteria = false){
		if(self::$myAdverts) return self::$myAdverts;

		$cacheKey= "listdata.myAdverts".user()->id;

		if(self::$myAdverts = Yii::app()->cache->get($cacheKey))
			return self::$myAdverts;

		if(!$criteria) $criteria = new CDbCriteria;

		$criteria->with = ['Home', 'Car', 'Electronics', 'Technics', 'Furniture', 'Fashion', 'Sport', 'Animal', 'Work', 'Services'];

		$criteria->compare('t.id', user()->id);
		
	  	$data = Client::model()->findAll($criteria);
	  	if($data && $data[0]){
		  	$data = $data[0];
		  	$results = [];
	  		foreach(self::$myAdvertRelations as $key){
	  			if($data->$key){
	  				$results[$key] = [];
	  				foreach($data->$key as $item){
	  					$results[$key][] = $item;
	  				}
	  			}
	  		}
  			self::$myAdverts = $results;
	  	}else{
	  		self::$myAdverts = [];
	  	}

		Yii::app()->cache->set($cacheKey, self::$myAdverts, 60*60*24*30);

		return self::$myAdverts;
	}
}

			
	
