<?php
/**
* Client Module BackController
* 
* Контроллер для управления пользователями
*/
class BackController extends BController
{    
	public $model = 'Client'; //for loadModel function

	//экшены
	public function actions()	
	{
		return CMap::mergeArray(parent::actions(), array(
			// Создаем actions captcha.
			// Он понадобиться нам для формы регистрации (да и авторизации)
			 'captcha'=>array(
				'class'=>'core.actions.SCaptchaAction',
				'height'=>34,
				'backend'=> 'gd',
			),
		));
	}

	/**
	 * метод который показывает список пользователей
	 */
	public function actionIndex(){

		$model = new Client('search');
		$criteria = new CDbCriteria;

		//для списков всегда вставлять этот кусок кода
		if (intval(app()->request->getParam('clearFilters'))==1) {
			SButtonColumn::clearFilters($this, $model);
		}

		if($type = Yii::app()->getRequest()->getParam('type', 'all')){
			
			$blocked = Yii::app()->db->createCommand()
									->select('id_user')
									->from('{{user_block}}')
									->queryColumn();

			switch($type){ //показать только заблокированных пользователей
				case 'blocked':
					if(!$blocked) $blocked = '-1';
					$criteria->compare('t.id', $blocked);
					break;

				default:
					$criteria->compare('t.id !', $blocked);
					break;
			}	
			
		}

		$provider = $model->search($criteria);

		if(Yii::app()->request->isAjaxRequest && Yii::app()->request->getParam('ajax')){
        	$this->renderPartial('index', compact('provider', 'type'));
		}
        else{
        	$this->layout = "//layouts/tabs";
        	$this->pageTitle   = 'Управление сотрудниками';
			
			$this->breadcrumbs = ['Сотрудники'];
			$this->tabs = [
				''=>t('admin', 'Все'),
				'blocked'=>t('admin', 'Заблокированные')
				];

       		$this->render('index', compact('provider', 'type'));
        }
	}

	/**
	 * метод который показывает форму с данными пользователя
	 */
	public function actionUpdate($id){

		$model = $this->loadModel($this->model, $id, ['profile']);
			
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		$this->pageTitle = 'Карточка сотрудника ['.$model->getFullName().']';
		
		$this->breadcrumbs = ['Сотрудники'=>['back/index'], t('user','Карточка сотрудника')];

		$this->tabs = [
			'main'    => t('admin', 'Общая информация'),
			// 'more'    => t('admin', 'Дополнительно'),
			// 'status'  => t('admin', 'Статусы'),
			// 'schedule' => t('admin','Графики'),
			// 'information' => t('admin','Информация')
			];

		if($model->is_social_user && user()->isRole('admin')){
			$this->tabs['auth'] = t('admin', 'Данные из соц. сети');
		}

		if(isset($_POST['Client']))
		{
			$model->attributes = $_POST;

			if($model->save()){

				if($model->id == user()->id){ //сохранение настроек профиля
					user()->UpdateInfo($model); //обновить данные текущего пользователя
				}

				$this->refresh();
			}
				
		}

		//объязательно
		if(request()->getParam('close') == 'true')
			$this->redirect(user()->gridIndex);

		$changePasswordModel = $model;
		$changePasswordModel->scenario = 'resetPassword';
		$changePasswordModel->password = '';

		$this->layout = '//layouts/tabs';

		cs()->registerScriptFile($this->module->assetsUrl.'/js/client.js');

		$this->render('update', compact('model', 'changePasswordModel', 'profile', 'ustatus', 'contract'));
	}

	public function actionProfile(){
		$this->actionUpdate(user()->id);
	}

	/**
	 * Смена пароля (AJAX)
	 */
	public function actionChangePassword($id = false){

		$model = $this->loadModel($this->model, $id);
		$model->scenario = 'resetPassword';
		$model->password = '';

		$this->performAjaxValidation($model);

		//если у редактироемого пользователя уровень выше 
		/**
		 * @todo
		 */

		if(!empty($_POST[$this->model])){    //случай изменения своего собственного пароля
			//сохранение нового пароля
			$model->attributes = $_POST[$this->model];

			if($model->save()){
				echo CJSON::encode(['success'=>true]);
			}  
			else {
				echo CJSON::encode(['success' => false, 'errors' => $model->getErrors()]);
			}
			
		}

	}

	public function actionDelete($id = 0){
		$type = request()->getParam('type', 'all');

		if($type == 'blocked') {
			$models = UserBlock::model()->findAll('id_user = :id', [':id'=>$id]);
			foreach($models as $model){
				$model->delete();
			}
		}
		else {
			$model = $this->loadModel($this->model, $id);
			$model->delete();
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
	}

	/**
	* Метод регистрации (AJAX)
	*
	* Выводим форму для регистрации пользователя и проверяем
	* данные которые придут от неё.
	*/
	public function actionRegistration() {

		$mdlUser = $this->loadModel('Client', false, ['profile']);
		// dump($mdlUser->profile, true);
		$mdlUser->scenario = 'registration';
		$mdlUser->is_social_user = User::SOCIAL_USER_NO;

		//ajax validation
		$this->performAjaxValidation($mdlUser);

		/**
		* Если $_POST['Client'] не пустой массив - значит была отправлена форма
		* следовательно нам надо заполнить $mdlUser этими данными
		* и провести валидацию.            
		*/
		if (request()->isPostRequest && !empty($_POST[ 'Client' ])) {

			// Заполняем $mdlUser данными которые пришли с формы
			$mdlUser->attributes = $_POST[ 'Client' ];
			$mdlUser->email_confirmed = User::EMAIL_CONFIRM_YES;

			//добавляем информацию в профиль

			// В validate мы передаем название сценария. Оно нам понадобиться
			// когда будем заниматься созданием правил валидации
			if($mdlUser->save()) {
				// Если валидация прошла успешно... 

				echo CJSON::encode(['success'=>true]);
			}
		 } 
	}

	/**
	 * Вернуть все комментарии указанной записи, в отформатированном виде
	 * @return JSON - список комментариев
	 */
	public function actionGetComments(){
		$model = $this->loadModel('Client', request()->getParam('id'));
		$comments = $model->comments; //todo: потом когда вся информация о пользователе будет в одной базе, сделать with=>'user'
	
		$result = [];
		foreach($comments as $comment){
			$result[] = $comment->render();
		}

		echo CJSON::encode($result);
	}

	/**
	 * Добавить комментарий для карточки
	 */
	public function actionAddComment(){
		$model = new PenaltyComment;

		$this->performAjaxValidation($model);

		$model->id_user = user()->id;
		$model->comment = request()->getParam('comment');
		$model->id_recipient = request()->getParam('id_payment');
		$model->date = date('y-m-d H:i:s');
	
		if($model->save())
			Common::jsonSuccess(true, ['data' => $model->render()]);
		else
			Common::jsonError("Ошибка при удалении");
	}


}
