<?php

class m150217_135920_create_comment_table extends EDbMigration
{
	public function up(){
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{comment}}"))
			$this->dropTable("{{comment}}");

		$this->createTable("{{comment}}", [
			"id"			=> "int UNSIGNED AUTO_INCREMENT",
			"created"		=> "datetime",
			"id_creator"	=> "int UNSIGNED",
			"changed"		=> "datetime",
			"id_changer"	=> "int UNSIGNED",
			"date"			=> "datetime",
			"type"			=> "int UNSIGNED",
			"object_id"		=> "int UNSIGNED",
			"text"			=> "varchar(500) CHARACTER SET UTF8",
			"status"		=> "tinyint(1)",
			"sender_name"	=> "varchar(255)",
			"sender_email"	=> "varchar(255)",
			"sender_ip"		=> "varchar(20)",
			"PRIMARY KEY (id)",
			"KEY type (type)",
			"KEY object_id (object_id)",
			"KEY text (text)",
			"KEY status (status)",
			]
		);
	}

	public function down(){
		//delete table if exists
		if(Yii::app()->db->getSchema()->getTable("{{comment}}"))
			$this->dropTable("{{comment}}");
	}
}