<?php

class m150131_140251_create_feedback_table extends EDbMigration
{
	public function up()
	{
		if(Yii::app()->db->getSchema()->getTable("{{feedback}}"))
			$this->dropTable("{{feedback}}");

		$transaction = Yii::app()->db->beginTransaction();

		try{
			$this->createTable("{{feedback}}", array(
				"id"				=> "int UNSIGNED AUTO_INCREMENT",
				"created"			=> "datetime",
				"id_creator"		=> "int(10) UNSIGNED",
				"changed"			=> "datetime",
				"id_changer"		=> "int(10) UNSIGNED",
				"author"			=> "varchar(100) CHARACTER SET UTF8",
				"photo"				=> "int(10) UNSIGNED",
				"title"				=> "varchar(100) CHARACTER SET UTF8",
				"text"				=> "text CHARACTER SET UTF8",
				"status"			=> "tinyint(1) UNSIGNED",
				"PRIMARY KEY (id)",
				"KEY author (author)",
				"KEY title (title)",
				"KEY status (status)"
			));

			$transaction->commit();
		}
		catch(Exception $e){
			$transaction->rollback();
		}

	}

	public function down()
	{
		if(Yii::app()->db->getSchema()->getTable("{{feedback}}"))
			$this->dropTable("{{feedback}}");
	}
}