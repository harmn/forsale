<?php
/* @var $this BackController */
/* @var $model Comment */

$url = $this->createUrl('change');
$script =<<<SCRIPT
$('span[contenteditable]').bind('blur', function(){
	val = $(this).html();
	id = $(this).data('id');
	jPost('$url', {id:id, val:val}, function(){});
});
SCRIPT;

cs()->registerScript('comment_change', $script, CClientScript::POS_READY);

?>

<div class="buttons mb20">
	<? $this->widget('UIButtons', ['buttons'=>[
			'DeleteSelected' => ['model'=>'SiteComment'],
			'clearFilters'
		], 'size' => 'small']) ?>
</div>

<?
$this->widget('SGridView', [
	'id' => 'grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate' => true,
	'gridIndex' => false, //не создавать (так как он мешает в других страницах)
	'columns'=>[
		[
			'header'=>t('admin', 'Ссылка'),
			'type' => 'raw',
			'headerHtmlOptions' => ['width'=>90],
			'value'=>function($data){
				return CHtml::link('Ссылка', $data->backUrl);
			}
		],
		[
			'class'=>'SDateColumn',
			'name'=>'date',
			'filter'=>UIHelpers::datePicker("SiteComment[date]", ''),
		],
		[	
			'type' => 'raw',
			'name'=>'sender_email',
			'value'=>function($data){
				return CHtml::link($data->getSender('email'), ($data->user ? $data->user->backUrl : ''));
			}
		],
		[
			'name'=>'text',
			'type'=>'raw',
			'value'=>function($data){
				return CHtml::openTag('span', [
						'contenteditable'=>'true',
						'data-id'=>$data->id,
					]).$data->text.CHtml::closeTag('span');}
		],
		[
			'class' => 'SDropdownColumn', 
			'name' => 'status',
			'action' => $this->createUrl('status'),
			'headerHtmlOptions' => ['width'=>100],
			'filter' => Lookup::items('CommentStatus'),	
			'value' => '$data->status',
		],
		[
			'class'=>'SButtonColumn',
			'template'=>'{delete}',
			'deleteButtonUrl'=>function($data){
				//данная стока нужна так, как при показе списка комментариев
				//из других модулей, ссылка удаления, генерируется не верно
				return $this->createUrl("status", ["id" =>  $data["id"]]); 
			}
		],
	],
]);

?>


