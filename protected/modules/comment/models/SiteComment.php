<?php

class SiteComment extends AR
{	
	const STATUS_DECLINED = 0;
	const STATUS_ACCEPTED = 1;
	const STATUS_NEW = 2;

	const TYPE_PRODUCT = 1;
	const TYPE_ADVERT = 1;

	public static $types = array(
		self::TYPE_PRODUCT	=> 'product',
		self::TYPE_ADVERT	=> 'advert',
	);

	//для ссылок в backurl
	public $backUrls = array(
		self::TYPE_PRODUCT	=> '/products/item',
		self::TYPE_ADVERT	=> '/advert/item',
	);

	public $verifyCode; //каптча

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{comment}}';
	}

	/**
	 * Правила валидации
	 */
	public function rules() {

		return array(
			
			//filter trim
			['sender_name, sender_email, text', 'filter', 'filter'=>'trim'],
			//filter purify
			['sender_name, text', 'filter', 'filter' => [$this->purifier, 'purify']],
			//required
			['sender_name, sender_email', 'required', 'on'=>'guest_insert'],
			['text', 'required'],
			//type
			['sender_email', 'email'],
			['status', 'numerical'],
			// капча
			['verifyCode', 'captcha',  'allowEmpty'=> !Yii::app()->user->isGuest, 'on'=>'guest_insert'],

			['type, object_id', 'numerical', 'integerOnly'=>true],
			//max, min
			['sender_name, sender_email', 'length', 'max'=>255],       

			//save validator
			['id, date, sender_email, text, status', 'safe', 'on'=>'search'],
		);

		return $rules;
	}

	public function relations(){
		return [
			'user'=>[self::BELONGS_TO, 'User', 'id_creator']
		];
	}

	public function behaviors() {
		return CMap::mergeArray(parent::behaviors(), [
			'dateBehavior' => [
				'class'=>'DateBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'changed',
				// 'dateAttribute' => 'date'
			],
		]);
	}

	public function beforeSave() {
		
		if(parent::beforeSave()) {

			if ($this->isNewRecord && !Common::isCLI()){

				// статус в зависимости от юзера - новый или принят
				$this->status = user()->isGuest ? self::STATUS_NEW : self::STATUS_ACCEPTED;
				
				$this->sender_ip = Yii::app()->request->userHostAddress;

				$this->date = new CDbExpression('NOW()'); //важно для данной модели, так как дата тут сохраняется не из админки
			}

			return true;
		}
		else return false;

	}

	public function defaultScope(){
		//if you use this in your defaultScope() you need to make sure it doesn't go into a recursive loop by:
		$alias = $this->getTableAlias(false, false);

		return array(
			'order'=>"{$alias}.date DESC"
		);
	}

	public function scopes(){
		$alias = $this->getTableAlias();
		
		return array(
			'new' => array(
				'condition' => $alias.'.status = :status',
				'params' => array(':status' => self::STATUS_NEW),
			),
			'accepted' => array(
				'condition' => $alias.'.status = :status',
				'params' => array(':status' => self::STATUS_ACCEPTED),
			),
			// 'front' => array(
			// 	'condition' => $alias.'.status = :status1 or '.$alias.'.status = :status2',
			// 	'params' => array(':status1' => self::STATUS_ACCEPTED, ':status2' => self::STATUS_NEW),
			// ),
			'declined' => array(
				'condition' => $alias.'.status = :status',
				'params' => array(':status' => self::STATUS_DECLINED),
			)
		);
	}

	/**
	 * Функция которая возвращает масив с названиями labels для соответствующих полей
	 */
	public function attributeLabels() {
		
		return array(
			'sender_name'    =>  t('admin', 'Имя'),
			'sender_email'   =>  t('admin', 'E-mail'),
			'text'           =>  t('admin', 'Комментарий'),
			'object'         =>  t('admin', 'Объект'),
			'status'         =>  t('admin', 'Статус'),
			'date'           =>  t('admin', 'Дата'),
		);
	}

	public function provider($type, $object_id, $limit = false){

		$criteria = new CDbCriteria;
		$criteria->compare('type', $type);
		$criteria->compare('object_id', $object_id);
		$criteria->scopes = ['accepted'];

		if($limit) $criteria->limit = $limit;

		return new CActiveDataProvider($this, 
			array(
				'criteria'=> $criteria,
				'pagination' => ($limit) ? false : array('pageSize'=>6)
			)
		);
	}

	public function getAvatar(){
		if($this->user)
			return $this->user->getThumbnail('thumb', 35, 35, $this->getSender('name'));

		return User::model()->getThumbnail('thumb', 35, 35, $this->getSender('name'));
	}

	public function formatDate(){
		return app()->dateFormatter->format('dd MMMM y '.t('front', 'в').' HH:mm:ss', $this->date);
	}

	public function getSender($field = false){
		if($this->user){
			$name = $this->user->getFullName();
			$email = $this->user->email;
		}
		else{
			$name = $this->sender_name;
			$email = $this->sender_email;
		}

		if($field == 'name') return $name;
		if($field == 'email') return $email;
		
		return ['name' => $name, 'email' => $email];
	}

	public function getBackUrl(){
		return app()->createUrl($this->backUrls[$this->type], array('id'=>$this->object_id));
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($criteria = false)
	{
		$alias = $this->getTableAlias();
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		if(!$criteria)
			$criteria = new CDbCriteria;
		
		$criteria->compare("{$alias}.id", $this->id); //против двусмысленности поля id
		$criteria->compare("{$alias}.status", $this->status);
		
		$criteria->compare("{$alias}.text", $this->text, true);

		$criteria->compare("{$alias}.type", $this->type);
		$criteria->compare("{$alias}.object_id", $this->object_id);
		
		$criteria->compare("{$alias}.sender_email", $this->sender_email, true);

		$this->compareDate($criteria, 'date', $this->date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			  'pageSize'=>Common::getPagerSize(__CLASS__),
			  'pageVar' => 'page'
			),
			'sort' => array(
				'defaultOrder'=>'date desc'
			)
		));
	}

	public function beforeDelete(){
		return true; //для того чтобы могли вызывать delete, из actionDeleteSelected в BController
	}

	//можно ли редактировать комментарий либо удалять
	public function getCanEdit(){
		//редактировать либо удалять комментарий могут только владельцы комментария либо модераторы
		return !user()->isGuest /*&& (user()->isRole(User::ROLE_MODERATOR) || $this->sender_id == user()->id)*/;
	}
}