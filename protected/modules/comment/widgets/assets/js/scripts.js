$(function(){
	//нажатие на кнопку ответить 
	$('#reviews').on('click', '.reply-btn', function(event){
		event.preventDefault();

		var self = $(this);

		//find form and append into 
		var block = $('#commentReplyForm');
		//сброс формы
		block.find('form').get(0).reset();

		//вставка формы на место
		block.insertAfter(self.closest('.review-content'))
			.show().find('textarea').focus();

		//показ кнопки закрытия
		$('#reviews .close-btn').hide();
		$('#reviews .reply-btn').removeAttr('style');
		self.hide().next().show();

		//set reply_id
		block.find('form #reply_id').val(self.closest('.review').data('id'));
	});

	//закрытие окна ответа
	$('#reviews').on('click', '.close-btn', function(event){
		event.preventDefault();

		//вставка формы на место
		$('#commentReplyForm').hide().appendTo($('#comment-forms'));

		//скрыть кнопку закрытия
		$(this).hide().prev().removeAttr('style');

	});

	//нажатие на кнопку редактирования
	$('#reviews').on('click', '.edit-btn', function(event){
		event.preventDefault();

		$('#reviews .reviewtext').removeAttr('contenteditable');
		var block = $(this).closest('.review-content');
		block.find('.reviewtext').attr('contentEditable', true).focus();
		block.addClass('active');
	});

	//потеря фокуса при редактировании
	$('#reviews').on('blur', '.reviewtext', function(event){

		$(this).removeAttr('contenteditable');

		var block = $(this).closest('.review-content');
		block.removeClass('active');

		//update comment
		id = $(this).closest('li.review').data('id');
		message = $(this).text();

		jPost($(this).data('url'), {id: id, message: message}, function(data){
			if(!data.success){
				// jAlert(Yii.t('front', 'Произошел сбой при обновлении'), Yii.t('front', 'Ошибка обновления'));
			}
		}, 'json');
            
	});
	
	//удаление комментария
	$('#reviews').on('click', '.delete-btn', function(event){
		event.preventDefault();
		self = $(this);

		id = self.closest('li.review').data('id');

		jConfirm(Yii.t('front', 'Вы уверены, что хотите удалить данный комментарий?'),
        	Yii.t('front', 'Удалить отзыв?'), function(result){
	            if(result){
					jPost(self.data('url'), {id: id}, function(data){
						if(!data.success){
							jAlert(Yii.t('front', 'Произошел сбой при удалении'), Yii.t('front', 'Ошибка удаления'));
						}
						else{
							if($('#comments-list .reviews.list > li').length == 1)
								$.fn.yiiListView.update('comments-list');
							else
								self.closest('li.review').remove();
						}
					}, 'json');
				}
		});
	});

});