<?php 
/*
чтобы показывать количество комментов для обьекта нужно создать 
реляцию   'hascomments'  => [self::STAT, 'SiteComment', 'object_id']

и <span class='site-comment-count'><?=$product->hascomments?></span>
*/
class SiteComments extends SWidget
{
	public $dataProvider;
	public $mode;

    public $type; //тип комментария
    public $objectId; //id объекта кому принадлещит комментарий

    public $title;

    public function run() {
    	// cs()->registerScriptFile($this->assetsUrl . '/js/scripts.js');

    	/**
    	 * register FANCYBOX
    	 */
    	// $fancy = app()->controller->assetsUrl.'/plugins/fancybox';
    	// //Add mousewheel plugin (this is optional)
    	// cs()->registerScriptFile($fancy.'/jquery.mousewheel-3.0.6.pack.js', CCLientScript::POS_END);
    	// //Add fancyBox
        // cs()->registerScriptFile($fancy.'/source/jquery.fancybox.pack.js', CCLientScript::POS_END);
		// cs()->registerCssFile($fancy.'/source/jquery.fancybox.css');
		// cs()->registerCssFile($fancy.'custom.css');
    	
    	switch ($this->mode) {
    		case 'list':
    			$this->renderList();
    			break;
    		
    		case 'form':
    			$this->renderForm();
    			break;

    		case 'formReply':
    			$this->renderFormReply();
    			break;

    		default:
                echo CHtml::openTag('div', array('id'=>'comment-forms'));
    			$this->renderForm();
                echo CHtml::closeTag('div');

				$this->renderList();
    			break;
    	}
    }

    public function renderList(){
        $title = ($this->title !== null ? $this->title : CHtml::tag('h3', array(), t('front', 'Что говорят посетители?')) );

    	$this->render('index', [
    		'dataProvider' => $this->dataProvider, 'title' => $title, 'objectId' => $this->objectId
    		]);
    }

    public function renderForm(){
    	$model = new SiteComment;
    	if(user()->isGuest) $model->scenario = 'guest_insert';

        $objectId = $this->objectId;
        $type = $this->type;

    	$this->render('form', compact('model', 'objectId', 'type'));
    }

}