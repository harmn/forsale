<?php 

/**
* CommentActions
*/
class CommentActions extends CBehavior
{
	// для обновления блока комментарий на странице открытого продукта
	public function actionAjaxUrl($id){
		$objectId = $id;
		$dataProvider = SiteComment::model()->provider(SiteComment::TYPE_ADVERT, $objectId);

		$this->owner->renderPartial('comment.widgets.views._list-view', compact('dataProvider', 'objectId'));
	}
}