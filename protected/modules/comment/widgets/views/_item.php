<li class="review clearfix" data-id="<?=$data->id?>">
	<div class="review-content">
		
		<div class="header clearfix">

			<? echo $data->getAvatar(); ?>

			<div class="info mb10">
				<p class="username fsize14"><?=$data->getSender('name')?></p>
				<span class="date"><?=$data->formatDate()?></span>
			</div>
			
		</div>

		<div class="reviewtext clearfix rel mt10" lang="am" data-url="<?=$this->createUrl('/comment/front/update')?>"><?=encode($data->text)?></div>
	</div>	
</li>