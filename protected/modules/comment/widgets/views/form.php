<div id="commentForm">
	<?php $form=$this->beginWidget('SActiveForm', [
			'id'=>'comment-form',
			'action' => ['/comment/front/add'],
			'enableAjaxValidation'=>true,
			'enableClientValidation'=>true,
			'clientOptions' => [
				'validateOnSubmit'=>true,
				'afterValidate' => "js:function(form, data, hasError){
					if (!hasError) {
						Forms.disableBtn(form.find('input[type=submit]'));
						
						jPost(form.prop('action'), form.serialize(), function(data){

							if(data.success){
								form.find('textArea').val('');
								Forms.enableBtn(form.find('input[type=submit]'));

								//добавить сообщение
								$.fn.yiiListView.update('comments-list');

								if($('span.site-comment-count').length != 0)
									$('span.site-comment-count').text(parseInt($('span.site-comment-count').text()) + 1);
							}
								 
						}, 'json');
					   
					}
					return false;
				}"
			],
		]); 
		?>
		<div class="w500 fl">
			<?php if (user()->isGuest): ?>

			<div class="clearfix">
				<div class="username input-wrap mr10 mb10 w245 fl">
					<?= $form->textField($model,'sender_name', ['placeholder' => 'Имя']); ?>
					<?= $form->error($model,'sender_name'); ?>
				</div>

				<div class="email input-wrap mb10 w245 fl">
					<?= $form->emailField($model,'sender_email', ['placeholder' => 'Эл.почта']); ?>
					<?= $form->error($model,'sender_email'); ?>
				</div>
			</div>
			
			<?endif?>

			<div class="bg-lighten-gray p10 clearfix">
				<div class="mb5">
					<?= $form->textArea($model,'text', ["class"=>"w100p", 'placeholder' => 'Комментарий']); ?>
					<?= $form->error($model,'text', ['style' => 'float:left; width: 300px']); ?>

				</div>

				<div>
					<?=$form->hiddenField($model, 'type', ['value'=>$type])?>
					<?=$form->hiddenField($model, 'object_id', ['value'=>$objectId])?>
				</div>

				<?php if (user()->isGuest): ?>
					<div class="captcha">
						<div class="fl">
							<?//php echo $form->textField($model, 'verifyCode'); ?>
						 </div>
							<?//php $this->widget('CCaptcha', ['clickableImage'=>true, 'showRefreshButton'=>false]); ?>
							<?//php echo $form->error($model, 'verifyCode'); ?>
					</div>
				<?php endif; ?>

				<?= CHtml::submitButton(t('front', 'Отправить'), [
					"class"=>"btn btn-primary bg-blue btn-sm fr w150",
					'data-wait' => t('front', 'Подождите'),
					'data-success' => t('front', 'Отправить'),
					]); ?>
			</div>
		</div>

	<?php $this->endWidget(); ?>
</div>