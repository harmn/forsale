<?php $this->widget('SListView', [
	'id' 			=> 'comments-list',
	'dataProvider'	=> $dataProvider,
	'itemView'		=> 'comment.widgets.views._item', // представление для одной записи
	'template' 		=> '{items}{pager}',
	'itemsTagName'	=>'ul',
	'itemsCssClass' => 'reviews list',
	'emptyText' 	=> t('front', 'Нет результатов'),
	'ajaxUrl' 		=> Yii::app()->createUrl('/advert/ajaxUrl', ['id' => $objectId]),
	'ajaxUpdate'	=> true,
	'pager'			=> [
		'class'	=>'FPager',
		'header'=>false,
		'htmlOptions'=> ['class'=>'pagination'],
	],
]); ?>