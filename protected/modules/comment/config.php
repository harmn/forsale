<?php
return [
	'modules' => [],
	'import' => [
		//user module
		'application.modules.comment.models.*',
	],
	'params' => [
	],
	'components' => [
	],
	'rules' => [
		'<controller:front>/<action:\w+>' => 'comment/<controller>/<action>',
		'<controller:front>/<action:\w+>/<id:\d+>' => 'comment/<controller>/<action>',
		'<controller:front>/<action:\w+>/<mode:\w+>' => 'comment/<controller>/<action>',

		ADMIN_PATH.'/comment/<controller:(back)>' => 'comment/<controller>/index',
		ADMIN_PATH.'/comment/<controller:(back)>/<action:\w+>' => 'comment/<controller>/<action>',
		ADMIN_PATH.'/comment/<controller:(back)>/<action:\w+>/<id:\d+>' => 'comment/<controller>/<action>',
	]
];