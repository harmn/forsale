<?

class BackController extends BController{

	public $model = 'SiteComment'; //for loadModel function

	public function filters(){
		return CMap::mergeArray(parent::filters(), [
			'postOnly + change'
		]);
	}

	public function actions(){
		return CMap::mergeArray(parent::actions(), [
			'index' => [
				'class' => 'modules.core.actions.IndexAction',
				'title' => 'Управление комментариями',
				'breadcrumbs' => ["Комментарии"],
				'beforeRender' => function($model, &$params){
					$type = app()->request->getParam('type');
					
					if($type){
						$model = new SiteComment('search');
						$model->type = $type;
					}
				}
			],
		]);
	}

	public function actionChange(){
		
		if(!empty($_POST) && isset($_POST['id']) && isset($_POST['val'])){
			$id =  $_POST['id']; 
			$val = $_POST['val'];

			$comment = $this->loadModel($this->model, $id);

			$comment->text = $val;
			$comment->save(true, ['text']);
		}
		
	}

	public function actionDelete($id = 0){

		$model = $this->loadModel($this->model, $id);

		$model->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
	}
}
		

?>