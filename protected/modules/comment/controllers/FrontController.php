<?php

class FrontController extends FController
{
	public $model = 'SiteComment'; //for loadModel function

	public function filters(){
		return CMap::mergeArray(parent::filters(), array(
			'postOnly',
			'accessControl',
		));
	}
	
	//Правила доступа
	public function accessRules()
	{
		return array(
			//даем доступ только зарегистрированным пользователям
			array(
				'allow',
				'users'=> array('@'),
			), 
			 array(
				'allow',
				'actions' => array('add'),
				'users'=> array('*'),
			), 
			array(
				'deny',
				'users'=>array('*')
			)
		);      
	}

	public function actionAdd($mode = false){

		$success = false;
		$comments = false;
		$comment = new SiteComment;
		$comm = '';

		$this->performAjaxValidation($comment);
		
		if(isset($_POST['SiteComment'])){
			$comment->attributes = $_POST['SiteComment'];

			if(!$mode){
				if($success = $comment->save()){
					$objectId = $comment->attributes['object_id'];
					$type = $comment->attributes['type'];
					$comment->date = time(); //для корректного показа

					$comm = $this->renderPartial('comment.widgets.views._item', ['data'=>$comment], true);
				}
			}
		}

		echo CJSON::encode(array('success'=>$success, 'comment'=>$comm));
	}

	public function actionUpdate(){
		$success = false;

		$id = request()->getParam('id');
		$message = request()->getParam('message');

		$model = $this->loadModel($this->model, $id);
		
		if($model->canEdit){
			$success = $model->updateByPk($id, array('text'=>$message));
		}

		echo CJSON::encode(array('success'=>(bool)$success));
	}

	public function actionDelete(){
		$success = false;

		$id = request()->getParam('id');

		$model = $this->loadModel($this->model, $id);

		if($model->canEdit)
			$success = $model->updateByPk($id, array('status'=>SiteComment::STATUS_DECLINED));

		if($success) {
			$all = $model->descendants()->findAll();

			foreach($all as $one){
				$one->updateByPk($one->id, array('status'=>SiteComment::STATUS_DECLINED));
			}
		}

		echo CJSON::encode(array('success'=>(bool)$success));
	}
}